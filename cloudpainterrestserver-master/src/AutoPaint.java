import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
 
@Path("/autopaint")
public class AutoPaint {

	@GET
	@Produces("application/xml")
	public String autoPaint() {
 
		String result = "@Produces(\"application/xml\") AutoPainting Generic Trace Image";
		return "<autoPaint>" + "<autoPaintOutput>" + result + "</autoPaintOutput>" + "</autoPaint>";
	}
 
	@Path("{paintingname}")
	@GET
	@Produces("application/xml")
	public String convertCtoFfromInput(@PathParam("paintingname") String paintingname) {

		String result = "@Produces(\"application/xml\") AutoPainting " + paintingname + " Image";
		return "<autoPaint>" + "<autoPaintOutput>" + result + "</autoPaintOutput>" + "</autoPaint>";
	}
}