package com.cloudpainter.robots;

import java.awt.Point;
import java.util.Vector;



public class LargeFlatPlotter extends RobotTemplate
{
	
	public LargeFlatPlotter() {
		
		this.zhomespeed = 100;
		this.zhalfspeed = 1000;
		this.zfullspeed = 2000;
	
		this.maxAccelSpeedPainting = 500;
		this.maxAccelSpeedHoming = 40;
		
		this.zRetracted = 5.0;   //units
		this.zLightTouch = 13.0;  //units
		this.zMediumTouch = 14.0; //units
		this.zHeavyTouch = 14.5;  //units
		

		
		this.xyhomespeed = 1000;
		this.xyhalfspeed = 4000;
		this.xyfullspeed = 8000;
	
		this.xOrigin = 0.0;
		this.yOrigin = 0.0;
	
		this.paintingSurfaceXOrigin = 72.0; //stepper units
		this.paintingSurfaceYOrigin = 10.0; //stepper units
	
	
		//on x axis 224 stepper units = 30" = 76.2 cm
		
		this.stepperUnitsXYPerMm = 0.293963254593; // 224 units does 76.2 cm
		this.stepperUnitsZPerMm =  0.062857142857; //   7 units does  4.4 cm
		
		
		
		this.paintingSurface = new PaintingSurface(50.8, 50.8, 3.81);  //20"x20"
		
		this.maxXUnits = 350.0; //stepper units
		this.maxYUnits = 230.0; //stepper units
		this.maxZUnits = 10.0;  //stepper units
		
		
		this.paintWellHeight = 61.0; //cm
		this.paintWellWidth = 61.0;  //cm
		this.tracksInPaintWell = new double[]{0.75, 1, 1.25};
	
		this.firstPaintWellX     = 55.0; //stepper units
		this.firstPaintWellY     = 10.0; //stepper units
		this.paintWellZ = 16.0;
		
		this.waterWellX = 8.0;
		this.waterWellY = 10.0;
		this.waterWellZ = 16.0;
		
		this.cleaningStartX =   8.0;;//stepper units
		this.cleaningStartY =   30.0;//stepper units
		this.cleaningEndX =     28.0;//stepper units
		this.cleaningEndY =     80.0;//stepper units
		this.cleaningZ = 15.0;	
		
	
		//shows orientation of paint wells
		//next paintwell will be this far away in this orientation
		this.nextPaintWellXShift = 0.0; //steps
		this.nextPaintWellYShift = paintWellHeight*stepperUnitsXYPerMm  ; //steps
		
		this.rotationalAxisActive = true;
		
		this.paintWells = new Vector();
	
	
	
}	
	

	
	public void updateBrushHeight(
			double brushHeight2,
			double canvasHeight2,
			double canvasSlump2,
			double waterHeight2,
			double cleaningHeight2,
			double wellHeight2)
 {
	}








	
	


	
}
