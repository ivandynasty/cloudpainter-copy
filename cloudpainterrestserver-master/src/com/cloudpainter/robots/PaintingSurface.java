package com.cloudpainter.robots;

public class PaintingSurface
{

	public double widthInMm  = 0.0;
	public double heightInMm = 0.0;
	public double depthInMm  = 0.0;
	
	public PaintingSurface(double width, double height, double depth) {
		this.widthInMm =  width;
		this.heightInMm = height;
		this.depthInMm =  depth;
	}
	
}
