package com.cloudpainter.robots;


import java.awt.Color;
import java.awt.geom.Point2D;
import java.text.DecimalFormat;
import java.util.Vector;

public abstract class RobotTemplate {

	
	public static int zhomespeed;
	public static int zhalfspeed;
	public static int zfullspeed;

	public static int maxAccelSpeedPainting;
	public static int maxAccelSpeedHoming;
	
	public static double brushHeight;   //units
	public static double canvasHeight;   //units
	public static double canvasSlump;   //units
	public static double waterHeight;   //units
	public static double cleaningHeight;   //units
	public static double wellHeight;   //units

	
	public static double zRetracted;   //units
	public static double zLightTouch;  //units
	public static double zMediumTouch; //units
	public static double zHeavyTouch;  //units
	//public static double zPaintWellDip;  //units
	
	public static int xyhomespeed;
	public static int xyhalfspeed;
	public static int xyfullspeed;

	public static double xOrigin;
	public static double yOrigin;
	
	public static double cameraXPosition;
	public static double cameraYPosition;
	public static double cameraZPosition;

	public static double paintingSurfaceXOrigin; //stepper units
	public static double paintingSurfaceYOrigin; //stepper units

	//on x axis 224 stepper units = 30" = 76.2 cm
	
	public static double stepperUnitsXYPerMm; //264 steps = 40.64 cm // 6.56746031746 = 50.4
	public static double stepperUnitsZPerMm; //   7 units does  4.4 cm
	
	public PaintingSurface paintingSurface = new PaintingSurface(406.4, 504.0, 1.0);  //10"x10"
	
	public static double maxXUnits; //stepper units
	public static double maxYUnits; //stepper units
	public static double maxZUnits;  //stepper units
	
	public static double paintWellHeight; //mm
	public static double paintWellWidth;  //mm
	public static double[] tracksInPaintWell;
	public static int maxPaintWells;
	
	public static double firstPaintWellX; //stepper units
	public static double firstPaintWellY; //stepper units
	public static double paintWellZ;

	public static double firstToolX; //stepper units
	public static double firstToolY; //stepper units
	public static double toolZ;
	public static double toolHolderWidth;//mm

	
	public static double waterWellX;
	public static double waterWellY;
	public static double waterWellZ;
	
	public static double cleaningStartX;//stepper units
	public static double cleaningStartY;//stepper units
	public static double cleaningEndX;//stepper units
	public static double cleaningEndY;//stepper units
	public static double cleaningZ;//stepper units
	
		//shows orientation of paint wells
	//next paintwell will be this far away in this orientation
	public static double nextPaintWellXShift; //steps
	public static double nextPaintWellYShift; //steps
	
	public static boolean rotationalAxisActive; //whether robot has rotational axis;
	
	public Vector paintWells = new Vector();

	public double mmX(double mm) {
		return this.stepperUnitsXYPerMm*mm;
	}

	public double mmY(double mm) {
		return this.stepperUnitsXYPerMm*mm;
	}

	public double mmZ(double mm) {
		return this.stepperUnitsZPerMm*mm;
	}

	
	public abstract	void updateBrushHeight(
			double brushHeight2,
			double canvasHeight2,
			double canvasSlump2,
			double waterHeight2,
			double cleaningHeight2,
			double wellHeight2);

	public void addPaintWell (String paintColor) {
		if (!paintWells.contains(paintColor))
		{
			paintWells.add(paintColor);
		}
	}
	
	public String getPaintWellString() {
		String paintWellString = "\"paintwells\":[ ";
		for (int i=0;i<paintWells.size();i++) {
			paintWellString = paintWellString + "\""+paintWells.elementAt(i)+"\"" + ", ";
		}
		paintWellString = paintWellString.substring(0,paintWellString.length()-2);
		paintWellString = paintWellString + " ]";
		return paintWellString;
	}
	
	public Point2D.Double getPaintWellLocationInMm(Color paintColor) {
		//get colorwell location
		int colorWellNumber = 0;
		for (int i=0; i<paintWells.size();i++) {
			if (paintWells.get(i).equals(paintColor)) {
				colorWellNumber = i;
				break;
			}
		}
		Point2D.Double wellLocation = new Point2D.Double();
		wellLocation.setLocation(  firstPaintWellX + nextPaintWellXShift*colorWellNumber , 
				                   firstPaintWellY + nextPaintWellYShift*colorWellNumber );

		return wellLocation;
	}

	public Point2D.Double getPointOnPaintingSurfaceInMm(double xPercentage, double yPercentage) {
		
		Point2D.Double pointInMm = new Point2D.Double();
		double xLocation = paintingSurfaceXOrigin + ((paintingSurface.widthInMm*xPercentage));
		double yLocation = paintingSurfaceYOrigin + ((paintingSurface.heightInMm*yPercentage));
		pointInMm.setLocation(xLocation, yLocation);

		return pointInMm;
	}
	
	//returns point scaled to painting surface as percentage of painting surface
	public Point2D.Double scalePointToPaintingSurfaceMm(double x, double y, double width, double height) {
	    //(x 1, y 30 30x60)
		
		Point2D.Double scaledPoint = new Point2D.Double();

		double xPercentage = x/width;
		double yPercentage = y/height;
		scaledPoint.setLocation(   xPercentage*paintingSurface.widthInMm,
				                   yPercentage*paintingSurface.heightInMm
				);
		return scaledPoint;
	}
	
	public static String getRotation(double movement) {
		if (rotationalAxisActive) {
			//String movementWithoutScientificNotation = new java.math.BigDecimal(movement).toPlainString();
			DecimalFormat df = new DecimalFormat("#");
  	        df.setMaximumFractionDigits(8);
            System.out.println("rotation: "+df.format(movement));
			String movementWithoutScientificNotation = df.format(movement); //Double.toString(((int)(movement * 10000000.0d))/10000000.0d); // Round to 5 dp
			return "E"+movementWithoutScientificNotation;
		} else {
			return "";
		}
	}
	

	
}
