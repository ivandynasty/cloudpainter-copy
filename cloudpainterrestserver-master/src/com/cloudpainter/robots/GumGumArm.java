package com.cloudpainter.robots;

import java.awt.Point;
import java.util.Vector;



public class GumGumArm extends RobotTemplate
{

	
	public GumGumArm() {

		this.zhomespeed = 100;
		this.zhalfspeed = 500;
		this.zfullspeed = 1000;

		this.maxAccelSpeedPainting = 500;
		this.maxAccelSpeedHoming = 100;
		
		this.zRetracted = 0;   //units mm
		this.brushHeight = 67; //mm
		this.canvasHeight = 35; //mm
	    this.canvasSlump = 2.5;
		this.waterHeight = 0; //mm
		this.cleaningHeight = 35; //mm
		this.wellHeight = 0; //mm

		updateBrushHeight(brushHeight,canvasHeight, canvasSlump, waterHeight, cleaningHeight, wellHeight);
		//this.zLightTouch = -2100+brushHeight;  //-2080 + brush length with 70mm brush
		//this.zMediumTouch = this.zLightTouch-33; //
		//this.zHeavyTouch =  this.zLightTouch-100;  //
		
		this.xyhomespeed = 100;
		this.xyhalfspeed = 500;
		this.xyfullspeed = 1000;

		this.xOrigin = -2300;    
		this.yOrigin = 2020;

		this.cameraXPosition = 2300;
		this.cameraYPosition = 1600;
		this.cameraZPosition = 6500;
		
		this.paintingSurfaceXOrigin = -2300; //stepper units
		this.paintingSurfaceYOrigin = 1980; //stepper units

		//on x axis 224 stepper units = 30" = 76.2 cm
		
		this.stepperUnitsXYPerMm = 100.0; //10mm per mm
		this.stepperUnitsZPerMm =  100.0; //10mm per mm
		
		this.paintingSurface = new PaintingSurface(46.0, 46.0, 1.0);  //approx 18"x18"
		
		this.maxXUnits = 5000; //stepper units
		this.maxYUnits = 7500; //stepper units
		this.maxZUnits = 4000;  //stepper units
		
		this.firstPaintWellX     = 3870; //stepper units
		this.firstPaintWellY     = 1550; //1580; //stepper units
		//this.paintWellZ          = -1760+brushHeight;	
		this.paintWellHeight = 475; //mm
		this.paintWellWidth = 50;  //mm
		this.tracksInPaintWell = new double[]{25, 75, 50};
		this.maxPaintWells = 10;
		
		this.waterWellX = -3650;
		this.waterWellY =  1600;
		//this.waterWellZ = -1680+brushHeight;	
		
		this.cleaningStartX =  -3900;//stepper units
		this.cleaningStartY =   3000;//stepper units
		this.cleaningEndX =    -3800;//stepper units
		this.cleaningEndY =     5000;//stepper units
		//this.cleaningZ =       -1520+brushHeight;	
		
		//shows orientation of paint wells
		//next paintwell will be this far away in this orientation
		this.nextPaintWellXShift = -1.1875; //steps
		this.nextPaintWellYShift = 475; //steps
		
		this.rotationalAxisActive = false;
		
		this.paintWells = new Vector();
	}
	
	public void updateBrushHeight(
			double brushHeight2,
			double canvasHeight2,
			double canvasSlump2,
			double waterHeight2,
			double cleaningHeight2,
			double wellHeight2) {
		
		this.brushHeight  =    brushHeight2;
		this.canvasHeight  =   canvasHeight2;
		this.canvasSlump  =    canvasSlump2;
		this.waterHeight  =    waterHeight2;
		this.cleaningHeight  = cleaningHeight2;
		this.wellHeight  =     wellHeight2;

		this.zLightTouch  =  -3087+(canvasHeight*10)+(brushHeight*10); //35  67
		this.zMediumTouch =   zLightTouch-33; 
		this.zHeavyTouch  =   zLightTouch-100; 
		
		this.waterWellZ   =  -2363+(waterHeight*10)+(brushHeight*10);	

		this.cleaningZ    =  -2547+(cleaningHeight*10)+(brushHeight*10);	

		this.paintWellZ   =  -2363+(wellHeight*10)+(brushHeight*10);	

	}
	
	
}
