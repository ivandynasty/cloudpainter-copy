package com.cloudpainter.robots;

import java.util.Vector;

public class PandoraVTwo extends RobotTemplate
{
	
	public PandoraVTwo() {

		this.stepperUnitsXYPerMm = 0.333; 
		this.stepperUnitsZPerMm =  0.1591;

		this.xOrigin = 50.0;//mm
		this.yOrigin = 50.0;//mm
		
		this.zhomespeed = 500;
		this.zhalfspeed = 1000;
		this.zfullspeed = 2000;
	
		this.maxAccelSpeedPainting = 500;
		this.maxAccelSpeedHoming   = 10; 
			
		this.zRetracted =   70.0;  //mm
		this.zLightTouch =  92.0;  //mm
		this.zMediumTouch = 95.0;  //mm
		this.zHeavyTouch =  97.0;  //mm
		
		this.xyhomespeed = 200;
		this.xyhalfspeed = 1000;
		this.xyfullspeed = 12000;
	
		this.paintingSurfaceXOrigin = 200.0; //mm 
		this.paintingSurfaceYOrigin = 100.0; //mm
	
		
		this.rotationalAxisActive = true;
		
		this.paintingSurface = new PaintingSurface(762.0, 762.0, 38.1);  //mm 30"x30"  
		
		this.maxXUnits = 500.0; //mm
		this.maxYUnits = 400.0; //mm
		this.maxZUnits = 15.0;   //mm
		
		this.paintWellHeight = 100.0; //mm
		this.paintWellWidth =  200.0; //mm
		this.tracksInPaintWell = new double[]{2.6, 3.0, 3.4};
	
		this.firstPaintWellX     = 200.0; //mm
		this.firstPaintWellY     = 30.0;   //mm
		this.paintWellZ          = 100.0;  //mm	
		
		this.firstToolX          =    5.0; //mm
		this.firstToolY          =  102.0; //mm
		this.toolZ               =   46.2; //mm	
		this.toolHolderWidth     =  100.0; //mm
		
		this.waterWellX =  0.0; //mm
		this.waterWellY =  0.0; //mm
		this.waterWellZ =  5.0; //mm	
		
		this.cleaningStartX =   15.0;   //mm
		this.cleaningStartY =   800.0;  //mm
		this.cleaningEndX =     45.0;   //mm
		this.cleaningEndY =     1000.0; //mm
		this.cleaningZ = 		26.00;   //mm			
	
		//shows orientation of paint wells
		this.nextPaintWellXShift = 0.0; //mm
		this.nextPaintWellYShift = paintWellHeight; //mm
		
		this.paintWells = new Vector();
	
	}
	
	public void updateBrushHeight(
			double brushHeight2,
			double canvasHeight2,
			double canvasSlump2,
			double waterHeight2,
			double cleaningHeight2,
			double wellHeight2)
 {
	}



}	
	
