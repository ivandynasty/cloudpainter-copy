package com.cloudpainter.robots;

import java.awt.Point;
import java.util.Vector;



public class MidSizedFortyFiveDegreePlotter extends RobotTemplate
{
	
	public MidSizedFortyFiveDegreePlotter() {

		this.zhomespeed = 200;
		this.zhalfspeed = 800;
		this.zfullspeed = 1600;

		this.maxAccelSpeedPainting = 200;
		this.maxAccelSpeedHoming = 40;
		
		this.zRetracted = 1.0;   //units
		this.zLightTouch = 6.0;  //units
		this.zMediumTouch = 6.5; //units
		this.zHeavyTouch = 6.0;  //units
		//this.zPaintWellDip = 12.0;  //units
		
		this.xyhomespeed = 1000;
		this.xyhalfspeed = 4000;
		this.xyfullspeed = 8000;

		this.xOrigin = 5.0;
		this.yOrigin = 5.0;

		this.paintingSurfaceXOrigin = 65.0; //stepper units
		this.paintingSurfaceYOrigin = 10.0; //stepper units

		//on x axis 224 stepper units = 30" = 76.2 cm
		
		this.stepperUnitsXYPerMm = 0.653; //264 steps = 40.64 cm // 6.56746031746 = 50.4
		this.stepperUnitsZPerMm =  0.062857142857; //   7 units does  4.4 cm
		
		this.paintingSurface = new PaintingSurface(50.4, 50.4, 1.0);  //20"x20"
		
		this.maxXUnits = 400.0; //stepper units
		this.maxYUnits = 350.0; //stepper units
		this.maxZUnits = 11.0;  //stepper units
		
		this.paintWellHeight = 52.067381317; //mm
		this.paintWellWidth = 5.0;  //mm
		this.tracksInPaintWell = new double[]{0.25, 0.75, 0.5, 0.33, 0.66};

		this.firstPaintWellX     = 17.0; //stepper units
		this.firstPaintWellY     = 58.0; //stepper units
		this.paintWellZ = 12.0;	
		

		this.waterWellX = 17.0;
		this.waterWellY = 28.0;
		this.waterWellZ = 12.0;	
		
		this.cleaningStartX =   0.0;//stepper units
		this.cleaningStartY =   240.0;//stepper units
		this.cleaningEndX =     20.0;//stepper units
		this.cleaningEndY =     330.0;//stepper units
		this.cleaningZ = 12.0;	
		

		//shows orientation of paint wells
		//next paintwell will be this far away in this orientation
		this.nextPaintWellXShift = 0.0; //steps
		this.nextPaintWellYShift = 33.5 ; //steps
		
		this.rotationalAxisActive = false;
		
		this.paintWells = new Vector();
	}
	
	public void updateBrushHeight(
			double brushHeight2,
			double canvasHeight2,
			double canvasSlump2,
			double waterHeight2,
			double cleaningHeight2,
			double wellHeight2)
 {
	}


}
