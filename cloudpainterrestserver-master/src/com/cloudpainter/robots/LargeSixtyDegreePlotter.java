package com.cloudpainter.robots;

import java.awt.Point;
import java.util.Vector;



public class LargeSixtyDegreePlotter extends RobotTemplate
{
	
	public LargeSixtyDegreePlotter() {

		this.zhomespeed = 100;
		this.zhalfspeed = 800;
		this.zfullspeed = 1600;

		this.maxAccelSpeedPainting = 100;
		this.maxAccelSpeedHoming = 100;
		
		this.zRetracted =   4.0;  //units
		this.zLightTouch =  10.5; //12.0;  //units
		this.zMediumTouch = 11.5; //13.5; //units
		this.zHeavyTouch =  12.5; //units
		
		this.xyhomespeed = 2000;
		this.xyhalfspeed = 4000;
		this.xyfullspeed = 4000;

		this.xOrigin = 5.0;
		this.yOrigin = 5.0;

		this.paintingSurfaceXOrigin = 93; //stepper units
		this.paintingSurfaceYOrigin = 65; //stepper units

		//on x axis 224 stepper units = 30" = 76.2 cm
		
		//60.96 = 593   => 9.72769028871 steps per cm
		//76.2 =  745   => 9.77690288714
		
		this.stepperUnitsXYPerMm = 0.975; //264 steps = 40.64 cm // 6.56746031746 = 50.4
		this.stepperUnitsZPerMm =  0.062857142857; //   7 units does  4.4 cm
		
		this.paintingSurface = new PaintingSurface(60.96, 76.2, 1.0);  //24"x30"
		
		
		
		this.maxXUnits = 950; //stepper units
		this.maxYUnits = 1000.0; //stepper units
		this.maxZUnits = 18.0;  //stepper units
		
		this.paintWellHeight = 25.0; //mm
		this.paintWellWidth =  25.0;  //mm
		this.tracksInPaintWell = new double[]{0.4, 0.5, 0.6};

		this.firstPaintWellX     = 93; //stepper units
		this.firstPaintWellY     = 5.0; //stepper units
		this.paintWellZ 		 = 18.5; //stepper units

		this.waterWellX = 20.0;
		this.waterWellY = 5.0;
		this.waterWellZ = 17.5;
		
		
		this.cleaningStartX =   5.0;//stepper units
		this.cleaningStartY =   100.0;//stepper units
		this.cleaningEndX =     45.0;//stepper units
		this.cleaningEndY =     350.0;//stepper units
		this.cleaningZ =        11.0;// 10.0;//stepper units 
		

		//shows orientation of paint wells
		//next paintwell will be this far away in this orientation
		this.nextPaintWellXShift = 43.4; //steps
		this.nextPaintWellYShift = 0.0 ; //steps
		
		this.rotationalAxisActive = false;
		
		this.paintWells = new Vector();
	}
	
	public void updateBrushHeight(
			double brushHeight2,
			double canvasHeight2,
			double canvasSlump2,
			double waterHeight2,
			double cleaningHeight2,
			double wellHeight2)
 {
	}


}
