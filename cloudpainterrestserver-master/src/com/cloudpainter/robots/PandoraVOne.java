package com.cloudpainter.robots;

import java.util.Vector;

public class PandoraVOne extends RobotTemplate
{
	
	public PandoraVOne() {
		
		this.zhomespeed = 400;
		this.zhalfspeed = 1000;
		this.zfullspeed = 2000;
	
		this.maxAccelSpeedPainting = 1000;
		this.maxAccelSpeedHoming = 400; //NOT implement
		
		//flat sample
		//this.zRetracted = 8.0;   //units
		//this.zLightTouch = 16.0;  //units
		//this.zMediumTouch = 17.5; //units
		//this.zHeavyTouch = 19.5;  //units
		//this.zPaintWellDip = 15.5;  //units
			
		this.zRetracted = 5.0;   //units          Feathered Brush / 10.5/11.5/12.5
		this.zLightTouch = 9.4;  //units
		this.zMediumTouch = 10.4; //units
		this.zHeavyTouch = 11.5;  //units
		//this.zPaintWellDip = 17.0;  //units
		
		this.xyhomespeed = 1000;
		this.xyhalfspeed = 8000;
		this.xyfullspeed = 16000;
	
		this.xOrigin = 5.0;
		this.yOrigin = 5.0;
	
		this.paintingSurfaceXOrigin = 200.0; //stepper units 
		this.paintingSurfaceYOrigin = 20.0; //stepper units
	
	
		//on x axis 224 stepper units = 30" = 76.2 cm
		
		this.stepperUnitsXYPerMm = 1.42624671916; // 1086.8 units does 76.2 cm
		this.stepperUnitsZPerMm =  0.062857142857; //   7 units does  4.4 cm
		
		this.rotationalAxisActive = true;
		
		this.paintingSurface = new PaintingSurface(76.2, 76.2, 3.81);  //30"x30"  
	
		
		this.maxXUnits = 1500.0; //stepper units
		this.maxYUnits = 1150.0; //stepper units
		this.maxZUnits = 18.0;  //stepper units
		
		
		this.paintWellHeight = 61.0; //mm
		this.paintWellWidth = 61.0;  //mm
		this.tracksInPaintWell = new double[]{2.6, 3.0, 3.4};
	
		//face north
		//z 15
		//40,40
		//40,110
		//turn 90 right
		//80,110
		//turn 90 right
		//80,40
		//turn 90 right
        //60,40
		//turn right 90
		//60, 75
		//turn right 90
		//70, 75
		//150, 75, 10
		
		
		this.firstPaintWellX     = 40.0; //stepper units
		this.firstPaintWellY     = 40.0; //stepper units
		this.paintWellZ = 13.0;	// pwas 15.00	

		
		this.waterWellX = 0.0;
		this.waterWellY = 0.0;
		this.waterWellZ = 5.0; // pwas 15.00	
		
		this.cleaningStartX =   5.0;;//stepper units
		this.cleaningStartY =   400.0;//stepper units
		this.cleaningEndX =     15.0;//stepper units
		this.cleaningEndY =     500.0;//stepper units
		this.cleaningZ = 		5.0;//stepper units			
		
	
		//shows orientation of paint wells
		//next paintwell will be this far away in this orientation
		this.nextPaintWellXShift = 0.0; //steps
		this.nextPaintWellYShift = paintWellHeight*stepperUnitsXYPerMm  ; //steps
		
		this.paintWells = new Vector();
	
	}
	
	public void updateBrushHeight(
			double brushHeight2,
			double canvasHeight2,
			double canvasSlump2,
			double waterHeight2,
			double cleaningHeight2,
			double wellHeight2)
 {
	}


}	
	
