package com.cloudpainter.painting;

import java.io.Serializable;

public class CanvasSetting implements
		Serializable
{
	
	//ClientSide Settings
	private static final long serialVersionUID = 1L;
	private String name;
	private String description;
	
	private int ppi; // points per inch
	private int canvasHeight; // inches
	private int canvasWidth; // inches
	
	private int liveImageHeight = 1080;//480 //liveImageBottom-liveImageTop;
	private int liveImageWidth = 1920;//640 liveImageRight-liveImageLeft;
	
	
	private int topleftx = 0;
	private int toplefty = 0;
	private int toprightx = 0;
	private int toprighty = 0;
	private int bottomleftx = 0;
	private int bottomlefty = 0;
	private int bottomrightx = 0;
	private int bottomrighty = 0;
	
	@SuppressWarnings("unused")
	private CanvasSetting() {}

	public CanvasSetting(
			final int ppi,
			final int height,
			final int width,
			final int topleftx,
			final int toplefty,
			final int toprightx,
			final int toprighty,
			final int bottomleftx,
			final int bottomlefty,
			final int bottomrightx,
			final int bottomrighty,
			final int liveImageHeight,
			final int liveImageWidth,
			final String name,
			final String description) {
		this.ppi = ppi;
		canvasHeight = height;
		canvasWidth = width;
	
		this.topleftx = topleftx;
		this.toplefty = toplefty ;
		this.toprightx = toprightx;
		this.toprighty =  toprighty;
		this.bottomleftx = bottomleftx ;
		this.bottomlefty = bottomlefty;
		this.bottomrightx = bottomrightx;
		this.bottomrighty = bottomrighty;
		this.liveImageHeight = liveImageHeight;
		this.liveImageWidth = liveImageWidth;
		this.name = name;
		this.description = description;
	}

	public int getPPI() {
		return ppi;
	}

	public int getCanvasHeight() {
		return canvasHeight;
	}

	public int getCanvasWidth() {
		return canvasWidth;
	}

	public int getLiveImageHeight() {
		return liveImageHeight;
	}

	public int getTopleftx() {
		return topleftx;
	}

	public int getToplefty() {
		return toplefty;
	}

	public int getToprightx() {
		return toprightx;
	}

	public int getToprighty() {
		return toprighty;
	}

	public int getBottomleftx() {
		return bottomleftx;
	}

	public int getBottomlefty() {
		return bottomlefty;
	}

	public int getBottomrightx() {
		return bottomrightx;
	}

	public int getBottomrighty() {
		return bottomrighty;
	}

	public int getLiveImageWidth() {
		return liveImageWidth;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}
}


