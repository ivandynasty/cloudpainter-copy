package com.cloudpainter.painting;

public class StaticPaintAlgorithms
{

	private static double prevAngle = 0;

	private static int maxSlope = 3;
	private static double minAngle = getAngle(0, 0, maxSlope, 1);
	private static double maxAngle = getAngle(0, 0, 1, maxSlope);
	
	   public static double getAngle(double x1, double y1, double x2, double y2)
	    {
		   return (float) Math.toDegrees(Math.atan2(y2 - y1, x2 - x1));
	    }
	
	/**
	 * @param endPoint
	 * @param startPoint
	 * @return intermediary CanvasPoint that ensures a multiple of 45 degree
	 *         angles between the previous and current points. Will return null
	 *         if there is no need for an intermediary point
	 */
	static public CanvasPoint calculateAddSmoothPathsToQueue(
			final CanvasPoint endPoint,
			final CanvasPoint startPoint ) {
		
		//NORMALIZE so start point is 0,0
		double nStartX = 0.0;
		double nStartY = 0.0;
		//And ENDPOINT is ABS difference
		double nEndX = Math.abs(endPoint.getX()-startPoint.getX());
		double nEndY = Math.abs(endPoint.getY()-startPoint.getY());
		//create midpoints
		double nMidX = 0.0;
		double nMidY = 0.0;
		
		//To reverse normalization later, record which axis were flipped
		boolean flipXCoordinate = (endPoint.getX()-startPoint.getX()) < 0;
		boolean flipYCoordinate = (endPoint.getY()-startPoint.getY()) < 0;
	    double angle = getAngle(nStartX, nStartY, nEndX, nEndY);
    
	    
		//	    System.out.println("Angle: "+angle);
		//	    double angle1 = getAngle(0, 0, 1, 1);
		//	    System.out.println("Angle1: "+angle1);
		//	    double angle2 = getAngle(0, 0, 1, 2);
		//	    System.out.println("Angle2: "+angle2);
		//			    double angle4 = getAngle(0, 0, 3, 1);
		//			    System.out.println("Angle4: "+angle4);
		//			    double angle6 = getAngle(0, 0, 1, 3);
		//			    System.out.println("Angle6: "+angle6);

	    if (angle <90 && angle >0) {
		    if (angle < minAngle) {  //getAngle(0, 0, 4, 1); rise/run = .25
		    	if (angle < minAngle/2 ){
	                //10.0, 2.0
		    		//8.0, 1.0
		    		//System.out.println("horizontal then 15 degrees");
		    		//mx = x-(y*maxSlope)
		    		//my = 0
		    		nMidX = nEndX-(nEndY*maxSlope);
		    		nMidY = 0.0;
		    	} else {
	                //10.0, 2.0
		    		//8.0, 1.0
		    		//System.out.println("15 degrees then horizontal ");
		    		//mx = y*maxSlope
	                //my = y
		    		nMidX = nEndY*maxSlope;
		    		nMidY = nEndY;
		    	}
		    } else if (angle > maxAngle) {	//getAngle(0, 0, 1, 4);  rise/run = 4.0
		    	if (angle > (maxAngle+(minAngle/2))) {
	                //2.0, 10.0
		    		//1.0, 8.0
		    		//System.out.println("vertical then 75 degrees");
		    		//x = 0
		    		//y = y-(x*maxSlope) 
		    		nMidX = 0.0;
		    		nMidY = nEndY-(nEndX*maxSlope);
		    	} else {
		    		//System.out.println("75 degrees then horizontal");
		    		//x = x
		    		//y = x*maxSlope 
		    		nMidX = nEndX;
		    		nMidY = nEndX*maxSlope;
		    	}
		    } else {
				//angle is in tolerable range where x and y change differ by no more than 4X
				return null;
		    }
	} else {
		//angle is either 0 or 90 so no intermediate points
		return null;
	}
	    
	   
	//de-normalize
	//System.out.println("nEndX: "+nEndX+ " nEndY: "+nEndY );
	//System.out.println("nMidX: "+nMidX+ " nMidY: "+nMidY );
	double midX = 0.0;
	double midY = 0.0;
	if (!flipXCoordinate) {
    	midX = startPoint.getX() + nMidX;
    } else {
    	//6.1
    	midX = endPoint.getX() + (nEndX-nMidX);

    }
    if (!flipYCoordinate) {
    	midY = startPoint.getY() + nMidY;
    } else {
    	midY = endPoint.getY() + (nEndY-nMidY);
    }

    
    CanvasPoint midPoint = new CanvasPoint(midX, midY);    

	//    System.out.println("startX: "+startPoint.getX()+ " startY: "+ startPoint.getY());
	//    System.out.println("midX: "+midPoint.getX()+ " midY: "+ midPoint.getY());
	//    System.out.println("endX: "+endPoint.getX()+ " endY: "+ endPoint.getY());
    //System.out.println("--------------------");
    
    return midPoint;
}
	
static public CanvasPoint deprecatedCalculateAddSmoothPathsToQueue(
			final CanvasPoint endPoint,
			final CanvasPoint startPoint ) {
		CanvasPoint returnPoint = null;
		// positive is forward
		// 0,0 is bottom left - typical cartesian coordinate system
		// System.out.println(currentPoint.getX()+"::"+currentPoint.getY());
		final double xDiff = endPoint.getX() - startPoint.getX();
		final double yDiff = endPoint.getY() - startPoint.getY();
		// System.out.println("x: "+currentPoint.getX());
		// System.out.println("y: "+currentPoint.getY());
		// System.out.println("prevx: "+canvas.getPrevPoint().getX());
		// System.out.println("prevy: "+canvas.getPrevPoint().getY());
		// System.out.println("xdiff: "+xDiff);
		// System.out.println("ydiff: "+yDiff);
		// 0 right x+, y0
		// 45 4:30 x+, y-
		// 90 down x0, y-
		// 135 7:30 x-, y-
		// 180 left x-, y0
		// 225 10:30 x-, y+
		// 270 up x0, y+
		// 315 1:30 x+, y+

		if ((xDiff == 0) || (yDiff == 0)) {
			// its straight line draw it as one point
			// register direction as lastDirection
			if (xDiff > 0) {
				prevAngle = 0.0;
			}
			else if (yDiff < 0) {
				prevAngle = 90.0;
			}
			else if (xDiff < 0) {
				prevAngle = 180.0;
			}
			else if (yDiff > 0) {
				prevAngle = 270.0;
			}
		}
		else if (Math.abs(xDiff) == Math.abs(yDiff)) {
			// its a diagonal draw it as one point
			// register direction as lastDirection
			if ((xDiff > 0) && (yDiff < 0)) {
				prevAngle = 45.0;
			}
			else if ((xDiff < 0) && (yDiff < 0)) {
				prevAngle = 135.0;
			}
			else if ((xDiff < 0) && (yDiff > 0)) {
				prevAngle = 225.0;
			}
			else if ((xDiff > 0) && (yDiff > 0)) {
				prevAngle = 310.0;
			}
		}
		else {
			// if snapping to 45 degree increments, all lines can be expressed
			// as two segments
			// one at diagonal, and one at right angle
			// if snapping to 22.5 degree angles this becomes more complex
			// (possible 3 angles)?
			double diagonalAngle = 0;
			double straightAngle = 0;
			// calculate diagonal angle needed
			if ((xDiff > 0) && (yDiff < 0)) { // 45 degree angle
				diagonalAngle = 45.0;
			}
			else if ((xDiff < 0) && (yDiff < 0)) { // 135 degree angle
				diagonalAngle = 135.0;
			}
			else if ((xDiff < 0) && (yDiff > 0)) { // 225 degree angle
				diagonalAngle = 225.0;
			}
			else if ((xDiff > 0) && (yDiff > 0)) { // 315 degree angle
				diagonalAngle = 315.0;
			}
			// Above will completely use up the lesser of the two differences
			// leaving only a right angle line segment to be consumed
			// calculate right angle to accompany diagonal
			if (Math.abs(xDiff) > Math.abs(yDiff)) {
				// if x is greater, line will be horizontal
				if (xDiff > 0) {
					straightAngle = 0.0;
				}
				else {
					straightAngle = 180.0;
				}
			}
			else {
				// if y is greater, line will be vertical
				if (yDiff > 0) {
					straightAngle = 270.0;
				}
				else {
					straightAngle = 90.0;
				}
			}
			// with two directions in hand, it is now time to comapre with last
			// direction and
			// decide which to use first
			// To get smallest degree difference
			// take the absolute value of their difference,
			// then, if larger than 180, substract 360
			// and take the absolute value of the result.
			double diagonalDifference = Math.abs(prevAngle - diagonalAngle);
			if (diagonalDifference > 180) {
				diagonalDifference = Math.abs(diagonalDifference - 360.0);
			}
			double straightDifference = Math.abs(prevAngle - straightAngle);
			if (straightDifference > 180) {
				straightDifference = Math.abs(straightDifference - 360.0);
			}
			if (straightDifference < diagonalDifference) {
				// do straight first, then diagonal
				// second point will always be the current point
				if (Math.abs(xDiff) > Math.abs(yDiff)) {
					// x is longer subtract y dist from it
					if (xDiff > 0) {
						returnPoint = new CanvasPoint(
								startPoint.getX() + (xDiff - Math.abs(yDiff)),
								startPoint.getY());
					}
					else {
						returnPoint = new CanvasPoint(
								startPoint.getX() + (xDiff + Math.abs(yDiff)),
								startPoint.getY());
					}
				}
				else {
					if (yDiff > 0) {
						returnPoint = new CanvasPoint(
								startPoint.getX(),
								startPoint.getY() + (yDiff - Math.abs(xDiff)));
					}
					else {
						returnPoint = new CanvasPoint(
								startPoint.getX(),
								startPoint.getY() + (yDiff + Math.abs(xDiff)));
					}
				}
				// register lastDirection
				prevAngle = diagonalAngle;
			}
			else {
				// do diagonal first, then straight
				// second point will always be the current point
				// use the diff in the least of the two
				double lowestAbsoluteDiff = 0;
				if (Math.abs(xDiff) < Math.abs(yDiff)) {
					lowestAbsoluteDiff = Math.abs(xDiff);
				}
				else {
					lowestAbsoluteDiff = Math.abs(yDiff);
				}
				if (diagonalAngle == 45.0) {
					returnPoint = new CanvasPoint(
							startPoint.getX() + lowestAbsoluteDiff,
							startPoint.getY() - lowestAbsoluteDiff);
				}
				else if (diagonalAngle == 135.0) {
					returnPoint = new CanvasPoint(
							startPoint.getX() - lowestAbsoluteDiff,
							startPoint.getY() - lowestAbsoluteDiff);
				}
				else if (diagonalAngle == 225.0) {
					returnPoint = new CanvasPoint(
							startPoint.getX() - lowestAbsoluteDiff,
							startPoint.getY() + lowestAbsoluteDiff);
				}
				else if (diagonalAngle == 315.0) {
					returnPoint = new CanvasPoint(
							startPoint.getX() + lowestAbsoluteDiff,
							startPoint.getY() + lowestAbsoluteDiff);
				}
				// register lastDirection
				prevAngle = straightAngle;
			}

			// compare with last direction
			// use direction that most closely matches last direction
		}
		return returnPoint;
	}
}
