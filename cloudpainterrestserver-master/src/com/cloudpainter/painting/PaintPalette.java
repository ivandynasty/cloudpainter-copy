package com.cloudpainter.painting;

import com.cloudpainter.painting.Paint;

public class PaintPalette
{
	private int id;
	private int well;
	private String color;
	private double inches;

	private PaintPalette() {}

	public PaintPalette(
			final int well,
			final String color,
			final double inches ) {
		this.well = well;
		this.color = color;
		this.inches = inches;
	}

	public Paint getPaint() {
		return new Paint(
				color,
				inches,
				well);
	}
}
