package com.cloudpainter.painting;

import java.io.Serializable;

public class Brush implements
		Serializable
{
	private static final long serialVersionUID = 1L;

	// In inches
	private double brushWidth;

	@SuppressWarnings("unused")
	private Brush() {}

	public Brush(
			final double brushWidth ) {
		this.brushWidth = brushWidth;
	}

	public double getWidth() {
		return brushWidth;
	}

}
