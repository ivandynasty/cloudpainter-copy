package com.cloudpainter.painting;

import java.io.Serializable;
import java.util.Vector;

//Represents distance from bottom left origin of painting in inches
public class BitmapPoint 
{
	private int x;
	private int y;

	private Vector midPoints = new Vector();
	
	private BitmapPoint() {}

	public BitmapPoint(
			final int x,
			final int y ) {
		this.x = x;
		this.y = y;
	}

	public void addMidPoints(BitmapPoint bp) {
		midPoints.add(bp);
	}
	
	public Vector getMidPoints() {
		return midPoints;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public boolean equals(
			final Object other ) {
		boolean equals = false;
		if (other instanceof BitmapPoint) {
			equals = (x == ((BitmapPoint) other).x) && (y == ((BitmapPoint) other).y);
		}
		return equals;
	}

	@Override
	public int hashCode() {
		int i = 17;
		i += i * 31 + (x * 1000);
		i += i * 31 + (y * 1000);
		return i;
	}
}
