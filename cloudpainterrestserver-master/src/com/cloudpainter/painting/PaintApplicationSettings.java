package com.cloudpainter.painting;

import java.io.Serializable;
import java.util.List;

import com.cloudpainter.painting.Paint;

public class PaintApplicationSettings implements
		Serializable
{
	private static final long serialVersionUID = 1L;

	private CanvasSetting canvasSetting;
	private Brush brush;
	private String canvasImageUrl = "C:/development/apache-tomcat-6.0.35/webapps/cloudpainter/liveimages";
	private String liveImageUrl = "-EXAMPLE-http://casper.vanarman.com:8889/out.jpg"; // examples
	private String tiltedImageUrl = "-EXAMPLE-http://192.168.1.6:8080/?action=snapshot"; // examples
	private String liveImageStream = "http://casper.vanarman.com:8889/update.jpg";
	private String traceImageUrl = "image/trace.jpg";
	private final String progressImageUrl = "image/progress.jpg";
	private String commissionVideoYouTubeId = "IlQ6mUne8Do";
	private List<Paint> paintPalette;
	private String uid;
	private boolean canPaint;
	private boolean autoPaint;
	private double xOffset;
	private double yOffset;
	private double wellWidthInInches;
	private boolean testing;
	private boolean isEvent;
	private String eventName;
	private String eventTag;
	private String eventUserCode;
	private String eventFeedCode;

	PaintApplicationSettings() {}

	public PaintApplicationSettings(
			final CanvasSetting canvasSetting,
			final Brush brush,
			final String canvasImageUrl,
			final String liveImageUrl,
			final String tiltedImageUrl,
			final String liveImageStream,
			final String traceImageUrl,
			final String tutorialVideoYouTubeId,
			final String commissionVideoYouTubeId,
			final List<Paint> paintPalette,
			final String uid,
			final String messageOfTheDay,
			final boolean canPaint,
			final boolean autoPaint,
			final double xOffset,
			final double yOffset,
			final double wellWidthInInches,
			final boolean testing ) {
		this.canvasSetting = canvasSetting;
		this.brush = brush;
		this.canvasImageUrl = canvasImageUrl;
		this.liveImageUrl = liveImageUrl;
		this.tiltedImageUrl = tiltedImageUrl;
		setLiveImageStream(liveImageStream);
		this.commissionVideoYouTubeId = commissionVideoYouTubeId;
		this.traceImageUrl = traceImageUrl;
		this.paintPalette = paintPalette;
		this.uid = uid;
		this.canPaint = canPaint;
		this.autoPaint = autoPaint;
		this.xOffset = xOffset;
		this.yOffset = yOffset;
		this.wellWidthInInches = wellWidthInInches;
		this.testing = testing;
		isEvent = false;
	}

	public boolean isTesting() {
		return testing;
	}

	public String getUID() {
		return uid;
	}

	public List<Paint> getPaints() {
		return paintPalette;
	}

	public CanvasSetting getCanvasSetting() {
		return canvasSetting;
	}

	public Brush getBrush() {
		return brush;
	}

	public String getLiveImageUrl() {
		return liveImageUrl;
	}

	public String getTiltedImageUrl() {
		return tiltedImageUrl;
	}

	public String getCanvasImageUrl() {
		return canvasImageUrl;
	}

	public String getTraceImageUrl() {
		return traceImageUrl;
	}

	public String getProgressImageUrl() {
		return progressImageUrl;
	}

	public boolean canPaint() {
		return canPaint;
	}

	public boolean autoPaint() {
		return autoPaint;
	}

	public double getxOffset() {
		return xOffset;
	}

	public double getyOffset() {
		return yOffset;
	}

	public double getWellWidthInInches() {
		return wellWidthInInches;
	}

	public String getLiveImageStream() {
		return liveImageStream;
	}

	public void setLiveImageStream(
			final String liveImageStream ) {
		this.liveImageStream = liveImageStream;
	}

	public String getCommissionVideoYouTubeId() {
		return commissionVideoYouTubeId;
	}

	public void setCommissionVideoYouTubeId(
			final String commissionVideoYouTubeId ) {
		this.commissionVideoYouTubeId = commissionVideoYouTubeId;
	}

	public boolean isEvent() {
		return isEvent;
	}

	public void setEvent(
			final boolean isEvent ) {
		this.isEvent = isEvent;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(
			final String eventName ) {
		this.eventName = eventName;
	}

	public String getEventTag() {
		return eventTag;
	}

	public void setEventTag(
			final String eventTag ) {
		this.eventTag = eventTag;
	}

	public String getEventUserCode() {
		return eventUserCode;
	}

	public void setEventUserCode(
			final String eventUserCode ) {
		this.eventUserCode = eventUserCode;
	}

	public String getEventFeedCode() {
		return eventFeedCode;
	}

	public void setEventFeedCode(
			final String eventFeedCode ) {
		this.eventFeedCode = eventFeedCode;
	}

}
