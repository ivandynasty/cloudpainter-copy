package com.cloudpainter.painting;

import java.io.Serializable;

//Represents distance from bottom left origin of painting in inches
public class RelativePoint implements
		Serializable
{
	private static final long serialVersionUID = 1L;
	private double x;
	private double y;

	@SuppressWarnings("unused")
	private RelativePoint() {}

	public RelativePoint(
			final double x,
			final double y ) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	@Override
	public boolean equals(
			final Object other ) {
		boolean equals = false;
		if (other instanceof RelativePoint) {
			equals = (x == ((RelativePoint) other).x) && (y == ((RelativePoint) other).y);
		}
		return equals;
	}

	@Override
	public int hashCode() {
		int i = 17;
		i += i * 31 + (x * 1000);
		i += i * 31 + (y * 1000);
		return i;
	}
}
