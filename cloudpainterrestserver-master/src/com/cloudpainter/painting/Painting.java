package com.cloudpainter.painting;

import java.util.Date;

public class Painting
{
	
	private int id;
	private int height;
	private int width;
	private int dpi;
	private String name;
	private String description;
	
	private Painting() {}

	public Painting(
			final int height,
			final int width,
			final int dpi,
			final String name,
			final String description ) {
		this.height = height;
		this.width = width;
		this.dpi = dpi;
		this.name = name;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public int getDpi() {
		return dpi;
	}

	public String getName() {
		return name;
	}

	public void setName(
			final String newName ) {
		name = newName;
	}

}
