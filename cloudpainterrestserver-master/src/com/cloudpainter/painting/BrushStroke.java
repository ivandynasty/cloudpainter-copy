package com.cloudpainter.painting;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.cloudpainter.painting.Paint;

public class BrushStroke implements
		Serializable
{
	private static final long serialVersionUID = 3L;

	private ArrayList<CanvasPoint> points;
	private ArrayList<Long> pointTimes;
	private Paint color;
	private double length;
	private long timeStamp;
	private String id;
	private String source;
	private double brushSize;

	@SuppressWarnings("unused")
	private BrushStroke() {}

	public BrushStroke(
			final Paint color,
			final String id,
			final double brushSize) {
		if ((color == null) || (id == null)) {
			throw new NullPointerException(
					"Color and Id cannot be null!");
		}
		points = new ArrayList<CanvasPoint>();
		pointTimes = new ArrayList<Long>();
		this.color = color;
		length = 0.0;
		this.source = id.substring(0,id.indexOf("|"));
		this.id = id.substring(id.indexOf("|")+1);
		this.brushSize = brushSize;
	}
	
	public double getBrushSize(){
		return brushSize;
	}

	public List<CanvasPoint> getPoints() {
		return new ArrayList<CanvasPoint>(
				points);
	}
	
	public List<Long> getPointTimes() {
		return new ArrayList<Long>(pointTimes);
	}

	public CanvasPoint checkPointLength(
			final CanvasPoint point ) {
		CanvasPoint returnValue = point;
		if ((points.size() > 0) && (point != null)) {
			final CanvasPoint lastPoint = points.get(points.size() - 1);
			final double xDistance = lastPoint.getX() - point.getX();
			final double yDistance = lastPoint.getY() - point.getY();
			final double totalDistance = Math.sqrt((xDistance * xDistance) + (yDistance * yDistance));

			if (totalDistance + length > color.getPaintLength()) {
				final double maxDistance = color.getPaintLength() - length;
				final double ratio = totalDistance / maxDistance;
				returnValue = new CanvasPoint(
						xDistance / ratio + lastPoint.getX(),
						yDistance / ratio + lastPoint.getY());
			}
		}
		return returnValue;
	}

	public void addPoint(
			final CanvasPoint point ) {
		if (point == null) {
			throw new NullPointerException(
					"Cannot add a null point!");
		}
		if (length < color.getPaintLength()) {
			if (points.size() > 0) {
				final CanvasPoint lastPoint = points.get(points.size() - 1);
				if(points.size() == 1 && lastPoint.equals(point)){
					//this appears to be a dab
					length += brushSize;
					points.add(point);
					pointTimes.add(System.currentTimeMillis());
				}
				else {
					//this appears to be a stroke
					final double xDistance = point.getX() - lastPoint.getX();
					final double yDistance = point.getY() - lastPoint.getY();
					final double totalDistance = Math.sqrt((xDistance * xDistance) + (yDistance * yDistance));
					if(totalDistance <= color.getPaintLength()){	
						length += totalDistance;
						points.add(point);
						pointTimes.add(System.currentTimeMillis());
					}
				}
			}
			else {
				points.add(point);
				pointTimes.add(System.currentTimeMillis());
			}
		}
	}

	public double getLength() {
		return length;
	}

	public Paint getColor() {
		return color;
	}

	public void setTimeStamp(
			final long timeStamp ) {
		this.timeStamp = timeStamp;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public String getId() {
		return id;
	}
	
	public String getSource() {
		return source;
	}
}
