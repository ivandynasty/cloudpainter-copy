package com.cloudpainter.painting;

import java.io.Serializable;

public class Paint implements
		Serializable,
		Comparable<Paint>
{
	private static final long serialVersionUID = 1L;
	private String paintColor;
	private double paintLength;
	private int paintId;

	@SuppressWarnings("unused")
	private Paint() {}

	public Paint(
			final String paintColor,
			final double paintLength,
			final int paintId ) {
		if(paintColor == null)
			throw new NullPointerException("Paint color cannot be null!");
		if(paintLength <= 0)
			throw new IllegalArgumentException("Paint length must be greater than zero!");
		if(paintId < 0)
			throw new IllegalArgumentException("Paint Id cannot be less than zero!");
		this.paintColor = paintColor;
		this.paintLength = paintLength;
		this.paintId = paintId;
	}

	public String getPaintColor() {
		return paintColor;
	}

	public void setPaintColor(
			String paintColor ) {
		this.paintColor = paintColor;
	}

	public double getPaintLength() {
		return paintLength;
	}

	public void setPaintLength(
			double paintLength ) {
		this.paintLength = paintLength;
	}

	public int getPaintId() {
		return paintId;
	}

	public void setPaintId(
			int paintId ) {
		this.paintId = paintId;
	}

	@Override
	public boolean equals(
			final Object other ) {
		boolean equals = false;
		if (other instanceof Paint) {
			if ((paintId == ((Paint) other).paintId) && paintColor.equalsIgnoreCase(((Paint) other).paintColor)) {
				equals = true;
			}
		}
		return equals;
	}

	@Override
	public int compareTo(
			final Paint o ) {
		return paintColor.compareTo(o.paintColor);
	}

	@Override
	public int hashCode() {
		int i = 17;
		i = (31 * i) + paintId;
		i = (31 * i) + paintColor.hashCode();
		return i;
	}

	public String getBackgroundSafeColor() {
		String returnColor = paintColor;
		int r = 0;
		int g = 0;
		int b = 0;
		if (paintColor.startsWith("rgb")) {
			final String[] stringList = paintColor.split("[(,)]");
			r = Integer.parseInt(stringList[1]);
			g = Integer.parseInt(stringList[2]);
			b = Integer.parseInt(stringList[3]);

			returnColor = "rgb(" + r + "," + g + "," + b + ")";
		}
		return returnColor;
	}

	public String getHexColor() {
		String returnColor = paintColor;
		int r = 0;
		int g = 0;
		int b = 0;
		if (paintColor.startsWith("rgb")) {
			final String[] stringList = paintColor.split("[(,)]");
			r = Integer.parseInt(stringList[1]);
			g = Integer.parseInt(stringList[2]);
			b = Integer.parseInt(stringList[3]);
			String redString = Integer.toHexString(r);
			String greenString = Integer.toHexString(g);
			String blueString = Integer.toHexString(b);
			if (redString.length() < 2) {
				redString = "0" + redString;
			}
			if (greenString.length() < 2) {
				greenString = "0" + greenString;
			}
			if (blueString.length() < 2) {
				blueString = "0" + blueString;
			}
			returnColor = "#" + redString + greenString + blueString;
		}
		return returnColor;
	}
	
}