package com.cloudpainter.processpaintmap;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

import javax.imageio.ImageIO;

import com.cloudpainter.autopaint.BitmapStroke;
import com.cloudpainter.autopaint.PaintMap;
import com.cloudpainter.autopaint.PaintMapColor;
import com.cloudpainter.autopaint.PaintMapDirection;
import com.cloudpainter.autopaint.RealWorldStroke;
import com.cloudpainter.autopaint.RelativeStroke;
import com.cloudpainter.painting.BitmapPoint;
import com.cloudpainter.painting.Paint;
import com.cloudpainter.painting.RelativePoint;
import com.cloudpainter.utils.ColorUtils;
import com.cloudpainter.utils.ElasticsearchUtility;

public class StrokePlanner
{
	PaintMap paintmap;
	Vector palette = new Vector();

	Vector brushStrokes = new Vector();

	int maxStrokeLengthInPixels = 20;
	int strokesBeforeRandomJump = 50;
	
	public StrokePlanner(PaintMap paintmap, Vector palette) {
		this.paintmap = paintmap;
		this.palette = palette;
	}
		
	
	
	public void autoGenerateStrokes() {
		
		//SET PATTERN
		final ArrayList<PaintMapDirection> strokeDirectionSequence = new ArrayList<PaintMapDirection>();
		strokeDirectionSequence.add(PaintMapDirection.SOUTH);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_WEST);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_EAST);
		
		
		palette = ColorUtils.sortColorVectorFromDarkToLight(palette);
		
		int currentPaletteIndex = 0;
		Color currentColor = (Color)palette.get(currentPaletteIndex);

		boolean paintmapComplete = false;

		int strokeDirectionIndex = 0;
		
		while (!paintmapComplete) {
		//Read palette and begin with lightest color
			BitmapPoint startPoint = paintmap.getRandomStartPointOfColor(currentColor);
			//get random start point of that color
			if (startPoint != null) {
				//if point returned 
				//get EndPoint...
				BitmapPoint endPoint = paintmap.getEndOfStroke(startPoint, strokeDirectionSequence.get(strokeDirectionIndex), maxStrokeLengthInPixels, false, true);
				strokeDirectionIndex = (strokeDirectionIndex+1)%strokeDirectionSequence.size();
				//draw a stroke
				BitmapStroke bitmapStroke = new BitmapStroke(startPoint, endPoint, currentColor);
				brushStrokes.add(bitmapStroke);
				paintmap.recordBitmapStrokeOnPaintMap(bitmapStroke, false, 0);
				for (int i = 0; i < strokesBeforeRandomJump; i++) {
					BitmapPoint nextStartPoint = paintmap.getNextStartPointOfColor(endPoint.getX(), endPoint.getY(), currentColor);
					if (nextStartPoint != null) {
						BitmapPoint nextEndPoint = paintmap.getEndOfStroke(nextStartPoint, strokeDirectionSequence.get(strokeDirectionIndex), maxStrokeLengthInPixels, false, true);
						strokeDirectionIndex = (strokeDirectionIndex+1)%strokeDirectionSequence.size();
						//draw a stroke
						BitmapStroke nextBitmapStroke = new BitmapStroke(nextStartPoint, nextEndPoint, currentColor);
						brushStrokes.add(nextBitmapStroke);
						paintmap.recordBitmapStrokeOnPaintMap(nextBitmapStroke, false, 0);
					}
				}
				//get next start point 20 times
				//go back to get random start point of that color
			} else {
				//if null Goto Next Color - if our of colors end
				if (currentPaletteIndex == palette.size()-1) {
					paintmapComplete = true;
				} else {
					currentPaletteIndex = currentPaletteIndex+1;
					currentColor = (Color)palette.get(currentPaletteIndex);
				}
			}
		}
		for (int i=0;i<brushStrokes.size();i++){
			System.out.println(brushStrokes.get(i));
		}
	}

	public void saveSimulation(String simulationFileName) {
		int width  = paintmap.getPaintmap().size()*5;
		int height = paintmap.getPaintmap().get(0).size()*5;
		
		BufferedImage simulation = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

		Graphics2D g = simulation.createGraphics();
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(0, 0 , width, height);
		g.dispose();

		for(int i=0;i<brushStrokes.size();i++) {
			Color thisColor = ((BitmapStroke)brushStrokes.get(i)).getStrokeColor();
			int sx = ((BitmapStroke)brushStrokes.get(i)).getStart().getX()*5; 
			int sy = ((BitmapStroke)brushStrokes.get(i)).getStart().getY()*5; 
			int ex = ((BitmapStroke)brushStrokes.get(i)).getEnd().getX()*5; 
			int ey = ((BitmapStroke)brushStrokes.get(i)).getEnd().getY()*5; 
			g = simulation.createGraphics();
			g.setColor(thisColor);
			g.setStroke(new BasicStroke(3));
			g.drawLine(sx+2, height-sy-2, ex+2, height-ey-2);
			g.dispose();
		}
 	    try {
 	    	System.out.println("SavingSimulation: "+simulationFileName);
 	    	File outputfile = new File(simulationFileName);
			ImageIO.write(simulation, "gif", outputfile);
	 	    System.out.println("Succesfully wrote: "+simulationFileName);
		}
		catch (IOException e) {
	 	    System.out.println("FAILED TO write: "+simulationFileName);
			e.printStackTrace();
		}
	}
	
	public void saveToElasticSearch(String imageName) {
		int width  = paintmap.getPaintmap().size();
		int height = paintmap.getPaintmap().get(0).size();
		//for(int i=0;i<2;i++) {
		for(int i=0;i<brushStrokes.size();i++) {
			//double sx = ((BitmapStroke)brushStrokes.get(i)).getStart().getX()+1/width; 
			//double sy = ((BitmapStroke)brushStrokes.get(i)).getStart().getY()+1/height; 
			//double ex = ((BitmapStroke)brushStrokes.get(i)).getEnd().getX()+1/width; 
			//double ey = ((BitmapStroke)brushStrokes.get(i)).getEnd().getY()+1/height; 
			//RelativePoint startRelativePoint = new RelativePoint(sx,sy);
			//RelativePoint endRelativePoint = new RelativePoint(ex,ey);
			//RelativeStroke rs = new RelativeStroke(startRelativePoint, endRelativePoint,((BitmapStroke)brushStrokes.get(i)).getStrokeColor() );
			BitmapStroke bms = ((BitmapStroke)brushStrokes.get(i));
			ElasticsearchUtility.SaveAutomatedStrokesToElasticSearchCluster(imageName, width, height, i, bms, false);
		}
	}
	
	public void saveToJsonFile(String fileName, String imageName) {
		int width  = paintmap.getPaintmap().size();
		int height = paintmap.getPaintmap().get(0).size();
		//for(int i=0;i<2;i++) {
			ElasticsearchUtility.SaveAutomatedStrokesToJsonTextFile(fileName, imageName, width, height, brushStrokes, false);
	}
	
	
	
}
