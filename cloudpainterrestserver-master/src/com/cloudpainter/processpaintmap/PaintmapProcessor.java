package com.cloudpainter.processpaintmap;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import javax.imageio.ImageIO;

import org.json.JSONObject;

import com.cloudpainter.autopaint.BitmapStroke;
import com.cloudpainter.autopaint.PaintMap;
import com.cloudpainter.autopaint.PaintMapColor;
import com.cloudpainter.utils.ColorUtils;
import com.cloudpainter.utils.ElasticsearchUtility;
import com.cloudpainter.utils.FileCopier;
import com.cloudpainter.utils.GCodeEmbellishmentGenerator;
import com.cloudpainter.utils.GCodeGenerator;
import com.cloudpainter.utils.LiveGCodeExecuter;
import com.cloudpainter.utils.LiveRoboForthExecuter;

public class PaintmapProcessor
{

	public static String rootDir = "C:\\cloudpainterrestserver\\currentpainting\\";
	
	PaintMap paintmap;
	
	//SOURCE IMAGE is the SOURCE GIF that is also used in tracing.
	//it is the painting people are trying to make
	BufferedImage sourceImage;// = Toolkit.getDefaultToolkit().getImage(fileName); 
	//BufferedImage progressImage;// = Toolkit.getDefaultToolkit().getImage(fileName); 
	
	static String paintingName;
	static String GCODEorFORTH;
	static boolean UseWellLocationsFromConfig;
	
	//PAINTING IMAGE is the result of the painting actually executed on
	Image paintingImage;// = Toolkit.getDefaultToolkit().getImage(fileName); 
	
	

	int paintingWidth;
	int paintingHeight;
	
	
	public PaintmapProcessor() {
	}
	
	public static void setPaintingParametersFromConfig() {
		try {
			JSONObject lpp = ElasticsearchUtility.getJsonObjectFromFile(rootDir+"config\\live_painting_parameters.json");
  		    paintingName = lpp.getString("paintingName");
  		    GCODEorFORTH = lpp.getString("GCODEorFORTH");
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//int topLeftX      = snapshotCorners.getInt("topLeftX");
		//int topLeftY      = snapshotCorners.getInt("topLeftY");
		
	}
	
	public static void main(String[] args) {
		//http://localhost:8082/CloudPainterRestServer/restservices/processpaintmapintostrokes/paintmap.gif
		//System.out.println( "C:\\cloudpainterrestserver\\currentpainting\\paintmap.gif");
		//String sourceImageFilename = "paintmap_test.gif";
		//String sourceImageF+ilename = "robotlineart.gif";
		//String sourceImageFilename = "anita_humans.gif";
		//String sourceImageFilename = "cooks_beta009.gif";
		//cook_childrens_eam_day0001
		//String sourceImageFilename = paintingName;
		
		setPaintingParametersFromConfig();
		
		String sourceImageFilename = paintingName+".gif";
		File sourceImageFile = new File(rootDir +"\\"+sourceImageFilename);
		if (!sourceImageFile.exists()) {
			//try png
			sourceImageFilename = paintingName+".png";
			sourceImageFile = new File(rootDir +"\\"+sourceImageFilename);
			if (!sourceImageFile.exists()) {
				File defaultGif = new File(rootDir +"\\default.gif");
				try {
					FileCopier.copyFileUsingStream(defaultGif, sourceImageFile);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}		
			}			
			
			
			
		}
		
		//testArtisticStrokeProcessor(sourceImageFilename);
		//testPaintmapProcessor(sourceImageFilename);
		//testElasticRestCall(sourceImageFilename);
		//convertElasticsearchBrushstrokesToGcode(rootDir, sourceImageFilename);

		if (sourceImageFilename.indexOf(".gif")!=-1) {
			testLiveExecution(rootDir, sourceImageFilename, false, getPalette(rootDir, sourceImageFilename), GCODEorFORTH);
		} else {
			testLiveExecution(rootDir, sourceImageFilename, false, null, GCODEorFORTH);
		}


		//replayStrokesInSimulation(rootDir, sourceImageFilename, 962, 1200);
	
		//convertElasticsearchBrushstrokesToEmbellishmentGcode(rootDir, sourceImageFilename);
		
	}
	
public static void replayStrokesInSimulation(String sourceDir, String sourceFilename, int startIndex, int endIndex) {

	String sourceFileNameRoot = sourceFilename.substring(0,sourceFilename.indexOf("."));
	String sourceFileName = sourceDir+sourceFilename;
	String simulationFileName = sourceDir+sourceFileNameRoot+"_simulation.gif";

	//SAVE GIF
	LiveGCodeExecuter.saveSimulation(sourceFileNameRoot, simulationFileName, startIndex, endIndex);


}

	
public void generateArtisticStrokes(String sourceDir, String sourceFilename) {
		
		String sourceFileNameRoot = sourceFilename.substring(0,sourceFilename.indexOf("."));
		String sourceFileName = sourceDir+sourceFilename;
		
		String simulationFileName = sourceDir+sourceFileNameRoot+"_simulation.gif";
		
		ArtisticStrokePlanner artisticStrokePlanner = new ArtisticStrokePlanner(paintmap, getPalette(sourceDir, sourceFilename));
		
		artisticStrokePlanner.autoGenerateStrokes();

		//SAVE GIF
		artisticStrokePlanner.saveSimulation(simulationFileName);
		//SAVE to Elastic
		artisticStrokePlanner.saveToElasticSearch(sourceFileNameRoot);
		//SAVE to Text File
		artisticStrokePlanner.saveToJsonFile(sourceDir+sourceFileNameRoot+".json", sourceFileNameRoot);
		
	}

	
	public void generateStrokes(String sourceDir, String sourceFilename) {
		
		String sourceFileNameRoot = sourceFilename.substring(0,sourceFilename.indexOf("."));
		String sourceFileName = sourceDir+sourceFilename;
		
		String simulationFileName = sourceDir+sourceFileNameRoot+"_simulation.gif";
		StrokePlanner strokePlanner = new StrokePlanner(paintmap, getPalette(sourceDir, sourceFilename));
		
		strokePlanner.autoGenerateStrokes();

		//SAVE GIF
		strokePlanner.saveSimulation(simulationFileName);
		//SAVE to Elastic
		strokePlanner.saveToElasticSearch(sourceFileNameRoot);
		//SAVE to Text File
		strokePlanner.saveToJsonFile(sourceDir+sourceFileNameRoot+".json", sourceFileNameRoot);
		
	}
	
	public void loadSourceImage(String sourceDir, String sourceFilename) {

		String sourceFileNameRoot = sourceFilename.substring(0,sourceFilename.indexOf("."));
		String sourceFileName = sourceDir+sourceFilename;
		String progressFileName = sourceDir+sourceFileNameRoot+"_1progress.jpg";
		
		//load paintmap filename to create paintmap		
		File imageFile = new File(sourceFileName);
		System.out.println("loading paintmapImage: "+imageFile.getAbsolutePath());
		
		try {
			sourceImage = ImageIO.read(imageFile);
			System.out.println("paintmapImage Dimensions: "+sourceImage.getWidth()+"x"+sourceImage.getHeight());
			paintingWidth = sourceImage.getWidth();
			paintingHeight = sourceImage.getHeight();
		} catch (IOException e) {
			System.out.println("Failed to read: "+sourceFileName);
			e.printStackTrace();
		}
		
		paintmap = new PaintMap(
				paintingWidth,
				paintingHeight,
				1);
		paintmap.createPaintMapFromFilename(sourceFileName);
		//at this point PAINTMAP is saved in global variable
	}

	public static Vector getPalette(String sourceDir, String sourceFilename) {

		Vector palette = new Vector();
		
		String sourceFileNameRoot = sourceFilename.substring(0,sourceFilename.indexOf("."));
		String sourceFileName = sourceDir+sourceFilename;
		String paletteFileName = sourceDir+sourceFileNameRoot+"_palette.gif";
		
		//read paintmap coverage file and fill in those values of paintmap
		File imageFile = new File(sourceFileName);
		System.out.println("loading source file: "+imageFile.getAbsolutePath());

		try {
				BufferedImage sourceImage = ImageIO.read(imageFile);
				System.out.println("sourceImage Dimensions: "+sourceImage.getWidth()+"x"+sourceImage.getHeight());
				//TODO Loop through file and update paintmap wherever it is not white
				System.out.println("Adding Following Colors to Palette:");
				for (int ix = 0; ix < sourceImage.getWidth(); ix++) {
					for (int iy =0; iy < sourceImage.getHeight(); iy++) {
						Color rgbColor = new Color(sourceImage.getRGB(ix,iy));
						if (!(rgbColor.getRed() == 255 &&
								rgbColor.getGreen() == 255 &&
								rgbColor.getBlue() == 255) ) {
							if (!palette.contains(rgbColor)){	
								palette.add(rgbColor);
								System.out.println("     "+rgbColor);
							}
						}
					}
				}
				palette = ColorUtils.sortColorVectorFromDarkToLight(palette);
				//palette = ColorUtils.sortColorFromUnltraVioletToInfraRed(palette);
				
				BufferedImage paletteFile = new BufferedImage((palette.size()*10), 10, BufferedImage.TYPE_INT_ARGB);
		        for (int i=0; i<palette.size();i++) {
		        	   Graphics2D g = paletteFile.createGraphics();
		        	   g.setColor((Color)palette.get(i));
			           g.fillRect((i*10), 0, (i*10)+10, 10);
			           g.dispose();
		        }
		        try {
		        System.out.println("Attempting to make palette file: "+paletteFileName);
		        	   File outputfile = new File(paletteFileName);
		        	   ImageIO.write(paletteFile, "gif", outputfile);
		        	   System.out.println("Succesfully wrote: "+paletteFileName);
				} catch (IOException f) {
						System.out.println("Failed to write: "+paletteFileName);
						f.printStackTrace();
				}
		        
		        String palleteFileNameJson = paletteFileName.substring(0,paletteFileName.lastIndexOf("\\")+1);
		        palleteFileNameJson = palleteFileNameJson+"config\\paintselection.json";
		        
		        ColorUtils.saveColorJsonFile(palleteFileNameJson, palette);
		        
		} catch (IOException e) {
		}
		return palette;
	}
	
	public void loadProgressImage(String sourceDir, String sourceFilename) {
		
		String sourceFileNameRoot = sourceFilename.substring(0,sourceFilename.indexOf("."));
		String sourceFileName = sourceDir+sourceFilename;
		String progressFileName = sourceDir+sourceFileNameRoot+"_1progress.jpg";

		//read paintmap coverage file and fill in those values of paintmap
		File progressFile = new File(progressFileName);
		System.out.println("loading progress file: "+progressFile.getAbsolutePath());

		try {
				BufferedImage progressImage = ImageIO.read(progressFile);
				System.out.println("progressImage Dimensions: "+progressImage.getWidth()+"x"+progressImage.getHeight());
				//TODO Loop through file and update paintmap wherever it is not white
				for (int ix = 0; ix < progressImage.getWidth(); ix++) {
					for (int iy =0; iy < progressImage.getHeight(); iy++) {
						Color rgbColor = new Color(progressImage.getRGB(ix,iy));
						if (!(rgbColor.getRed() == 255 &&
								rgbColor.getGreen() == 255 &&
								rgbColor.getBlue() == 255) ) {
							paintmap.setPaintMapPixelAsPainted(ix,iy,1);
						}
					}
				}
				
		} catch (IOException e) {
				System.out.println("Failed to read progress: "+progressFileName);
				//TODO if no file, get dimensions and create one.
				File imageFile = new File(sourceFileName);
				try {
					sourceImage = ImageIO.read(imageFile);
					System.out.println("Gettting paintmapImage Dimensions: "+sourceImage.getWidth()+"x"+sourceImage.getHeight());
					paintingWidth = sourceImage.getWidth();
					paintingHeight = sourceImage.getHeight();
				} catch (IOException ee) {
					System.out.println("Failed to read: "+sourceFileName);
					e.printStackTrace();
				}
				
				BufferedImage blankProgressFile = new BufferedImage((paintingWidth), (paintingHeight), BufferedImage.TYPE_INT_ARGB);
				Graphics2D g = blankProgressFile.createGraphics();
		           g.setColor(Color.WHITE);
		           g.fillRect(0, 0, blankProgressFile.getWidth(), blankProgressFile.getHeight());
		           g.dispose();
		           try {
		        	   System.out.println("Attempting to make blank progress file: "+progressFileName);
		        	   File outputfile = new File(progressFileName);
		        	   ImageIO.write(blankProgressFile, "gif", outputfile);
		        	   System.out.println("Succesfully wrote: "+progressFileName);
					} catch (IOException f) {
						System.out.println("Failed to write: "+progressFileName);
						f.printStackTrace();
					}
			}
	}

	public void savePaintmapProgress(String sourceDir, String sourceFilename) {
		
		String sourceFileNameRoot = sourceFilename.substring(0,sourceFilename.indexOf("."));
		String sourceFileName = sourceDir+sourceFilename;
		String progressFileName = sourceDir+sourceFileNameRoot+"_1progress.jpg";
		
		try {
			ArrayList<ArrayList<PaintMapColor>> paintMapGrid = paintmap.getPaintmap();
			BufferedImage updatedProgress = new BufferedImage(paintMapGrid.size(), paintMapGrid.get(0).size(), BufferedImage.TYPE_INT_ARGB);
			for (int i=0;i<paintMapGrid.size();i++) {
				for (int j=0;j<paintMapGrid.get(i).size();j++) {
					Color setColor;
						if ( ((PaintMapColor)(paintMapGrid.get(i).get(j))).isPainted() ) {
							setColor = Color.RED;
						} else {
							setColor = Color.WHITE;
						}
						Graphics2D g = updatedProgress.createGraphics();
						g.setColor(setColor);
						g.fillRect(i, j, i+1, j+1);
						g.dispose();
				}
			}
			System.out.println("Updating progress file: "+progressFileName);
     	    File outputfile = new File(progressFileName);
     	    ImageIO.write(updatedProgress, "gif", outputfile);
     	    System.out.println("Succesfully wrote: "+progressFileName);
		} catch (IOException f) {
			System.out.println("Failed to write: "+progressFileName);
			f.printStackTrace();
		}
	}
	
	

	public static void convertElasticsearchBrushstrokesToGcode(String sourceDir, String sourceFilename) {
		
		String sourceFileNameRoot = sourceFilename.substring(0,sourceFilename.indexOf("."));
		String gcodeFilename = sourceDir+sourceFileNameRoot+".gcode";
		    
		GCodeGenerator gCodeEngine = new GCodeGenerator();
		
		String gCodeToSave = gCodeEngine.makeGCodeString(sourceFileNameRoot);
		
		BufferedWriter writer;
		try {
			writer = new BufferedWriter( new FileWriter( gcodeFilename ));
			writer.write( gCodeToSave );
			writer.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static void convertElasticsearchBrushstrokesToEmbellishmentGcode(String sourceDir, String sourceFilename) {
		
		String sourceFileNameRoot = sourceFilename.substring(0,sourceFilename.indexOf("."));
		String gcodeFilename = sourceDir+sourceFileNameRoot+".gcode";
		    
		GCodeEmbellishmentGenerator gCodeEngine = new GCodeEmbellishmentGenerator();
		
		String gCodeToSave = gCodeEngine.makeGCodeString(sourceFileNameRoot);
		
		BufferedWriter writer;
		try {
			writer = new BufferedWriter( new FileWriter( gcodeFilename ));
			writer.write( gCodeToSave );
			writer.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	public void savePaintmapProgress() {
	}


	
	
	
	public static void testArtisticStrokeProcessor(String sourceImageFilename) {
		PaintmapProcessor pp = new PaintmapProcessor();
		pp.loadSourceImage(rootDir, sourceImageFilename);
		//pp.createPalette(rootDir, sourceImageFilename);
		pp.loadProgressImage(rootDir, sourceImageFilename);
		pp.savePaintmapProgress(rootDir, sourceImageFilename);
		pp.generateArtisticStrokes(rootDir, sourceImageFilename);
	}
	
	public static void testPaintmapProcessor(String sourceImageFilename) {

		PaintmapProcessor pp = new PaintmapProcessor();
		pp.loadSourceImage(rootDir, sourceImageFilename);
		//pp.createPalette(rootDir, sourceImageFilename);
		pp.loadProgressImage(rootDir, sourceImageFilename);
		pp.savePaintmapProgress(rootDir, sourceImageFilename);
		pp.generateStrokes(rootDir, sourceImageFilename);

	}


	
	public static void testElasticRestCall(String sourceImageFilename) {
		
		String sourceFileNameRoot = sourceImageFilename.substring(0,sourceImageFilename.indexOf("."));
		ElasticsearchUtility.sendGetStrokeRequest(sourceFileNameRoot, 1);
		
	}

	
	public static void testLiveExecution(String rootDir, String sourceImageFilename, boolean makeAIStrokes, Vector palette, String GCODEorFORTH) {

		PaintmapProcessor pp = new PaintmapProcessor();
		String sourceFileNameRoot = sourceImageFilename.substring(0,sourceImageFilename.indexOf("."));
		
		//make goalfile
		System.out.println("making: "+rootDir+sourceFileNameRoot+"_0goal.png");
		File imageFile = new File(rootDir+sourceFileNameRoot+".png");
		try {
				BufferedImage sourceImage = ImageIO.read(imageFile);
				File outputFile = new File(rootDir+sourceFileNameRoot+"_0goal.png");
				ImageIO.write(sourceImage, "png", outputFile);
		} catch (Exception e)		 {
			System.out.println("Error: Could not create goal file");
		}
		
		/*//make mask image...  anything on the mask image can be made white to block it out...
		System.out.println("making: "+rootDir+sourceFileNameRoot+"_mask.gif");
		try {
				BufferedImage sourceImage = ImageIO.read(imageFile);
				File outputFile = new File(rootDir+sourceFileNameRoot+"_mask.gif");
				ImageIO.write(sourceImage, "gif", outputFile);
		} catch (Exception e)		 {
			System.out.println("Error: Could not create mask file");
		}
		*/
		
		//also load progress image so areas can be manually masked out...
		//pp.loadProgressImage(rootDir, sourceImageFilename);
		
		if (GCODEorFORTH.equalsIgnoreCase("GCODE")) {
			LiveGCodeExecuter gCodeEngine = new LiveGCodeExecuter(rootDir, sourceFileNameRoot, makeAIStrokes, palette);
			gCodeEngine.startPainting();
		} else if (GCODEorFORTH.equalsIgnoreCase("FORTH")) {
			LiveRoboForthExecuter roboForthEngine = new LiveRoboForthExecuter(rootDir, sourceFileNameRoot, makeAIStrokes, palette);
			roboForthEngine.startPainting();
		} else {
			System.out.println("Error: Please Specify GCODE or FORTH");
		}
		
	}



	
	
}
