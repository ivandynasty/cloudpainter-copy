package com.cloudpainter.processpaintmap;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Vector;

import javax.imageio.ImageIO;

import com.cloudpainter.autopaint.BitmapStroke;
import com.cloudpainter.autopaint.PaintMap;
import com.cloudpainter.autopaint.PaintMapColor;
import com.cloudpainter.autopaint.PaintMapDirection;
import com.cloudpainter.autopaint.RealWorldStroke;
import com.cloudpainter.autopaint.RelativeStroke;
import com.cloudpainter.painting.BitmapPoint;
import com.cloudpainter.painting.Paint;
import com.cloudpainter.painting.RelativePoint;
import com.cloudpainter.utils.ColorUtils;
import com.cloudpainter.utils.ElasticsearchUtility;

public class ArtisticStrokePlanner
{
	//PaintMap Paintmap

	PaintMap virginPaintmap;
	PaintMap dirtyPaintmap;
	
	Vector palette = new Vector();

	Vector brushStrokeCandidates = new Vector();

	Vector brushStrokeClusters = new Vector();
	
	Vector<BitmapStroke> brushStrokes = new Vector();

	BitmapStroke singleBrushStroke;
	
	int maxStrokeLengthInPixels = 20;
	int strokesBeforeRandomJump = 50;
	
	public ArtisticStrokePlanner(PaintMap paintmap, Vector palette) {
		this.virginPaintmap = paintmap;
		this.palette = palette;
	}
		
	
	public void autoGenerateStrokes() {
		
		//SET PATTERN
		final ArrayList<PaintMapDirection> strokeDirectionSequence = new ArrayList<PaintMapDirection>();
		//strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_SOUTH_WEST);
		//strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_WEST);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_WEST);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_SOUTH_WEST);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_WEST);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_WEST_WEST);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_WEST_WEST_WEST);
		strokeDirectionSequence.add(PaintMapDirection.WEST);
		strokeDirectionSequence.add(PaintMapDirection.NORTH_WEST_WEST_WEST);
		strokeDirectionSequence.add(PaintMapDirection.NORTH_WEST_WEST);
		strokeDirectionSequence.add(PaintMapDirection.NORTH_WEST);
		strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_WEST);
		strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_NORTH_WEST);
		strokeDirectionSequence.add(PaintMapDirection.NORTH);
		strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_NORTH_EAST);
		strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_EAST);
		strokeDirectionSequence.add(PaintMapDirection.NORTH_EAST);
		strokeDirectionSequence.add(PaintMapDirection.NORTH_EAST_EAST);
		strokeDirectionSequence.add(PaintMapDirection.NORTH_EAST_EAST_EAST);
		strokeDirectionSequence.add(PaintMapDirection.EAST);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_EAST_EAST);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_EAST_EAST_EAST);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_EAST);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_EAST);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_SOUTH_EAST);

		
		palette = ColorUtils.sortColorVectorFromDarkToLight(palette);
		int paletteSize = palette.size();
		Color currentColor = (Color)palette.get(0);

		boolean paintmapComplete = false;
		int strokeDirectionIndex = 0;
		
		//400 //20 /20
//for (int cp=paletteSize-1;cp>=0;cp--) {		
	for (int cp=0;cp<paletteSize;cp++) {
		brushStrokeCandidates = new Vector();
		brushStrokeClusters = new Vector();
		currentColor = (Color)palette.get(cp);
		//horizontalstrokes
	//for (int currentStrokeLength=50; currentStrokeLength>10; currentStrokeLength--) {
		int maxStrokeLength = 25;
		int minStrokeLength = 1;
		for (int y=0; y<virginPaintmap.getHeight(); y=y+3) {
			for (int x=0; x<virginPaintmap.getWidth();x=x+3) {
				PaintMapDirection longestPaintMapDirection = PaintMapDirection.SOUTH;
				double longestStroke = 0;
				BitmapStroke longestBitmapStroke = new BitmapStroke(new BitmapPoint(0,0),new BitmapPoint(0,0), currentColor);
				boolean strokeFound = false;
				for (int direction= 0; direction<strokeDirectionSequence.size();direction++) {
					Color colorAtPoint = virginPaintmap.getColorFromPaintMapXY(x, y);
					if ( colorAtPoint.equals(currentColor )) {
						BitmapPoint startPoint = new BitmapPoint(x,y);//virginPaintmap.getRandomStartPointOfColor(currentColor);
						BitmapPoint endPoint = virginPaintmap.getEndOfStroke(startPoint, strokeDirectionSequence.get(direction), maxStrokeLength, false, true);
						BitmapStroke bitmapStroke = new BitmapStroke(startPoint, endPoint, currentColor);
						double thisLength = bitmapStroke.getLength(startPoint, endPoint);
						if (thisLength >= minStrokeLength) {
							//if (!checkIfInteresectsWithExistingStrokes(bitmapStroke)){
								//brushStrokes.add(bitmapStroke);
						//		brushStrokeCandidates.add(bitmapStroke);
							//}
						//}
							if (thisLength>longestStroke) {
								strokeFound = true;
								longestStroke = thisLength;
								longestPaintMapDirection = strokeDirectionSequence.get(direction);
								longestBitmapStroke = bitmapStroke;
							}
						}						
						
					}
				}
				if (strokeFound) {
					brushStrokeCandidates.add(longestBitmapStroke);
				}
			}
			
			
			
		}
		System.out.println("number of candidates:"+brushStrokeCandidates.size());
		//	reduceIntersectingBrushStrokeCandidates();
		//brushStrokes = brushStrokeCandidates;
			
		putBrushStrokeCandidatesInClusters();
		//colorClusters();
		cleanClusters();
		
		addClustersToBrushstrokes();
		System.out.println("number of strokes:"+brushStrokes.size());
	//}
}

	combineSimilarColors();
	
	}

	public void putBrushStrokeCandidatesInClusters() {
		//put brush strokes in clusters that intersect and have similar slopes
		for (int i=0;i<brushStrokeCandidates.size();i++) {
			//if does not intersect with any clusters add to new cluster
			//if it does intersect, add it to that cluster
			if (!addTooExistingBrushstrokeCluster((BitmapStroke)brushStrokeCandidates.elementAt(i))) {
				Vector newBrushStrokeCluster = new Vector();
				newBrushStrokeCluster.add((BitmapStroke)brushStrokeCandidates.elementAt(i));
				brushStrokeClusters.add(newBrushStrokeCluster);
			} 
		}
		//combine clusters
	}
	
	public int minClusterSize = 2;
	
	int numberOfClustersBigEnough = 0;
	
	public void cleanClusters() {
		for (int i=0;i<brushStrokeClusters.size();i++) {
			int brushstrokesInCluster = ((Vector)brushStrokeClusters.elementAt(i)).size();
			if (brushstrokesInCluster >minClusterSize) {
				//System.out.println(brushstrokesInCluster);
				numberOfClustersBigEnough++;
				double totalArcTan = 0;
					for (int j=0; j<((Vector)brushStrokeClusters.elementAt(i)).size();j++) {
						BitmapStroke newStroke = (BitmapStroke)  ((Vector)brushStrokeClusters.elementAt(i)).elementAt(j) ;
						Line2D.Double s1 = new Line2D.Double(newStroke.getStart().getX(),newStroke.getStart().getY(),newStroke.getEnd().getX(),newStroke.getEnd().getY());
						double firstSlope = slope(s1.x1,s1.y1, s1.x2, s1.y2);
						double thisArcTan = Math.atan(firstSlope);
						totalArcTan +=  thisArcTan;
					}
					double averageArcTan = totalArcTan/((Vector)brushStrokeClusters.elementAt(i)).size();
					for (int j=0; j<((Vector)brushStrokeClusters.elementAt(i)).size();j++) {
							Vector thisCluster = ((Vector)brushStrokeClusters.elementAt(i));
							BitmapStroke newStroke = (BitmapStroke)  thisCluster.elementAt(j) ;
							Line2D.Double s1 = new Line2D.Double(newStroke.getStart().getX(),newStroke.getStart().getY(),newStroke.getEnd().getX(),newStroke.getEnd().getY());
							double firstSlope = slope(s1.x1,s1.y1, s1.x2, s1.y2);
							double firstArcTan = Math.atan(firstSlope);
							boolean close1 = Math.abs(firstArcTan-averageArcTan) < Math.PI/6;
							boolean close2 = Math.PI-(Math.abs(firstArcTan)+Math.abs(averageArcTan)) < Math.PI/6;
							if (!(close2 || close1)) {
							  //System.out.println("remove");
							  ((Vector)brushStrokeClusters.elementAt(i)).removeElementAt(j);
							}
					}
			}
		}
		System.out.println(numberOfClustersBigEnough);
	}


	public void combineSimilarColors() {
		Vector allColors = new Vector();
		Vector groupedColors = new Vector();
		for (int i=0;i<brushStrokes.size();i++) {
			Color clusterColor = ((BitmapStroke)(brushStrokes.elementAt(i))).getStrokeColor();
			if (!allColors.contains(clusterColor)) {
				allColors.add(clusterColor);
			}
		}
		groupedColors.add(allColors.elementAt(0));
		for (int i=0;i<allColors.size();i++) {
			for (int j = 0; j<groupedColors.size(); j++) {
				Color newA = (Color)allColors.elementAt(i);
				Color existingB = (Color)groupedColors.elementAt(j);
				if ( (newA.getRed() != existingB.getRed()) || (newA.getGreen() != existingB.getGreen()) || (Math.abs( newA.getBlue()-existingB.getBlue())>6) ) {
					//check if color exists in grouped colors
					//try to add it
					boolean alreadyInGroupedColors = false;
					for (int q=0; q<groupedColors.size();q++) {
						Color qc = (Color)groupedColors.elementAt(q);
						if ( (newA.getRed() == qc.getRed()) && (newA.getGreen() == qc.getGreen()) && (Math.abs( newA.getBlue()-qc.getBlue())<6) ) {
							alreadyInGroupedColors = true;
						} 
					}
					if (!alreadyInGroupedColors) {
						groupedColors.add(newA);
					}
				}
			}
				
		}
		
				for (int i=0; i<brushStrokes.size();i++) {
					BitmapStroke bms = ((BitmapStroke)(brushStrokes).elementAt(i));
					Color colorA = bms.getStrokeColor();
					for (int k = 0; k<groupedColors.size(); k++) {
						Color colorB = (Color)groupedColors.elementAt(k);
						if (    (colorA.getRed() == colorB.getRed()) && (colorA.getGreen() == colorB.getGreen())  ) {
							if (Math.abs( colorA.getBlue() - colorB.getBlue()  ) < 6     ) {
								BitmapStroke newStroke = (BitmapStroke)(brushStrokes).elementAt(i);
								newStroke.setStrokeColor(colorB);
								brushStrokes.setElementAt(newStroke, i);
							}
						}
				}
		}
		
		
	}

	
	public void addClustersToBrushstrokes() {
		Vector clustersBigEnough = new Vector();
		Vector brushstrokesToAdd = new Vector();

		for (int i=0;i<brushStrokeClusters.size();i++) {
			int clusterSize = ((Vector)brushStrokeClusters.elementAt(i)).size();
			
			if (clusterSize>minClusterSize) {
				//System.out.println("clusterSize:"+clusterSize);
				int numberOfStrokesToAdd = (clusterSize/8)+2;
				//System.out.println("stroeks:"+numberOfStrokesToAdd);
				clustersBigEnough.add((Vector)brushStrokeClusters.elementAt(i));
				brushstrokesToAdd.add(numberOfStrokesToAdd);
				//for (int j=0; j<((Vector)brushStrokeClusters.elementAt(i)).size();j++) {
				//for (int q= 0; q<numberOfStrokesToAdd; q++) {	
				//	Random rand = new Random();
				//	int  n = rand.nextInt(clusterSize);
				//	brushStrokes.add((BitmapStroke)  ((Vector)brushStrokeClusters.elementAt(i)).elementAt(n) );
				//	//RemovestrokesFromCluster
				//}
				}
			}
		System.out.println("clustersToPaint:"+clustersBigEnough.size());
	
			Collections.shuffle(clustersBigEnough);
			for (int i=0;i<clustersBigEnough.size();i++) {
				int clusterSize = ((Vector)clustersBigEnough.elementAt(i)).size();
				//System.out.println("clusterSize:"+clusterSize);
				//if (run <=((int)brushstrokesToAdd.elementAt(i))) {
				for (int q= 0; q<((int)brushstrokesToAdd.elementAt(i)); q++) {	
					Random rand = new Random();
					int  n = rand.nextInt(clusterSize);
					brushStrokes.add((BitmapStroke)  ((Vector)clustersBigEnough.elementAt(i)).elementAt(n) );
					//RemovestrokesFromCluster
				}
	
		}
	}
	
	public void colorClusters() {
		final ArrayList<Color> colors = new ArrayList<Color>();
		//strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_SOUTH_WEST);
		//strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_WEST);

		colors.add(Color.RED);
		colors.add(Color.BLUE);
		colors.add(Color.GREEN);
		colors.add(Color.CYAN);
		colors.add(Color.YELLOW);
		colors.add(Color.MAGENTA);
		colors.add(Color.PINK);
		colors.add(Color.ORANGE);
		colors.add(Color.BLACK);
		//colors.add(Color.DARK_GRAY);
		colors.add(Color.WHITE);
		
		for (int i=0;i<brushStrokeClusters.size();i++) {
			for (int j=0; j<((Vector)brushStrokeClusters.elementAt(i)).size();j++) {
				Color thisColor = colors.get(i%colors.size());
				((BitmapStroke)  ((Vector)brushStrokeClusters.elementAt(i)).elementAt(j) ).setStrokeColor(thisColor);
			}
		}
	}
	
	public boolean addTooExistingBrushstrokeCluster(BitmapStroke newStroke) {
		boolean belongsInExistingCluster = false;
		for (int i=0;i<brushStrokeClusters.size();i++) {
			for (int j=0; j<((Vector)brushStrokeClusters.elementAt(i)).size();j++) {
				BitmapStroke existingStroke = (BitmapStroke)((Vector)brushStrokeClusters.elementAt(i)).elementAt(j);
				Line2D.Double s1 = new Line2D.Double(existingStroke.getStart().getX(),existingStroke.getStart().getY(),existingStroke.getEnd().getX(),existingStroke.getEnd().getY());
				Line2D.Double s2 = new Line2D.Double(newStroke.getStart().getX(),newStroke.getStart().getY(),newStroke.getEnd().getX(),newStroke.getEnd().getY());
				if (strokesIntersect(s1,s2)) {
					  double firstSlope = slope(s1.x1,s1.y1, s1.x2, s1.y2);
					  double secondSlope = slope(s2.x1,s2.y1, s2.x2, s2.y2);
					  double firstArcTan = Math.atan(firstSlope);
					  double secondArcTan = Math.atan(secondSlope);
					  
					  boolean close1 = Math.abs(firstArcTan-secondArcTan) < Math.PI/5;
					  boolean close2 = Math.PI-(Math.abs(firstArcTan)+Math.abs(secondArcTan)) < Math.PI/5;
					  //if (firstArcTan == 0 || secondArcTan ==0) {
					  //	  int p = 1;
					  //}
					  if (close2 || close1) {
						  	((Vector)brushStrokeClusters.elementAt(i)).add(newStroke);
						  	belongsInExistingCluster = true;
						  	return belongsInExistingCluster;
					  }
				}
			}
		}
		return belongsInExistingCluster;
	}
	
	static double slope(double x1, double y1, 
	  		double x2, double y2) 
	  { 
		  double thisSlope = 0;
	  	if ((x2 - x1) != 0) {		
	  		thisSlope = (y2 - y1) / (x2 - x1); 	
	  	}
	  	else if (y1 < y2){
	  		thisSlope =  1000000.0;
	  	}	  	
	  	else if (y1 > y2){
	  		thisSlope =  -1000000.0;
	  	}

	  
	  	return thisSlope;
	  	
	  } 
	
	public void reduceIntersectingBrushStrokeCandidates() {
			//already sorted from bongest to smallest
			//so start at smallest see if it intersects with anything larger
			
		
		for (int i=brushStrokeCandidates.size()-1;i>=0;i--) {
			BitmapStroke strokeOne = (BitmapStroke)brushStrokeCandidates.elementAt(i);
			boolean longestStroke = true;
			int numberOfIntersections = 0;
			for (int j=0; j<brushStrokeCandidates.size();j++) {
				BitmapStroke strokeTwo = (BitmapStroke)brushStrokeCandidates.elementAt(j);
				Line2D.Double s1 = new Line2D.Double(strokeOne.getStart().getX(),strokeOne.getStart().getY(),strokeOne.getEnd().getX(),strokeOne.getEnd().getY());
				Line2D.Double s2 = new Line2D.Double(strokeTwo.getStart().getX(),strokeTwo.getStart().getY(),strokeTwo.getEnd().getX(),strokeTwo.getEnd().getY());
				if (strokesIntersect(s1, s2)) {
					double strokeOneLength = strokeOne.getLength(strokeOne.getStart(),strokeOne.getEnd());
					double strokeTwoLength = strokeTwo.getLength(strokeTwo.getStart(),strokeTwo.getEnd());
					if (strokeOneLength<strokeTwoLength) {
						numberOfIntersections++;
						if (numberOfIntersections > 2) {
							longestStroke = false;
							numberOfIntersections = 0;
							j=brushStrokeCandidates.size();
						}
					}
				}
			}
			if (longestStroke) {
				brushStrokes.add(strokeOne);
			}
		}
		
		BitmapStroke strokeOne = (BitmapStroke)brushStrokeCandidates.elementAt(0);
		BitmapStroke strokeTwo = (BitmapStroke)brushStrokeCandidates.elementAt(1);

		
			//if it does not add it
	}
	
	
	
	public boolean checkIfInteresectsWithExistingStrokes(BitmapStroke strokeOne) {
		Line2D.Double s1 = new Line2D.Double(strokeOne.getStart().getX(),strokeOne.getStart().getY(),strokeOne.getEnd().getX(),strokeOne.getEnd().getY());
		for (int i=0;i<brushStrokeCandidates.size();i++) {
			BitmapStroke existingStroke = (BitmapStroke)brushStrokeCandidates.elementAt(i);
			Line2D.Double s2 = new Line2D.Double(existingStroke.getStart().getX(),existingStroke.getStart().getY(),existingStroke.getEnd().getX(),existingStroke.getEnd().getY());
			if(strokesIntersect(s1,s2)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean strokesIntersect(Line2D.Double s1, Line2D.Double s2) {
		boolean intersects = s1.intersectsLine(s2);
		return intersects;
	}
	
	
	public void saveSimulation(String simulationFileName) {
		int width  = virginPaintmap.getPaintmap().size()*5;
		int height = virginPaintmap.getPaintmap().get(0).size()*5;
		
		BufferedImage simulation = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

		Graphics2D g = simulation.createGraphics();
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(0, 0 , width, height);
		g.dispose();

		for(int i=0;i<brushStrokes.size();i++) {
			Color thisColor = ((BitmapStroke)brushStrokes.get(i)).getStrokeColor();
			int sx = ((BitmapStroke)brushStrokes.get(i)).getStart().getX()*5; 
			int sy = ((BitmapStroke)brushStrokes.get(i)).getStart().getY()*5; 
			int ex = ((BitmapStroke)brushStrokes.get(i)).getEnd().getX()*5; 
			int ey = ((BitmapStroke)brushStrokes.get(i)).getEnd().getY()*5; 
			g = simulation.createGraphics();
			g.setColor(thisColor);
			g.setStroke(new BasicStroke(3));
			g.drawLine(sx+2, height-sy-2, ex+2, height-ey-2);
			g.dispose();
		}
 	    try {
 	    	System.out.println("SavingSimulation: "+simulationFileName);
 	    	File outputfile = new File(simulationFileName);
			ImageIO.write(simulation, "gif", outputfile);
	 	    System.out.println("Succesfully wrote: "+simulationFileName);
		}
		catch (IOException e) {
	 	    System.out.println("FAILED TO write: "+simulationFileName);
			e.printStackTrace();
		}
	}
	
	public void saveToElasticSearch(String imageName) {
		int width  = virginPaintmap.getPaintmap().size();
		int height = virginPaintmap.getPaintmap().get(0).size();
		//for(int i=0;i<2;i++) {
		for(int i=0;i<brushStrokes.size();i++) {
			//double sx = ((BitmapStroke)brushStrokes.get(i)).getStart().getX()+1/width; 
			//double sy = ((BitmapStroke)brushStrokes.get(i)).getStart().getY()+1/height; 
			//double ex = ((BitmapStroke)brushStrokes.get(i)).getEnd().getX()+1/width; 
			//double ey = ((BitmapStroke)brushStrokes.get(i)).getEnd().getY()+1/height; 
			//RelativePoint startRelativePoint = new RelativePoint(sx,sy);
			//RelativePoint endRelativePoint = new RelativePoint(ex,ey);
			//RelativeStroke rs = new RelativeStroke(startRelativePoint, endRelativePoint,((BitmapStroke)brushStrokes.get(i)).getStrokeColor() );
			BitmapStroke bms = ((BitmapStroke)brushStrokes.get(i));
			ElasticsearchUtility.SaveAutomatedStrokesToElasticSearchCluster(imageName, width, height, i, bms, false);
		}
	}


	
	public void saveToJsonFile(String fileName, String imageName) {
		int width  = virginPaintmap.getPaintmap().size();
		int height = virginPaintmap.getPaintmap().get(0).size();
		//for(int i=0;i<2;i++) {
			ElasticsearchUtility.SaveAutomatedStrokesToJsonTextFile(fileName, imageName, width, height, brushStrokes, false);
	}
	
	
	
}
