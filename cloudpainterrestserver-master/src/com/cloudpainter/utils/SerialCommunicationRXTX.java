package com.cloudpainter.utils;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;



public class SerialCommunicationRXTX 
{

	static boolean simulate = false;
	
	
	
	private static SerialPort serialPort;
	private OutputStream outputStream;
	private InputStream inputStream;

	public SerialCommunicationRXTX() {
		
	}
	
	public static void setSimulateMode(boolean simulateB) {
		simulate = simulateB;
	}
	
	public static void main(String[] args) {
		
		SerialCommunicationRXTX sc = new SerialCommunicationRXTX();
		sc.openSerialConnection();

		//sc.sendGCodeToController("M107                ; Turn Fan Off");
		sc.sendGCodeToController("G21                 ; set units to millimeters");
		sc.sendGCodeToController("G90                 ; use absolute coordinates");
		sc.sendGCodeToController("M82                 ; use absolute distances for extrusion");
		sc.sendGCodeToController("G92 E0.0            ; ???");
		
		//sc.sendGCodeToController("G29");
		sc.sendGCodeToController("G28 X");
		sc.sendGCodeToController("G28 Y");
		sc.sendGCodeToController("G28 Z");

		sc.sendGCodeToController("G1 F8000           ; set X Y speed");
		
		sc.sendGCodeToController("G1 X200.0");
		sc.sendGCodeToController("G1 Y200.0");
		sc.sendGCodeToController("G1 Z4.0");

		
		sc.closeSerialConnection();
		
		
	}
	
	public void closeSerialConnection() {
		serialPort.close();
	}
	
	public void openSerialConnection() {
			Enumeration<?> portList;
			CommPortIdentifier portId;
			portList = CommPortIdentifier.getPortIdentifiers();
			while (portList.hasMoreElements()) {
				portId = (CommPortIdentifier) portList.nextElement();
				if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
					if (portId.getName().equals("COM5")) {
						try {
						
							serialPort = (SerialPort) portId.open(
									"SimpleWriteApp",
									2000);
						}
						catch (final PortInUseException e) {
							
						}
						try {
							
							outputStream = serialPort.getOutputStream();
							inputStream = serialPort.getInputStream();
						}
						catch (final IOException e) {
							
						}
						try {
							
							serialPort.setSerialPortParams(
									250000,
									SerialPort.DATABITS_8,
									SerialPort.STOPBITS_1,
									SerialPort.PARITY_NONE);
						}
						catch (final UnsupportedCommOperationException e) {
							
						}

					}
				}
			}
		}

	//10 111 107 10
	int[] okString = {0,0} ;
	public boolean isOkString(int lastChar, int nextChar) {
		if (lastChar == 111 && 
			nextChar == 107) {
			return true;
		}
		return false;
	}
	
	int emptyResponses = 0;
	public void sendGCodeToController(
			final String rawcommand ) {
		String command = rawcommand+"\r\n";
		System.out.println(rawcommand);
		int lastX = 0;
		if (!simulate) {
		
			try {
				outputStream.write(command.getBytes());
				int executionTime = 0;
				int lastCommand = 0;
				StringBuffer responseStringBuffer = new StringBuffer("\n");
				boolean responseRecieved = false;
				while (!responseRecieved) {
					
					int x = 0;
					try {
						int inputStreamAvailable = inputStream.available();
						if(inputStreamAvailable>0) {
							x = inputStream.read();
						}
						if (x!=0 ){
							responseStringBuffer.append(Character.toString ((char) x));
						} else {
							emptyResponses++;	
						}
						if (isOkString(lastX, x)) {
							System.out.println("emptyResponses:"+emptyResponses);
							System.out.println("ok");
							responseRecieved = true;
							try {
								Thread.sleep(10);
							}
							catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							System.out.println(":"+responseStringBuffer.toString());//
						} else if (x!= 0){
							lastX=x;
							System.out.print(x+"|");
						}
						
						//System.out.println(responseStringBuffer.toString());
						//Thread.sleep(sleepBetweenChars);
						//executionTime += 1;
					}
					catch (final IOException e) {
						e.printStackTrace();
					}
				}
				
				
	
			} catch (final IOException e) {
			}
		} else {
			System.out.println("Simulated Command");
		}
	}

}


	
