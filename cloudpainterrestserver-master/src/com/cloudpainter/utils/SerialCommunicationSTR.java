package com.cloudpainter.utils;

import gnu.io.CommPortIdentifier;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Vector;

import javax.imageio.ImageIO;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cloudpainter.robots.GumGumArm;
import com.cloudpainter.robots.RobotTemplate;


//To USE
//1: Stand Robot Up
//2: Turn on Robot in COLD MODE
//3: Calibrate by Running This MAIN

//Well: 1.0   3870   1640
//Well: 2.0   3868   2115
//Well: 3.0   3867   2590
//Well: 4.0   3866   3065
//Well: 5.0   3865   3540
//Well: 6.0   3864   4015
//Well: 7.0   3862   4490
//Well: 8.0   3861   4965

//Slump
//2.7
//5.5
//4.2
//2.5
//0.0


public class SerialCommunicationSTR 
{

	
	
	public static RobotTemplate robotObject = new GumGumArm();
	
	static boolean simulate = false;
	
	static String COMnum = "COM1";
	static int Baud = 19200;
	
	private static SerialPort serialPort;
	private OutputStream outputStream;
	private InputStream inputStream;

	public SerialCommunicationSTR() {
		
	}
	
	public static void setSimulateMode(boolean simulateB) {
		simulate = simulateB;
	}
	
	public static void main(String[] args) {
		
		try {
			JSONObject lpp = ElasticsearchUtility.getJsonObjectFromFile("C:\\cloudpainterrestserver\\currentpainting\\config\\live_painting_parameters.json");
			((GumGumArm)robotObject).updateBrushHeight(
					lpp.getInt("brushHeightMm"), 
					lpp.getInt("canvasHeightMm"),
					lpp.getInt("canvasSlumpMm"),
					lpp.getInt("waterHeightMm"),
					lpp.getInt("cleaningHeightMm"),
					lpp.getInt("wellHeightMm"));
		} catch (Exception e) {
		}
		
		
		SerialCommunicationSTR sc = new SerialCommunicationSTR();
		sc.openSerialConnection(COMnum, Baud);
		sc.sendFORTHToController("ROBOFORTH");
		sc.sendFORTHToController("START");
		sc.sendFORTHToController("CALIBRATE");
		sc.sendFORTHToController("HOME");
		sc.sendFORTHToController("READY");
		sc.sendFORTHToController("40000 SPEED !");
		sc.sendFORTHToController("1000 ACCEL !");
		//sc.sendFORTHToController("SETTINGS");
		sc.sendFORTHToController("CARTESIAN");
		
		
		startPosition(sc);
		//--------------------------------------------
		cameraPosition(sc);
		takeSnapshot("C:\\cloudpainterrestserver\\currentpainting\\");
		jogCorners(sc);
		calibrateCenter(sc);
		jogInsideCorners(sc);
		//calibratePaintWells(sc);
		startPosition(sc);
		//--------------------------------------------
		sc.closeSerialConnection();
		
	}

	public static void startPosition(SerialCommunicationSTR sc) {
		sc.sendFORTHToController("0 4250 0 MOVETO");
	}

	static double absX = 0;
	static double absY = 0;
	static double absZ = 0;
	
	public static void setAbsoluteXYZ(double x, double y, double z) {
		absX = x;
		absY = y;
		absZ = z;
	}
	
	public static String getAbsoluteMoveToString() {
		return ""+(int)absX+" "+(int)absY+" "+(int)absZ+" MOVETO";
	}
	
	
	public static void cameraPosition(SerialCommunicationSTR sc) {
		setAbsoluteXYZ(
				robotObject.xOrigin+robotObject.cameraXPosition, 
				robotObject.yOrigin+robotObject.cameraYPosition, 
				robotObject.cameraZPosition);//-350);
		sc.sendFORTHToController(getAbsoluteMoveToString()); 
		System.out.println("camera in position");
	}
	
	public static void takeSnapshot(String rootDir) {

		String sourceDir = rootDir;

		try {
			Thread.sleep(3000);//0);
		}
		catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		File stream = new File(sourceDir+"progress_stream.jpg");

		BufferedImage img = null;
		try {
		    img = ImageIO.read(stream);
		} catch (IOException e) {
		}
		try {
		   File snapshot = new File(sourceDir+"progress_photo.jpg");
		   ImageIO.write(img, "jpg", snapshot);
		} catch (IOException e) {
		}
		
		JSONObject snapshotCorners;
		Vector palette = new Vector();
		
		
		try {
			snapshotCorners = ElasticsearchUtility.getJsonObjectFromFile(sourceDir+"config\\camera_calibration.json");
			int colorTolerance = snapshotCorners.getInt("colorTolerance");

			
			JSONArray paintWells = snapshotCorners.getJSONArray("paintWells");
			
			//String sourceFileNameRoot = sourceFilename.substring(0,sourceFilename.indexOf("."));
			//String sourceFileName = sourceDir+sourceFilename;
			String paletteFileName = sourceDir+"_calibration"+"_livepalette.gif";
			String liveProgressPhotoName = sourceDir+"progress_photo.jpg";

			
			//read paintmap coverage file and fill in those values of paintmap
			File imageFile = new File(liveProgressPhotoName);
			System.out.println("loading source file: "+imageFile.getAbsolutePath());


			BufferedImage sourceImage = ImageIO.read(imageFile);
			System.out.println("sourceImage Dimensions: "+sourceImage.getWidth()+"x"+sourceImage.getHeight());
			

			for (int i=0; i<paintWells.length();i++) {
				JSONArray coord = paintWells.getJSONArray(i);
				
				int pixelsAveraged = 0;
				int averageRed = 0;
				int averageGreen = 0;
				int averageBlue = 0;
				
				for (int j=-2;j<=2;j++) {
					for (int k=-2;k<=2;k++) {
						pixelsAveraged++;
						int xcoord = coord.getInt(0)+j;
						int ycoord = coord.getInt(1)+k;
						Color rgbColor = new Color(sourceImage.getRGB(xcoord,ycoord));
						averageRed = averageRed+ rgbColor.getRed();
						averageGreen = averageGreen+ rgbColor.getGreen();
						averageBlue = averageBlue+ rgbColor.getBlue();
					}
				}
				averageRed = averageRed/pixelsAveraged;
				averageGreen = averageGreen/pixelsAveraged;
				averageBlue = averageBlue/pixelsAveraged;
				Color averageRGB = new Color(averageRed, averageGreen, averageBlue);
				System.out.println("adding "+averageRGB+" to available colors");
				palette.add(averageRGB);

				
			}
			
			//normalize values of palette
			palette = LiveRoboForthExecuter.normalizePalette(palette);
			//reduce number of colors beyond black
			palette = LiveRoboForthExecuter.reducePalette(palette);
			
				BufferedImage paletteFile = new BufferedImage((palette.size()*10), 10, BufferedImage.TYPE_INT_ARGB);
		        for (int i=0; i<palette.size();i++) {
		        	   Graphics2D g = paletteFile.createGraphics();
		        	   g.setColor((Color)palette.get(i));
			           g.fillRect((i*10), 0, (i*10)+10, 10);
			           g.dispose();
		        }
		        try {
		        System.out.println("Attempting to make palette file: "+paletteFileName);
		        	   File outputfile = new File(paletteFileName);
		        	   ImageIO.write(paletteFile, "gif", outputfile);
		        	   System.out.println("Succesfully wrote: "+paletteFileName);
				} catch (IOException f) {
						System.out.println("Failed to write: "+paletteFileName);
						f.printStackTrace();
				}
		        
		        String palleteFileNameJson = paletteFileName.substring(0,paletteFileName.lastIndexOf("\\")+1);
		        palleteFileNameJson = palleteFileNameJson+"config\\paintavailable.json";
		        
		        ColorUtils.saveColorJsonFile(palleteFileNameJson, palette);
		        
		} catch (IOException e) {
		}
		
	}
	
	public static void calibrateCenter(SerialCommunicationSTR sc) {
		
		int height =  Math.abs((int) robotObject.xOrigin*2); //mm
		int width =  Math.abs((int) robotObject.xOrigin*2); //mm
		int yOrigin = (int) robotObject.yOrigin;
		int canvasHeight = (int) robotObject.zLightTouch;
		
		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" 0 MOVETO");

		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");

		sc.sendFORTHToController("1000 "+(1000+yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController("1000 "+(1000+yOrigin+(height/2))+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController("1000 "+(1000+yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");

		sc.sendFORTHToController("1000 "+(yOrigin+(height/2)-1000)+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController("1000 "+(yOrigin+(height/2)-1000)+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController("1000 "+(yOrigin+(height/2)-1000)+" "+(canvasHeight+500)+" MOVETO");

		sc.sendFORTHToController("-1000 "+(yOrigin+(height/2)-1000)+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController("-1000 "+(yOrigin+(height/2)-1000)+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController("-1000 "+(yOrigin+(height/2)-1000)+" "+(canvasHeight+500)+" MOVETO");

		sc.sendFORTHToController("-1000 "+(1000+yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController("-1000 "+(1000+yOrigin+(height/2))+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController("-1000 "+(1000+yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");
		
		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");
		
		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" 0 MOVETO");
	}
	
	
	public static void jogCorners(SerialCommunicationSTR sc) {
		
		int mmInset = 254;
		int height =  Math.abs((int) robotObject.xOrigin*2); //mm
		int width =  Math.abs((int) robotObject.xOrigin*2); //mm
		int yOrigin = (int) robotObject.yOrigin;
		int canvasHeight = (int) robotObject.zLightTouch;
		
		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" 0 MOVETO");

//Top left
				sc.sendFORTHToController(""+(width/2*(-1))+" "+(yOrigin+(height))+" "+(canvasHeight+500)+" MOVETO");
				sc.sendFORTHToController(""+(width/2*(-1))+" "+(yOrigin+(height))+" "+canvasHeight+" MOVETO");
				sc.sendFORTHToController(""+(width/2*(-1))+" "+(yOrigin+(height))+" "+(canvasHeight+500)+" MOVETO");
//topRight		
				sc.sendFORTHToController(""+(width/2)+" "+(yOrigin+(height))+" "+(canvasHeight+500)+" MOVETO");
				sc.sendFORTHToController(""+(width/2)+" "+(yOrigin+(height))+" "+canvasHeight+" MOVETO");
				sc.sendFORTHToController(""+(width/2)+" "+(yOrigin+(height))+" "+(canvasHeight+500)+" MOVETO");
//bottom Right		
				sc.sendFORTHToController(""+(width/2)+" "+yOrigin+" "+(canvasHeight+500)+" MOVETO");
				sc.sendFORTHToController(""+(width/2)+" "+yOrigin+" "+canvasHeight+" MOVETO");
				sc.sendFORTHToController(""+(width/2)+" "+yOrigin+" "+(canvasHeight+500)+" MOVETO");
//Bottom Left		
				sc.sendFORTHToController(""+(width/2*(-1))+" "+yOrigin+" "+(canvasHeight+500)+" MOVETO");
				sc.sendFORTHToController(""+(width/2*(-1))+" "+yOrigin+" "+canvasHeight+" MOVETO");
				sc.sendFORTHToController(""+(width/2*(-1))+" "+yOrigin+" "+(canvasHeight+500)+" MOVETO");

		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" 0 MOVETO");
	}

	public static void jogInsideCorners(SerialCommunicationSTR sc) {
		
		int mmInset = 254;
		int height =  Math.abs((int) robotObject.xOrigin*2); //mm
		int width =  Math.abs((int) robotObject.xOrigin*2); //mm
		int yOrigin = (int) robotObject.yOrigin;
		int canvasHeight = (int) robotObject.zLightTouch;
		
		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" 0 MOVETO");

//middle
		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");
//top center		
		sc.sendFORTHToController("0 "+(yOrigin+(height)-mmInset)+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController("0 "+(yOrigin+(height)-mmInset)+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController("0 "+(yOrigin+(height)-mmInset)+" "+(canvasHeight+500)+" MOVETO");
//topRight		
		sc.sendFORTHToController(""+((width/2)-mmInset)+" "+(yOrigin+(height)-mmInset)+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController(""+((width/2)-mmInset)+" "+(yOrigin+(height)-mmInset)+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController(""+((width/2)-mmInset)+" "+(yOrigin+(height)-mmInset)+" "+(canvasHeight+500)+" MOVETO");
//right center		
		sc.sendFORTHToController(""+((width/2)-mmInset)+" "+(yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController(""+((width/2)-mmInset)+" "+(yOrigin+(height/2))+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController(""+((width/2)-mmInset)+" "+(yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");
//bottom Right		
		sc.sendFORTHToController(""+((width/2)-mmInset)+" "+(yOrigin+mmInset)+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController(""+((width/2)-mmInset)+" "+(yOrigin+mmInset)+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController(""+((width/2)-mmInset)+" "+(yOrigin+mmInset)+" "+(canvasHeight+500)+" MOVETO");
	
//bottom center				
		sc.sendFORTHToController("0 "+(yOrigin+mmInset)+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController("0 "+(yOrigin+mmInset)+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController("0 "+(yOrigin+mmInset)+" "+(canvasHeight+500)+" MOVETO");
//Bottom Left		
		sc.sendFORTHToController(""+(width/2*(-1)+mmInset)+" "+(yOrigin+mmInset)+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController(""+(width/2*(-1)+mmInset)+" "+(yOrigin+mmInset)+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController(""+(width/2*(-1)+mmInset)+" "+(yOrigin+mmInset)+" "+(canvasHeight+500)+" MOVETO");
//left center		
		sc.sendFORTHToController(""+(width/2*(-1)+mmInset)+" "+(yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController(""+(width/2*(-1)+mmInset)+" "+(yOrigin+(height/2))+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController(""+(width/2*(-1)+mmInset)+" "+(yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");
//Top left
		sc.sendFORTHToController(""+(width/2*(-1)+mmInset)+" "+(yOrigin+(height)-mmInset)+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController(""+(width/2*(-1)+mmInset)+" "+(yOrigin+(height)-mmInset)+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController(""+(width/2*(-1)+mmInset)+" "+(yOrigin+(height)-mmInset)+" "+(canvasHeight+500)+" MOVETO");
//top center		
		sc.sendFORTHToController("0 "+(yOrigin+(height)-mmInset)+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController("0 "+(yOrigin+(height)-mmInset)+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController("0 "+(yOrigin+(height)-mmInset)+" "+(canvasHeight+500)+" MOVETO");
//middle
		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");
		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" "+canvasHeight+" MOVETO");
		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" "+(canvasHeight+500)+" MOVETO");

		sc.sendFORTHToController("0 "+(yOrigin+(height/2))+" 0 MOVETO");
	}

	
	public static void calibratePaintWells(SerialCommunicationSTR sc) {
		
		int startX = (int) robotObject.firstPaintWellX;
		int startY = (int) robotObject.firstPaintWellY;
		double step = 0;
		int bottomOfPaintWell = (int) robotObject.paintWellZ;
		

		double distToNextWell = robotObject.paintWellHeight;
		double xShift = distToNextWell/400;
		
		int nextX = (int) (startX-((int)step*xShift));
		int nextY = (int) (startY+((int)step*distToNextWell));
		
		System.out.println("Well: " +(step+1)+" "+nextX+" "+nextY);
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+bottomOfPaintWell+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");
		
		step= step + 1;
		nextX = (int) (startX-((int)step*xShift));
		nextY = (int) (startY+((int)step*distToNextWell));

		System.out.println("Well: " +(step+1)+" "+nextX+" "+nextY);
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+bottomOfPaintWell+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");
		
		step= step + 1;
		nextX = (int) (startX-((int)step*xShift));
		nextY = (int) (startY+((int)step*distToNextWell));

		System.out.println("Well: " +(step+1)+" "+nextX+" "+nextY);
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+bottomOfPaintWell+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");

		
		step= step + 1;
		nextX = (int) (startX-((int)step*xShift));
		nextY = (int) (startY+((int)step*distToNextWell));

		System.out.println("Well: " +(step+1)+" "+nextX+" "+nextY);
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+bottomOfPaintWell+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");

		
		step= step + 1;
		nextX = (int) (startX-((int)step*xShift));
		nextY = (int) (startY+((int)step*distToNextWell));

		System.out.println("Well: " +(step+1)+" "+nextX+" "+nextY);
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+bottomOfPaintWell+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");

		
		step= step + 1;
		nextX = (int) (startX-((int)step*xShift));
		nextY = (int) (startY+((int)step*distToNextWell));

		System.out.println("Well: " +(step+1)+" "+nextX+" "+nextY);
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+bottomOfPaintWell+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");

		
		step= step + 1;
		nextX = (int) (startX-((int)step*xShift));
		nextY = (int) (startY+((int)step*distToNextWell));

		System.out.println("Well: " +(step+1)+" "+nextX+" "+nextY);
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+bottomOfPaintWell+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");

		
		step= step + 1;
		nextX = (int) (startX-((int)step*xShift));
		nextY = (int) (startY+((int)step*distToNextWell));

		System.out.println("Well: " +(step+1)+" "+nextX+" "+nextY);
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+bottomOfPaintWell+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");


		step= step + 1;
		nextX = (int) (startX-((int)step*xShift));
		nextY = (int) (startY+((int)step*distToNextWell));

		System.out.println("Well: " +(step+1)+" "+nextX+" "+nextY);
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+bottomOfPaintWell+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");

		
		step= step + 1;
		nextX = (int) (startX-((int)step*xShift));
		nextY = (int) (startY+((int)step*distToNextWell));

		System.out.println("Well: " +(step+1)+" "+nextX+" "+nextY);
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+bottomOfPaintWell+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+500)+" MOVETO");
		sc.sendFORTHToController(""+nextX+" "+(nextY)+" "+(bottomOfPaintWell+1000)+" MOVETO");

		

}

	
	public void closeSerialConnection() {
		serialPort.close();
	}
	
	public void openSerialConnection() {
		//try default values
		openSerialConnection(COMnum, Baud);
	}
	
	public void openSerialConnection(String COMnum, int Baud) {
			Enumeration<?> portList;
			CommPortIdentifier portId;
			portList = CommPortIdentifier.getPortIdentifiers();
			while (portList.hasMoreElements()) {
				portId = (CommPortIdentifier) portList.nextElement();
				if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
					if (portId.getName().equals(COMnum)) {
						try {
						
							serialPort = (SerialPort) portId.open(
									"SimpleWriteApp",
									2000);
						}
						catch (final PortInUseException e) {
							
						}
						try {
							
							outputStream = serialPort.getOutputStream();
							inputStream = serialPort.getInputStream();
						}
						catch (final IOException e) {
							
						}
						try {
							
							serialPort.setSerialPortParams(
									Baud,
									SerialPort.DATABITS_8,
									SerialPort.STOPBITS_1,
									SerialPort.PARITY_NONE);
						}
						catch (final UnsupportedCommOperationException e) {
							
						}

					}
				}
			}
		}

	//10 111 107 10
	int[] okString = {0,0} ;
	public boolean isOkString(int lastChar, int nextChar) {
		if (lastChar == 79 && 
			nextChar == 75) {
			return true;
		}
		return false;
	}
	
	int emptyResponses = 0;
	public void sendFORTHToController(
			final String rawcommand ) {
		String command = rawcommand+"\r\n";
		System.out.println(rawcommand);
		int lastX = 0;
		if (!simulate) {
		
		  try {
			outputStream.write(command.getBytes());
			int executionTime = 0;
			int lastCommand = 0;
			StringBuffer responseStringBuffer = new StringBuffer("\n");
			boolean responseRecieved = false;
			while (!responseRecieved) {
				
				int x = 0;
				try {
					int inputStreamAvailable = inputStream.available();
					if(inputStreamAvailable>0) {
						x = inputStream.read();
					}
					if (x!=0 ){
						responseStringBuffer.append(Character.toString ((char) x));
					} else {
						emptyResponses++;	
					}
					if (isOkString(lastX, x)) {
						System.out.println("\nemptyResponses:"+emptyResponses);
						System.out.println("OK FOUND");
						responseRecieved = true;
						try {
							Thread.sleep(10);
						}
						catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println(":"+responseStringBuffer.toString());//
					} else if (x!= 0){
						lastX=x;
						//System.out.print(x+"|");
						System.out.print(Character.toString ((char) x));
						
					}
					
					//System.out.println(responseStringBuffer.toString());
					//Thread.sleep(sleepBetweenChars);
					//executionTime += 1;
				}
				catch (final IOException e) {
					e.printStackTrace();
				}
			}

		  } catch (final IOException e) {
		  }
		}
	}

}





