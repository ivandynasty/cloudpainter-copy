package com.cloudpainter.utils;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.Vector;

import javax.imageio.ImageIO;

import com.cloudpainter.aistrokes.AIStrokes;
import com.cloudpainter.autopaint.BitmapStroke;
import com.cloudpainter.robots.LargeSixtyDegreePlotter;
import com.cloudpainter.robots.PandoraVOne;
import com.cloudpainter.robots.PandoraVTwo;
import com.cloudpainter.robots.LargeFlatPlotter;
import com.cloudpainter.robots.MidSizedFortyFiveDegreePlotter;
import com.cloudpainter.robots.RobotTemplate;
import com.cloudpainter.strokes.FB_AH_FlatBrush_AlwaysHorizontal;
import com.cloudpainter.strokes.FB_AV_FlatBrush_AlwaysVertical;
import com.cloudpainter.strokes.FB_D_FlatBrush_WideFullStroke_SpinOnDot_NoRise;
import com.cloudpainter.strokes.FB_M_FlatBrush_MediumFullStroke_NoRise;
import com.cloudpainter.strokes.FB_TIWSR_FlatBrush_Thin_ImmediatelyRotateToWide_WideToEnd_SlowRise;
import com.cloudpainter.strokes.FB_TIW_FlatBrush_Thin_ImmediatelyRotateToWide_WideToEnd;
import com.cloudpainter.strokes.FB_T_FlatBrush_ThinFullStroke_NoRise;
import com.cloudpainter.strokes.FB_WMT_FlatBrush_WideFirstQuarter_MediumSecondQuarter_ThinSecondHalf;
import com.cloudpainter.strokes.FB_WTSR_FlatBrush_Wide_GraduallyRotateToThin_SlowRise;
import com.cloudpainter.strokes.FB_W_FlatBrush_WideFullStroke_NoRise;
import com.cloudpainter.strokes.PK_KING_PaletteKnifeKingStroke_RotatingArc;
import com.cloudpainter.strokes.PK_ROOK_PaletteKnifeRookStroke_FlatDrag;
import com.cloudpainter.strokes.RB_SS_RoundBrush_SimpleStroke;
import com.cloudpainter.strokes.StrokeTemplate;

import org.json.*;




public class LiveGCodeExecuter {
	
	SerialCommunicationRXTX sconn = new SerialCommunicationRXTX();

	public static boolean simulateWithoutMovingRobot = false;
	
	String indexName = "";
	String rootDir = "";
	
	int startIndex = 0;
	int nextStrokeNumber = startIndex;

	boolean makeAIStrokes = false;
	int attemptsBeforeChangingToAI = 1000;
	
	boolean waitForPaintDrip = false;
	
	public AIStrokes aiStrokes = new AIStrokes();

	public int numberOfAIStrokesToMake = 30;
	
	public RobotTemplate robo = new PandoraVTwo();//new LargeFlatPlotter();//new LargeSixtyDegreePlotter(); //MidSizedFortyFiveDegreePlotter();//new PandoraVOne(); //AcroPrinterFortyBySixty   //MidSizedFortyFiveDegreePlotter(); //new LargeFlatPlotter(); //

	public int maxStrokeNumber = 1000000;//62;//2805;//7552;

	public int maxStrokesPerPaintDip = 10; //3; //2;
	public double homeAfterHowManyStrokes = 100;  //6

	public double distanceTravelledByStroke = 0.0;  //TODO right now it is points on line
	public double refillPaintAfterThisDistance = 200.0;
	
	
	public StringBuffer gcodeStringBuffer = new StringBuffer();
	public Vector strokeJSONObjectVector = new Vector();

	public Vector colorPalette = new Vector();
	public Vector brushVector = new Vector();
		
	public static boolean readFromFile = false;
	public static boolean saveToFile = false;
	
	public double paintMeterAmount = 1.0;
	
	public LiveGCodeExecuter(String rootDir, String indexName, boolean activateAI, Vector palette) {
		
		this.indexName = indexName;
		this.rootDir = rootDir;
		this.makeAIStrokes = activateAI;
		this.colorPalette = palette;
		robo.paintWells = colorPalette;   

		File makeDir = new File(rootDir+indexName);
		makeDir.mkdir();
		
		//connect to robot
		this.startIndex = ElasticsearchUtility.getMaxStrokeNumber(indexName)+1;
		this.nextStrokeNumber = startIndex;
		
		//Pull in any opverrides
		getPaintingParameters();
		
		System.out.println("Starting on strokeNumber : "+nextStrokeNumber);
	}


	public void startPainting(){
		sconn.openSerialConnection();
		try {
			Thread.sleep(2000);
		}
		catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		initializeRobot(robo.stepperUnitsXYPerMm, robo.stepperUnitsXYPerMm, robo.stepperUnitsZPerMm);
		homeRobot();
		goToDefaultStart();
		
		int strokesSinceLastHome = 0;
		int strokesSinceLastDip = 100000;

		String currentColor = "#ffffff";
		String lastColor = "#ffffff";
		
		String currentBrush = "NONE";
		String lastBrush = "NONE";

		
		int numberOfNullStrokes = 0;
		
		while (nextStrokeNumber <= maxStrokeNumber) {
//RESET WHEN OUT OF STROKES   			
//RESET WHEN OUT OF STROKES   			
			if (numberOfNullStrokes>10) {
				parkBrush(currentBrush);
				currentBrush = "NONE";
				lastBrush = "NONE";
				snapshotProgress(indexName);
				currentColor = "#ffffff";
				lastColor = "#ffffff";
				strokesSinceLastDip = 1000000;
				strokesSinceLastHome = 0;
				//go into listening mode when strokes stop.
				int attemptsToDetectNextStroke = 0;
				while (numberOfNullStrokes>10) {
					getPaintingParameters();
					int highestElasticSearchStrokeNumber = ElasticsearchUtility.getMaxStrokeNumber(indexName);
					if (nextStrokeNumber < highestElasticSearchStrokeNumber) {
						nextStrokeNumber++;
					}
					try {
						Thread.sleep(4000);
						attemptsToDetectNextStroke++; 
						System.out.print(".|");
						System.out.println("Looking for: "+(nextStrokeNumber));
						JSONObject stroke = getNextStrokeJSON(indexName, nextStrokeNumber);
						if (stroke!=null) {
							numberOfNullStrokes=0;
						}
//IF NO STROKES EXIST IT CHECKS TO SEE IF IT SHOULD MAKE AI STROKES						
//IF NO STROKES EXIST IT CHECKS TO SEE IF IT SHOULD MAKE AI STROKES						
						if (numberOfNullStrokes>0 && attemptsToDetectNextStroke>=attemptsBeforeChangingToAI) {
								System.out.print(".|");
								System.out.print("Getting AI Strokes");
//IF IT SHOULD, IT MAKES AI STROKES
//IF IT SHOULD, IT MAKES AI STROKES
								Color lastColorUsed = ColorUtils.getColorFromString(currentColor);
								int strokesCreated = aiStrokes.generateAiStrokes(this.makeAIStrokes, rootDir, indexName, numberOfAIStrokesToMake, lastColorUsed);
								if (strokesCreated>0) {
									numberOfNullStrokes = 0;
								}
							} else {
								System.out.println("nextStrokeNumber: "+nextStrokeNumber);
								System.out.println("NOT getting AI Strokes");
							}
					}
					catch (InterruptedException ie) {
						// TODO Auto-generated catch block
						ie.printStackTrace();
					} 
					catch (Exception e) {
						System.out.println("problem while parked getting json");
					}	

					
				}
			}
//LOOK FOR THE NEXT STROKE			
//LOOK FOR THE NEXT STROKE			
			JSONObject stroke = getNextStrokeJSON(indexName, nextStrokeNumber);
//IF FOUND PAINT IT
//IF NOT FOUND LOOK AGAIN			
				if (stroke!=null) { 
					currentColor = this.getColorFromJSON(stroke);
					currentBrush = this.getBrushFromJSON(stroke);
//IF THE BRUSH HAS CHANGED - CHANGE BRUSH
//IF THE BRUSH HAS CHANGED - CHANGE BRUSH
				if (!currentBrush.equalsIgnoreCase(lastBrush)) {
					if (!lastBrush.equalsIgnoreCase("NONE")) {
						returnBrush(lastBrush);
					} else {
					}
					takeBrush(currentBrush);
					lastBrush = currentBrush;
				}
//IF THE COLOR HAS CHANGED - CLEAN BRUSH
//IF THE COLOR HAS CHANGED - CLEAN BRUSH
				if (!currentColor.equalsIgnoreCase(lastColor)) {
					cleanBrush();
					snapshotProgress(indexName);
					strokesSinceLastDip = 1000000;
					distanceTravelledByStroke = 10000.0;
					lastColor = currentColor;
				}
//IF ROBOT HASN"T HOMED IN A WHILE, HOME
//IF ROBOT HASN"T HOMED IN A WHILE, HOME
				if (strokesSinceLastHome>=homeAfterHowManyStrokes) {
						homeBrush();
						strokesSinceLastHome = 0;
				}
//GET MORE PAINT IF NEEDED
//GET MORE PAINT IF NEEDED
				if ((strokesSinceLastDip>=maxStrokesPerPaintDip) || (distanceTravelledByStroke > refillPaintAfterThisDistance)) {
					dipBrushInPaint(currentColor, (int)distanceTravelledByStroke);
					snapshotProgress(indexName);
					strokesSinceLastDip = 0;
					distanceTravelledByStroke = 0.0;
				}
//MAKE STROKE
//MAKE STROKE
				paintStroke(stroke, nextStrokeNumber);
				nextStrokeNumber++;
				distanceTravelledByStroke = distanceTravelledByStroke+getStrokeLength(stroke);
				strokesSinceLastDip = strokesSinceLastDip + 1;
				strokesSinceLastHome = strokesSinceLastHome + 1;
			} else {
				numberOfNullStrokes ++;
			}
		}
	}

	public void getPaintingParameters() {
		try {
			JSONArray brushArray = ElasticsearchUtility.getJsonArrayFromFile(rootDir+"config\\brushselection.json");
			for (int j=0;j<brushArray.length();j++) {
				JSONObject brush = brushArray.getJSONObject(j);
				String brushName = brush.getString("name");
				brushVector.add(brushName);
			}
			
			JSONObject lpp = ElasticsearchUtility.getJsonObjectFromFile(rootDir+"config\\live_painting_parameters.json");

 		    simulateWithoutMovingRobot = lpp.getBoolean("simulateWithoutMovingRobot");
 		    sconn.setSimulateMode(simulateWithoutMovingRobot);
 		    
			boolean overrideStrokeNumber = lpp.getBoolean("overrideNextStrokeNumberOverride");
			if (overrideStrokeNumber) {
				nextStrokeNumber = lpp.getInt("nextStrokeNumberOverride");
			}
			  this.makeAIStrokes = lpp.getBoolean("makeAIStrokes");  
			  this.attemptsBeforeChangingToAI = lpp.getInt("attemptsBeforeChangingToAI");  
			  this.numberOfAIStrokesToMake = lpp.getInt("numberOfAIStrokesToMake");

			  this.maxStrokesPerPaintDip = lpp.getInt("maxStrokesPerPaintDip");
			  this.homeAfterHowManyStrokes = lpp.getInt("homeAfterHowManyStrokes");
			  this.refillPaintAfterThisDistance = lpp.getDouble("refillPaintAfterThisDistance");

			  boolean repaintSegment = lpp.getBoolean("repaintSegment");
			  if (repaintSegment) {
				  nextStrokeNumber = lpp.getInt("segmentStart");
				  maxStrokeNumber =  lpp.getInt("segmentEnd");
			  }
			  
			  readFromFile = lpp.getBoolean("readFromFile");
			  saveToFile = lpp.getBoolean("saveToFile");
			  
			  if (saveToFile || readFromFile) {
				  nextStrokeNumber = lpp.getInt("fileStrokeStart");
				  maxStrokeNumber =  lpp.getInt("fileStrokeEnd");
			  }
			  
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//int topLeftX      = snapshotCorners.getInt("topLeftX");
		//int topLeftY      = snapshotCorners.getInt("topLeftY");
		
	}
	
	public void snapshotProgress(String imageName) {
		try {
			File f = new File(rootDir+"_progress_motiondetection.jpg");

			BufferedImage img = null;
			try {
			    img = ImageIO.read(f);
			} catch (IOException e) {
			}

			JSONObject snapshotCorners = ElasticsearchUtility.getJsonObjectFromFile(rootDir+"config\\camera_calibration.json");
			int topLeftX      = snapshotCorners.getInt("topLeftX");
			int topLeftY      = snapshotCorners.getInt("topLeftY");
			int topRightX     = snapshotCorners.getInt("topRightX");
			int topRightY     = snapshotCorners.getInt("topRightY");
			int bottomLeftX   = snapshotCorners.getInt("bottomLeftX");
			int bottomLeftY   = snapshotCorners.getInt("bottomLeftY");
			int bottomRightX  = snapshotCorners.getInt("bottomRightX");
			int bottomRightY  = snapshotCorners.getInt("bottomRightY");
			int canvasWidth =       snapshotCorners.getInt("canvasWidth");
			int canvasHeight =       snapshotCorners.getInt("canvasHeight");
			int dpi =       snapshotCorners.getInt("dpi");
			
			TrapezoidImageTransformer TrapImageConverter = new TrapezoidImageTransformer(
					img, 
					topLeftX, topLeftY, 
					topRightX, topRightY,
					bottomLeftX, bottomLeftY,
					bottomRightX, bottomRightY,
					canvasWidth*dpi,
					canvasHeight*dpi);


				BufferedImage rectImage = TrapImageConverter.GetUnwarpedImage();
				
				//String newfilename = "C:\\cloudpainterlibrary\\currentpainting\\progress.jpg";
							
				//try {
				//    File outputfile = new File(newfilename);
				//   ImageIO.write(rectImage, "jpg", outputfile);
				//} catch (IOException e) {
				//    
				//}
				
				String newfilename = rootDir+imageName+"_1progress.jpg";
				String paddedNumber = ElasticsearchUtility.paintingIdInMillion(nextStrokeNumber);
				String paddedFilename = rootDir+indexName+"\\" + "xtl_" + indexName + "_"+paddedNumber+".jpg";

				try {
					
				    File outputfile = new File(newfilename);
				    ImageIO.write(rectImage, "jpg", outputfile);
				    
				    File paddedOutputfile = new File(paddedFilename);
				    ImageIO.write(rectImage, "jpg", paddedOutputfile);
				    
				} catch (IOException e) {
				    
				}
				
			}
			catch (Exception e) {}

	}
	
	public void initializeRobot(double XstepsPerMm, double YstepsPerMm, double ZstepsPerMm) {
		sconn.sendGCodeToController("M503; report settings");
		sconn.sendGCodeToController("M107; turn off fan");
		sconn.sendGCodeToController("G21; mm units");
		sconn.sendGCodeToController("G90; absolute positioning");
		sconn.sendGCodeToController("M82; set extruder to absolute");
		sconn.sendGCodeToController("G92; set position");
		sconn.sendGCodeToController("T0; Tool 0");
		//sconn.sendGCodeToController("M92 X"+XstepsPerMm+" Y"+YstepsPerMm+" Z"+ZstepsPerMm+";");
	}

	public void homeRobot() {
		//P is print T is travel
		sconn.sendGCodeToController("M204 P10 T10       ; slowing down accel to go home\n");
		sconn.sendGCodeToController("G28 Z F100          ; home z Axes\n");
		sconn.sendGCodeToController("G28 Y F100          ; home Y Axes\n");
		sconn.sendGCodeToController("G28 X F100          ; home X Axes\n");
		sconn.sendGCodeToController("G1 Z0.0             ; move Z to top\n");
	}
	
	public void softHomeX() {
		sconn.sendGCodeToController("M204 P"+robo.maxAccelSpeedPainting+"  T"+robo.maxAccelSpeedPainting+"           ; paint accel\n");
		sconn.sendGCodeToController("G1 X"+robo.mmX(10.0)+"          ; move X close to home\n");
		sconn.sendGCodeToController("M204 P"+robo.maxAccelSpeedHoming+"  T"+robo.maxAccelSpeedHoming+"           ; slowing to home accel\n");
		sconn.sendGCodeToController("G28 X F"+robo.xyhomespeed+"          ; home X Axes\n");
		sconn.sendGCodeToController("M204 P"+robo.maxAccelSpeedPainting+"  T"+robo.maxAccelSpeedPainting+"           ; paint accel\n");
	}
	
	public void softHomeY() {
		sconn.sendGCodeToController("M204 P"+robo.maxAccelSpeedPainting+"  T"+robo.maxAccelSpeedPainting+"           ; paint accel\n");
		sconn.sendGCodeToController("G1 Y"+robo.mmY(10.0)+"          ; move Y close to home\n");
		sconn.sendGCodeToController("M204 P"+robo.maxAccelSpeedHoming+"  T"+robo.maxAccelSpeedHoming+"           ; slowing to home accel\n");
		sconn.sendGCodeToController("G28 Y F"+robo.xyhomespeed+"          ; home Y Axes\n");
		sconn.sendGCodeToController("M204 P"+robo.maxAccelSpeedPainting+"  T"+robo.maxAccelSpeedPainting+"           ; paint accel\n");
	}
	
	public void goToDefaultStart() {
		sconn.sendGCodeToController("M204 P"+robo.maxAccelSpeedPainting+" T"+robo.maxAccelSpeedPainting+"           ; returning to max accel\n");
		sconn.sendGCodeToController("G1 F"+robo.xyfullspeed+"          ; set X Y speed\n");
		sconn.sendGCodeToController("G1 Z"+robo.mmZ(1.0)+"         ; move X Y to default origin\n");
		sconn.sendGCodeToController("G1 X"+robo.mmY(robo.xOrigin)+"          ; move X Y to default origin\n");
		sconn.sendGCodeToController("G1 Y"+robo.mmY(robo.yOrigin)+"          ; move X Y to default origin\n");
	}

	public void returnBrush(String lastBrush) {
		if (!lastBrush.equalsIgnoreCase("NONE")) {
			int brushIndex = getBrushIndex(lastBrush);
			//   Go to x = 5cm
			double toolXApproachMm = robo.firstToolX+100.0;
			sconn.sendGCodeToController("G1 X"+robo.mmX(toolXApproachMm)+"          ; move X  from source\n");
			double toolYInMm = robo.firstToolY+((robo.toolHolderWidth)*brushIndex);
			sconn.sendGCodeToController("G1 Y"+robo.mmY(toolYInMm)+"          ; position in front of tool\n");
			//   HomeZ
			sconn.sendGCodeToController("G28 Z F200          ; home z Axes");
			//   SEND Z DOWN
			sconn.sendGCodeToController("G1 Z"+(robo.mmZ(robo.toolZ))+     "          ; lower z to tool level");
			//   SLOW EVERYTHING
			sconn.sendGCodeToController("G1 F"+robo.xyhalfspeed+"          ; set X Y speed\n");
			//Send X to zero
			sconn.sendGCodeToController("G1 X"+robo.mmX(toolXApproachMm/3)+"          ; move X to zero\n");
			//   SLOW EVERYTHING
			sconn.sendGCodeToController("G1 F"+robo.xyhomespeed+"          ; set X Y speed\n");
			//Send X to zero
			sconn.sendGCodeToController("G1 X"+robo.mmX(robo.firstToolX)+"          ; move X to zero\n");
			//   RETURN SPEED OF EVERYTHING TO NORMAL
			sconn.sendGCodeToController("G1 F"+robo.xyfullspeed+"          ; set X Y speed\n");
			//   HomeZ
			sconn.sendGCodeToController("G28 Z F200          ; home z Axes");
			//   got to x = 5cm
			sconn.sendGCodeToController("G1 X"+robo.mmX(toolXApproachMm)+"          ; move X  from source\n");
			//   RETURN SPEED to full
			sconn.sendGCodeToController("G1 F"+robo.xyfullspeed+"          ; set X Y speed\n");
		}
	}
	
	public void takeBrush(String currentBrush) {
		int brushIndex = getBrushIndex(currentBrush);
		//go to x = 5cm
		double toolXApproachInMm = robo.firstToolX+100.0;
		sconn.sendGCodeToController("G1 X"+robo.mmX(toolXApproachInMm)+"          ; move X from source\n");
		//go to y = firstToolY + (5cm) * (nextBrush index)
		double toolYInMm = robo.firstToolY+((robo.toolHolderWidth)*brushIndex);
		sconn.sendGCodeToController("G1 Y"+robo.mmY(toolYInMm)+"          ; move Y in front of tool\n");
		//TAKE
		//   SEND Z DOWN
		sconn.sendGCodeToController("G1 Z"+robo.mmZ(5.0)+"          ; take Z up close to home");
		//   got to x = 5cm
		sconn.sendGCodeToController("G1 X"+robo.mmX(robo.firstToolX)+"          ; move X onto tool\n");
		//   SEND Z DOWN
		sconn.sendGCodeToController("G1 Z"+robo.mmZ(robo.toolZ)+     "          ; lower z to tool level");
		//   SLOW EVERYTHING
		sconn.sendGCodeToController("G1 F"+robo.xyhomespeed+"          ; set X Y speed\n");
		//   got to x = 5cm
		sconn.sendGCodeToController("G1 X"+robo.mmX(toolXApproachInMm/3)+" E0.01          ; move X 50mm from source\n");
		//   SLOW EVERYTHING
		sconn.sendGCodeToController("G1 F"+robo.xyhalfspeed+"          ; set X Y speed\n");
		//   got to x = 5cm
		sconn.sendGCodeToController("G1 X"+robo.mmX(toolXApproachInMm/2)+" E-0.01          ; move X 50mm from source\n");
		//   got to x = 5cm
		sconn.sendGCodeToController("G1 X"+robo.mmX(toolXApproachInMm)+" E0.0          ; move X 50mm from source\n");
		//   RETURN SPEED to full
		sconn.sendGCodeToController("G1 F"+robo.xyfullspeed+"          ; set X Y speed\n");
	}
	
	public int getBrushIndex(String brushName) {
		for (int i=0;i<brushVector.size();i++) {
			String brushInVector = (String)brushVector.elementAt(i);
			if (brushInVector.equalsIgnoreCase(brushName)) {
				return i;
			}
		}
		return -1;
	}
	
	public void cleanBrush() {
		
		Random rand = new Random();
		int  n = rand.nextInt(5);
/*		
		sconn.sendGCodeToController("G1 F"+robotObject.xyfullspeed+"          ; set X Y speed\n");
		sconn.sendGCodeToController("G1 X"+robotObject.waterWellX+"          ; move X to above water \n");
		sconn.sendGCodeToController("G1 Y"+robotObject.waterWellY+"          ; move Y to above water \n");
		sconn.sendGCodeToController("G1 Z"+robotObject.waterWellZ+"          ; move Z to dip in water \n");
		sconn.sendGCodeToController("G1 Z"+robotObject.zRetracted+"          ; move Z into air \n");
		sconn.sendGCodeToController("G1 "+robotObject.getRotation((Math.PI/2))+"         ; move dip paint in well\n");
		sconn.sendGCodeToController("G1 X"+robotObject.cleaningStartX+"           ; move X Y to 10,100 above cleaning area \n");
		sconn.sendGCodeToController("G1 X"+(robotObject.cleaningStartX+n)+"           ; move X Y to 10,100 above cleaning area \n");
		sconn.sendGCodeToController("G1 Y"+robotObject.cleaningStartY+"           ; move X Y to 10,100 above cleaning area \n");
		sconn.sendGCodeToController("G1 Z"+robotObject.cleaningZ+"          ; move XYZ into cleaning stroke \n");
		sconn.sendGCodeToController("G1 Y"+robotObject.cleaningEndY+"  F\"+robotObject.xyhomespeed+\"        ; move XYZ into cleaning stroke \n");
		sconn.sendGCodeToController("G1 X"+(robotObject.cleaningEndX+n)+" F\"+robotObject.xyhomespeed+\"         ; move XYZ into cleaning stroke \n");
		sconn.sendGCodeToController("G1 Y"+robotObject.cleaningStartY+" F\"+robotObject.xyhomespeed+\"         ; move XYZ into cleaning stroke \n");
		sconn.sendGCodeToController("G1 Z"+robotObject.zRetracted+"        ; move XYZ out of cleaning stroke \n");
		sconn.sendGCodeToController("G1 "+robotObject.getRotation(0)+"         ; move dip paint in well\n");
		sconn.sendGCodeToController("G1 Y"+robotObject.cleaningStartX+"          ; move XYZ into cleaning stroke \n");
		sconn.sendGCodeToController("G1 Y"+robotObject.yOrigin+"          ; move Y to default origin\n");
		sconn.sendGCodeToController("G1 X"+robotObject.xOrigin+"          ; move X into origin \n");
		sconn.sendGCodeToController("G1 Z0         ; move Y to default origin\n");
*/
		
	}
	
	public void homeBrush() {
		sconn.sendGCodeToController("G1 Z0             ; move X Y Z to default origin\n");
		softHomeY();
		softHomeX();
		sconn.sendGCodeToController("G28 Z F"+robo.zhomespeed+"          ; home Z Axes\n");
		sconn.sendGCodeToController("M204 P400 T"+robo.maxAccelSpeedPainting+"           ; returning to regular speed\n");
		sconn.sendGCodeToController("G1 X"+robo.mmX(robo.xOrigin)+"           ; move X Y to default origin\n");
		sconn.sendGCodeToController("G1 Y"+robo.mmY(robo.yOrigin)+"          ; move X Y to default origin\n");
	}
	

	
	/*public void dipBrushInSinglePaintWell() {
		sconn.sendGCodeToController("G1 Z0          ; move Z to 1\n");
		sconn.sendGCodeToController("G28 Z F"+robo.zhomespeed+"          ; home z Axes\n");
		//40,40
		sconn.sendGCodeToController("G1 X40 Y40\n");
		//face north
		sconn.sendGCodeToController("G1 "+robo.getRotation((Math.PI/2))+"\n");
		//z 15
		sconn.sendGCodeToController("G1 Z"+(robo.paintWellZ-1)+"\n");
		//40,110
		sconn.sendGCodeToController("G1 X40 Y110\n");
		//turn 90 right
		sconn.sendGCodeToController("G1 "+robo.getRotation((0))+"\n");
		//80,110
		sconn.sendGCodeToController("G1 X80 Y110\n");
		//turn 90 right
		sconn.sendGCodeToController("G1 "+robo.getRotation((-Math.PI/2))+"\n");
		//80,40
		sconn.sendGCodeToController("G1 X80 Y40\n");
		//turn 90 right
		sconn.sendGCodeToController("G1 "+robo.getRotation((-Math.PI))+"\n");
        //60,40
		sconn.sendGCodeToController("G1 X60 Y40\n");
		//turn right 90
		sconn.sendGCodeToController("G1 "+robo.getRotation((-Math.PI-(Math.PI/2)))+"\n");
		//60, 75
		sconn.sendGCodeToController("G1 X60 Y75\n");
		//turn right 90
		sconn.sendGCodeToController("G1 "+robo.getRotation((-2*Math.PI))+"\n");
		//70, 75
		sconn.sendGCodeToController("G1 X70 Y75\n");
		//150, 75, 10
		sconn.sendGCodeToController("G1 X150 Y75 Z"+(robo.paintWellZ-5)+"\n");
		sconn.sendGCodeToController("G1 Z"+(robo.paintWellZ-10)+"\n");
		//unwind
		sconn.sendGCodeToController("G1 Z"+(robo.paintWellZ-8)+" "+robo.getRotation((-1.5*Math.PI))+"\n");
		sconn.sendGCodeToController("G1 Z"+(robo.paintWellZ-10)+" "+robo.getRotation((-Math.PI))+"\n");
		sconn.sendGCodeToController("G1 Z"+(robo.paintWellZ-8)+" "+robo.getRotation((-Math.PI/2))+"\n");
		sconn.sendGCodeToController("G1 Z"+(robo.paintWellZ-10)+" "+robo.getRotation((0))+"\n");
	}

	public void dipWideBrushInSinglePaintWell() {
		sconn.sendGCodeToController("G1 "+robo.getRotation((0))+"\n");
		sconn.sendGCodeToController("G1 Z0          ; move Z to 1\n");
		sconn.sendGCodeToController("G28 Z F"+robo.zhomespeed+"          ; home z Axes\n");
		sconn.sendGCodeToController("G1 X50 Y75\n");
		sconn.sendGCodeToController("G1 Z"+(robo.paintWellZ)+"\n");
		sconn.sendGCodeToController("G1 "+robo.getRotation((3))+"\n");
		sconn.sendGCodeToController("G1 "+robo.getRotation((-3))+"\n");
		sconn.sendGCodeToController("G1 "+robo.getRotation((0))+"\n");
		
		sconn.sendGCodeToController("G1 X60 Y75 Z"+ (robo.paintWellZ-2)+"\n");
		sconn.sendGCodeToController("G1 X40 Y75 Z"+ (robo.paintWellZ-4)+"\n");
		
		//sconn.sendGCodeToController("G1 X50 Y75\n");
		//sconn.sendGCodeToController("G1 X150 Y75 Z"+(robotObject.paintWellZ-5)+"\n");
		sconn.sendGCodeToController("G1 Z5\n");
		//unwind
	}
*/
	
	public void dipBrushInPaint(String currentStrokeColor, int strokeNumber) {
		//dip brush in color
		Color strokeColorObject = ColorUtils.getColorFromString(currentStrokeColor);
		Color bestMatchInPalette = this.getBestMatchColor(strokeColorObject,colorPalette);
		
		Point2D.Double wellPoint = robo.getPaintWellLocationInMm(bestMatchInPalette);
		double wellLength = robo.paintWellWidth;
		double wellX = wellPoint.getX();
		double wellY = wellPoint.getY();
		
		sconn.sendGCodeToController("G1 Z0          ; move Z to 1\n");
		sconn.sendGCodeToController("G28 Z F"+robo.zhomespeed+"          ; home z Axes\n");
		sconn.sendGCodeToController("G1 X"+robo.mmY(wellX)+"          ; move Y to well location\n");
		sconn.sendGCodeToController("G1 Y"+robo.mmY(wellY)+"          ; move Y to well location\n");
		sconn.sendGCodeToController("G1 Z"+robo.mmZ(robo.paintWellZ)+" "+robo.getRotation((Math.PI))+"         ; move dip paint in well\n");
		sconn.sendGCodeToController("G1 Y"+robo.mmY((wellY+wellLength/2))+"          ; sweep brush\n");

		sconn.sendGCodeToController("G1 Z"+robo.mmZ(robo.zRetracted)+"          ; move dip paint iout of well\n");

		sconn.sendGCodeToController("G1 "+robo.getRotation((Math.PI/2))+"          ; move dip paint iout of well\n");
		sconn.sendGCodeToController("G1 "+robo.getRotation(0.0)+"          ; move dip paint iout of well\n");

		sconn.sendGCodeToController("T1; Tool 1"); 
		
		//PINDAR UNDO
//		sconn.sendGCodeToController("G1 E"+paintMeterAmount+" ; Extrude"); 
		sconn.sendGCodeToController("T0; Tool 0"); 
		sconn.sendGCodeToController("G92 E0.0"); 		
			
	}
	
	public Color getBestMatchColor(Color colorToMatch, Vector palette) {
		Color closestColor = (Color)colorPalette.elementAt(0);
		double smallestDifference = ColorUtils.getColourDistance(colorToMatch, closestColor);
		for (int i=1;i<colorPalette.size();i++) {
			double thisDifference = ColorUtils.getColourDistance(colorToMatch, (Color)colorPalette.elementAt(i));
			if (thisDifference<smallestDifference) {
				smallestDifference = thisDifference;
				closestColor = (Color)colorPalette.elementAt(i);
			}
		}
		return closestColor;
	}
	
	public void parkBrush(String currentBrush) {
			softHomeY();
			softHomeX();
			returnBrush(currentBrush);
			sconn.sendGCodeToController("G28 Z F"+robo.zhomespeed+"          ; home z Axes\n");
			sconn.sendGCodeToController("M204 P"+robo.maxAccelSpeedPainting+" T"+robo.maxAccelSpeedPainting+"           ; returning to regular speed\n");
			sconn.sendGCodeToController("G1 Y"+robo.mmY(robo.waterWellY)+"          ; move Y above water \n");
			sconn.sendGCodeToController("G1 X"+robo.mmX(robo.waterWellX)+"           ; move X above water \n");
			sconn.sendGCodeToController("G1 Z"+robo.mmZ(robo.waterWellZ)+"          ; move Z to dip in water \n");
	}
	
	
	
	public void paintStroke(JSONObject jsonStroke, int strokeNumber) {
		
		String thisStrokeType = "";
		
		double thisStrokeLength = 0.0;
		double thisStrokeWidth = 0.0; 
		try {
			thisStrokeType = getStrokeFromJSON(jsonStroke);
			thisStrokeLength = getDoubleFromStrokeJSON(jsonStroke, "length_mm");
			thisStrokeWidth =  getDoubleFromStrokeJSON(jsonStroke, "width_mm");
		} catch (Exception e) {
			thisStrokeType = "notfound";
		}
		
		
		String firstAxisToMoveOnApproach = "xy";
		if (robo.nextPaintWellXShift > robo.nextPaintWellYShift) {
			firstAxisToMoveOnApproach = "y";
		} else {
			firstAxisToMoveOnApproach = "x";
		}
		
		//DEFAULT
		StrokeTemplate thisStroke = null;  //new FB_D_FlatBrush_WideFullStroke_SpinOnDot_NoRise(strokeNumber, jsonStroke, robotObject, 10.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
			
		if (thisStrokeType.equalsIgnoreCase("RB_SS")) {
				thisStroke = new RB_SS_RoundBrush_SimpleStroke(strokeNumber, jsonStroke, robo, 1.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
		} else 			if (thisStrokeType.equalsIgnoreCase("RB_L")) {
				thisStroke = new RB_SS_RoundBrush_SimpleStroke(strokeNumber, jsonStroke, robo, 0.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
		} else 			if (thisStrokeType.equalsIgnoreCase("RB_M")) {
				thisStroke = new RB_SS_RoundBrush_SimpleStroke(strokeNumber, jsonStroke, robo, 1.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
		} else 			if (thisStrokeType.equalsIgnoreCase("RB_H")) {
			thisStroke = new RB_SS_RoundBrush_SimpleStroke(strokeNumber, jsonStroke, robo, 2.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
		} else 			if (thisStrokeType.equalsIgnoreCase("FB_D")) {
			thisStroke = new FB_D_FlatBrush_WideFullStroke_SpinOnDot_NoRise(strokeNumber, jsonStroke, robo, 10.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
		} else 			if (thisStrokeType.equalsIgnoreCase("FB_W")) {
			thisStroke = new FB_W_FlatBrush_WideFullStroke_NoRise(strokeNumber, jsonStroke, robo, 10.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
		} else 			if (thisStrokeType.equalsIgnoreCase("FB_M")) {
			thisStroke = new FB_M_FlatBrush_MediumFullStroke_NoRise(strokeNumber, jsonStroke, robo, 10.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
		} else 			if (thisStrokeType.equalsIgnoreCase("FB_T")) {
			thisStroke = new FB_T_FlatBrush_ThinFullStroke_NoRise(strokeNumber, jsonStroke, robo, 10.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
			
		} else 			if (thisStrokeType.equalsIgnoreCase("FB_AH")) {
			thisStroke = new FB_AH_FlatBrush_AlwaysHorizontal(strokeNumber, jsonStroke, robo, 10.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
		} else 			if (thisStrokeType.equalsIgnoreCase("FB_AV")) {
			thisStroke = new FB_AV_FlatBrush_AlwaysVertical(strokeNumber, jsonStroke, robo, 10.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED

		} else 			if (thisStrokeType.equalsIgnoreCase("FB_TIW"   )) {
			thisStroke = new FB_TIW_FlatBrush_Thin_ImmediatelyRotateToWide_WideToEnd(strokeNumber, jsonStroke, robo, 10.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
		} else 			if (thisStrokeType.equalsIgnoreCase("FB_TIWSR")) {
			thisStroke = new FB_TIWSR_FlatBrush_Thin_ImmediatelyRotateToWide_WideToEnd_SlowRise(strokeNumber, jsonStroke, robo, 10.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
		
		} else 			if (thisStrokeType.equalsIgnoreCase("FB_WMT")) {
			thisStroke = new FB_WMT_FlatBrush_WideFirstQuarter_MediumSecondQuarter_ThinSecondHalf(strokeNumber, jsonStroke, robo, 10.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
		
		} else 			if (thisStrokeType.equalsIgnoreCase("FB_WTSR")) {
			thisStroke = new FB_WTSR_FlatBrush_Wide_GraduallyRotateToThin_SlowRise(strokeNumber, jsonStroke, robo, 10.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
		} else 			if (thisStrokeType.equalsIgnoreCase("PK_KING")) {
			thisStroke = new PK_KING_PaletteKnifeKingStroke_RotatingArc(strokeNumber, jsonStroke, robo, thisStrokeLength, thisStrokeWidth, firstAxisToMoveOnApproach);
		} else 			if (thisStrokeType.equalsIgnoreCase("PK_ROOK")) {
			thisStroke = new PK_ROOK_PaletteKnifeRookStroke_FlatDrag(strokeNumber, jsonStroke, robo, thisStrokeLength, thisStrokeWidth, firstAxisToMoveOnApproach);
		} 				
		
		Vector iStrokes = thisStroke.getIndividualGCodeLines();
		for (int ss=0;ss<iStrokes.size();ss++) {
			String gcodeLine = (String)iStrokes.elementAt(ss);
			if (!gcodeLine.substring(0,1).equalsIgnoreCase(";")) {
				sconn.sendGCodeToController(gcodeLine);
			}
		}
	}
	
	public static JSONObject getNextStrokeJSON(String sourceFileNameRoot, int strokeNumber) {
				String JSONString = ElasticsearchUtility.sendGetStrokeRequest(sourceFileNameRoot, strokeNumber);
				JSONObject obj = new JSONObject(JSONString);
				JSONObject hitsobj = (JSONObject) obj.get("hits");
				if ((int)hitsobj.get("total") > 0) { 
					return obj;
				} else {
					return null;
				}
	}
	
	public String getStrokeFromJSON(JSONObject strokeJSON) {
		JSONObject hits     = strokeJSON.getJSONObject("hits");
		JSONArray hitsArray     = hits.getJSONArray("hits");
		JSONObject source     = hitsArray.getJSONObject(0).getJSONObject("_source");
		JSONObject paint      = source.getJSONObject("stroke");
		String colorValue    = paint.getString("name");
		return colorValue;
	}
	
	public Double getDoubleFromStrokeJSON(JSONObject strokeJSON, String value) {
		JSONObject hits     = strokeJSON.getJSONObject("hits");
		JSONArray hitsArray     = hits.getJSONArray("hits");
		JSONObject source     = hitsArray.getJSONObject(0).getJSONObject("_source");
		JSONObject paint      = source.getJSONObject("stroke");
		double doubleVal    = paint.getDouble(value);
		return doubleVal;
	}
	
	
	public String getColorFromJSON(JSONObject strokeJSON) {
		JSONObject hits     = strokeJSON.getJSONObject("hits");
		JSONArray hitsArray     = hits.getJSONArray("hits");
		JSONObject source     = hitsArray.getJSONObject(0).getJSONObject("_source");
		JSONObject paint      = source.getJSONObject("paint");
		String colorValue    = paint.getString("color");
		return colorValue;
	}

	public String getBrushFromJSON(JSONObject strokeJSON) {
		JSONObject hits     = strokeJSON.getJSONObject("hits");
		JSONArray hitsArray     = hits.getJSONArray("hits");
		JSONObject source     = hitsArray.getJSONObject(0).getJSONObject("_source");
		JSONObject paint      = source.getJSONObject("brush");
		String colorValue    = paint.getString("name");
		return colorValue;
	}
	
	public double getStrokeLength(JSONObject strokeJSON) {
		JSONObject hits     = strokeJSON.getJSONObject("hits");
		JSONArray hitsArray     = hits.getJSONArray("hits");
		JSONObject source     = hitsArray.getJSONObject(0).getJSONObject("_source");
		double strokePoints    = 1.0*source.getInt("strokePoints");
		return strokePoints;

	}
	
	public static void saveSimulation(String indexName, String simulationFileName, int startIndex, int endIndex) {
			
			Vector jsonObjects = new Vector();
			for (int i=startIndex; i<=endIndex; i++) {
				 
				JSONObject stroke = getNextStrokeJSON(indexName, i);
				if (stroke!=null) {
					jsonObjects.add(stroke);
				}
			}
			
			int canvasScale = 4;
			JSONObject hits     = ((JSONObject)jsonObjects.get(0)).getJSONObject("hits");
			JSONArray hitsArray     = hits.getJSONArray("hits");
			JSONObject source     = hitsArray.getJSONObject(0).getJSONObject("_source");
			JSONObject strokeCanvas = source.getJSONObject("strokeCanvas");
			int width = (((int)(strokeCanvas.getInt("width")*10))/10)*canvasScale;
			int height = (((int)(strokeCanvas.getInt("height")*10))/10)*canvasScale;
			JSONObject paint = source.getJSONObject("paint");
			String colorString = paint.getString("color");
			Color thisColor = ColorUtils.getColorFromString(colorString);
			BufferedImage simulation = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = simulation.createGraphics();
			g.setColor(Color.LIGHT_GRAY);
			g.fillRect(0, 0 , width, height);
			g.dispose();
			//read strokes 
			for (int i=0;i<jsonObjects.size();i++) {
				hits     = ((JSONObject)jsonObjects.get(i)).getJSONObject("hits");
				hitsArray     = hits.getJSONArray("hits");
				source     = hitsArray.getJSONObject(0).getJSONObject("_source");
				paint = source.getJSONObject("paint");
				colorString = paint.getString("color");
				thisColor = ColorUtils.getColorFromString(colorString);
				JSONArray pathArray = source.getJSONArray("strokePath");
				for (int j=0;j<pathArray.length()-1;j++) {
					JSONArray firstPoint = pathArray.getJSONArray(j);
					JSONArray secondPoint = pathArray.getJSONArray(j+1);
					
					int sx = firstPoint.getInt(0)*canvasScale; 
					int sy = firstPoint.getInt(1)*canvasScale; 
					int ex = secondPoint.getInt(0)*canvasScale;  
					int ey = secondPoint.getInt(1)*canvasScale; 
					g = simulation.createGraphics();
					g.setColor(thisColor);
					g.setStroke(new BasicStroke(3));
					g.drawLine(sx+2, height-sy-2, ex+2, height-ey-2);
					g.dispose();
					
				}
			}
			//scale * 4
			//get width
			//get height
			
			//make buffered image
			//iterate through strokes and draw lines
			
	 	    try {
	 	    	System.out.println("SavingSimulation: "+simulationFileName);
	 	    	File outputfile = new File(simulationFileName);
				ImageIO.write(simulation, "gif", outputfile);
		 	    System.out.println("Succesfully wrote: "+simulationFileName);
			}
			catch (IOException e) {
		 	    System.out.println("FAILED TO write: "+simulationFileName);
				e.printStackTrace();
			}
		}

	public String getColorFromJson() {
		return "";
	}

	public String getPointArrayFromJson() {
		return "";
	}

	
}