package com.cloudpainter.utils;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.Random;
import java.util.Vector;

import javax.imageio.ImageIO;

import com.cloudpainter.aistrokes.AIStrokes;
import com.cloudpainter.autopaint.BitmapStroke;
import com.cloudpainter.autopaint.FeedbackStrokes;
import com.cloudpainter.robots.LargeSixtyDegreePlotter;
import com.cloudpainter.robots.PandoraVOne;
import com.cloudpainter.robots.GumGumArm;
import com.cloudpainter.robots.LargeFlatPlotter;
import com.cloudpainter.robots.MidSizedFortyFiveDegreePlotter;
import com.cloudpainter.robots.RobotTemplate;
import com.cloudpainter.strokes.FB_AH_FlatBrush_AlwaysHorizontal;
import com.cloudpainter.strokes.FB_AV_FlatBrush_AlwaysVertical;
import com.cloudpainter.strokes.FB_D_FlatBrush_WideFullStroke_SpinOnDot_NoRise;
import com.cloudpainter.strokes.FB_M_FlatBrush_MediumFullStroke_NoRise;
import com.cloudpainter.strokes.FB_TIWSR_FlatBrush_Thin_ImmediatelyRotateToWide_WideToEnd_SlowRise;
import com.cloudpainter.strokes.FB_TIW_FlatBrush_Thin_ImmediatelyRotateToWide_WideToEnd;
import com.cloudpainter.strokes.FB_T_FlatBrush_ThinFullStroke_NoRise;
import com.cloudpainter.strokes.FB_WMT_FlatBrush_WideFirstQuarter_MediumSecondQuarter_ThinSecondHalf;
import com.cloudpainter.strokes.FB_WTSR_FlatBrush_Wide_GraduallyRotateToThin_SlowRise;
import com.cloudpainter.strokes.FB_W_FlatBrush_WideFullStroke_NoRise;
import com.cloudpainter.strokes.PK_KING_PaletteKnifeKingStroke_RotatingArc;
import com.cloudpainter.strokes.PK_ROOK_PaletteKnifeRookStroke_FlatDrag;
import com.cloudpainter.strokes.RB_SS_RoundBrush_SimpleStroke;
import com.cloudpainter.strokes.StrokeTemplate;

import org.json.*;




public class LiveRoboForthExecuter {
	
	SerialCommunicationSTR sconn = new SerialCommunicationSTR();

	String indexName = "";
	String rootDir = "";
	
	public static boolean simulateWithoutMovingRobot = false;
	
	public static boolean readFromFile = false;
	public static String readFileName = "";

	Vector<JSONObject> jsonStrokes = new Vector();

	Date date= new Date();
	final long TIMESIGNATURE = date. getTime();
	
	int startIndex = 0;
	int nextStrokeNumber = startIndex;

	boolean makeAIStrokes = false;
	int attemptsBeforeChangingToAI = 1000;
	
	boolean waitForPaintDrip = true;
	
	public AIStrokes aiStrokes = new AIStrokes();
	public FeedbackStrokes feedbackStrokes = new FeedbackStrokes(1,1);

	public int numberOfAIStrokesToMake = 30;
	
	public RobotTemplate robotObject = new GumGumArm();

	public int maxStrokeNumber = 1000000;//62;//2805;//7552;

	
	//public int brushPressureAdjustment = 0; //5000
	
	public int maxStrokesPerPaintDip = 10; //3; //2;
	public double homeAfterHowManyStrokes = 100;  //6

	public double distanceTravelledByStroke = 0.0;  //TODO right now it is points on line
	public double refillPaintAfterThisDistance = 200.0;
	
	
	public StringBuffer gcodeStringBuffer = new StringBuffer();
	public Vector strokeJSONObjectVector = new Vector();

	public Vector colorPaletteInFile = new Vector();
	public Vector colorPaletteInWells = new Vector();

	public int colorTolerance = 0;
	
	public LiveRoboForthExecuter (String rootDir, String indexName, boolean activateAI, Vector palette) {
		
		this.indexName = indexName;
		this.rootDir = rootDir;
		this.makeAIStrokes = activateAI;
		this.colorPaletteInFile = palette;
		robotObject.paintWells = colorPaletteInFile;   

		File makeDir = new File(rootDir+indexName);
		makeDir.mkdir();
		
		//connect to robot
		this.startIndex = ElasticsearchUtility.getMaxStrokeNumber(indexName)+1;
		this.nextStrokeNumber = startIndex;
		
		//Pull in any opverrides
		updatePaintingParameters();
		
		System.out.println("Starting on strokeNumber : "+nextStrokeNumber);
	}

	
	double absX = 0;
	double absY = 0;
	double absZ = 0;
	
	public void setAbsoluteXYZ(double x, double y, double z) {
		absX = x;
		absY = y;
		absZ = z;
	}
	
	public String getAbsoluteMoveToString() {
		return ""+(int)absX+" "+(int)absY+" "+(int)absZ+" MOVETO";
	}
	
	public void startPainting(){
		sconn.openSerialConnection();
		try {
			Thread.sleep(1000);
		}
		catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		initializeRobot();
		homeRobot();
		goToDefaultStart();
		
		int strokesSinceLastHome = 0;
		int strokesSinceLastDip = 100000;
		String currentColor = "#ffffff";
		String lastColor = "#ffffff";
		
		int numberOfNullStrokes = 0;
		
		while (nextStrokeNumber <= maxStrokeNumber) {
			//if 5 tries return nothing / park
			if (numberOfNullStrokes>10) {
				parkBrush();
				snapshotProgress(indexName);
				currentColor = "#ffffff";
				lastColor = "#ffffff";
				strokesSinceLastDip = 1000000;
				strokesSinceLastHome = 0;
				
				//go into listening mode when strokes stop.
				int attemptsToDetectNextStroke = 0;
				
				while (numberOfNullStrokes>10) {

					updatePaintingParameters();

					int highestElasticSearchStrokeNumber = ElasticsearchUtility.getMaxStrokeNumber(indexName);

					if (nextStrokeNumber < highestElasticSearchStrokeNumber) {
						nextStrokeNumber++;
					}
					
					try {
						Thread.sleep(1000);
						attemptsToDetectNextStroke++; 
						System.out.print(".|");
						System.out.println("Looking for: "+(nextStrokeNumber));
						JSONObject stroke = getNextStrokeJSON(rootDir, indexName, nextStrokeNumber);
						if (stroke!=null) {
							//goToDefaultStart();
							numberOfNullStrokes=0;
						}
							if (numberOfNullStrokes>0 && attemptsToDetectNextStroke>=attemptsBeforeChangingToAI) {
								System.out.print(".|");
								System.out.print("Getting AI Strokes");
								Color lastColorUsed = ColorUtils.getColorFromString(currentColor);
								//OLD WAY
								//int strokesCreated = aiStrokes.generateAiStrokes(this.makeAIStrokes, rootDir, indexName, numberOfAIStrokesToMake, lastColorUsed);
								int strokesCreated = feedbackStrokes.getNextSetOfFeedbackStrokes(this.makeAIStrokes, rootDir, indexName, numberOfAIStrokesToMake);
								if (strokesCreated>0) {
									numberOfNullStrokes = 0;
								}
							} else {
								System.out.println("nextStrokeNumber: "+nextStrokeNumber);
								System.out.println("NOT getting AI Strokes");
							}
					}
					catch (InterruptedException ie) {
						// TODO Auto-generated catch block
						ie.printStackTrace();
					} 
					catch (Exception e) {
						System.out.println("problem while parked getting json");
					}	

					
				}
			}
			
			JSONObject stroke = getNextStrokeJSON(rootDir, indexName, nextStrokeNumber);
			//if there is a hit
			if (stroke!=null) { 
					currentColor = this.getColorFromJSON(stroke);
				//if(!colorPalette.contains(currentColor)) {
				//	colorPalette.add(currentColor);
				//	robotObject.paintWells = colorPalette; //update robot palette
				//}
				//CLEAN BRUSH if color changes
				if (!currentColor.equalsIgnoreCase(lastColor)) {
					cleanBrush();
					snapshotProgress(indexName); //p911
					strokesSinceLastDip = 1000000;
					distanceTravelledByStroke = 10000.0;
					lastColor = currentColor;
				}
				//HOME if color changes
				if (strokesSinceLastHome>=homeAfterHowManyStrokes) {
						homeBrush();
						strokesSinceLastHome = 0;
				}
				//Dip if Color Needed
				if ((strokesSinceLastDip>=maxStrokesPerPaintDip) || (distanceTravelledByStroke > refillPaintAfterThisDistance)) {
					//dipBrushInSinglePaintWell();
//psundayUNDO					dipWideBrushInSinglePaintWell();
					updatePaintingParameters();
					dipBrushInPaint(currentColor, (int)distanceTravelledByStroke);
					snapshotProgress(indexName);
					strokesSinceLastDip = 0;
					distanceTravelledByStroke = 0.0;
				}
				
				//MAKE STROKE
				paintStroke(stroke, nextStrokeNumber);
				nextStrokeNumber++;
				distanceTravelledByStroke = distanceTravelledByStroke+getStrokeLength(stroke);
				strokesSinceLastDip = strokesSinceLastDip + 1;
				strokesSinceLastHome = strokesSinceLastHome + 1;
			} else {
				numberOfNullStrokes ++;
			}
		}
		
		//sconn.closeSerialConnection();
	}

	
	public void updatePaintingParameters() {
		try {
			JSONObject lpp = ElasticsearchUtility.getJsonObjectFromFile(rootDir+"config\\live_painting_parameters.json");

 		    simulateWithoutMovingRobot = lpp.getBoolean("simulateWithoutMovingRobot");
 		    sconn.setSimulateMode(simulateWithoutMovingRobot);
 		    
			boolean overrideStrokeNumber = lpp.getBoolean("overrideNextStrokeNumberOverride");
			if (overrideStrokeNumber) {
				nextStrokeNumber = lpp.getInt("nextStrokeNumberOverride");
			}
			  this.makeAIStrokes = lpp.getBoolean("makeAIStrokes");  
			  this.attemptsBeforeChangingToAI = lpp.getInt("attemptsBeforeChangingToAI");  
			  this.numberOfAIStrokesToMake = lpp.getInt("numberOfAIStrokesToMake");

			  this.robotObject.updateBrushHeight( lpp.getDouble("brushHeightMm"),
					                              lpp.getDouble("canvasHeightMm"),
					                              lpp.getDouble("canvasSlumpMm"),
					                              lpp.getDouble("waterHeightMm"),
					                              lpp.getDouble("cleaningHeightMm"),
					                              lpp.getDouble("wellHeightMm"));
			  
			  
			  //this.brushPressureAdjustment = lpp.getInt("brushPressureAdjustment");
			  
			  this.maxStrokesPerPaintDip = lpp.getInt("maxStrokesPerPaintDip");
			  this.homeAfterHowManyStrokes = lpp.getInt("homeAfterHowManyStrokes");
			  this.refillPaintAfterThisDistance = lpp.getDouble("refillPaintAfterThisDistance");

			  boolean repaintSegment = lpp.getBoolean("repaintSegment");
			  if (repaintSegment) {
				  nextStrokeNumber = lpp.getInt("segmentStart");
				  maxStrokeNumber =  lpp.getInt("segmentEnd");
			  }
			   
			  readFromFile = lpp.getBoolean("readFromFile");
			  readFileName = lpp.getString("readFileName");
			  
			  if (readFromFile) {
				  nextStrokeNumber = lpp.getInt("fileStrokeStart");
				  maxStrokeNumber =  lpp.getInt("fileStrokeEnd");
			  }

		
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//int topLeftX      = snapshotCorners.getInt("topLeftX");
		//int topLeftY      = snapshotCorners.getInt("topLeftY");
		
	}
	
	public void snapshotProgress(String imageName) {
		try {
			File f = new File(rootDir+"progress_photo.jpg");
			BufferedImage img = null;
			try {
			    img = ImageIO.read(f);
			} catch (IOException e) {
			}

			JSONObject snapshotCorners = ElasticsearchUtility.getJsonObjectFromFile(rootDir+"config\\camera_calibration.json");

			
			int topLeftX      = snapshotCorners.getInt("topLeftX");
			int topLeftY      = snapshotCorners.getInt("topLeftY");
			int topRightX     = snapshotCorners.getInt("topRightX");
			int topRightY     = snapshotCorners.getInt("topRightY");
			int bottomLeftX   = snapshotCorners.getInt("bottomLeftX");
			int bottomLeftY   = snapshotCorners.getInt("bottomLeftY");
			int bottomRightX  = snapshotCorners.getInt("bottomRightX");
			int bottomRightY  = snapshotCorners.getInt("bottomRightY");
			int canvasWidth =       snapshotCorners.getInt("canvasWidth");
			int canvasHeight =       snapshotCorners.getInt("canvasHeight");
			int dpi =       snapshotCorners.getInt("dpi");
			
			TrapezoidImageTransformer TrapImageConverter = new TrapezoidImageTransformer(
					img, 
					topLeftX, topLeftY, 
					topRightX, topRightY,
					bottomLeftX, bottomLeftY,
					bottomRightX, bottomRightY,
					canvasWidth*dpi,
					canvasHeight*dpi);


				BufferedImage rectImage = TrapImageConverter.GetUnwarpedImage();
				
				//String newfilename = "C:\\cloudpainterlibrary\\currentpainting\\progress.jpg";
							
				//try {
				//    File outputfile = new File(newfilename);
				//   ImageIO.write(rectImage, "jpg", outputfile);
				//} catch (IOException e) {
				//    
				//}
				
				String newfilename = rootDir+imageName+"_1progress.jpg";
				String paddedNumber = ElasticsearchUtility.paintingIdInMillion(nextStrokeNumber);
				String paddedFilename = rootDir+indexName+"\\" + "tl_"+paddedNumber+".jpg";

				try {
					
				    File outputfile = new File(newfilename);
				    ImageIO.write(rectImage, "jpg", outputfile);
				    
				    File paddedOutputfile = new File(paddedFilename);
				    ImageIO.write(rectImage, "jpg", paddedOutputfile);
				    
				} catch (IOException e) {
				    
				}
				
			}
			catch (Exception e) {}

	}
	
	
	
	public Vector refreshPalette(String sourceDir, String sourceFilename) {

		JSONObject snapshotCorners;
		Vector palette = new Vector();

		String paletteFileName = "";

		
		try {
			snapshotCorners = ElasticsearchUtility.getJsonObjectFromFile(sourceDir+"config\\camera_calibration.json");
			this.colorTolerance = snapshotCorners.getInt("colorTolerance");

			
			int maxPaintWells = snapshotCorners.getInt("maxPaintWells");
			JSONArray paintWells = snapshotCorners.getJSONArray("paintWells");
			
			//String sourceFileNameRoot = sourceFilename.substring(0,sourceFilename.indexOf("."));
			//String sourceFileName = sourceDir+sourceFilename;

			boolean useLivePalette = snapshotCorners.getBoolean("useLivePalette");
			
			
			if (!useLivePalette) {
				paletteFileName = sourceDir+sourceFilename+"_palette.gif";
				palette = colorPaletteInFile; 
			} else {
				paletteFileName = sourceDir+sourceFilename+"_livepalette.gif";
				String liveProgressPhotoName = sourceDir+"progress_photo.jpg";
				
				//read paintmap coverage file and fill in those values of paintmap
				File imageFile = new File(liveProgressPhotoName);
				System.out.println("loading source file: "+imageFile.getAbsolutePath());


				BufferedImage sourceImage = ImageIO.read(imageFile);
				System.out.println("sourceImage Dimensions: "+sourceImage.getWidth()+"x"+sourceImage.getHeight());
				

				for (int i=0; i<maxPaintWells;i++) {
					JSONArray coord = paintWells.getJSONArray(i);
					
					int pixelsAveraged = 0;
					int averageRed = 0;
					int averageGreen = 0;
					int averageBlue = 0;
					
					for (int j=-2;j<=2;j++) {
						for (int k=-2;k<=2;k++) {
							pixelsAveraged++;
							int xcoord = coord.getInt(0)+j;
							int ycoord = coord.getInt(1)+k;
							Color rgbColor = new Color(sourceImage.getRGB(xcoord,ycoord));
							averageRed = averageRed+ rgbColor.getRed();
							averageGreen = averageGreen+ rgbColor.getGreen();
							averageBlue = averageBlue+ rgbColor.getBlue();
						}
					}
					averageRed = averageRed/pixelsAveraged;
					averageGreen = averageGreen/pixelsAveraged;
					averageBlue = averageBlue/pixelsAveraged;
					Color averageRGB = new Color(averageRed, averageGreen, averageBlue);
					System.out.println("adding "+averageRGB+" to available colors");
					palette.add(averageRGB);

					
				}
				
				//normalize values of palette
				palette = normalizePalette(palette);
				//reduce number of colors beyond black
				palette = reducePalette(palette);
				
					BufferedImage paletteFile = new BufferedImage((palette.size()*10), 10, BufferedImage.TYPE_INT_ARGB);
			        for (int i=0; i<palette.size();i++) {
			        	   Graphics2D g = paletteFile.createGraphics();
			        	   g.setColor((Color)palette.get(i));
				           g.fillRect((i*10), 0, (i*10)+10, 10);
				           g.dispose();
			        }
			        try {
			        System.out.println("Attempting to make palette file: "+paletteFileName);
			        	   File outputfile = new File(paletteFileName);
			        	   ImageIO.write(paletteFile, "gif", outputfile);
			        	   System.out.println("Succesfully wrote: "+paletteFileName);
					} catch (IOException f) {
							System.out.println("Failed to write: "+paletteFileName);
							f.printStackTrace();
					}
			}
		        
		} catch (IOException e) {
		}
        String palleteFileNameJson = paletteFileName.substring(0,paletteFileName.lastIndexOf("\\")+1);
        palleteFileNameJson = palleteFileNameJson+"config\\paintavailable.json";
        
        ColorUtils.saveColorJsonFile(palleteFileNameJson, palette);

        return palette;
	}
	
	public static Vector normalizePalette(Vector palette) {
		int rDist = 0;
		int rMax = -1;
		int rMin = 257;
		int gDist = 0;
		int gMax = -1;
		int gMin = 257;
		int bDist = 0;
		int bMax = -1;
		int bMin = 257;

		for (int i=0;i<palette.size();i++) {
			Color thisColor = (Color)palette.get(i);
			if (thisColor.getRed()>rMax) {
				rMax = thisColor.getRed();
			}
			if (thisColor.getRed()<rMin) {
				rMin = thisColor.getRed();
			}
			if (thisColor.getGreen()>gMax) {
				gMax = thisColor.getGreen();
			}
			if (thisColor.getGreen()<gMin) {
				gMin = thisColor.getGreen();
			}
			if (thisColor.getBlue()>bMax) {
				bMax = thisColor.getBlue();
			}
			if (thisColor.getBlue()<bMin) {
				bMin = thisColor.getBlue();
			}
		}
		rDist = rMax - rMin;
		gDist = gMax - gMin;
		bDist = bMax - bMin;
		
		for (int i=0;i<palette.size();i++) {
			Color thisColor = (Color)palette.get(i);
			int adjustedRed = thisColor.getRed()-rMin;
			double percentageRed = (double)adjustedRed/(double)rDist;
			int normalizedRed = (int)(255.0*percentageRed);
			int adjustedGreen = thisColor.getGreen()-gMin;
			double percentageGreen = (double)adjustedGreen/(double)gDist;
			int normalizedGreen = (int)(255.0*percentageGreen);
			int adjustedBlue = thisColor.getBlue()-bMin;
			double percentageBlue = (double)adjustedBlue/(double)bDist;
			int normalizedBlue = (int)(255.0*percentageBlue);
			Color normalizedColor = new Color(normalizedRed, normalizedGreen, normalizedBlue);
			palette.set(i, normalizedColor);
		}
		return palette;
	}
	
	public static Vector reducePalette(Vector palette) {
		int darkestColor = 255*3;
		int darkestColorIndex = 9;
		for (int i=0;i<palette.size();i++) {
			Color thisColor = (Color)palette.get(i);
			int darkness = thisColor.getRed()+thisColor.getGreen()+thisColor.getBlue();
			if (darkness<darkestColor) {
				darkestColor = darkness;
				darkestColorIndex = i;
			}
		}
		for (int i=darkestColorIndex; i<palette.size();i++) {
			Color thisColor = (Color)palette.get(i);
			int darkness = thisColor.getRed()+thisColor.getGreen()+thisColor.getBlue();
			if (darkness<(darkestColor+510)) {
				darkestColorIndex = i;
			} else {
				i = palette.size();
			}
		}
		
		Vector newPalette = new Vector();
		for (int i=0;i<=darkestColorIndex;i++) {
			newPalette.add(palette.elementAt(i));
		}
		
		return newPalette;
		
	}	
	
	public void initializeRobot() {
		sconn.sendFORTHToController("ROBOFORTH");
		sconn.sendFORTHToController("START");
		sconn.sendFORTHToController("CALIBRATE");
		sconn.sendFORTHToController("40000 SPEED !");
		sconn.sendFORTHToController("1000 ACCEL !");
	}

	public void homeRobot() {
		sconn.sendFORTHToController("HOME");
	}
	
	public void goToDefaultStart() {
		sconn.sendFORTHToController("READY");
		sconn.sendFORTHToController("CARTESIAN");
		setAbsoluteXYZ(robotObject.xOrigin+2500, robotObject.yOrigin+4000, 1000);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
	}
	

	public void cleanBrush() {
		
		Random rand = new Random();
		int  n = rand.nextInt(500);
		//sc.sendFORTHToController("0 1750 -1900 MOVETO");
		//sc.sendFORTHToController("0 4250 -1900 MOVETO");
		
		//Go over water dip in twice then retract
		setAbsoluteXYZ(robotObject.waterWellX, robotObject.waterWellY, robotObject.zRetracted);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		setAbsoluteXYZ(robotObject.waterWellX, robotObject.waterWellY, robotObject.waterWellZ/2);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		setAbsoluteXYZ(robotObject.waterWellX, robotObject.waterWellY, robotObject.waterWellZ);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		setAbsoluteXYZ(robotObject.waterWellX, robotObject.waterWellY, robotObject.waterWellZ/2);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		setAbsoluteXYZ(robotObject.waterWellX, robotObject.waterWellY, robotObject.waterWellZ);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		setAbsoluteXYZ(robotObject.waterWellX, robotObject.waterWellY, robotObject.zRetracted);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");

		//drag on water area
		setAbsoluteXYZ(robotObject.cleaningStartX, robotObject.cleaningStartY, robotObject.zRetracted);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		setAbsoluteXYZ(robotObject.cleaningStartX+n, robotObject.cleaningStartY, robotObject.zRetracted);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		setAbsoluteXYZ(robotObject.cleaningStartX+n, robotObject.cleaningStartY, robotObject.cleaningZ);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		
		double tenSteps = (robotObject.cleaningStartY - robotObject.cleaningEndY)/10;
		for (int i=0; i<10; i++) {
			setAbsoluteXYZ(robotObject.cleaningStartX+n, robotObject.cleaningStartY-(i*tenSteps), robotObject.cleaningZ);
			sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		}
		
		setAbsoluteXYZ(robotObject.cleaningEndX+n, robotObject.cleaningEndY, robotObject.cleaningZ);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");

		for (int i=0; i<5; i++) {
			setAbsoluteXYZ(robotObject.cleaningStartX+n, robotObject.cleaningEndY+(i*tenSteps), robotObject.cleaningZ);
			sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		}
		
		//setAbsoluteXYZ(robotObject.cleaningEndX+n, robotObject.cleaningStartY, robotObject.cleaningZ);
		//sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		setAbsoluteXYZ(robotObject.cleaningEndX+n, robotObject.cleaningStartY, robotObject.zRetracted);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		
		setAbsoluteXYZ(robotObject.xOrigin+2500, robotObject.yOrigin+4000, 1000);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		
	}
	
	public void homeBrush() {
		//sconn.sendFORTHToController("HOME");
		goToDefaultStart();
		
		sconn.sendFORTHToController("CALIBRATE");
		sconn.sendFORTHToController("40000 SPEED !");
		sconn.sendFORTHToController("1000 ACCEL !");

		goToDefaultStart();
		
		sconn.sendFORTHToController("READY");
		setAbsoluteXYZ(robotObject.xOrigin+2500, robotObject.yOrigin+4000, 1000);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
	}
	
	public void dipBrushInPaint(String currentStrokeColor, int strokeNumber) {
		//dip brush in color
		//public static double paintWellWidth = 15.0;  //cm
		//public static double[] tracksInPaintWell = new double[]{2.5, 3.5, 1.5, 3.0, 2.0};
		Color strokeColorObject = ColorUtils.getColorFromString(currentStrokeColor);
		Color bestMatchInPalette = this.getBestMatchColor(strokeColorObject,colorPaletteInWells);
		
		Point2D.Double wellPoint = robotObject.getPaintWellLocationInMm(bestMatchInPalette);
		double wellX = wellPoint.getX();
		double wellY = wellPoint.getY();
		double wellZ = robotObject.paintWellZ;
		int wiggle = 50;

		setAbsoluteXYZ(robotObject.xOrigin+robotObject.cameraXPosition, robotObject.yOrigin+robotObject.cameraYPosition, +robotObject.cameraZPosition);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); 

		//wait minute, then process image
		
		setAbsoluteXYZ(wellX, wellY, 2000);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX, wellY, 0);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX, wellY, wellZ+500);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX, wellY, wellZ);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		
		//wiggle
		setAbsoluteXYZ(wellX-wiggle, wellY, wellZ);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX+wiggle, wellY, wellZ);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		
		setAbsoluteXYZ(wellX, wellY, wellZ);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX, wellY+50, wellZ+250);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX, wellY, wellZ);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX, wellY+50, wellZ+250);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		
		if( waitForPaintDrip) {
			//go home on axis perpedicular to wells and move back and forth
			setAbsoluteXYZ(wellX-wiggle, wellY+50, wellZ+250);
			sconn.sendFORTHToController(getAbsoluteMoveToString());
			setAbsoluteXYZ(wellX+wiggle, wellY+50, wellZ+250);
			sconn.sendFORTHToController(getAbsoluteMoveToString());
			setAbsoluteXYZ(wellX-wiggle, wellY+50, wellZ+250);
			sconn.sendFORTHToController(getAbsoluteMoveToString());
			setAbsoluteXYZ(wellX+wiggle, wellY+50, wellZ+250);
			sconn.sendFORTHToController(getAbsoluteMoveToString());
			setAbsoluteXYZ(wellX-wiggle, wellY+50, wellZ+250);
			sconn.sendFORTHToController(getAbsoluteMoveToString());
			setAbsoluteXYZ(wellX+wiggle, wellY+50, wellZ+250);
			sconn.sendFORTHToController(getAbsoluteMoveToString());
			setAbsoluteXYZ(wellX-wiggle, wellY+50, wellZ+250);
			sconn.sendFORTHToController(getAbsoluteMoveToString());
			setAbsoluteXYZ(wellX+wiggle, wellY+50, wellZ+250);
			sconn.sendFORTHToController(getAbsoluteMoveToString());
			setAbsoluteXYZ(wellX-wiggle, wellY+50, wellZ+250);
			sconn.sendFORTHToController(getAbsoluteMoveToString());
			setAbsoluteXYZ(wellX+wiggle, wellY+50, wellZ+250);
			sconn.sendFORTHToController(getAbsoluteMoveToString());
			setAbsoluteXYZ(wellX-wiggle, wellY+50, wellZ+250);
			sconn.sendFORTHToController(getAbsoluteMoveToString());
			setAbsoluteXYZ(wellX+wiggle, wellY+50, wellZ+250);
			sconn.sendFORTHToController(getAbsoluteMoveToString());
		} 		
		
		setAbsoluteXYZ(wellX-800, wellY, wellZ+500);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX-800, wellY, wellZ+500);
		sconn.sendFORTHToController(getAbsoluteMoveToString());

		moveCloserToYOrigin(wellX-800, wellY, wellZ+500, robotObject.yOrigin, 2);
		
		
		//setAbsoluteXYZ(wellX-800, robotObject.yOrigin+1000, wellZ-500);
		//sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX-800, robotObject.yOrigin+2000, wellZ+500);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		//setAbsoluteXYZ(wellX-800, robotObject.yOrigin+3000, wellZ-500);
		//sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX-800, robotObject.yOrigin+4000, wellZ+500);
		sconn.sendFORTHToController(getAbsoluteMoveToString());

		/*
		setAbsoluteXYZ(wellX-800, robotObject.yOrigin+3000, wellZ-500);

		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX-800, robotObject.yOrigin+2000, wellZ-500);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX-800, robotObject.yOrigin+1000, wellZ-500);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX-800, robotObject.yOrigin, wellZ-500);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX-800, robotObject.yOrigin+1000, wellZ-500);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX-800, robotObject.yOrigin+2000, wellZ-500);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX-800, robotObject.yOrigin+3000, wellZ);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
       */
		setAbsoluteXYZ(wellX-600, robotObject.yOrigin+4000, 1000);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
/*

		setAbsoluteXYZ(wellX-600, wellY, wellZ+500);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX-600, wellY, wellZ);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX-600, wellY, wellZ+500);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(wellX-600, robotObject.yOrigin+4000, 1000);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		setAbsoluteXYZ(robotObject.xOrigin+2500, robotObject.yOrigin+4000, 1000);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
*/
	
	}
	
	
	
	public void moveCloserToYOrigin(double x, double startY, double z, double yOrigin, double steps ) {

		double diff = startY-yOrigin;
		double moveBy = diff/steps;
		for (int i=1; i<=steps; i++) {
			setAbsoluteXYZ(x, startY-(moveBy*i), z);
			sconn.sendFORTHToController(getAbsoluteMoveToString());
		}
	}
	
	public Color getBestMatchColor(Color colorToMatch, Vector palette) {
		
		Color closestColor = (Color)colorPaletteInWells.elementAt(0);

		Vector otherCloseMatches = new Vector();
		
		double smallestDifference = ColorUtils.getColourDistance(colorToMatch, closestColor);

		for (int i=1;i<colorPaletteInWells.size();i++) {
			double thisDifference = ColorUtils.getColourDistance(colorToMatch, (Color)colorPaletteInWells.elementAt(i));
			
			//if colorTolerance is ZERO - Only one return else, randomly select
			if(thisDifference<this.colorTolerance) {
				otherCloseMatches.add((Color)colorPaletteInWells.elementAt(i));
			}
			if (thisDifference<smallestDifference) {
				smallestDifference = thisDifference;
				closestColor = (Color)colorPaletteInWells.elementAt(i);
			}
		}

		if (this.colorTolerance==0 || otherCloseMatches.size()<2) {
			return closestColor;
		} else {
			int matches = otherCloseMatches.size();
			int randomlySelectedColor = getRandomNumberInRange(0,matches-1);
			return (Color)otherCloseMatches.elementAt(randomlySelectedColor);
		}

	}
	
	
	private static int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
	
	public void parkBrush() {
		
		
		setAbsoluteXYZ(robotObject.xOrigin+robotObject.cameraXPosition, robotObject.yOrigin+robotObject.cameraYPosition, +robotObject.cameraZPosition);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); 

		//wait minute, then process image
		try {
			//PINDARUNDO
			Thread.sleep(3000);
		}
		catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		File stream = new File(rootDir+"progress_stream.jpg");

		BufferedImage img = null;
		try {
		    img = ImageIO.read(stream);
		} catch (IOException e) {
		}
		try {
		   File snapshot = new File(rootDir+"progress_photo.jpg");
		   ImageIO.write(img, "jpg", snapshot);
		   
		   String paddedNumber = ElasticsearchUtility.paintingIdInMillion(nextStrokeNumber);
   		   File paddedOutputfile = new File(rootDir+indexName+"\\" + indexName+"_tl_"+paddedNumber+".jpg");
		   ImageIO.write(img, "jpg", paddedOutputfile);
		   
		} catch (IOException e) {
		}
		
		//ALSO REFRESH PALLETE
		
		colorPaletteInWells = refreshPalette(rootDir, indexName);
		robotObject.paintWells = colorPaletteInWells;   
		
		
		setAbsoluteXYZ(robotObject.xOrigin+2500, robotObject.yOrigin+4000, 1000);
		sconn.sendFORTHToController(getAbsoluteMoveToString());
		
		setAbsoluteXYZ(robotObject.waterWellX, robotObject.waterWellY, robotObject.zRetracted);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		setAbsoluteXYZ(robotObject.waterWellX, robotObject.waterWellY, robotObject.waterWellZ/2);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		setAbsoluteXYZ(robotObject.waterWellX, robotObject.waterWellY, robotObject.waterWellZ);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		
		//finalParkPosition();
		
		saveJsonStrokes();
		
	}
	
	/*
	public void finalParkPosition() {

		setAbsoluteXYZ(robotObject.waterWellX, robotObject.waterWellY, robotObject.waterWellZ);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		
	}

	public void unPark() {

		setAbsoluteXYZ(robotObject.waterWellX, robotObject.waterWellY, robotObject.zRetracted);
		sconn.sendFORTHToController(getAbsoluteMoveToString()); // move to X Y to above water \n");
		
		
	}
	*/
	
	public void saveJsonStrokes() {
		StringBuffer jsonStrokeStringBuffer = new StringBuffer();
		jsonStrokeStringBuffer.append("{   \"brushstrokes\": [ \n");
		for (int i=0;i<jsonStrokes.size();i++) {
			jsonStrokeStringBuffer.append(jsonStrokes.elementAt(i).toString()+", \n");
		}
		jsonStrokeStringBuffer.deleteCharAt(jsonStrokeStringBuffer.length()-3);
		jsonStrokeStringBuffer.append("] }");
		
		BufferedWriter writer;
		try {
			String rootFileDir = rootDir+indexName+"_strokes_"+TIMESIGNATURE+".json";
			writer = new BufferedWriter( new FileWriter( rootFileDir ));
			writer.write( jsonStrokeStringBuffer.toString());
			writer.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void paintStroke(JSONObject jsonStroke, int strokeNumber) {
		
		jsonStrokes.add(jsonStroke);
		
		String thisStrokeType = "";
		
		double thisStrokeLength = 0.0;
		double thisStrokeWidth = 0.0; 
		try {
			thisStrokeType = getStrokeFromJSON(jsonStroke);
			thisStrokeLength = getDoubleFromStrokeJSON(jsonStroke, "length_mm");
			thisStrokeWidth =  getDoubleFromStrokeJSON(jsonStroke, "width_mm");
		} catch (Exception e) {
			thisStrokeType = "notfound";
		}
		
		
		String firstAxisToMoveOnApproach = "xy";
		if (robotObject.nextPaintWellXShift > robotObject.nextPaintWellYShift) {
			firstAxisToMoveOnApproach = "y";
		} else {
			firstAxisToMoveOnApproach = "x";
		}
		
		//DEFAULT
		StrokeTemplate thisStroke = null;  //new FB_D_FlatBrush_WideFullStroke_SpinOnDot_NoRise(strokeNumber, jsonStroke, robotObject, 10.0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
			
		if (thisStrokeType.equalsIgnoreCase("RB_SS")) {
				thisStroke = new RB_SS_RoundBrush_SimpleStroke(strokeNumber, jsonStroke, robotObject, 0, firstAxisToMoveOnApproach); //10.0 is calibration UNUSED
		} else {
			//if stoke not found defaul to RB_SS
			thisStroke = new RB_SS_RoundBrush_SimpleStroke(strokeNumber, jsonStroke, robotObject, 0, firstAxisToMoveOnApproach); //10.0 is brushlength UNUSED
		}
		
		Vector iStrokes = thisStroke.getIndividualRoboForthLines();

		for (int ss=0;ss<iStrokes.size();ss++) {
			String gcodeLine = (String)iStrokes.elementAt(ss);
			if (!gcodeLine.substring(0,1).equalsIgnoreCase(";")) {
				sconn.sendFORTHToController(gcodeLine);
			}
		}
		
		//setAbsoluteXYZ(robotObject.xOrigin+2500, robotObject.yOrigin+4000, 1000);
		//sconn.sendFORTHToController(getAbsoluteMoveToString());
		
		
	}
	
	public static JSONObject getNextStrokeJSON(String rootDir, String sourceFileNameRoot, int strokeNumber) {
		JSONObject jsonO = new JSONObject();
		if (readFromFile) {
			jsonO = getBrushStrokeJSONFromFileString(rootDir+readFileName, strokeNumber);
		} else { 
			jsonO = getStrokeJSONFromDatabase(sourceFileNameRoot, strokeNumber);
		}		
		
		return jsonO;
	}
	
	
	static boolean fileAlreadyRead = false;
	static JSONObject brushStrokesJSON = new JSONObject();
	public static JSONObject getBrushStrokeJSONFromFileString(String fullFileName, int strokeNumber) {
		//krook_gray_02_strokes.json
		JSONObject jsonO = new JSONObject();
		try {
			if (!fileAlreadyRead) {
				brushStrokesJSON = ElasticsearchUtility.getJsonObjectFromFile(fullFileName);
				fileAlreadyRead = true;
			}

			JSONArray brushStrokeList = brushStrokesJSON.getJSONArray("brushstrokes");
			JSONObject brushStroke = brushStrokeList.getJSONObject(strokeNumber);
			jsonO = brushStroke;
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return jsonO;
	}

	public static JSONObject getStrokeJSONFromDatabase(String sourceFileNameRoot, int strokeNumber) {
		String JSONString = ElasticsearchUtility.sendGetStrokeRequest(sourceFileNameRoot, strokeNumber);
		try {
			JSONObject obj = new JSONObject(JSONString);
			JSONObject hitsobj = (JSONObject) obj.get("hits");
			if ((int)hitsobj.get("total") > 0) { 
				return obj;
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}
	
	
	
	
	public String getStrokeFromJSON(JSONObject strokeJSON) {
		JSONObject source = new JSONObject();
		if (strokeJSON.has("hits")) {
			JSONObject hits     = strokeJSON.getJSONObject("hits");
			JSONArray hitsArray     = hits.getJSONArray("hits");
			source     = hitsArray.getJSONObject(0).getJSONObject("_source");
		} else {
			source = strokeJSON;
		}
		JSONObject paint      = source.getJSONObject("stroke");
		String colorValue    = paint.getString("name");
		return colorValue;
	}
	
	public Double getDoubleFromStrokeJSON(JSONObject strokeJSON, String value) {
		JSONObject source = new JSONObject();
		if (strokeJSON.has("hits")) {
			JSONObject hits     = strokeJSON.getJSONObject("hits");
			JSONArray hitsArray     = hits.getJSONArray("hits");
			source     = hitsArray.getJSONObject(0).getJSONObject("_source");
		} else {
			source = strokeJSON;
		}
		JSONObject paint      = source.getJSONObject("stroke");
		double doubleVal    = paint.getDouble(value);
		return doubleVal;
	}
	
	
	public String getColorFromJSON(JSONObject strokeJSON) {
		JSONObject source = new JSONObject();
		if (strokeJSON.has("hits")) {
			JSONObject hits     = strokeJSON.getJSONObject("hits");
			JSONArray hitsArray     = hits.getJSONArray("hits");
			source     = hitsArray.getJSONObject(0).getJSONObject("_source");
		} else {
			source = strokeJSON;
		}
		JSONObject paint      = source.getJSONObject("paint");
		String colorValue    = paint.getString("color");
		return colorValue;
	}
	
	public double getStrokeLength(JSONObject strokeJSON) {
		JSONObject source = new JSONObject();
		if (strokeJSON.has("hits")) {
			JSONObject hits     = strokeJSON.getJSONObject("hits");
			JSONArray hitsArray     = hits.getJSONArray("hits");
			source     = hitsArray.getJSONObject(0).getJSONObject("_source");
		} else {
			source = strokeJSON;
		}
		double strokePoints    = 1.0*source.getInt("strokePoints");
		return strokePoints;
	}
	
	public static void saveSimulation(String rootDir, String indexName, String simulationFileName, int startIndex, int endIndex) {
			
			Vector jsonObjects = new Vector();
			for (int i=startIndex; i<=endIndex; i++) {
				 
				JSONObject stroke = getNextStrokeJSON(rootDir, indexName, i);
				if (stroke!=null) {
					jsonObjects.add(stroke);
				}
			}
			
			int canvasScale = 4;
			JSONObject hits     = ((JSONObject)jsonObjects.get(0)).getJSONObject("hits");
			JSONArray hitsArray     = hits.getJSONArray("hits");
			JSONObject source     = hitsArray.getJSONObject(0).getJSONObject("_source");
			JSONObject strokeCanvas = source.getJSONObject("strokeCanvas");
			int width = (((int)(strokeCanvas.getInt("width")*10))/10)*canvasScale;
			int height = (((int)(strokeCanvas.getInt("height")*10))/10)*canvasScale;
			JSONObject paint = source.getJSONObject("paint");
			String colorString = paint.getString("color");
			Color thisColor = ColorUtils.getColorFromString(colorString);
			BufferedImage simulation = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = simulation.createGraphics();
			g.setColor(Color.LIGHT_GRAY);
			g.fillRect(0, 0 , width, height);
			g.dispose();
			//read strokes 
			for (int i=0;i<jsonObjects.size();i++) {
				hits     = ((JSONObject)jsonObjects.get(i)).getJSONObject("hits");
				hitsArray     = hits.getJSONArray("hits");
				source     = hitsArray.getJSONObject(0).getJSONObject("_source");
				paint = source.getJSONObject("paint");
				colorString = paint.getString("color");
				thisColor = ColorUtils.getColorFromString(colorString);
				JSONArray pathArray = source.getJSONArray("strokePath");
				for (int j=0;j<pathArray.length()-1;j++) {
					JSONArray firstPoint = pathArray.getJSONArray(j);
					JSONArray secondPoint = pathArray.getJSONArray(j+1);
					
					int sx = firstPoint.getInt(0)*canvasScale; 
					int sy = firstPoint.getInt(1)*canvasScale; 
					int ex = secondPoint.getInt(0)*canvasScale;  
					int ey = secondPoint.getInt(1)*canvasScale; 
					g = simulation.createGraphics();
					g.setColor(thisColor);
					g.setStroke(new BasicStroke(3));
					g.drawLine(sx+2, height-sy-2, ex+2, height-ey-2);
					g.dispose();
					
				}
			}
			//scale * 4
			//get width
			//get height
			
			//make buffered image
			//iterate through strokes and draw lines
			
	 	    try {
	 	    	System.out.println("SavingSimulation: "+simulationFileName);
	 	    	File outputfile = new File(simulationFileName);
				ImageIO.write(simulation, "gif", outputfile);
		 	    System.out.println("Succesfully wrote: "+simulationFileName);
			}
			catch (IOException e) {
		 	    System.out.println("FAILED TO write: "+simulationFileName);
				e.printStackTrace();
			}
		}

	public String getColorFromJson() {
		return "";
	}

	public String getPointArrayFromJson() {
		return "";
	}
	


	
	
}