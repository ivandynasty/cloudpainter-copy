package com.cloudpainter.utils;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

public class TrapezoidImageTransformer
{

	BufferedImage warpedSourceImage;
	int destImageWidth;
	int destImageHeight;

	ArrayList<HorizontalLine> horizontalRows = new ArrayList<HorizontalLine>();

	public TrapezoidImageTransformer(
			BufferedImage warpedSourceImage, 
			int sourceTopLeftX, 
			int sourceTopLeftY, 
			int sourceTopRightX, 
			int sourceTopRightY, 
			int sourceBottomLeftX, 
			int sourceBottomLeftY, 
			int sourceBottomRightX,
			int sourceBottomRightY,
			int destWidth, 
			int destHeight ) {
		
		//TO DO with a 3x3 grid need to get these values and the midpoints
		//check lines, then check calculations
		//TOP
		//344,65                        632,77
		//mid 
		//291,277  						561,340
		//bottom
		//238,488						490,602
		
		
		destImageWidth = destWidth;//3;//destWidth;
		destImageHeight = destHeight;//3;//destHeight;

		//(a2+b2 = c2)
		double leftDistance = getPythagoreanDistance(sourceTopLeftX,sourceTopLeftY,sourceBottomLeftX,sourceBottomLeftY);
		double rightDistance = getPythagoreanDistance(sourceTopRightX,sourceTopRightY,sourceBottomRightX,sourceBottomRightY);
		double leftRightAspectRatio;
		if(leftDistance>rightDistance) {
			leftRightAspectRatio=1;
		} else {
			leftRightAspectRatio=leftDistance/rightDistance;
		}
		
		double bottomDistance = getPythagoreanDistance(sourceBottomLeftX,sourceBottomLeftY,sourceBottomRightX,sourceBottomRightY);
		double topDistance = getPythagoreanDistance(sourceTopLeftX,sourceTopLeftY,sourceTopRightX,sourceTopRightY);
		double bottomTopAspectRatio;
		if(bottomDistance>topDistance) {
			bottomTopAspectRatio=1;
		} else {
			bottomTopAspectRatio=bottomDistance/topDistance;
		}		
		
		//source is a quadish like trapezoid onsource image
		//dest is a rectangle
		//First make a bunch of rows 

		for (int i=0; i<destImageHeight; i++) {
			double percentOfHeight = (double)i/(double)(destImageHeight-1);
			double leftX = (sourceTopLeftX - ((sourceTopLeftX - sourceBottomLeftX)*percentOfHeight));
	        double leftY = (sourceTopLeftY - ((sourceTopLeftY - sourceBottomLeftY)*percentOfHeight));
			double rightX = (sourceTopRightX - ((sourceTopRightX - sourceBottomRightX)*percentOfHeight));
	 		double rightY = (sourceTopRightY - ((sourceTopRightY - sourceBottomRightY)*percentOfHeight));
	 		
			horizontalRows.add(new HorizontalLine(
					leftRightAspectRatio,
					bottomTopAspectRatio,
					(int)Math.round(leftX),
					(int)Math.round(leftY),
					(int)Math.round(rightX),
					(int)Math.round(rightY)
					));
		}
		this.warpedSourceImage = warpedSourceImage;
	}

	public BufferedImage GetUnwarpedImage() {
		BufferedImage unwarpedImage = new BufferedImage(destImageWidth,destImageHeight,BufferedImage.TYPE_INT_RGB);
		for (int i=0; i<destImageHeight;i++) {
			for (int j=0; j< destImageWidth;j++) {
				//System.out.println(" "+j+" : "+i);
				int transformedX = horizontalRows.get(i).getTransformedX(destImageWidth, j);
				int transformedY = horizontalRows.get(i).getTransformedY(destImageWidth, j);
				//System.out.println(" "+transformedX+" : "+transformedY);

				unwarpedImage.setRGB(j, i, warpedSourceImage.getRGB(transformedX, transformedY));
			}
		}
		return unwarpedImage;	
	}
	
	public static BufferedImage getDifferenceBufferedImage(BufferedImage contentImage, BufferedImage progressImage, double percentDone) {

		System.out.println("making difference image");
		int width = contentImage.getWidth();
		int height = contentImage.getHeight();
		BufferedImage differenceImage = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
		for (int x = 0; x<width; x++) {
			for (int y =0; y<height; y++) {
				int contentRGB = contentImage.getRGB(x, y);
				int progressRGB = progressImage.getRGB(x, y);
				Color contentPixelColor = new Color(contentRGB);
				Color progressPixelColor = new Color(progressRGB);
				Color colorDif = getColorDifference(contentPixelColor, progressPixelColor, percentDone);
				//System.out.println("x:" +x+" y:"+y+" colorDif:"+colorDif.getRed()+":"+colorDif.getGreen()+":"+colorDif.getBlue());
				
				differenceImage.setRGB(x, y, colorDif.getRGB());
			}
		}
		return differenceImage;
	}
	
	public static Color getColorDifference(Color contentPixel, Color progressPixel, double percentDone) {
		
		//Point1 has R1 G1 B1
		//Point2 has R2 G2 B2
		//Distance between colors is

		int redDiff = Math.abs(progressPixel.getRed()-contentPixel.getRed());
		int greenDiff = Math.abs(progressPixel.getGreen()-contentPixel.getGreen());
		int blueDiff = Math.abs(progressPixel.getBlue()-contentPixel.getBlue());
		double sumOfDiff = ( (redDiff*redDiff) +
	              (greenDiff*greenDiff)+
	              (blueDiff*blueDiff) ) ;
		double diff = Math.sqrt(sumOfDiff); 
				
		double diffPercentage = diff / (Math.sqrt((255*255)   +(255*255)+(255*255)));
		double contentValue = (contentPixel.getRed() + contentPixel.getGreen()+contentPixel.getBlue())/3;
		double progressValue = (progressPixel.getRed() + progressPixel.getGreen()+progressPixel.getBlue())/3;

		//99% = 100       10
		//50% = 10 sqrt    7%
		double exponentialDiffPercentage = (diffPercentage*diffPercentage);
		//System.out.println(diffPercentage+":::"+exponentialDiffPercentage);
		Color heatmapPixel = new Color(255,255,255);
		int intensity = (int) (255 - ((exponentialDiffPercentage*255)*(1-(percentDone/4))));
		if (intensity>255) {
			intensity = 255;
		} else if (intensity < 0) {
			intensity = 0;
		}
		
		if (contentValue>progressValue) {
			heatmapPixel = new Color(intensity, intensity, 255);
		} else {
			heatmapPixel = new Color(255, intensity, intensity);
		}
		//p=d/sqrt((255*255+(255*255+(255)*255)
		return heatmapPixel;
	}
	
	
	public double getPythagoreanDistance(double x1, double y1, double x2, double y2) {
		double a = (x2-x1)*(x2-x1); 
		double b = (y2-y1)*(y2-y1);
		return Math.sqrt((a+b));
	}
	
}

/*



try {
File f = new File(dir,l[x]);

BufferedImage img = null;
try {
    img = ImageIO.read(f);
} catch (IOException e) {
}

TrapezoidImageTransformer TrapImageConverter = new TrapezoidImageTransformer(
		img, 
		478, 105, 
		738, 90,
		352, 485,
		609, 558,
		6*18,
		6*24);
//163, 115
//298, 119
//94, 317
//231, 355

BufferedImage rectImage = TrapImageConverter.GetUnwarpedImage();
double percentDone = ((double)x/(double)l.length);
if (percentDone<0.01) { 
	percentDone = 0.01;
}

String newfilename = dir.toString()+File.separator+l[x].substring(0,l[x].indexOf("."));

try {
    File outputfile = new File(newfilename+"_flat.png");
    ImageIO.write(rectImage, "png", outputfile);
} catch (IOException e) {
    
}

BufferedImage diffImage = TrapImageConverter.getDifferenceBufferedImage(contentImage, rectImage, percentDone );


try {
    File heatmapfile = new File(newfilename+"_heatmap.png");
    ImageIO.write(diffImage, "png", heatmapfile);						
} catch (IOException e) {
    
}
}
catch (Exception e) {}

*/
