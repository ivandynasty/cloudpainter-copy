package com.cloudpainter.utils;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

public final class ColorUtils
{
	private ColorUtils(){}
	
	
	
	
	static public Vector sortColorVectorFromDarkToLight(Vector colorVector){
		for (int i=0; i<colorVector.size();i++) {
			for (int j=0; j<colorVector.size()-1;j++) {
				Color firstColor = (Color)colorVector.get(j);
				Color secondColor = (Color)colorVector.get(j+1);
				double averageRGB1 = getAverageRGB(firstColor);
				double averageRGB2 = getAverageRGB(secondColor);
				if (averageRGB1 < averageRGB2) {
					colorVector.set(j, secondColor);
					colorVector.set(j+1, firstColor);
				}
			}
		}
		return colorVector;
	}

	static public Vector sortColorFromUnltraVioletToInfraRed(Vector colorVector){
		for (int i=0; i<colorVector.size();i++) {
			for (int j=0; j<colorVector.size()-1;j++) {
				Color firstColor = (Color)colorVector.get(j);
				Color secondColor = (Color)colorVector.get(j+1);
				
				double firstColorHue =  RGB2HSV(firstColor)[0];
				double secondColorHue =  RGB2HSV(secondColor)[0];

				if (firstColorHue < secondColorHue) {
					colorVector.set(j, secondColor);
					colorVector.set(j+1, firstColor);
				}
			}
		}
		return colorVector;
	}

	
	public static double getAverageRGB(Color rgb) {
		return (rgb.getRed()+rgb.getGreen()+rgb.getBlue());
	}

	public static double[] RGB2HSV(Color thisColor){

		    double R = thisColor.getRed() / 255.0;
		    double G = thisColor.getGreen() / 255.0;
		    double B = thisColor.getBlue() / 255.0;

		    double min = Math.min(Math.min(R, G), B);
		    double max = Math.max(Math.max(R, G), B);
		    double delta = max - min;

		    double H = max;
		    double S = max;
		    double V = max;

		    if(delta == 0){
		        H = 0;
		        S = 0;
		    }else{

		        S = delta / max;

		        double delR = ( ( ( max - R ) / 6 ) + ( delta / 2 ) ) / delta;
		        double delG = ( ( ( max - G ) / 6 ) + ( delta / 2 ) ) / delta;
		        double delB = ( ( ( max - B ) / 6 ) + ( delta / 2 ) ) / delta;

		        if(R == max){
		            H = delB - delG;
		        }else if(G == max){
		            H = (1.0/3.0) + delR - delB;
		        }else if(B == max){
		            H = (2.0/3.0) + delG - delR;
		        }

		        if(H < 0) H += 1;
		        if(H > 1) H -= 1;
		    }

		    double[] hsv = new double[3];
		    hsv[0] = H;
		    hsv[1] = S;
		    hsv[2] = V;
		    return hsv;
		}
	
	static public Color getColorFromString(
			final String colorString ) {
		int r = 0;
		int g = 0;
		int b = 0;
		float a = 1.0F;
		try {
			if (colorString != null) {
				if (colorString.startsWith("#")) {
					if (colorString.length() == 7) {
						r = Integer.parseInt(
								colorString.substring(
										1,
										3),
								16);
						g = Integer.parseInt(
								colorString.substring(
										3,
										5),
								16);
						b = Integer.parseInt(
								colorString.substring(
										5,
										7),
								16);
					}
				}
				else if (colorString.startsWith("rgb")) {
					final String[] stringList = colorString.split("[(,)]");
					r = Integer.parseInt(stringList[1]);
					g = Integer.parseInt(stringList[2]);
					b = Integer.parseInt(stringList[3]);
					if (stringList.length == 5) {
						a = Float.parseFloat(stringList[4]);
					}
				}
			}
		}
		catch (final NumberFormatException e) {

		}
		return new Color(
				r,
				g,
				b,
				(int) (a * 255));
	}
	
	
	// d2 = (30*(r1-r2))**2 + (59*(g1-g2))**2 + (11*(b1-b2))**2;
	static private final double redWeight = 0.30;
	static private final double greenWeight = 0.59;
	static private final double blueWeight = 0.11;
	
	static public double getColourDistance(
			final Color c1,
			final Color c2 ) {
		final int r = c1.getRed() - c2.getRed();
		final int g = c1.getGreen() - c2.getGreen();
		final int b = c1.getBlue() - c2.getBlue();
		// double rmean = ( c1.getRed() + c2.getRed() )/2;
		final double weightR = redWeight; // 2 + rmean/256;
		final double weightG = greenWeight;// 4.0;
		final double weightB = blueWeight; // 2 + (255-rmean)/256;
		return Math.sqrt(weightR * r * r + weightG * g * g + weightB * b * b);
	}

	public static BufferedImage makeCopyOfBufferedImage(BufferedImage bi) {
		 ColorModel cm = bi.getColorModel();
		 boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		 WritableRaster raster = bi.copyData(null);
		 return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
		}
	
	
	public static void saveColorJsonFile(String fileName, Vector colorVector) {
	
		StringBuffer colorFileJson = new StringBuffer();
		colorFileJson.append("[\n");
		
		
		for (int i=0;i<colorVector.size();i++) {
			String colorAsHex = "#"+Integer.toHexString(((Color)colorVector.elementAt(i)).getRGB()).substring(2);
			colorFileJson.append(
					"{\"name\":\"\", \"color\":\""+colorAsHex+"\", \"medium\":\"high flow acrylic\", \"coverage_mm\":100, \"opacity\":0.5, \"viscosity\":0.5, \"notes\":\"liquitex white\"}"
					);
			if (i<colorVector.size()-1) {
				colorFileJson.append(",\n");
			}
		}
		colorFileJson.append("\n]\n");
		
		BufferedWriter writer;
		try {
			writer = new BufferedWriter( new FileWriter( fileName ));
			writer.write( colorFileJson.toString());
			writer.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	/*	[
		{"name":"", "color":"#f6edf5", "medium":"high flow acrylic", "coverage_cm":10, "opacity":0.5, "viscosity":0.5, "notes":"liquitex white"}, 
		{"name":"", "color":"#800d05", "medium":"high flow acrylic", "coverage_cm":10, "opacity":0.5, "viscosity":0.5, "notes":"liquitex 1.0 red"}
		]
	 */
		
	}
	
	public String getStrokeColorAsHexString(Color color) {
		String hex = "#"+Integer.toHexString(color.getRGB()).substring(2);
		return hex;
	}
	
}
