package com.cloudpainter.utils;


//import Base64;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.TimeUnit;




import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import com.cloudpainter.autopaint.BitmapStroke;
import com.cloudpainter.autopaint.RelativeStroke;
import com.cloudpainter.painting.Painting;
import com.cloudpainter.painting.BitmapPoint;
import com.cloudpainter.painting.BrushStroke;
import com.cloudpainter.painting.CanvasPoint;

public class ElasticsearchUtility
{

	public static void sendToElasticSearch(int paintingId, String paintingName, String strokeId, String payload) {
		try {
			String elasticEndPoint = "http://87b256626e4ad11cd37ac6d84dcba640.us-east-1.aws.found.io:9200/strokes_"+paintingName.toLowerCase()+"/brushstroke/"+strokeId;
			String elasticUser =     "pvanarman:Jjkl1asdb!";
			sendPostRequest(elasticEndPoint, elasticUser, payload);
			System.out.println("Saved to Elastic");
		} catch (Exception e){
			System.out.println("Error Saving to Elastic");
		}
	}

	public static String sendGetRequest(String paintingName) {

	    String name = "pvanarman";
	    String namet = "Jjkl1asdb!";
		
		Header[] headers = { new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"),	new BasicHeader("Role", "Read") };
		final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(name, namet));
		RestClient restClient = RestClient.builder(new HttpHost("87b256626e4ad11cd37ac6d84dcba640.us-east-1.aws.found.io", 9200)).setDefaultHeaders(headers).setHttpClientConfigCallback(
					new RestClientBuilder.HttpClientConfigCallback() {
						public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder arg0) {
							System.out.println("sendGetRequest() in callback");
							return arg0.setDefaultCredentialsProvider(credentialsProvider);
						}
					}).build();

		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("pretty", "true");
		//paramMap.put("q", "strokeId:2");
		
		
		Response response;
		try {
			//strokes_"+paintingIdInMillion(paintingId)+"_"+paintingName.toLowerCase()+"/brushstroke/
			response = restClient.performRequest("GET", "/*_"+paintingName.toLowerCase()+"/_search", paramMap);
			System.out.println(EntityUtils.toString(response.getEntity()));
			System.out.println("Host -" + response.getHost() );
			System.out.println("RequestLine -"+ response.getRequestLine() );
		
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("sendGetRequest() succeeded");
				
		return "";
	}

	public static String sendGetStrokeRequest(String paintingName, int strokeId) {

		String responseString = "";
		
	    String name = "pvanarman";
	    String namet = "Jjkl1asdb!";
		
		Header[] headers = { new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"),	new BasicHeader("Role", "Read") };
		final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(name, namet));
		RestClient restClient = RestClient.builder(new HttpHost("87b256626e4ad11cd37ac6d84dcba640.us-east-1.aws.found.io", 9200)).setDefaultHeaders(headers).setHttpClientConfigCallback(
					new RestClientBuilder.HttpClientConfigCallback() {
						public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder arg0) {
							System.out.println("sendGetRequest() in callback");
							return arg0.setDefaultCredentialsProvider(credentialsProvider);
						}
					}).build();

		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("pretty", "true");
		paramMap.put("q", "strokeNumber:"+strokeId);
		
		Response response;
		try {
			//strokes_"+paintingIdInMillion(paintingId)+"_"+paintingName.toLowerCase()+"/brushstroke/
			response = restClient.performRequest("GET", "/*"+paintingName.toLowerCase()+"/_search", paramMap);
			System.out.println("RESPONSE:");
			responseString = EntityUtils.toString(response.getEntity()); 
			System.out.println(responseString);
			System.out.println("Host -" + response.getHost() );
			System.out.println("RequestLine -"+ response.getRequestLine() );
			restClient.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		System.out.println("sendGetRequest() succeeded");
				
		return responseString ;
	}

	
	public static int getMaxStrokeNumber(String paintingName) {

		String responseString = "";
		
	    String name = "pvanarman";
	    String namet = "Jjkl1asdb!";
		
		Header[] headers = { new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"),	new BasicHeader("Role", "Read") };
		final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(name, namet));
		RestClient restClient = RestClient.builder(new HttpHost("87b256626e4ad11cd37ac6d84dcba640.us-east-1.aws.found.io", 9200)).setDefaultHeaders(headers).setHttpClientConfigCallback(
					new RestClientBuilder.HttpClientConfigCallback() {
						public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder arg0) {
							System.out.println("sendGetRequest() in callback");
							return arg0.setDefaultCredentialsProvider(credentialsProvider);
						}
					}).build();

		String queryAsJson = "{ \"sort\" : { \"strokeNumber\" : { \"order\" : \"desc\" } } }";

		
		
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("pretty", "true");
		paramMap.put("source", queryAsJson);

		
		Response response;
		try {
			//strokes_"+paintingIdInMillion(paintingId)+"_"+paintingName.toLowerCase()+"/brushstroke/
			response = restClient.performRequest("GET", "/*"+paintingName.toLowerCase()+"/_search", paramMap);
			System.out.println("RESPONSE:");
			responseString = EntityUtils.toString(response.getEntity()); 
			System.out.println(responseString.substring(0,100)+"...");
			System.out.println("Host -" + response.getHost() );
			System.out.println("RequestLine -"+ response.getRequestLine() );
			restClient.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int strokeNumber = 0;
		System.out.println("sendGetRequest() succeeded");
		try {
			JSONObject obj = new JSONObject(responseString);
			JSONObject hits     = obj.getJSONObject("hits");
			JSONArray hitsArray     = hits.getJSONArray("hits");
			JSONObject source     = hitsArray.getJSONObject(0).getJSONObject("_source");
			strokeNumber   = source.getInt("strokeNumber");
		} catch (Exception e) {
			strokeNumber = 0;
		}
		System.out.println("maxStrokeNumberInElastic = "+strokeNumber);
		
		return strokeNumber ;
	}
	
	
	
	public static String sendPostRequest(String requestUrl, String auth, String payload) {
	
 
	    String authStr = auth;
	    String authEncoded = Base64.encodeBytes(authStr.getBytes());
	
		
		StringBuffer jsonString = new StringBuffer();
		try {
	        URL url = new URL(requestUrl);
	        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	        connection.setRequestProperty("Authorization", "Basic " + authEncoded);
	        connection.setDoInput(true);
	        connection.setDoOutput(true);
	        connection.setRequestMethod("POST");
	        connection.setRequestProperty("Accept", "application/json");
	        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	        connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
	        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
	        writer.write(payload);
	        writer.close();
	        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	 
	        String line;
	        while ((line = br.readLine()) != null) {
	                jsonString.append(line);
	        }
	        br.close();
	        connection.disconnect();
	       // count++;
	    	System.out.println("posted to elastic");
	    } catch (Exception e) {
	    	System.out.println("failed to post to elastic");
	    	//failcount++;
	            //throw new RuntimeException(e.getMessage());
	    }
		try {
			TimeUnit.MILLISECONDS.sleep(10);
		}
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return jsonString.toString();
	    
	}
	
	////////////////////////////
	////////////////////////////
	////////////////////////////
	////////////////////////////

	
	public static void SendStrokesToElasticSearchCluster(int strokeNumber, BrushStroke stroke, Painting painting) {
		System.out.println(stroke.getId());
		System.out.println(painting.getId());
		StringBuffer json = new StringBuffer();
		json.append( "{ ");
		json.append( "\"paintingName\":\""+painting.getName()+"\"");
		json.append( ",\"canvasHeight\":"+painting.getHeight());
		json.append( ",\"canvasWidth\":"+painting.getWidth());
		json.append( ",\"strokeNumber\":"+strokeNumber);
		json.append( ",\"id\":"+stroke.getId());
		json.append( ",\"source\":\""+stroke.getSource()+"\"");
		json.append( ",\"length\":"+stroke.getLength()+"");
		json.append( ",\"timeStamp\":"+stroke.getTimeStamp());
		json.append( ",\"color\":\""+stroke.getColor().getHexColor()+"\"");
		json.append( ",\"brushSize\":"+stroke.getBrushSize());
		json.append( ",\"numberPoints\":"+stroke.getPoints().size());
		json.append( ","+getJsonGeometricStrokeDescription(stroke) );
		json.append( " }");
		
		System.out.println(json.toString());
		sendToElasticSearch(painting.getId(), painting.getName(), ""+strokeNumber, json.toString());
		
	}

	public static void SaveAutomatedStrokesToElasticSearchCluster(String imageName, int imageWidth, int imageHeight, int strokeNumber, BitmapStroke bitmapStroke, boolean invertY) {

		Date now = new Date();
		long timestamp = now.getTime();
		System.out.println(getElasticDateString());
		
		StringBuffer json = new StringBuffer();
		json.append     ("{ ");
		json.append     ("\"paintingId\":"    	+timestamp+","); 
		json.append     ("\"paintingName\":\""  +imageName+"\","); 
		json.append     ("\"paintingSeries\":\""  +imageName+"\","); 
		json.append     ("\"strokeNumber\":"   		+strokeNumber+",");
		json.append     ("\"timeStamp\":"  		+timestamp+",");
		json.append     ("\"startDate\":\""     +getElasticDateString()+"\",");
		json.append     ("\"paintingData\":"   	+"\"autopainted\",");
		json.append     ("\"color\":"      		+"\""+(bitmapStroke.getStrokeColorAsHexString())+"\",");
		json.append     ("\"paint\":"      		+getPaintJson(bitmapStroke.getStrokeColor())+",");
		String todoBrush = "{ \"name\": \"FB_D\", \"width_mm\": 2.5, \"length_mm\": 2.5,  \"depth_mm\": 15.0, \"stiffness\": 0.5, \"shape\": \"Flat Brush Default\", \"material\": \"synthetic\" }";
		json.append     ("\"brush\":"      		+todoBrush+",");
		json.append     ("\"stroke\":"		    +todoBrush+",");
		json.append     ("\"mode\":"       		+"\"ai\",");
		json.append     ("\"strokeCanvas\":"	+getStrokeCanvasJson(imageWidth, imageHeight)+",");
		json.append     ("\"strokePoints\":"	+(bitmapStroke).getAllPoints().size()+",");
		json.append     ("\"strokePath\":" 		+getPathStrokeJson( bitmapStroke, imageHeight, invertY));
		json.append     ("}");
		
		System.out.println(json.toString());
		
		sendToElasticSearch(1001, imageName, ""+strokeNumber, json.toString());
		
	}
	
	public static void SaveAutomatedStrokesToJsonTextFile(String fileName, String imageName, int imageWidth, int imageHeight, Vector bitmapStrokes, boolean invertY) {
		StringBuffer bigString = new StringBuffer();
		bigString.append("{	\"brushstrokes\": [\n");
		int strokes = bitmapStrokes.size();
		for (int i=0; i<strokes; i++) {

			Date now = new Date();
			long timestamp = now.getTime();
			StringBuffer json = new StringBuffer();
			json.append     ("{ ");
			json.append     ("\"paintingId\":"    	+timestamp+","); 
			json.append     ("\"paintingName\":\""  +imageName+"\","); 
			json.append     ("\"paintingSeries\":\""  +imageName+"\","); 
			json.append     ("\"strokeNumber\":"   		+i+",");
			json.append     ("\"timeStamp\":"  		+timestamp+",");
			json.append     ("\"startDate\":\""     +getElasticDateString()+"\",");
			json.append     ("\"paintingData\":"   	+"\"autopainted\",");
			json.append     ("\"color\":"      		+"\""+((BitmapStroke)bitmapStrokes.get(i)).getStrokeColorAsHexString()+"\",");
			json.append     ("\"paint\":"      		+getPaintJson(((BitmapStroke)bitmapStrokes.get(i)).getStrokeColor())+",");
			String todoBrush = "{ \"name\": \"FB_D\", \"width_mm\": 2.5, \"length_mm\": 2.5,  \"depth_mm\": 15.0, \"stiffness\": 0.5, \"shape\": \"Flat Brush Default\", \"material\": \"synthetic\" }";
			json.append     ("\"brush\":"      		+todoBrush+",");
			json.append     ("\"stroke\":"		    +todoBrush+",");
			json.append     ("\"mode\":"       		+"\"ai\",");
			json.append     ("\"strokeCanvas\":"	+getStrokeCanvasJson(imageWidth, imageHeight)+",");
			json.append     ("\"strokePoints\":"	+((BitmapStroke)bitmapStrokes.get(i)).getAllPoints().size()+",");
			json.append     ("\"strokePath\":" 		+getPathStrokeJson( (BitmapStroke)bitmapStrokes.get(i), imageHeight, invertY));
			json.append     ("}");
			if (i!=strokes-1) {
				json.append     (",\n");
			}
			bigString.append(json.toString());
			
		}		
		bigString.append("\n] }");
		//System.out.println(bigString);
		
		BufferedWriter writer;
		try {
			writer = new BufferedWriter( new FileWriter( fileName ));
			writer.write( bigString.toString());
			writer.close();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	////////////////////////////
	////////////////////////////
	////////////////////////////
	////////////////////////////
	

	public static String getPaintJson(Color color) {
			
		  //HEX COLOR
  		  String hexColor = String.format("#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue());		
		  
  		  StringBuffer strokeColorJson = new StringBuffer();
		  strokeColorJson.append("{ ");
		  strokeColorJson.append("\"name\": \""+hexColor+"\", ");
		  strokeColorJson.append("\"color\": \""+hexColor+"\", ");
		  strokeColorJson.append("\"medium\": \"high flow acrylic\", ");
		  strokeColorJson.append("\"coverage_mm\": 100, ");
		  strokeColorJson.append("\"opacity\": 0.5, ");
		  strokeColorJson.append("\"viscosity\": 0.5, ");
		  strokeColorJson.append("\"notes\": \"none\" ");
		  strokeColorJson.append("}");
		  return strokeColorJson.toString();
	}
	
	public static String getStrokeCanvasJson(int width, int height) {
		  StringBuffer strokeCanvasJson = new StringBuffer();
		  strokeCanvasJson.append("{ ");
		  strokeCanvasJson.append("\"x\": 0, ");
		  strokeCanvasJson.append("\"y\": 0, ");
		  strokeCanvasJson.append("\"width\": "+width+", ");
		  strokeCanvasJson.append("\"height\": "+height+", ");
		  strokeCanvasJson.append("\"right\": "+width+", ");
		  strokeCanvasJson.append("\"left\": 0, ");
		  strokeCanvasJson.append("\"top\": "+height+", ");
		  strokeCanvasJson.append("\"bottom\": 0 ");

		  strokeCanvasJson.append("}");
		  return strokeCanvasJson.toString();
	}
	
	public static String getPathStrokeJson(BitmapStroke stroke, int height, boolean invertY) {

		StringBuffer strokeJson = new StringBuffer();
		int numStrokes = stroke.getAllPoints().size();
		//((BitmapStroke)bitmapStrokes.get(i)).getAllPoints().size()
  	  	strokeJson.append(" [");
		
		for (int i=0;i<numStrokes; i++) { 
			BitmapPoint thisPoint = (BitmapPoint)stroke.getAllPoints().elementAt(i);
			int pointX = thisPoint.getX();
		    int pointY = thisPoint.getY();
		    if(invertY) {
			  pointY = height-pointY;
		    }
		  strokeJson.append(" [ ");
		  strokeJson.append(""+pointX+", "+pointY+"");
		  strokeJson.append("],");
		}
		strokeJson.deleteCharAt(strokeJson.length()-1);
		strokeJson.append(" ] ");
		return strokeJson.toString();		  
		  
	}
	
	public static String getJsonGeometricStrokeDescription(BrushStroke stroke) {
		int numberPoints = stroke.getPoints().size();

		double maxX = stroke.getPoints().get(0).getX();
		double minX = stroke.getPoints().get(0).getX();
		double totalX = stroke.getPoints().get(0).getX();

		double maxY = stroke.getPoints().get(0).getY();
		double minY = stroke.getPoints().get(0).getY();
		double totalY = stroke.getPoints().get(0).getY();
		
		for (int i=1; i<numberPoints; i++) {
			double thisX = stroke.getPoints().get(i).getX();
			double thisY = stroke.getPoints().get(i).getY();
			if ( thisX > maxX) {
				maxX = thisX;
			}
			if ( thisX < minX) {
				minX = thisX;
			}
			totalX += thisX;
			if ( thisY > maxY) {
				maxY = thisY;
			}
			if ( thisY < minY) {
				minY = thisY;
			}
			totalY += thisY;
		}
		
		double midX = (maxX+minX)/2;
		double averageX = totalX/numberPoints;
		double midY = (maxY+minY)/2;
		double averageY = totalY/numberPoints;

		double slope = 0;
		try {
			slope = (maxY-minY)/(maxX-minX);
		} catch (Exception e) {
			slope = 1000000000;
		}
		
		int decPlaces = 3;

		StringBuffer json = new StringBuffer();
		json.append( "\"firstPoint\":["+round(((CanvasPoint)stroke.getPoints().get(0)).getX(),decPlaces)+","+round(((CanvasPoint)stroke.getPoints().get(0)).getY() ,decPlaces)+"]") ;
		json.append( ",\"lastPoint\":["+round(((CanvasPoint)stroke.getPoints().get(0)).getX(),decPlaces) +","+round(((CanvasPoint)stroke.getPoints().get(0)).getY(),decPlaces)+"]") ;
		json.append( ",\"pathBound\":[ ["+round(minX,decPlaces)+","+round(maxY,decPlaces)+"],["+round(maxX,decPlaces)+","+round(maxY,decPlaces)+"],["+round(maxX,decPlaces)+","+round(minY,decPlaces)+"],["+round(minX,decPlaces)+","+round(minY,decPlaces)+"] ]" );
		json.append( ",\"pathBoundCenter\":["+round((midX),decPlaces)+","+round((midY),decPlaces)+" ]");
		json.append( ",\"pathAveragePoint\":["+round((averageX),decPlaces)+","+round((averageY),decPlaces)+" ]");
		json.append( ",\"path\":[ ");
		for (int i=0;i<numberPoints;i++) {
			if (i>0 && i<numberPoints) {
				json.append(",");
			}
			json.append("["+round(stroke.getPoints().get(i).getX(),decPlaces)+","+round(stroke.getPoints().get(i).getY(),decPlaces)+"]");
		}
		json.append( " ]");
		json.append( ",\"path4d\":[ ");
		for (int i=0;i<numberPoints;i++) {
			if (i>0 && i<numberPoints) {
				json.append(",");
			}
			json.append("["+round(stroke.getPoints().get(i).getX(),decPlaces)+","+round(stroke.getPoints().get(i).getY(),decPlaces)+",1.0,"+stroke.getPointTimes().get(i)+"]");
		}
		json.append( " ]");
		
		//yyyy/MM/dd HH:mm:ss
		
		Date thisDate = new Date(stroke.getPointTimes().get(0));
		 Calendar cal = Calendar.getInstance();
	    cal.setTime(thisDate);
	    int year = cal.get(Calendar.YEAR);
	    int month = cal.get(Calendar.MONTH)+1;
	    int day = cal.get(Calendar.DAY_OF_MONTH);
		String thisYearString = ""+year;
		String thisMonthString = ""+month;
		String thisDayString = ""+day;
	    
		int thisHour = cal.get(Calendar.HOUR_OF_DAY);
		int thisMinute = cal.get(Calendar.MINUTE);
		int thisSecond = cal.get(Calendar.SECOND);
		String thisHourString = ""+thisHour;
		String thisMinuteString = ""+thisMinute;
		String thisSecondString = ""+thisSecond;
		
		if ((month)<10) {
			thisMonthString = "0"+(month);
		}
		if (day<10) {
			thisDayString = "0"+day;
		}
		
		if (thisHour<10) {
			thisHourString = "0"+thisHour;
		}
		if (thisMinute<10) {
			thisMinuteString = "0"+thisMinute;
		}
		if (thisSecond<10) {
			thisSecondString = "0"+thisSecond;
		}
		
	    String dateString = ""+thisYearString+"/"+(thisMonthString)+"/"+thisDayString+" "+thisHourString+":"+thisMinuteString+":"+thisSecondString;
				
    	json.append( ",\"startDate\":\""+dateString+"\"");
		json.append( ",\"startTime\":"+((stroke.getPointTimes().get(0))));
		json.append( ",\"duration\":"+(stroke.getPointTimes().get(numberPoints-1)-stroke.getPointTimes().get(0)) );
		
		return json.toString();
	}

	public static String getElasticDateString() {
	Date thisDate = new Date();
    Calendar cal = Calendar.getInstance();
    cal.setTime(thisDate);
    int year = cal.get(Calendar.YEAR);
    int month = cal.get(Calendar.MONTH)+1;
    int day = cal.get(Calendar.DAY_OF_MONTH);
	String thisYearString = ""+year;
	String thisMonthString = ""+month;
	String thisDayString = ""+day;
   
	int thisHour = cal.get(Calendar.HOUR_OF_DAY);
	int thisMinute = cal.get(Calendar.MINUTE);
	int thisSecond = cal.get(Calendar.SECOND);
	String thisHourString = ""+thisHour;
	String thisMinuteString = ""+thisMinute;
	String thisSecondString = ""+thisSecond;
	
	if ((month)<10) {
		thisMonthString = "0"+(month);
	}
	if (day<10) {
		thisDayString = "0"+day;
	}
	
	if (thisHour<10) {
		thisHourString = "0"+thisHour;
	}
	if (thisMinute<10) {
		thisMinuteString = "0"+thisMinute;
	}
	if (thisSecond<10) {
		thisSecondString = "0"+thisSecond;
	}
	
   String dateString = ""+thisYearString+"/"+(thisMonthString)+"/"+thisDayString+" "+thisHourString+":"+thisMinuteString+":"+thisSecondString;
   return dateString;
}

	public static String paintingIdInMillion(int id) {
		
		if (id<10) {
			return "00000000"+id;
		}
		if (id<100) {
			return "0000000"+id;
		}
		if (id<1000) {
			return "000000"+id;
		}
		if (id<10000) {
			return "00000"+id;
		}
		if (id<100000) {
			return "0000"+id;
		}
		if (id<1000000) {
			return "000"+id;
		}
		if (id<10000000) {
			return "00"+id;
		}
		if (id<100000000) {
			return "0"+id;
		}
		return ""+id;
		
	}

	public static double round(double number, int places) {
		double roundedNumber;
		int tempRoundNumber;
		double multiplier = 1;
		for (int i = 0; i<places; i++) {
			multiplier=multiplier*10;
		}
		roundedNumber = number * multiplier;
		tempRoundNumber = (int)Math.round(roundedNumber);
		roundedNumber = ((double)tempRoundNumber)/multiplier;		
		return roundedNumber;
	}
	
	public static JSONObject getJsonObjectFromFile(String filename) throws IOException {
		JSONObject jsonObject = new JSONObject();
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			try {
			    StringBuilder sb = new StringBuilder();
			    String line = br.readLine();
		
			    while (line != null) {
			        sb.append(line);
			        sb.append(System.lineSeparator());
			        line = br.readLine();
			    }
			    String everything = sb.toString();
		        jsonObject = new JSONObject(everything);
			} finally {
			    br.close();
			}
		} catch (IOException ioe) {
			return null;
		}
		return jsonObject;
	}	
	
	public static JSONArray getJsonArrayFromFile(String filename) throws IOException {
		JSONArray jsonArray = new JSONArray();
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			try {
			    StringBuilder sb = new StringBuilder();
			    String line = br.readLine();
		
			    while (line != null) {
			        sb.append(line);
			        sb.append(System.lineSeparator());
			        line = br.readLine();
			    }
			    String everything = sb.toString();
		        jsonArray = new JSONArray(everything);
			} finally {
			    br.close();
			}
		} catch (IOException ioe) {
			return null;
		}
		return jsonArray;
	}	
	
	
}




