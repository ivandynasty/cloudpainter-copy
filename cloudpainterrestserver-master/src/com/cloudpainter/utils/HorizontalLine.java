package com.cloudpainter.utils;

public class HorizontalLine
{

	double leftRightAspectRatio;
	double bottomTopAspectRatio;
	
	int leftX;
	int leftY;
	int rightX;
	int rightY;

	HorizontalLine(double leftRightAspectRatio, double bottomTopAspectRatio, int leftX, int leftY, int rightX, int rightY) {

		//Very Important Concept => this is ratio of left sides height to right sides height
		//needed to calculate perspective - inch on shorter side will be smaller in image than inch on taller side
		//Camera is designed to look from right side, so left side of painting is always more distant and smaller.
		//If this changes, this code will prolly have to change as well.
		this.leftRightAspectRatio = leftRightAspectRatio;
		this.bottomTopAspectRatio = bottomTopAspectRatio;

		this.leftX =leftX;
		this.leftY = leftY;
		this.rightX = rightX;
		this.rightY = rightY;
	}

	public double getPercentOfPerspectiveWidth(double percentOfWidth) {

		double ratioDelta = 1.0-leftRightAspectRatio;  
		double averageRatio = leftRightAspectRatio+(ratioDelta*(percentOfWidth));
		double percentOfPerspective = (averageRatio)*(percentOfWidth);
		return percentOfPerspective;
	}

	public double getPercentOfPerspectiveHeight(double percentOfHeight) {
		double ratioDelta = 1.0-bottomTopAspectRatio;  
		double averageRatio = bottomTopAspectRatio+(ratioDelta*(percentOfHeight));
		double percentOfPerspective = (averageRatio)*(percentOfHeight);
		return percentOfPerspective;
	}
	
	public int getTransformedX(int width, int xColumn) {
		//percentOfWidth is actual location on canvas.  When brush is in middle, it is at 50% mark
		double percentOfWidth = (double)xColumn/(double)(width-1);
		//percentOfPerspective is the percentWidth calculated for perspective.
		double percentOfPerspective = getPercentOfPerspectiveWidth(percentOfWidth);
		double lineWidthX = (double)(rightX-leftX);				
		double xPos = leftX+(lineWidthX*percentOfPerspective);
		return (int)Math.round(xPos);
	}

	public int getTransformedY(int height, int yRow) {
		//percentOfWidth is actual location on canvas.  When brush is in middle, it is at 50% mark
		double percentOfHeight = (double)yRow/(double)(height-1);
		//percentOfPerspective is the percentWidth calculated for perspective.
		double percentOfPerspective = getPercentOfPerspectiveHeight(percentOfHeight);
		double lineHeightY = (double)(rightY-leftY);
		double yPos = leftY+(lineHeightY*percentOfPerspective);
		return (int)Math.round(yPos);
	}

	
	
}
