package com.cloudpainter.aistrokes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Vector;

import javax.imageio.ImageIO;

import com.cloudpainter.autopaint.BitmapStroke;
import com.cloudpainter.autopaint.PaintMap;
import com.cloudpainter.autopaint.PaintMapColor;
import com.cloudpainter.autopaint.PaintMapDirection;
import com.cloudpainter.autopaint.StrokeVectors;
import com.cloudpainter.autopaint.VectorMap;
import com.cloudpainter.painting.BitmapPoint;
import com.cloudpainter.utils.ColorUtils;
import com.cloudpainter.utils.ElasticsearchUtility;
import com.cloudpainter.utils.TrapezoidImageTransformer;

public class AIStrokes
{

	
	int aiDecisionNumber = 0;

	PaintMap paintmap;
	VectorMap vectormap;
	
	Vector<BitmapStroke> brushStrokes = new Vector();
	
	
	BufferedImage lastGoalImage = null;
	
	BufferedImage goalImage;//      = new BufferedImage(0, 0, 0);
	BufferedImage progressImage;//  = new BufferedImage(0, 0, 0);

	//int imageWidth = 0;
	//int imageHeight = 0;

	Point greatestDif = new Point(0,0); 
	
	public static void main(String[] args) {
		AIStrokes ais = new AIStrokes();
		String goalImageFileLocation = "C:\\cloudpainterrestserver\\currentpainting\\robotportrait_001.gif";
		String progressImageFileLocation = "C:\\cloudpainterrestserver\\currentpainting\\robotportrait_001_1progress.jpg";
		String differenceImageFileLocation = "C:\\cloudpainterrestserver\\currentpainting\\robotportrait_001_dif.gif";
		//ais.generateAiStrokes(goalImageFileLocation, progressImageFileLocation, differenceImageFileLocation, 1);
		String timelapseFolder = "C:\\cloudpainterrestserver\\currentpainting\\robotportrait_001\\";
		ais.generateAiStrokes(true, "C:\\cloudpainterrestserver\\currentpainting\\", "robotportrait_001" , 1, Color.WHITE);
	}
	
	public PaintMap getPaintmap() {
		return paintmap;
	}
	
	public int generateAiStrokes(boolean executeAIStrokes, String rootDir, String indexName, int numberOfStrokes, Color lastColor) {
		//MAKING GOAL IMAGE
		String timelapseFolder = rootDir+indexName+"\\";
		File makeDir = new File(rootDir+indexName);
		makeDir.mkdir();
		String goalImageFileLocation = rootDir+indexName+"_"+"0goal.gif";
		String progressImageFileLocation = rootDir+indexName+"_"+"1progress.jpg";
		String differenceImageFileLocation = rootDir+indexName+"_"+"dif.gif";
		String heatmapImageFileLocation = rootDir+indexName+"_"+"_heatmap.gif";
		String jsonFileName = rootDir+indexName+"_"+"strokes.json";
		

		return generateAiStrokes(executeAIStrokes, indexName, goalImageFileLocation, heatmapImageFileLocation, progressImageFileLocation, differenceImageFileLocation, jsonFileName, timelapseFolder, numberOfStrokes, lastColor);
	}
	
	public void updatePaintmapBufferedImage(BufferedImage imageWithRedCompleted) {
		
	}
	
	
	public int getRandomInteger(
			final int size ) {
		
		// creates random number between 0 and (size-1)
		final Random randomGenerator = new Random();
		final int randomInt = randomGenerator.nextInt(size);
		return randomInt;

	}
	
	public void generatePaintMap(BufferedImage bi) {
		paintmap = new PaintMap(
				bi.getWidth(),
				bi.getHeight(),
				1);
		paintmap.createPaintMapFromBufferedImage(bi);
	}
	
	
	public VectorMap getVectorMap() {
		return vectormap;	
	}
	
	public void generateVectorMap(BufferedImage bi) {
		
		int width = bi.getWidth();
		int height = bi.getHeight();

		vectormap = new VectorMap(width, height);
		
		//Stroke Vectors - Each PIXEL has largest Vector Drawn on it
		StrokeVectors allStrokes = new StrokeVectors();

		//Vector Map - each pixel has record of every stroke through it

		VectorMap allVectors = new VectorMap(width, height);
		System.out.println("creating vector map");
		for (int yy=0; yy<height;yy++) {
			for (int xx=0; xx<width;xx++) {
				BitmapStroke longestStroke = this.getLongestSingleStrokeAtPosition(bi, new Point(xx , yy));
				allStrokes.addStrokes( longestStroke );
				allVectors.addVectorToPixelVector(longestStroke);
			}
		}
		
		for (int yy=0; yy<height;yy++) {
			for (int xx=0; xx<width;xx++) {		

				Vector thisVector = allVectors.getPixelVectors(xx,yy);
				float totalAngle = 0;
				int weight = 0;
				Color thisColor = new Color(0);

				for (int q=0; q<thisVector.size(); q++ ) {
					BitmapStroke ls = (BitmapStroke)thisVector.elementAt(q);
					thisColor = ls.getStrokeColor();
					BitmapPoint bp1 = ls.getStart();
					BitmapPoint bp2 = ls.getEnd();
					float a1 = (float) VectorMap.GetAngleOfLineBetweenTwoPoints((double)bp1.getX(), (double)bp1.getY(), (double)bp2.getX(), (double)bp2.getY());
					totalAngle += a1;
				}
				float avgDegreeAngle = (totalAngle/thisVector.size());
				float avgRadianAngle = (float) (avgDegreeAngle * Math.PI / 180);
				int startX = xx;
				int startY = yy;
				int endX = (int) (startX + (3 * Math.cos(avgRadianAngle)));
				int endY = (int) (startY + (3 * Math.sin(avgRadianAngle)));
				if (endX>0 && endY>0 ) {
					//drawStrokeVectorsOnImage(strokeImage , new BitmapPoint(startX, startY), new BitmapPoint(endX, endY), Color.RED, false);
					vectormap.setVectorAngle(xx, yy, avgDegreeAngle);
				}
			}
		}
	}

	public BitmapPoint getEndBitmapPoint(BitmapPoint startXY, float avgDegreeAngle) {
		float avgRadianAngle = (float) (avgDegreeAngle * Math.PI / 180);
		int startX = startXY.getX();
		int startY = startXY.getY();
		int endX = (int) (startX + (3 * Math.cos(avgRadianAngle)));
		int endY = (int) (startY + (3 * Math.sin(avgRadianAngle)));
		BitmapPoint endPoint = new BitmapPoint(endX, endY);
		return endPoint;
	}

	
	public BitmapStroke generateSingleStrokeFromVectorMap(int xx, int yy, int maxStrokeLengthInPixels, boolean recordOnPaintMap) {
		

		
		Color thisColor = paintmap.getPaintMapColorFromPaintMapXY(xx, yy);
		BitmapPoint bp1 = new BitmapPoint(xx,yy);
		BitmapPoint bp2 = new BitmapPoint(xx,yy);
		BitmapStroke bs = new BitmapStroke(bp1, bp2, thisColor);
		//get angle in radians
		//walk as far north / northwest / or northeast as possible same color and not already painted
		
		//TURNING NORTH WALK BACK ON
		
		  final ArrayList<PaintMapDirection> strokeDirectionSequence = new ArrayList<PaintMapDirection>();
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_NORTH_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_NORTH_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_EAST);
		  int strokeDirectionIndex = 0;
		  PaintMapDirection longestPaintMapDirection = PaintMapDirection.NORTH;
		  double longestStroke = 0;
		  BitmapStroke longestBitmapStroke = new BitmapStroke(new BitmapPoint(0,0),new BitmapPoint(0,0), thisColor);
		  boolean strokeFound = false;
		  int maxDistanceToEdgeInPixels = 20;
		  int minStrokeLength = 0;
		  int equidistantStrokesFound = 0;
		  for (int direction= 0; direction<strokeDirectionSequence.size();direction++) {
				Color colorAtPoint = thisColor;
				if ( colorAtPoint.equals(thisColor )) {
					BitmapPoint startPoint = new BitmapPoint(xx, yy);//goalImage.getHeight()-1-greatestDifPoint.y);//virginPaintmap.getRandomStartPointOfColor(currentColor);
					BitmapPoint endPoint = paintmap.getEndOfStroke(startPoint, strokeDirectionSequence.get(direction), maxDistanceToEdgeInPixels, true, false);
					BitmapStroke bitmapStroke = new BitmapStroke(startPoint, endPoint, thisColor);
					double thisLength = bitmapStroke.getLength(startPoint, endPoint);
					if (thisLength >= minStrokeLength) {
						if (thisLength > longestStroke) {
							equidistantStrokesFound = 1;
							strokeFound = true;
							longestStroke = thisLength;
							longestPaintMapDirection = strokeDirectionSequence.get(direction);
							longestBitmapStroke = bitmapStroke;
						} else if (thisLength==longestStroke) {
							//Do Lottery System Here if lengths of multiple strokes are equidistant
							//we want to randomly pick from multiple similar maximum lengths though we do not know how
							//many there will be before hand
							// so first stroke always gets set
							// if second stroke found / 50% chance it replaces last one
							// if third stroke found / 33% chance it replaces last one
							// if fourth stroke found / 25% chance ... etc
							//this should make even odds with equidistant strokes
							equidistantStrokesFound = equidistantStrokesFound+1;
							double chanceToReplace = 1.0/(double)equidistantStrokesFound;
							//100% chance the first time
							//50% chance second time then 33,25,20,etc
							double randomPercentage = Math.random();
							if (randomPercentage<=chanceToReplace) {
								strokeFound = true;
								longestStroke = thisLength;
								longestPaintMapDirection = strokeDirectionSequence.get(direction);
								longestBitmapStroke = bitmapStroke;
							}
						}
					}						
					
				}
			}

		  
		  	BitmapPoint strokeBeginning =  longestBitmapStroke.getEnd();
		  	
			//THIS IS START OF THE STROKE
	  		Vector allpoint = new Vector();		  	
		  	//BitmapPoint strokeBeginning =  new BitmapPoint(xx, yy);
		  	
		  	allpoint.add(strokeBeginning);
			//Beginning here, walk down the angle of the stroke until color ends
		  	double currentX = (double)strokeBeginning.getX();
		  	double currentY = (double)strokeBeginning.getY();
		  	int radius = 3;
		  	if (recordOnPaintMap) {
		  	
		  		paintmap.setPaintMapPixelAsPainted((int)currentX, (int)currentY, radius);

		  	}
			boolean endFound = false;
			int maxPoint = maxStrokeLengthInPixels;
			int pointCount = 0;
			while (!endFound && pointCount<maxPoint) {
			  	float currentAngle = vectormap.getVectorAngle((int)currentX, (int)currentY);
			  	float avgRadianAngle = (float) (currentAngle * Math.PI / 180);
				double startX = currentX;
				double startY = currentY;
				//System.out.println("angle: "+currentAngle);
				//System.out.println("radian: "+avgRadianAngle);
				double endX = (1.0 * startX + (Math.cos(avgRadianAngle)));
				double endY = (1.0 * startY + (Math.sin(avgRadianAngle)));
				if (endX>=0 &&
					endY>=0 &&
					endX<paintmap.getWidth() &&
					endY<paintmap.getHeight() ) {
					if ( paintmap.getColorFromPaintMapXY( (int)endX, (int)endY ).equals(thisColor) ) {
						allpoint.add(new BitmapPoint((int)endX, (int)endY));
						if (recordOnPaintMap) {
							paintmap.setPaintMapPixelAsPainted((int)endX, (int)endY, radius);
						}
						currentX = endX;
						currentY = endY;
					} else {
						endFound = true;
					}
				} else {
					endFound = true;
				}
				pointCount++;
			}		  	
		  	allpoint.add(new BitmapPoint((int)currentX, (int)currentY));
			//paintmap.((int)currentX, (int)currentY);
		
			BitmapStroke flow = new BitmapStroke(strokeBeginning, new BitmapPoint((int)currentX, (int)currentY), thisColor, allpoint);
			return flow;
			
		}

	public void updatePaintMapFromBufferedImage(BufferedImage bi) {
		paintmap.updatePaintMapFromBufferedImage(bi);
	}
	
	
	private int generateAiStrokes(boolean executeAIStrokes, String indexName, String goalImageFileLocation, String heatmapImageFileLocation, String progressImageFileLocation, String differenceImageFileLocation, String jsonFileName, String timelapseFolder, int numberOfStrokes, Color lastColor) {

		//compare goal to progress image
		//GET GOAL IMAGE
		goalImage = getBufferedImage(goalImageFileLocation);
		if (lastGoalImage==null || !bufferedImagesEqual(goalImage,lastGoalImage)) {
			lastGoalImage = goalImage;
			paintmap = new PaintMap(
					goalImage.getWidth(),
					goalImage.getHeight(),
					1);
			paintmap.createPaintMapFromBufferedImage(goalImage);
		}
		
		//GET PROGRESS IMAGE
		progressImage = getBufferedImage(progressImageFileLocation);
		makeProgressImageSameAsGoalImageSize(goalImage, progressImage);

		
		
		//create and save heatmap
		System.out.println("creating heatmap");
		BufferedImage heatmap = TrapezoidImageTransformer.getDifferenceBufferedImage(goalImage, progressImage, 1);
        
		saveBufferedImage(heatmap, heatmapImageFileLocation );
		
		BufferedImage heatmapWithStrokes = ColorUtils.makeCopyOfBufferedImage(heatmap);
		BufferedImage progressImageWithStrokes = ColorUtils.makeCopyOfBufferedImage(progressImage);

		int startIndex = ElasticsearchUtility.getMaxStrokeNumber(indexName)+1;
		//get hottest area
		
		Color hottestColor = null;

		int strokesCreated = 0;
		
		int randomX = getRandomInteger(heatmap.getWidth());
		
		if (executeAIStrokes) {
			for (int i=0; i<numberOfStrokes; i++) {
	
				//find area with biggest difference between it and hottest color / then find the X hottest place with that color
				if (hottestColor == null) {

					greatestDif = getGreatestDif(paintmap, heatmap);
					hottestColor = new Color(goalImage.getRGB(greatestDif.x, greatestDif.y));
					System.out.println(hottestColor);
				
				} else {
					
					greatestDif = getGreatestDifOfColor(paintmap, heatmap, goalImage, hottestColor, randomX);
			
				}
				
				if (greatestDif!=null) {
					
					strokesCreated++;
					
					heatmap = drawOnGreatestDif(heatmap, greatestDif);
					//flip y axis to make strokes
					
					
					BitmapStroke ls = getAutoGeneratedSingleStroke(indexName, goalImage, greatestDif);
					
					boolean invertBitmapY = false;
					//save to paintmap
					paintmap.recordBitmapStrokeOnPaintMap(ls, invertBitmapY, goalImage.getHeight());
					
					invertBitmapY = false;
					heatmapWithStrokes = drawStrokesOnHeatmap(heatmapWithStrokes, ls.getStart(), ls.getEnd(), hottestColor, invertBitmapY);
					progressImageWithStrokes = drawStrokesOnHeatmap(progressImageWithStrokes, ls.getStart(), ls.getEnd(), hottestColor, invertBitmapY);
							
					boolean invertY = true;  //CHECK ALL OTHER PLACES TOO
					
					brushStrokes.add(ls);
					
					saveSingleStrokeToElasticSearch(indexName, goalImage, ls, startIndex+i, invertY);
				}
			}
			
		}
		saveBufferedImage(heatmap, differenceImageFileLocation);

		String paddedNumber = ElasticsearchUtility.paintingIdInMillion(startIndex);
		String paddedFilename = timelapseFolder + "heatmap_"+paddedNumber+".gif";
		saveBufferedImage(heatmap, paddedFilename );

		paddedFilename = timelapseFolder + "h_strokes_"+paddedNumber+".gif";
		saveBufferedImage(heatmapWithStrokes, paddedFilename );
		
		String paddedProgressFilename = timelapseFolder + "prog_"+paddedNumber+".gif";
		saveBufferedImage(progressImage, paddedProgressFilename );

		String paddedWithStrokesProgressFilename = timelapseFolder + "p_strokes_"+paddedNumber+".gif";
		saveBufferedImage(progressImageWithStrokes, paddedWithStrokesProgressFilename );

		String paintmapFilename = timelapseFolder + "paintmap_"+paddedNumber+".gif";
		saveBufferedImage(paintmap.getBufferedImageFromPaintMap(), paintmapFilename );
		
		//boolean invertY = true;  //CHECK ALL OTHER PLACES TOO
		//saveStrokeToFile(jsonFileName,  indexName, goalImage.getWidth(), goalImage.getHeight(), brushStrokes, invertY);
		
		
		//save heatmap
		//aiDecisionNumber++;
		//heatmap = drawOnGreatestDif(heatmap, greatestDif);
		//saveBufferedImage(heatmap, differenceImageFileLocation);
		//saveBufferedImage(heatmap, differenceImageFileLocation);
		//get longest stroke in that area
		
		//select from colors to reduce hotness
		return strokesCreated;
	}
	
	public void addBrushStroke(BitmapStroke bs) {
		brushStrokes.add(bs);
	}
	
	public void recordBitmapStrokeOnPaintMap(BitmapStroke ls, boolean invertBitmapY, int height) {
		paintmap.recordBitmapStrokeOnPaintMap(ls, invertBitmapY, height);
	}
	
	public BitmapStroke getAutoGeneratedSingleStroke(String elasticIndexName, BufferedImage goalImage, Point greatestDifPoint) {
		//SET PATTERN
		final ArrayList<PaintMapDirection> strokeDirectionSequence = new ArrayList<PaintMapDirection>();
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_SOUTH_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_WEST_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_WEST_WEST_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.WEST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_WEST_WEST_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_WEST_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_NORTH_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_NORTH_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_EAST_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_EAST_EAST_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.EAST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_EAST_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_EAST_EAST_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_SOUTH_EAST);		

		
		int strokeDirectionIndex = 0;
		
		//getColor at goal location
		int colorRGB = goalImage.getRGB(greatestDifPoint.x, greatestDifPoint.y);
		Color currentColor = new Color(colorRGB);
		
		PaintMapDirection longestPaintMapDirection = PaintMapDirection.SOUTH;
		double longestStroke = 0;
		BitmapStroke longestBitmapStroke = new BitmapStroke(new BitmapPoint(0,0),new BitmapPoint(0,0), currentColor);
		boolean strokeFound = false;
		int maxStrokeLength = goalImage.getHeight()/9;//50; //PIXELS
		int minStrokeLength = 0;
		int equidistantStrokesFound = 0;
	for (int direction= 0; direction<strokeDirectionSequence.size();direction++) {
			Color colorAtPoint = currentColor;
			if ( colorAtPoint.equals(currentColor )) {
				BitmapPoint startPoint = new BitmapPoint(greatestDifPoint.x, greatestDifPoint.y);//goalImage.getHeight()-1-greatestDifPoint.y);//virginPaintmap.getRandomStartPointOfColor(currentColor);
				BitmapPoint endPoint = paintmap.getEndOfStroke(startPoint, strokeDirectionSequence.get(direction), maxStrokeLength, false, true);
				BitmapStroke bitmapStroke = new BitmapStroke(startPoint, endPoint, currentColor);
				double thisLength = bitmapStroke.getLength(startPoint, endPoint);
				if (thisLength >= minStrokeLength) {
					if (thisLength > longestStroke) {
						equidistantStrokesFound = 1;
						strokeFound = true;
						longestStroke = thisLength;
						longestPaintMapDirection = strokeDirectionSequence.get(direction);
						longestBitmapStroke = bitmapStroke;
					} else if (thisLength==longestStroke) {
						//Do Lottery System Here if lengths of multiple strokes are equidistant
						//we want to randomly pick from multiple similar maximum lengths though we do not know how
						//many there will be before hand
						// so first stroke always gets set
						// if second stroke found / 50% chance it replaces last one
						// if third stroke found / 33% chance it replaces last one
						// if fourth stroke found / 25% chance ... etc
						//this should make even odds with equidistant strokes
						equidistantStrokesFound = equidistantStrokesFound+1;
						double chanceToReplace = 1.0/(double)equidistantStrokesFound;
						//100% chance the first time
						//50% chance second time then 33,25,20,etc
						double randomPercentage = Math.random();
						if (randomPercentage<=chanceToReplace) {
							strokeFound = true;
							longestStroke = thisLength;
							longestPaintMapDirection = strokeDirectionSequence.get(direction);
							longestBitmapStroke = bitmapStroke;
						}
					}
				}						
				
			}
		}
		return longestBitmapStroke;
	}
	
	public BitmapStroke getLongestSingleStrokeAtPosition(BufferedImage goalImage, Point pointOnCanvas) {
		//SET PATTERN
		final ArrayList<PaintMapDirection> strokeDirectionSequence = new ArrayList<PaintMapDirection>();
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_SOUTH_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_WEST_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_WEST_WEST_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.WEST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_WEST_WEST_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_WEST_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_NORTH_WEST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_NORTH_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_NORTH_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_EAST_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.NORTH_EAST_EAST_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.EAST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_EAST_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_EAST_EAST_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_EAST);
		  strokeDirectionSequence.add(PaintMapDirection.SOUTH_SOUTH_SOUTH_EAST);		

		
		int strokeDirectionIndex = 0;
		
		//getColor at goal location
		int colorRGB = goalImage.getRGB(pointOnCanvas.x, pointOnCanvas.y);
		Color currentColor = new Color(colorRGB);
		
		PaintMapDirection longestPaintMapDirection = PaintMapDirection.SOUTH;
		double longestStroke = 0;
		BitmapStroke longestBitmapStroke = new BitmapStroke(new BitmapPoint(0,0),new BitmapPoint(0,0), currentColor);
		
		boolean strokeFound = false;
		int maxStrokeLength = goalImage.getHeight()/9;//50; //PIXELS
		int minStrokeLength = 0;
		int equidistantStrokesFound = 0;
		
	for (int direction= 0; direction<strokeDirectionSequence.size();direction++) {
			Color colorAtPoint = currentColor;
			if ( colorAtPoint.equals(currentColor )) {
				BitmapPoint thisPoint = new BitmapPoint(pointOnCanvas.x, pointOnCanvas.y);
				BitmapPoint endPoint = paintmap.getEndOfStroke(thisPoint, strokeDirectionSequence.get(direction), maxStrokeLength, false, true);
				BitmapPoint startPoint = paintmap.getEndOfStroke(thisPoint, strokeDirectionSequence.get(direction), maxStrokeLength, true, true);
				Vector allPointsTraversed = new Vector();
				allPointsTraversed.addAll(startPoint.getMidPoints());
				allPointsTraversed.addAll(endPoint.getMidPoints());
				BitmapStroke bitmapStroke = new BitmapStroke(startPoint, endPoint, currentColor, allPointsTraversed);
				double thisLength = bitmapStroke.getLength(startPoint, endPoint);
				if (thisLength >= minStrokeLength) {
					if (thisLength > longestStroke) {
						equidistantStrokesFound = 1;
						strokeFound = true;
						longestStroke = thisLength;
						longestPaintMapDirection = strokeDirectionSequence.get(direction);
						longestBitmapStroke = bitmapStroke;
					} else if (thisLength==longestStroke) {
						//Do Lottery System Here if lengths of multiple strokes are equidistant
						//we want to randomly pick from multiple similar maximum lengths though we do not know how
						//many there will be before hand
						// so first stroke always gets set
						// if second stroke found / 50% chance it replaces last one
						// if third stroke found / 33% chance it replaces last one
						// if fourth stroke found / 25% chance ... etc
						//this should make even odds with equidistant strokes
						equidistantStrokesFound = equidistantStrokesFound+1;
						double chanceToReplace = 1.0/(double)equidistantStrokesFound;
						//100% chance the first time
						//50% chance second time then 33,25,20,etc
						double randomPercentage = Math.random();
						if (randomPercentage<=chanceToReplace) {
							strokeFound = true;
							longestStroke = thisLength;
							longestPaintMapDirection = strokeDirectionSequence.get(direction);
							longestBitmapStroke = bitmapStroke;
						}
					}
				}						
				
			}
		}
		return longestBitmapStroke;
	}
	
	
	public void saveSingleStrokeToElasticSearch(String imageName, BufferedImage goalImage, BitmapStroke bitmapStroke, int strokeNumber, boolean invertY) {
			int width  = goalImage.getWidth();
			int height = goalImage.getHeight();
			ElasticsearchUtility.SaveAutomatedStrokesToElasticSearchCluster(imageName, width, height, strokeNumber, bitmapStroke, invertY);
	}

	
	public void saveStrokeToFile(String fileName, String imageName, int width, int height, Vector<BitmapStroke> bitmapStroke, boolean invertY) {
		ElasticsearchUtility.SaveAutomatedStrokesToJsonTextFile(fileName, imageName, width, height, brushStrokes, invertY);
	}

	public BufferedImage drawOnGreatestDif(BufferedImage heatmap, Point greatestDif) {
			Graphics2D g = heatmap.createGraphics();
			g.setColor(Color.white);
			g.fillRect(greatestDif.x-1, greatestDif.y-1 , 3, 3);
			g.dispose();
			return heatmap;
	}
	
	public BufferedImage drawStrokesOnHeatmap(BufferedImage heatmapStrokes, BitmapPoint startPoint, BitmapPoint endPoint, Color strokeColor, boolean invertY) {
		int startX = startPoint.getX();
		int startY = startPoint.getY();
		int endX = endPoint.getX();
		int endY = endPoint.getY();
		
		if (invertY) {
			startY = heatmapStrokes.getHeight()-1-startY;
			endY = heatmapStrokes.getHeight()-1-endY;
		}
		Graphics2D g = heatmapStrokes.createGraphics();
		g.setColor(strokeColor);
		BasicStroke basicStroke = new BasicStroke(3);
		g.setStroke(basicStroke);
		g.drawLine(startX, startY, endX, endY);
		g.dispose();
		return heatmapStrokes;
		
	}
	
	public Point getGreatestDif(PaintMap paintmap, BufferedImage heatmap) {
		int coldestColor = 0;
		int hottestColor = 0;
		Point coldestPoint = new Point(0,0);
		Point hottestPoint = new Point(0,0);
		for (int x = 0; x<heatmap.getWidth(); x++) {
			for (int y =0; y<heatmap.getHeight(); y++) {
				PaintMapColor paintmapColor = paintmap.getPaintMapColorFromPaintMapXY(x, y);
				
				if (
						//Do not paint white
						!(
						(paintmapColor.getRed()==255 && paintmapColor.getGreen()==255 && paintmapColor.getBlue()==255 )
						||
						//do not paint already painted stuff
						(paintmapColor.isPainted())
						) ) {

					int heatmapRGB = heatmap.getRGB(x, y);
					Color heatmapPixelColor = new Color(heatmapRGB);
					if (heatmapPixelColor.getRed() == 255 ) {   //red
						if (heatmapPixelColor.getRed()-heatmapPixelColor.getGreen() > hottestColor) {
							System.out.println("hottestcolor: "+paintmapColor.getRed()+" : "+paintmapColor.getGreen()+" : "+paintmapColor.getBlue());
							hottestColor = heatmapPixelColor.getRed()-heatmapPixelColor.getGreen();
							hottestPoint = new Point(x,y);
						}
					}
					if (heatmapPixelColor.getBlue() == 255 ) {   //red
						if (heatmapPixelColor.getBlue()-heatmapPixelColor.getGreen() > coldestColor) {
							System.out.println("coldestcolor: "+paintmapColor.getRed()+" : "+paintmapColor.getGreen()+" : "+paintmapColor.getBlue());
							coldestColor = heatmapPixelColor.getBlue()-heatmapPixelColor.getGreen();
							coldestPoint = new Point(x,y);
						}
					}
				} else {
				//	System.out.println("white or already painted");
				}
			}
		}
		if (hottestColor > 0 || coldestColor > 0) {
			if (hottestColor > coldestColor) {
				return hottestPoint;
			} else {
				return coldestPoint;
			}
		} else {
			return null;
		}
	}

	public Point getGreatestDifOfColor(PaintMap paintmap, BufferedImage heatmap, BufferedImage goalImage, Color color, int startX) {

		int colorMatchRGB = color.getRGB();
		int coldestColor = 0;
		int hottestColor = 0;
		Point coldestPoint = new Point(0,0);
		Point hottestPoint = new Point(0,0);
		for (int x = startX; x>=0; x++) {
			for (int y =0; y<heatmap.getHeight(); y++) {
				PaintMapColor paintmapColor = paintmap.getPaintMapColorFromPaintMapXY(x, y);
				if (!paintmapColor.isPainted()) {
					int goalImageRGB = goalImage.getRGB(x, y);
					if (goalImageRGB == colorMatchRGB) {
						int heatmapRGB = heatmap.getRGB(x, y);
						Color heatmapPixelColor = new Color(heatmapRGB);
						if (heatmapPixelColor.getRed() == 255 ) {   //red
							if (heatmapPixelColor.getRed()-heatmapPixelColor.getGreen() > hottestColor) {
									hottestColor = heatmapPixelColor.getRed()-heatmapPixelColor.getGreen();
									hottestPoint = new Point(x,y);
								}	
						}
						if (heatmapPixelColor.getBlue() == 255 ) {   //red
							if (heatmapPixelColor.getBlue()-heatmapPixelColor.getGreen() > coldestColor) {
								coldestColor = heatmapPixelColor.getBlue()-heatmapPixelColor.getGreen();
								coldestPoint = new Point(x,y);
							}
						}
					}
				}
			}
			if (x==heatmap.getWidth()-1) {
				x = -1;
			}
			if (x==startX-1) {
				x=-10;
			}
			
		}
		
		if (hottestColor > 0 || coldestColor > 0) {
			if (hottestColor > coldestColor) {
				return hottestPoint;
			} else {
				return coldestPoint;
			}
		} else {
			return null;
		}

	}
	
	public void saveBufferedImage(BufferedImage difImage, String fileName) {
		System.out.println("Saving Image: "+fileName);
    	File outputfile = new File(fileName);
    	try {
			ImageIO.write(difImage, "gif", outputfile);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public BufferedImage getBufferedImage(String pathToFile) {
		System.out.println("OpeningFile");
		File cf = new File(pathToFile);
		BufferedImage contentImage = null;
		try {
			contentImage = ImageIO.read(cf);
		} catch (IOException e) {
		}
		return contentImage;
	}
	
	public void makeProgressImageSameAsGoalImageSize(BufferedImage goal, BufferedImage progress) {
			progressImage = scale(progress, goal.getWidth(), goal.getHeight());
	}

	public static BufferedImage scale(BufferedImage src, int w, int h)
	{
	    BufferedImage img = 
	            new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
	    int x, y;
	    int ww = src.getWidth();
	    int hh = src.getHeight();
	    int[] ys = new int[h];
	    for (y = 0; y < h; y++)
	        ys[y] = y * hh / h;
	    for (x = 0; x < w; x++) {
	        int newX = x * ww / w;
	        for (y = 0; y < h; y++) {
	            int col = src.getRGB(newX, ys[y]);
	            img.setRGB(x, y, col);
	        }
	    }
	    return img;
	}
	
	boolean bufferedImagesEqual(BufferedImage img1, BufferedImage img2) {
	    if (img1.getWidth() == img2.getWidth() && img1.getHeight() == img2.getHeight()) {
	        for (int x = 0; x < img1.getWidth(); x++) {
	            for (int y = 0; y < img1.getHeight(); y++) {
	                if (img1.getRGB(x, y) != img2.getRGB(x, y))
	                    return false;
	            }
	        }
	    } else {
	        return false;
	    }
	    return true;
	}
	
}