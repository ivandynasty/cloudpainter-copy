package com.cloudpainter.strokes;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONObject;

//12.1 cm

//6 light
//9 heavy




import com.cloudpainter.robots.RobotTemplate;

public class FB_D_FlatBrush_WideFullStroke_SpinOnDot_NoRise extends BrushTemplate{

	
	public FB_D_FlatBrush_WideFullStroke_SpinOnDot_NoRise (int strokeNumber, JSONObject JSONStroke, RobotTemplate robotObject, double brushLength, String strokeApproachAxis) {
		
		super(JSONStroke, robotObject, brushLength, strokeApproachAxis);

		double lastHeadingInRadians = 0.0;
		
		Point2D.Double previousPointAbsolute = new Point2D.Double();
		Point2D.Double nextPointAbsolute = new Point2D.Double();
		
		Point2D.Double firstPointAbsolute    = (Point2D.Double)pathVector.elementAt(0);
		Point2D.Double secondPointAbsolute    = (Point2D.Double)pathVector.elementAt(1);
		
		//calculate 2cm prior to first point
		Point2D.Double prePointAbsolute = interpolatePreviousPoint(firstPointAbsolute, secondPointAbsolute, 3.0);
		
		Point2D.Double pre = this.getPointOnPaintingSurface(prePointAbsolute);
		Point2D.Double p1 = this.getPointOnPaintingSurface(firstPointAbsolute);
		
		double angleOfRotationalBrushInRadians = 0.0;
		double rotationalOffsetForFrontOfBrush = 0.0;
		previousPointAbsolute = prePointAbsolute;
		nextPointAbsolute = firstPointAbsolute;
		
		gcodeStringBuffer.append("\n");
		gcodeStringBuffer.append("; stroke "+strokeNumber+" \n");
		gcodeStringBuffer.append("; type:  "+this.toString()+" \n");
				
		//TODO calculate PREposition 2 cm in front of stroke segment
		//TODO go to PREposition x , y in 45 degree angles

		angleOfRotationalBrushInRadians = getAngleofRotationalBrushInRadians(previousPointAbsolute, nextPointAbsolute, angleOfRotationalBrushInRadians);
		//angleOfRotationalBrushInRadians = angleOfRotationalBrushInRadians+(Math.PI/2);
		if (strokeNumber%2==0) {
			rotationalOffsetForFrontOfBrush = Math.PI;
		}
		
		
		//Go to prepoint at pi/2 of trajectory
		if (approachAxis.equalsIgnoreCase("x")) {
			gcodeStringBuffer.append("G1 X"+pre.getX()+"            ; move to X / Y before stroke\n");
			gcodeStringBuffer.append("G1 Y"+pre.getY()+" "+robotObject.getRotation((angleOfRotationalBrushInRadians+rotationalOffsetForFrontOfBrush))+"          ; move to X / Y before stroke\n");
		} else if (approachAxis.equalsIgnoreCase("y")){
			gcodeStringBuffer.append("G1 Y"+pre.getY()+"            ; move to X / Y before stroke\n");
			gcodeStringBuffer.append("G1 X"+pre.getX()+" "+robotObject.getRotation((angleOfRotationalBrushInRadians+rotationalOffsetForFrontOfBrush))+"          ; move to X / Y before stroke\n");
		} else {
			gcodeStringBuffer.append("G1 X"+pre.getX()+" Y"+pre.getY()+" "+robotObject.getRotation((angleOfRotationalBrushInRadians+rotationalOffsetForFrontOfBrush))+"          ; move both before stroke\n");
		}
		
		//align brush parrallel
		//TODO
		//TODO
		//TODO
		//TODO
		
		/*

rotation: 2.023
rotation: 2.023
rotation: 2.023
rotation: 3.141
rotation: 2.356
rotation: 3.338
rotation: 3.419
rotation: 3.36
rotation: 3.321
rotation: 3.49
rotation: 3.246
rotation: 3.44
rotation: 3.44
rotation: 3.49
rotation: 3.522
rotation: 0
rotation: 2.023
rotation: 2.023
rotation: 2.023
rotation: 3.141
rotation: 2.356
rotation: 3.338
rotation: 3.419
rotation: 3.36
rotation: 3.321
rotation: 3.49
rotation: 3.246
rotation: 3.44
rotation: 3.44
rotation: 3.49
rotation: 3.522
rotation: 0

		 */
		
		//Descend to first point
		gcodeStringBuffer.append("G1 X"+p1.getX()+" Y"+p1.getY()+" Z"+robotObject.zLightTouch+"  "+robotObject.getRotation((angleOfRotationalBrushInRadians+rotationalOffsetForFrontOfBrush))+"       ; move xyz to first point\n");
		
		//Go to second point while rotating brush to perpendicular
		//TODO
		//TODO
		//TODO
		//TODO
		//TODO

		//Go point to point keeping brush perpendicular
		//double brushPressure = getBrushPressure((JSONObject)JSONStroke, robotObject);
		//go to next stroke starting at second point
		gcodeStringBuffer.append("G1 F"+robotObject.xyhalfspeed+"          ; slowing speed for stroke\n");

		
		double distanceFromMediumTouchToLightTouch = robotObject.zMediumTouch - robotObject.zLightTouch;
		double steps = distanceFromMediumTouchToLightTouch/pathVector.size();
		
		if (singlePoint) {
			gcodeStringBuffer.append("G1 Z"+robotObject.zMediumTouch+" "+robotObject.getRotation(Math.PI)+"     ; spin on dot        \n");
			gcodeStringBuffer.append("G1 Z"+robotObject.zLightTouch+" "+robotObject.getRotation(Math.PI*2)+"    ; spin on dot        \n");
		} else {
		
			for (int j=1;j<pathVector.size()-1;j++) {
					nextPointAbsolute = (Point2D.Double)pathVector.elementAt(j);				
	
					angleOfRotationalBrushInRadians = getAngleofRotationalBrushInRadians(previousPointAbsolute, nextPointAbsolute, angleOfRotationalBrushInRadians);
					
					Point2D.Double np = this.getPointOnPaintingSurface(nextPointAbsolute);
					if (j==1) {
						gcodeStringBuffer.append("G1 X"+np.getX()+" Y"+np.getY()+" Z"+robotObject.zMediumTouch+" "+robotObject.getRotation(angleOfRotationalBrushInRadians+rotationalOffsetForFrontOfBrush)+"    ;  dip        \n");
					} else {
						gcodeStringBuffer.append("G1 X"+np.getX()+" Y"+np.getY()+" "+robotObject.getRotation(angleOfRotationalBrushInRadians+rotationalOffsetForFrontOfBrush)+"  ;           \n");
					}
	
					previousPointAbsolute = nextPointAbsolute;
	
			}
		}		
		//Ascend from last point 
		
		//raise on final point
		
		Point2D.Double lastPointAbsolute = (Point2D.Double)pathVector.elementAt(pathVector.size()-1);
		Point2D.Double postPointAbsolute = interpolateNextPoint(nextPointAbsolute, lastPointAbsolute, 1.0);
		
		Point2D.Double lp = this.getPointOnPaintingSurface(lastPointAbsolute);
		Point2D.Double post = this.getPointOnPaintingSurface(postPointAbsolute);
		
		gcodeStringBuffer.append("G1 X"+lp.getX()+" Y"+lp.getY()+" Z"+robotObject.zLightTouch+"    ;         \n");

		gcodeStringBuffer.append("G1 X"+post.getX()+" Y"+post.getY()+" Z"+robotObject.zRetracted+" "+robotObject.getRotation(0.0)+"  ; retract\n");

		
	}

	
	
	
}
