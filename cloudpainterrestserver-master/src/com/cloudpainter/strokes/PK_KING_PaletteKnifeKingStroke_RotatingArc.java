package com.cloudpainter.strokes;


import java.awt.geom.Point2D;
import java.util.Vector;

	import org.json.JSONArray;
import org.json.JSONObject;

	//12.1 cm

	//6 light
	//9 heavy



import com.cloudpainter.robots.RobotTemplate;

	public class PK_KING_PaletteKnifeKingStroke_RotatingArc  extends PaletteKnifeTemplate{

		
		public PK_KING_PaletteKnifeKingStroke_RotatingArc (int strokeNumber, JSONObject JSONStroke, RobotTemplate robo, double brushLength,  double brushWidth, String strokeApproachAxis) {
			
			super(JSONStroke, robo, (brushLength), brushWidth, strokeApproachAxis);

			//double kissingTheCanvasMm = 13.7;//13.7; //17.3 - is keychain  //14.1  is for white knife;//13.8 ;
			
			//PaletteKnife Strokes have 3 points only!  Angle of Movement is from first to last
			//TODO: if more than 1 cm - calculate a shift in X/Y perpendicular to that arc
			
			Point2D.Double startPointAbsolute    = (Point2D.Double)pathVector.elementAt(0);
			Point2D.Double endPointAbsolute    = (Point2D.Double)pathVector.elementAt(2);
			
			Point2D.Double knifeOffsetAtStartAbsolute = getPaletteKnifePerpendicularOffsetAtStart(startPointAbsolute, endPointAbsolute);
			Point2D.Double knifeOffsetAtEndAbsolute   = getPaletteKnifePerpendicularOffsetAtEnd(startPointAbsolute, endPointAbsolute);

			System.out.println( "startpoint: "+startPointAbsolute.getX() +","+startPointAbsolute.getY());
			System.out.println( "offset: "+knifeOffsetAtStartAbsolute.getX() +","+knifeOffsetAtStartAbsolute.getY());

			System.out.println( endPointAbsolute.getX() +","+endPointAbsolute.getY());
			System.out.println( knifeOffsetAtEndAbsolute.getX() +","+knifeOffsetAtEndAbsolute.getY());
			
			Point2D.Double knifeOffsetAt50percent           = getMidpoint(knifeOffsetAtStartAbsolute, knifeOffsetAtEndAbsolute);
			Point2D.Double knifeOffsetAt25percent           = getMidpoint(knifeOffsetAtStartAbsolute, knifeOffsetAt50percent);
			Point2D.Double knifeOffsetAt75percent           = getMidpoint(knifeOffsetAt50percent, knifeOffsetAtEndAbsolute);
			Point2D.Double knifeOffsetAt37percent           = getMidpoint(knifeOffsetAt25percent, knifeOffsetAt50percent);
			Point2D.Double knifeOffsetAt62percent           = getMidpoint(knifeOffsetAt50percent, knifeOffsetAt75percent);
			
			Point2D.Double canvasPointMm1 = this.getPointOnPaintingSurface(knifeOffsetAtStartAbsolute);
			Point2D.Double canvasPointMm2 = this.getPointOnPaintingSurface(knifeOffsetAt37percent);
			Point2D.Double canvasPointMm3 = this.getPointOnPaintingSurface(knifeOffsetAt62percent);
			Point2D.Double canvasPointMm4 = this.getPointOnPaintingSurface(knifeOffsetAtEndAbsolute);
			
			double startAngleOfRotationalBrushInRadians = 0.0;
			int direction = 1;
			
			double lastHeadingInRadians = 0.0;
			startAngleOfRotationalBrushInRadians = getAngleofRotationalBrushInRadians(startPointAbsolute, endPointAbsolute, lastHeadingInRadians);

			//if getAngleOfRotationalBrushInRadians has flipped the radian direction to save travel - flip direction of offset
			if (this.clockwiseRotation) {
				direction = direction * -1;
			}

			startAngleOfRotationalBrushInRadians = startAngleOfRotationalBrushInRadians + (direction*(Math.PI/1.75));
			
			gcodeStringBuffer.append("\n");
			gcodeStringBuffer.append("; stroke "+strokeNumber+" \n");
			gcodeStringBuffer.append("; type:  "+this.toString()+" \n");
			gcodeStringBuffer.append("; recipe strokeA2AgBe_N \n");

			gcodeStringBuffer.append("G4 P1000                      ; PAUSE (REFILL PAINT)\n");
			//Set speeds
			gcodeStringBuffer.append("M204 P"+robo.maxAccelSpeedPainting+" T"+robo.maxAccelSpeedPainting+"               ; ACCEL SPEED\n");
			gcodeStringBuffer.append("M203 E5                        ; MAX ACCEL SPEED ROTATION\n");
			gcodeStringBuffer.append("G1 F"+robo.xyfullspeed+"                      ; XY SPEED\n");
			
			//start position
			gcodeStringBuffer.append("G1 X"+robo.mmX(canvasPointMm1.getX())+" Y"+robo.mmY(canvasPointMm1.getY())+" E0.0          ; START POINT WITH PALETTE KNIFE IN HOME POSITION\n");
			gcodeStringBuffer.append("G1 Z"+robo.mmZ(robo.zRetracted)+"                        ; LOWER PALETTE BELOW AXIS\n");

			double angleOfRotationalBrushInRadians = startAngleOfRotationalBrushInRadians;
			//19.2 is old max value
			gcodeStringBuffer.append("G1 Z"+robo.mmZ((robo.zLightTouch-5.2))+" E"+angleOfRotationalBrushInRadians+"                ; ROTATE INTO START OF SWEEP\n"); 
			gcodeStringBuffer.append("G1 Z"+robo.mmZ((robo.zLightTouch-0.2))+"                       ; LOWER JUST ABOVE CANVAS\n");
			gcodeStringBuffer.append("G1 Z"+robo.mmZ((robo.zLightTouch))+"                       ; TOUCH CANVAS (WHEN CALIBRATING THIS IS SLIGHTLY TOUCHING CANVAS\n)");
			gcodeStringBuffer.append("G4 P100                       ; pause 1 sec\n");
			gcodeStringBuffer.append("G1 Z"+robo.mmZ((robo.zLightTouch-0.1))+"                       ; LIFT BACK UP A TOUCH\n");
			
			gcodeStringBuffer.append("M204 P"+robo.maxAccelSpeedHoming+" T"+robo.maxAccelSpeedHoming+"               ; LOWER ACCELERATION\n");
			gcodeStringBuffer.append("M203 E2                         ; LOWER ROTATIONAL ACCELERATION\n");

			//ROTATE 0.628 ( 36 degrees) CLOCKWISE
			if (this.clockwiseRotation) {
				angleOfRotationalBrushInRadians = angleOfRotationalBrushInRadians - 0.628;
			} else {
				angleOfRotationalBrushInRadians = angleOfRotationalBrushInRadians + 0.628;
			}
			gcodeStringBuffer.append("G1 X"+robo.mmX(canvasPointMm2.getX())+" Y"+robo.mmY(canvasPointMm2.getY())+" Z"+robo.mmZ(robo.zLightTouch-0.1)+" E"+angleOfRotationalBrushInRadians+"          ; START POINT WITH PALETTE KNIFE IN HOME POSITION\n");
			if (this.clockwiseRotation) {
				angleOfRotationalBrushInRadians = angleOfRotationalBrushInRadians - 0.628;
			} else {
				angleOfRotationalBrushInRadians = angleOfRotationalBrushInRadians + 0.628;
			}
			gcodeStringBuffer.append("G1 X"+robo.mmX(canvasPointMm3.getX())+" Y"+robo.mmY(canvasPointMm3.getY())+" Z"+robo.mmZ((robo.zLightTouch-1.1))+" E"+angleOfRotationalBrushInRadians+"          ; START POINT WITH PALETTE KNIFE IN HOME POSITION\n");
			if (this.clockwiseRotation) {
				angleOfRotationalBrushInRadians = angleOfRotationalBrushInRadians - 1.884;
			} else {
				angleOfRotationalBrushInRadians = angleOfRotationalBrushInRadians + 1.884;
			}
			double avgLightRetracted = (robo.zRetracted+robo.zLightTouch)/2;
			gcodeStringBuffer.append("G1 X"+robo.mmX(canvasPointMm4.getX())+" Y"+robo.mmY(canvasPointMm4.getY())+" Z"+robo.mmZ((avgLightRetracted))+" E"+angleOfRotationalBrushInRadians+"          ; START POINT WITH PALETTE KNIFE IN HOME POSITION\n");
 
			gcodeStringBuffer.append("G1 Z"+robo.mmZ((robo.zRetracted))+" E0.0                      ; POSITION PALETTE KNIFE IN HOME POSITION\n");
			//gcodeStringBuffer.append("G1 Z"+robo.mmZ((robo.zRetracted))+" E0                       ; POSITION PALETTE KNIFE IN HOME POSITION\n");
			
			gcodeStringBuffer.append("M204 P"+robo.maxAccelSpeedPainting+" T"+robo.maxAccelSpeedPainting+"               ; RETURN TO FULL SPEED\n");
			gcodeStringBuffer.append("M203 E5                        ; RETURN ROTATION TO FULL SPEED\n");  
			
			gcodeStringBuffer.append("\n");
			gcodeStringBuffer.append("\n");
			
		}
		
	}
