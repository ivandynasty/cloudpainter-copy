package com.cloudpainter.strokes;

import java.awt.Point;
import java.awt.geom.Point2D;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONObject;

import com.cloudpainter.robots.RobotTemplate;

public class RB_SS_RoundBrush_SimpleStroke extends BrushTemplate{

	
	public RB_SS_RoundBrush_SimpleStroke(int strokeNumber, JSONObject JSONStroke, RobotTemplate robotObject, double tempFudgeNumber, String strokeApproachAxis) {
		
		super(JSONStroke, robotObject, tempFudgeNumber, strokeApproachAxis);
		
		Point2D.Double firstPointAbsolute    = (Point2D.Double)pathVector.elementAt(0);

//		getSlumpAdjustmentInMm
		
		Point2D.Double firstPointTranslatedToPaintingSurface = this.getPointOnPaintingSurface(firstPointAbsolute);
		
		double canvasSlumpAdjustmentMm = this.getCanvasSlumpAdjustmentMm(firstPointAbsolute);
		
		//TODO calculate PREposition 2 cm in front of stroke segment
		//TODO go to PREposition x , y in 45 degree angles
		gcodeStringBuffer.append("\n");
		gcodeStringBuffer.append("; stroke "+strokeNumber+" \n");
		gcodeStringBuffer.append("; type:  "+this.toString()+" \n");
		
		//GO ABOVE FIRST STROKE on Canvas Surface
		gcodeStringBuffer.append("G1 X"+firstPointTranslatedToPaintingSurface.getX()+"          ; move to X of stroke start\n");
		gcodeStringBuffer.append("G1 Y"+firstPointTranslatedToPaintingSurface.getY()+"          ; move to Y of stroke start\n");
		
		//Go to prepoint at pi/2 of trajectory
		if (approachAxis.equalsIgnoreCase("x")) {
			gcodeStringBuffer.append("G1 X"+firstPointTranslatedToPaintingSurface.getX()+"      ; move to X / Y before stroke\n");
			gcodeStringBuffer.append("G1 Y"+firstPointTranslatedToPaintingSurface.getY()+"      ; move to X / Y before stroke\n");
		} else if (approachAxis.equalsIgnoreCase("y")){
			gcodeStringBuffer.append("G1 Y"+firstPointTranslatedToPaintingSurface.getY()+"      ; move to X / Y before stroke\n");
			gcodeStringBuffer.append("G1 X"+firstPointTranslatedToPaintingSurface.getX()+"      ; move to X / Y before stroke\n");
		} else {
			gcodeStringBuffer.append("G1 X"+firstPointTranslatedToPaintingSurface.getX()+" Y"+firstPointTranslatedToPaintingSurface.getY()+"          ; move both before stroke\n");
		}

		//ROBOFORTH VERSION
		roboforthStringBuffer.append(""+(int)firstPointTranslatedToPaintingSurface.getX()+" "+(int)firstPointTranslatedToPaintingSurface.getY()+" "+((int)robotObject.zLightTouch+1000)+" MOVETO \n");
		
		gcodeStringBuffer.append("G1 Z"+(robotObject.zLightTouch-(canvasSlumpAdjustmentMm*10))+"          ; move Z down\n");
		//ROBOFORTH VERSION
		roboforthStringBuffer.append(""+(int)firstPointTranslatedToPaintingSurface.getX()+" "+(int)firstPointTranslatedToPaintingSurface.getY()+" "+(int)(robotObject.zLightTouch-(canvasSlumpAdjustmentMm*10))+" MOVETO \n");
		
		//descend
		//double brushPressure = getBrushPressure((JSONObject)JSONStroke, robotObject);
		//go to next stroke starting at second point
		gcodeStringBuffer.append("G1 F"+robotObject.xyhalfspeed+"          ; slowing speed for stroke\n");
		for (int j=1;j<pathVector.size()-1;j++) {
			if (j%10==0) {
				Point2D.Double nextPointAbsolute = (Point2D.Double)pathVector.elementAt(j);
				Point2D.Double nextPointTranslatedToPaintingSurface = this.getPointOnPaintingSurface(nextPointAbsolute);
				gcodeStringBuffer.append("G1 X"+nextPointTranslatedToPaintingSurface.getX()+" Y"+nextPointTranslatedToPaintingSurface.getY()+" ;           \n");
				//ROBOFORTH VERSION
				roboforthStringBuffer.append(""+(int)nextPointTranslatedToPaintingSurface.getX()+" "+(int)nextPointTranslatedToPaintingSurface.getY()+" "+(int)(robotObject.zMediumTouch-(canvasSlumpAdjustmentMm*10))+" MOVETO \n");
			}
		}
		//raise on final point
		Point2D.Double lastPointAbsolute = (Point2D.Double)pathVector.elementAt(pathVector.size()-1);
		Point2D.Double lastPointTranslatedToPaintingSurface = this.getPointOnPaintingSurface(lastPointAbsolute);
		
		gcodeStringBuffer.append("G1 X"+lastPointTranslatedToPaintingSurface.getX()+" Y"+lastPointTranslatedToPaintingSurface.getY()+" ;         \n");
		//ROBOFORTH VERSION
		roboforthStringBuffer.append(""+(int)lastPointTranslatedToPaintingSurface.getX()+" "+(int)lastPointTranslatedToPaintingSurface.getY()+" "+(int)(robotObject.zLightTouch-(canvasSlumpAdjustmentMm*10))+" MOVETO \n");

		//ascend from last position past end of stroke
		gcodeStringBuffer.append("G1 Z"+robotObject.zRetracted+"          ; move Z up\n");
		//ROBOFORTH VERSION
		roboforthStringBuffer.append(""+(int)lastPointTranslatedToPaintingSurface.getX()+" "+(int)lastPointTranslatedToPaintingSurface.getY()+" "+((int)(robotObject.zLightTouch-(canvasSlumpAdjustmentMm*10))+1000)+" MOVETO \n");

		
	}

	
	
	
}
