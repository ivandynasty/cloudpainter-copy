
	package com.cloudpainter.strokes;

	import java.awt.Point;
	import java.awt.geom.Point2D;
	import java.util.StringTokenizer;
	import java.util.Vector;

	import org.json.JSONArray;
	import org.json.JSONObject;

	import com.cloudpainter.robots.RobotTemplate;
	import com.cloudpainter.utils.ElasticsearchUtility;

	public class BrushTemplate extends StrokeTemplate {

		double brushLength = 0;
		
		String approachAxis = "both";
		
		boolean invertY = false;
		
		StringBuffer gcodeStringBuffer = new StringBuffer();
		StringBuffer roboforthStringBuffer = new StringBuffer();
		
		Vector pathVector = new Vector();
		
		boolean singlePoint = false;
		
		RobotTemplate robotObject;
		
		JSONObject strokeCanvas = new JSONObject();
		JSONObject paintingData = new JSONObject();

		
		double canvasWidthInPixels = 0.0;
		double canvasHeightInPixels = 0.0;
		
		double canvasWidthInMm = 0.0;
		double canvasHeightInMm = 0.0;
		
		double averagePixelPerMm = 0.0;
		
		
		public BrushTemplate(JSONObject JSONStroke, RobotTemplate robotObject, double brushLength, String approachAxis) {

			this.brushLength = brushLength;
			
			this.approachAxis = approachAxis;
			
			this.robotObject = robotObject;
			
			double paintingWidth = 0;
			double paintingHeight = 0;

			JSONObject source = new JSONObject();
			if (JSONStroke.has("hits")) {
				JSONObject hits     = ((JSONObject)JSONStroke).getJSONObject("hits");
				JSONArray hitsArray     = hits.getJSONArray("hits");
				source     = hitsArray.getJSONObject(0).getJSONObject("_source");
			} else {
				source = JSONStroke;
			}
						
			try {
				strokeCanvas      = source.getJSONObject("strokeCanvas");
			} catch (Exception e) {
				strokeCanvas = new JSONObject(ElasticsearchUtility.getStrokeCanvasJson( source.getInt("canvasWidth"), source.getInt("canvasHeight"))) ;
			}

			canvasWidthInPixels    = strokeCanvas.getDouble("width");
			canvasHeightInPixels    = strokeCanvas.getDouble("height");

			double strokeCanvasTop = strokeCanvas.getDouble("top");  
			double strokeCanvasBottom = strokeCanvas.getDouble("bottom");  
			
			
			try {	
				paintingData      = source.getJSONObject("paintingData"); //TODO FIX
				canvasWidthInMm = paintingData.getDouble("width_mm");
				canvasHeightInMm = paintingData.getDouble("height_mm");
			} catch (Exception e) {
				canvasWidthInMm = robotObject.paintingSurface.widthInMm;
				canvasHeightInMm = robotObject.paintingSurface.heightInMm;
			}
				
			double avgW = canvasWidthInPixels/canvasWidthInMm; 
			double avgH = canvasHeightInPixels/canvasHeightInMm; 
			
			averagePixelPerMm = (avgW+avgH)/2;
			
			pathVector = getArrayOfPoints((JSONObject)JSONStroke);
			
			//if (strokeCanvasTop < strokeCanvasBottom) {
			//	invertY = true;
			//}

			//if (invertY) {
			//	invertYOfPathVector();
			//}
			//remove duplicate point
			removeDuplicatePointsFromPathVector();
			
			//make resolution of points half centimeter
			reduceResolutionFromPathVector(0.05);

			//make sure vector has three lines - a start/mid/end
			makeSurePathVectorHasAtleastThreePoints();
			
			singlePoint = this.checkIfSinglePoint();
			
			
		}
		
		public Point2D.Double getPointOnPaintingSurface(Point2D.Double pointToTranslate) {
			double xPercentage 			=  pointToTranslate.getX()/canvasWidthInPixels;
			double yPercentage 			=  pointToTranslate.getY()/canvasHeightInPixels;
			return robotObject.getPointOnPaintingSurfaceInMm(xPercentage, yPercentage);
		}
		
		public double getCanvasSlumpAdjustmentMm(Point2D.Double pointToTranslate) {
			double xPercentage 			=  pointToTranslate.getX()/canvasWidthInPixels;
			double yPercentage 			=  pointToTranslate.getY()/canvasHeightInPixels;
			double xDistToMiddle = (1.0 - Math.abs((2*(xPercentage - .50)))); 
			double yDistToMiddle = (1.0 - Math.abs((2*(yPercentage - .50)))); 
			double canvasSlumpAdjustmentMm = robotObject.canvasSlump * xDistToMiddle * yDistToMiddle;
			return canvasSlumpAdjustmentMm;
			
		}
		
		
		
		public String getGcodeString() {
			return gcodeStringBuffer.toString();
		}
		public String getRoboforthString() {
			return roboforthStringBuffer.toString();
		}
		
		public Vector getIndividualGCodeLines() {
			Vector individualLines = new Vector();
			StringTokenizer st = new StringTokenizer(gcodeStringBuffer.toString(), "\n");
			while (st.hasMoreTokens()) {
				individualLines.add(st.nextToken());
			}
			return individualLines;
		}

		public Vector getIndividualRoboForthLines() {
			Vector individualLines = new Vector();
			StringTokenizer st = new StringTokenizer(roboforthStringBuffer.toString(), "\n");
			while (st.hasMoreTokens()) {
				individualLines.add(st.nextToken());
			}
			return individualLines;
		}

		
		
		/*public double getBrushPressure(JSONObject strokeJSON, RobotTemplate robotObject) {
			JSONObject hits     = strokeJSON.getJSONObject("hits");
			JSONArray hitsArray     = hits.getJSONArray("hits");
			JSONObject source     = hitsArray.getJSONObject(0).getJSONObject("_source");
			JSONObject brush      = source.getJSONObject("brush");
			double brushLength = brush.getDouble("depth_cm");
			double brushPressure = 1.0;
			if (brushLength <= 1.0) {
				brushPressure = robotObject.zHeavyTouch;
			} else if (brushLength > 1.0 && brushLength < 2.0) {
				brushPressure = robotObject.zMediumTouch;
			} else {
				brushPressure = robotObject.zLightTouch;
			}
			return brushPressure;
		}
		*/
		
		public Vector getArrayOfPoints(JSONObject strokeJSON) {
			JSONObject source = new JSONObject();
			if (strokeJSON.has("hits")) {
				JSONObject hits     = strokeJSON.getJSONObject("hits");
				JSONArray hitsArray     = hits.getJSONArray("hits");
				source     = hitsArray.getJSONObject(0).getJSONObject("_source");
			} else {
				source = strokeJSON;
			}
			JSONArray path = null;
			try {
				path = source.getJSONArray("strokePath");
			} catch (Exception e) {
				System.out.println("<strokePath> not found looking for legacy <path>");
				path = source.getJSONArray("path");
				//StringTokenizer st = new StringTokenizer(pathString);
				//while (st.hasMoreTokens()) {
				//	Point2D.Double pathCoord = new Point2D.Double();
				//	pathCoord.setLocation(Double.parseDouble(st.nextToken()), Double.parseDouble(st.nextToken()));
				//	pathVector.add(pathCoord);
				//}
			}
			int numberOfPoints = path.length();
			Vector pathVector = new Vector();
			for (int i=0;i<numberOfPoints;i++) {
				JSONArray thisPoint = path.getJSONArray(i);
				Point2D.Double pathCoord = new Point2D.Double();
				pathCoord.setLocation(thisPoint.getDouble(0), thisPoint.getDouble(1));
				pathVector.add(pathCoord);
			}
			
			
			return pathVector;
		}
		
		public Point2D.Double interpolatePreviousPoint(Point2D.Double firstPoint, Point2D.Double secondPoint, double Mm) {
			
			Point2D.Double previousPoint = new Point2D.Double();

			double thisSlope = slope(firstPoint.getX(), firstPoint.getY(), secondPoint.getX(), secondPoint.getY());
			
			if (firstPoint.getX() < secondPoint.getX()) {
				previousPoint.x = (int) (firstPoint.getX() - ((Mm*averagePixelPerMm)*(Math.cos(Math.atan(thisSlope)))));
				previousPoint.y = (int) (firstPoint.getY() - ((Mm*averagePixelPerMm)*(Math.sin(Math.atan(thisSlope)))));
			} else if (firstPoint.getX() > secondPoint.getX()) {
				previousPoint.x = (int) (firstPoint.getX() + ((Mm*averagePixelPerMm)*(Math.cos(Math.atan(thisSlope)))));
				previousPoint.y = (int) (firstPoint.getY() + ((Mm*averagePixelPerMm)*(Math.sin(Math.atan(thisSlope)))));
			} else if (firstPoint.getY() < secondPoint.getY()) {
				previousPoint.x = (int) (firstPoint.getX() - ((Mm*averagePixelPerMm)*(Math.cos(Math.atan(thisSlope)))));
				previousPoint.y = (int) (firstPoint.getY() - ((Mm*averagePixelPerMm)*(Math.sin(Math.atan(thisSlope)))));
			} else {
				previousPoint.x = (int) (firstPoint.getX() + ((Mm*averagePixelPerMm)*(Math.cos(Math.atan(thisSlope)))));
				previousPoint.y = (int) (firstPoint.getY() + ((Mm*averagePixelPerMm)*(Math.sin(Math.atan(thisSlope)))));
			}
			
			return previousPoint;
		}
		
		public Point2D.Double interpolateNextPoint(Point2D.Double firstPoint, Point2D.Double secondPoint, double Mm) {
			
			Point2D.Double nextPoint = new Point2D.Double();

			double thisSlope = slope(firstPoint.getX(), firstPoint.getY(), secondPoint.getX(), secondPoint.getY());
			
			if (firstPoint.getX() < secondPoint.getX()) {
				nextPoint.x = (int) (secondPoint.getX() + ((Mm*averagePixelPerMm)*(Math.cos(Math.atan(thisSlope)))));
				nextPoint.y = (int) (secondPoint.getY() + ((Mm*averagePixelPerMm)*(Math.sin(Math.atan(thisSlope)))));
			} else if (firstPoint.getX() > secondPoint.getX()) {
				nextPoint.x = (int) (secondPoint.getX() - ((Mm*averagePixelPerMm)*(Math.cos(Math.atan(thisSlope)))));
				nextPoint.y = (int) (secondPoint.getY() - ((Mm*averagePixelPerMm)*(Math.sin(Math.atan(thisSlope)))));
			} else if (firstPoint.getY() < secondPoint.getY()) {
				nextPoint.x = (int) (secondPoint.getX() + ((Mm*averagePixelPerMm)*(Math.cos(Math.atan(thisSlope)))));
				nextPoint.y = (int) (secondPoint.getY() + ((Mm*averagePixelPerMm)*(Math.sin(Math.atan(thisSlope)))));
			} else {
				nextPoint.x = (int) (secondPoint.getX() - ((Mm*averagePixelPerMm)*(Math.cos(Math.atan(thisSlope)))));
				nextPoint.y = (int) (secondPoint.getY() - ((Mm*averagePixelPerMm)*(Math.sin(Math.atan(thisSlope)))));
			}
			
			return nextPoint;
		}
		
		public boolean checkIfSinglePoint() {
				Point2D.Double firstPoint = (Point2D.Double)pathVector.elementAt(0);
				Point2D.Double midPoint = (Point2D.Double)pathVector.elementAt(1);
				Point2D.Double lastPoint = (Point2D.Double)pathVector.elementAt(2);
				if (firstPoint.getX() == midPoint.getX() &&
					midPoint.getX() == lastPoint.getX() &&	
					firstPoint.getY() == midPoint.getY() &&
					midPoint.getY() == lastPoint.getY()) {
					return true;
				}
				return false;
		}
		
		public void makeSurePathVectorHasAtleastThreePoints() {
			Vector cleanPathVector = new Vector();
			
			if (pathVector.size()==1) {
				Point2D.Double point = (Point2D.Double)pathVector.elementAt(0);
				cleanPathVector.add(point);
				cleanPathVector.add(point);
				cleanPathVector.add(point);
				pathVector = cleanPathVector;
			} else if (pathVector.size()==2) {
				Point2D.Double firstEndPoint = (Point2D.Double)pathVector.elementAt(0);
				Point2D.Double lastEndPoint = (Point2D.Double)pathVector.elementAt(1);
				if (firstEndPoint.getX() != lastEndPoint.getX() || firstEndPoint.getY() != lastEndPoint.getY()) {
					double xPix = 0;
					if (Math.abs(firstEndPoint.getX()-lastEndPoint.getX()) <= 1) {
						xPix =  lastEndPoint.getX();
					} else {
						xPix = ((firstEndPoint.x+lastEndPoint.x)/2.0);
					}
					double yPix = 0;
					if (Math.abs(firstEndPoint.getY()-lastEndPoint.getY()) <= 1) {
						yPix =  lastEndPoint.getY();
					} else {
						yPix = ((firstEndPoint.y+lastEndPoint.y)/2.0);
					}
					Point2D.Double midPoint = new Point2D.Double( xPix, yPix );
					cleanPathVector.add(firstEndPoint);
					cleanPathVector.add(midPoint);
					cleanPathVector.add(lastEndPoint);
					pathVector = cleanPathVector;
				} else {
					Point2D.Double point = (Point2D.Double)pathVector.elementAt(0);
					cleanPathVector.add(point);
					cleanPathVector.add(point);
					cleanPathVector.add(point);
					pathVector = cleanPathVector;
				}
			} else {
				//DO NOTHING
			}
		}
		
		public void removeDuplicatePointsFromPathVector() {
			Vector cleanPathVector = new Vector();
			Point2D.Double prevPoint = (Point2D.Double)pathVector.elementAt(0);
			cleanPathVector.add(prevPoint);

			for (int i=1; i<pathVector.size();i++) {
				Point2D.Double thisPoint = (Point2D.Double)pathVector.elementAt(i);
				if (prevPoint.getX() != thisPoint.getX() || prevPoint.getY() != thisPoint.getY()) {
					cleanPathVector.add(thisPoint);
				}
				prevPoint = thisPoint;
			}
			pathVector = cleanPathVector;
		}
		
		public void reduceResolutionFromPathVector(double minDistanceBetweenPoints_Mm) {
			
			Vector reducedPathVector = new Vector();
			Point2D.Double prevPoint = (Point2D.Double)pathVector.elementAt(0);
			reducedPathVector.add(prevPoint);
			
			double distanceBetweenPoints_mm = 0.0;
					
			for (int i=1; i<pathVector.size()-1;i++) {
				Point2D.Double thisPoint = (Point2D.Double)pathVector.elementAt(i);
				//a2 = b2 + c2
				double xDistSquared =  Math.abs(((double)(prevPoint.getX()-thisPoint.getX())))*Math.abs(((double)(prevPoint.getX()-thisPoint.getX())));
				double yDistSquared =  Math.abs(((double)(prevPoint.getY()-thisPoint.getY())))*Math.abs(((double)(prevPoint.getY()-thisPoint.getY())));
			
				distanceBetweenPoints_mm += ( Math.sqrt( xDistSquared + yDistSquared )/averagePixelPerMm ); 
				
				if ( distanceBetweenPoints_mm > minDistanceBetweenPoints_Mm) {
					reducedPathVector.add(thisPoint);
					distanceBetweenPoints_mm = 0.0;
				}
				prevPoint = thisPoint;
			}
			
			reducedPathVector.add(pathVector.lastElement());
			
			pathVector = reducedPathVector;
		}
		
		public void invertYOfPathVector() {
			Vector invertedPathVector = new Vector();
			for (int i=0; i<pathVector.size();i++) {
				Point2D.Double thisPoint = (Point2D.Double)pathVector.elementAt(i);
				Point2D.Double invertedPoint = new Point2D.Double((thisPoint.x), ((int)canvasHeightInPixels) - thisPoint.y  );
				invertedPathVector.add(invertedPoint);
			}
			pathVector = invertedPathVector;
		}
		
		public double getRawRadiansUnderPi(double brushAngleRadians) {
			
			double rawRadians = brushAngleRadians;
			
			if (brushAngleRadians >= -Math.PI && brushAngleRadians <= Math.PI) {
				return rawRadians;
			} else if (rawRadians > Math.PI) {
				while (rawRadians > Math.PI) {
					rawRadians = rawRadians-(2*Math.PI);
				}
				return rawRadians;
			} else if (rawRadians < -Math.PI) {
				while (rawRadians < -Math.PI) {
					rawRadians = rawRadians+(2*Math.PI);
				}
				return rawRadians;
			}		
			return rawRadians;
		}
		
		public double getAngleofRotationalBrushInRadians(Point2D.Double firstPoint, Point2D.Double secondPoint, double brushAngleInRadians) {
		
			double nextBrushAngleInRadians = 0.0;
			
			double thisSlope = slope(firstPoint.getX(), firstPoint.getY(), secondPoint.getX(), secondPoint.getY());

			double lastRadianRawHeading = getRawRadiansUnderPi(brushAngleInRadians);
			double nextRadianRawHeading = Math.atan(thisSlope);

			if (firstPoint.getX() < secondPoint.getX()) {
				//regular slope do nothing
			} else if (firstPoint.getX() == secondPoint.getX()) {
				if (firstPoint.getY() < secondPoint.getY()) {
					nextRadianRawHeading = Math.PI/2;
				} else {
					nextRadianRawHeading = -Math.PI/2;
				}
			} else {
				if (thisSlope >= 0) {
					nextRadianRawHeading = nextRadianRawHeading+(Math.PI);
				} else {
					nextRadianRawHeading = nextRadianRawHeading-(Math.PI);
				}
			}
			
			if (lastRadianRawHeading > nextRadianRawHeading ) {
				//+
				//check distance between 
				double counterClockwiseDistance = nextRadianRawHeading-lastRadianRawHeading;              // -10
				double clockwiseDistance =        nextRadianRawHeading+(2*Math.PI)-lastRadianRawHeading;  //   2
				if (Math.abs(counterClockwiseDistance) < Math.abs(clockwiseDistance)) {
					nextBrushAngleInRadians = brushAngleInRadians+counterClockwiseDistance;
				} else {
					nextBrushAngleInRadians = brushAngleInRadians+clockwiseDistance;
				}
			} else {
			    //-
				//check distance between 
				double counterClockwiseDistance = (nextRadianRawHeading-(2*Math.PI))-lastRadianRawHeading;  //-2
				double clockwiseDistance =        nextRadianRawHeading-lastRadianRawHeading;  //10
				if (Math.abs(counterClockwiseDistance) < Math.abs(clockwiseDistance)) {
					nextBrushAngleInRadians = brushAngleInRadians+counterClockwiseDistance;
				} else {
					nextBrushAngleInRadians = brushAngleInRadians+clockwiseDistance;
				}
			}
			

			
			nextBrushAngleInRadians = ((double)((int)(nextBrushAngleInRadians*1000)))/1000;
			
			
			
			return nextBrushAngleInRadians;		
			
		}
		
	    static double slope(double x1, double y1, 
	    		double x2, double y2) 
	    { 
	    	if ((x2 - x1) != 0) {		
	    		return (y2 - y1) / (x2 - x1); 	}
	    	else {
	    		return  1000000.0;
	    	}
	    } 
		
	}
