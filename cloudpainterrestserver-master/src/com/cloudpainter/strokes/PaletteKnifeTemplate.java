package com.cloudpainter.strokes;

	import java.awt.Point;
	import java.awt.geom.Point2D;
	import java.util.StringTokenizer;
	import java.util.Vector;

	import org.json.JSONArray;
	import org.json.JSONObject;

	import com.cloudpainter.robots.RobotTemplate;
	import com.cloudpainter.utils.ElasticsearchUtility;

	public class PaletteKnifeTemplate extends StrokeTemplate {

		double brushLength_mm = 0;
		double brushWidth_mm = 0;
		
		boolean clockwiseRotation = true; 
		
		String approachAxis = "both";
		
		boolean invertY = false;
		
		StringBuffer gcodeStringBuffer = new StringBuffer();
		StringBuffer roboforthStringBuffer = new StringBuffer();
		
		Vector pathVector = new Vector();
		
		boolean singlePoint = false;
		
		RobotTemplate robo;
		
		JSONObject strokeCanvas = new JSONObject();
		JSONObject paintingData = new JSONObject();

		
		double canvasWidthInPixels = 0.0;
		double canvasHeightInPixels = 0.0;
		
		double canvasWidthInMm = 0.0;
		double canvasHeightInMm = 0.0;
		
		double averagePixelWidthPerMm = 0.0;
		double averagePixelHeightPerMm = 0.0;
		
		
		public PaletteKnifeTemplate(JSONObject JSONStroke, RobotTemplate robotObject, double brushLength, double brushWidth, String approachAxis) {

			this.brushLength_mm = brushLength;
			this.brushWidth_mm = brushWidth;
			
			this.approachAxis = approachAxis;
			
			this.robo = robotObject;
			
			double paintingWidth = 0;
			double paintingHeight = 0;
			JSONObject hits     = ((JSONObject)JSONStroke).getJSONObject("hits");
			JSONArray hitsArray     = hits.getJSONArray("hits");
			JSONObject source     = hitsArray.getJSONObject(0).getJSONObject("_source");
			
			try {
				strokeCanvas      = source.getJSONObject("strokeCanvas");
			} catch (Exception e) {
				strokeCanvas = new JSONObject(ElasticsearchUtility.getStrokeCanvasJson( source.getInt("canvasWidth"), source.getInt("canvasHeight"))) ;
			}

			canvasWidthInPixels    = strokeCanvas.getDouble("width");
			canvasHeightInPixels    = strokeCanvas.getDouble("height");

			double strokeCanvasTop = strokeCanvas.getDouble("top");  
			double strokeCanvasBottom = strokeCanvas.getDouble("bottom");  
			
			
			try {	
				paintingData      = source.getJSONObject("paintingData"); //TODO FIX
				canvasWidthInMm = paintingData.getDouble("width_mm");
				canvasHeightInMm = paintingData.getDouble("height_mm");
			} catch (Exception e) {
				canvasWidthInMm = robotObject.paintingSurface.widthInMm;
				canvasHeightInMm = robotObject.paintingSurface.heightInMm;
			}
				
			double avgW = canvasWidthInPixels/canvasWidthInMm; 
			double avgH = canvasHeightInPixels/canvasHeightInMm; 
			
			averagePixelWidthPerMm = avgW;
			averagePixelHeightPerMm = avgH;
			
			pathVector = getArrayOfPoints((JSONObject)JSONStroke);
			
			//if (strokeCanvasTop < strokeCanvasBottom) {
			//	invertY = true;
			//}

			//if (invertY) {
			//	invertYOfPathVector();
			//}
			//remove duplicate point
			removeDuplicatePointsFromPathVector();
			
			//make resolution of points half centimeter
			reduceResolutionFromPathVector(5.0);
			
			singlePoint = false; //this.checkIfSinglePoint();

			//make sure vector has three lines - a start/mid/end
			makeSurePathVectorHasExactlyThreePoints();

			//with three lines - use start and end to define movement, but mid point to decide arc of palette knife
			clockwiseRotation = getPaletteKnifeRotation( (Point2D.Double)pathVector.elementAt(0), (Point2D.Double)pathVector.elementAt(1), (Point2D.Double)pathVector.elementAt(2) );
			
		}
		
		public Point2D.Double getPointOnPaintingSurface(Point2D.Double pointToTranslate) {
			double xPercentage 			=  pointToTranslate.getX()/canvasWidthInPixels;
			double yPercentage 			=  pointToTranslate.getY()/canvasHeightInPixels;
			return robo.getPointOnPaintingSurfaceInMm(xPercentage, yPercentage);
		}
		
		
		public String getGcodeString() {
			return gcodeStringBuffer.toString();
		}
		
		public String getRoboforthString() {
			return roboforthStringBuffer.toString();
		}

		
		public Vector getIndividualGCodeLines() {
			Vector individualLines = new Vector();
			StringTokenizer st = new StringTokenizer(gcodeStringBuffer.toString(), "\n");
			while (st.hasMoreTokens()) {
				individualLines.add(st.nextToken());
			}
			return individualLines;
		}

		public Vector getIndividualRoboForthLines() {
			Vector individualLines = new Vector();
			StringTokenizer st = new StringTokenizer(roboforthStringBuffer.toString(), "\n");
			while (st.hasMoreTokens()) {
				individualLines.add(st.nextToken());
			}
			return individualLines;
		}

		
		/*public double getBrushPressure(JSONObject strokeJSON, RobotTemplate robotObject) {
			JSONObject hits     = strokeJSON.getJSONObject("hits");
			JSONArray hitsArray     = hits.getJSONArray("hits");
			JSONObject source     = hitsArray.getJSONObject(0).getJSONObject("_source");
			JSONObject brush      = source.getJSONObject("brush");
			double brushLength = brush.getDouble("depth_cm");
			double brushPressure = 1.0;
			if (brushLength <= 1.0) {
				brushPressure = robotObject.zHeavyTouch;
			} else if (brushLength > 1.0 && brushLength < 2.0) {
				brushPressure = robotObject.zMediumTouch;
			} else {
				brushPressure = robotObject.zLightTouch;
			}
			return brushPressure;
		}
		*/
		
		public Vector getArrayOfPoints(JSONObject strokeJSON) {
			JSONObject hits     = strokeJSON.getJSONObject("hits");
			JSONArray hitsArray     = hits.getJSONArray("hits");
			JSONObject source     = hitsArray.getJSONObject(0).getJSONObject("_source");
			JSONArray path = null;
			try {
				path = source.getJSONArray("strokePath");
			} catch (Exception e) {
				System.out.println("<strokePath> not found looking for legacy <path>");
				path = source.getJSONArray("path");
				//StringTokenizer st = new StringTokenizer(pathString);
				//while (st.hasMoreTokens()) {
				//	Point2D.Double pathCoord = new Point2D.Double();
				//	pathCoord.setLocation(Double.parseDouble(st.nextToken()), Double.parseDouble(st.nextToken()));
				//	pathVector.add(pathCoord);
				//}
			}
			int numberOfPoints = path.length();
			Vector pathVector = new Vector();
			for (int i=0;i<numberOfPoints;i++) {
				JSONArray thisPoint = path.getJSONArray(i);
				Point2D.Double pathCoord = new Point2D.Double();
				pathCoord.setLocation(thisPoint.getDouble(0), thisPoint.getDouble(1));
				pathVector.add(pathCoord);
			}
			
			
			return pathVector;
		}
		
		public Point2D.Double interpolatePreviousPoint(Point2D.Double firstPoint, Point2D.Double secondPoint, double mm) {
			
			Point2D.Double previousPoint = new Point2D.Double();

			double thisSlope = slope(firstPoint.getX(), firstPoint.getY(), secondPoint.getX(), secondPoint.getY());
			
			if (firstPoint.getX() < secondPoint.getX()) {
				previousPoint.x = (int) (firstPoint.getX() - ((mm*averagePixelWidthPerMm)*(Math.cos(Math.atan(thisSlope)))));
				previousPoint.y = (int) (firstPoint.getY() - ((mm*averagePixelHeightPerMm)*(Math.sin(Math.atan(thisSlope)))));
			} else if (firstPoint.getX() > secondPoint.getX()) {
				previousPoint.x = (int) (firstPoint.getX() + ((mm*averagePixelWidthPerMm)*(Math.cos(Math.atan(thisSlope)))));
				previousPoint.y = (int) (firstPoint.getY() + ((mm*averagePixelHeightPerMm)*(Math.sin(Math.atan(thisSlope)))));
			} else if (firstPoint.getY() < secondPoint.getY()) {
				previousPoint.x = (int) (firstPoint.getX() - ((mm*averagePixelWidthPerMm)*(Math.cos(Math.atan(thisSlope)))));
				previousPoint.y = (int) (firstPoint.getY() - ((mm*averagePixelHeightPerMm)*(Math.sin(Math.atan(thisSlope)))));
			} else {
				previousPoint.x = (int) (firstPoint.getX() + ((mm*averagePixelWidthPerMm)*(Math.cos(Math.atan(thisSlope)))));
				previousPoint.y = (int) (firstPoint.getY() + ((mm*averagePixelHeightPerMm)*(Math.sin(Math.atan(thisSlope)))));
			}
			
			return previousPoint;
		}
		
		public Point2D.Double interpolateNextPoint(Point2D.Double firstPoint, Point2D.Double secondPoint, double mm) {
			
			Point2D.Double nextPoint = new Point2D.Double();

			double thisSlope = slope(firstPoint.getX(), firstPoint.getY(), secondPoint.getX(), secondPoint.getY());
			
			if (firstPoint.getX() < secondPoint.getX()) {
				nextPoint.x = (int) (secondPoint.getX() + ((mm*averagePixelWidthPerMm)*(Math.cos(Math.atan(thisSlope)))));
				nextPoint.y = (int) (secondPoint.getY() + ((mm*averagePixelHeightPerMm)*(Math.sin(Math.atan(thisSlope)))));
			} else if (firstPoint.getX() > secondPoint.getX()) {
				nextPoint.x = (int) (secondPoint.getX() - ((mm*averagePixelWidthPerMm)*(Math.cos(Math.atan(thisSlope)))));
				nextPoint.y = (int) (secondPoint.getY() - ((mm*averagePixelHeightPerMm)*(Math.sin(Math.atan(thisSlope)))));
			} else if (firstPoint.getY() < secondPoint.getY()) {
				nextPoint.x = (int) (secondPoint.getX() + ((mm*averagePixelWidthPerMm)*(Math.cos(Math.atan(thisSlope)))));
				nextPoint.y = (int) (secondPoint.getY() + ((mm*averagePixelHeightPerMm)*(Math.sin(Math.atan(thisSlope)))));
			} else {
				nextPoint.x = (int) (secondPoint.getX() - ((mm*averagePixelWidthPerMm)*(Math.cos(Math.atan(thisSlope)))));
				nextPoint.y = (int) (secondPoint.getY() - ((mm*averagePixelHeightPerMm)*(Math.sin(Math.atan(thisSlope)))));
			}
			
			return nextPoint;
		}
		
		public boolean checkIfSinglePoint() {
				Point2D.Double firstPoint = (Point2D.Double)pathVector.elementAt(0);
				Point2D.Double midPoint = (Point2D.Double)pathVector.elementAt(1);
				Point2D.Double lastPoint = (Point2D.Double)pathVector.elementAt(2);
				if (firstPoint.getX() == midPoint.getX() &&
					midPoint.getX() == lastPoint.getX() &&	
					firstPoint.getY() == midPoint.getY() &&
					midPoint.getY() == lastPoint.getY()) {
					return true;
				}
				return false;
		}
		
		public void makeSurePathVectorHasExactlyThreePoints() {
			Vector cleanPathVector = new Vector();
			
			if (pathVector.size()==1) {
				Point2D.Double point = (Point2D.Double)pathVector.elementAt(0);
				cleanPathVector.add(point);
				cleanPathVector.add(point);
				cleanPathVector.add(point);
				pathVector = cleanPathVector;
			} else if (pathVector.size()==2) {
				Point2D.Double firstEndPoint = (Point2D.Double)pathVector.elementAt(0);
				Point2D.Double lastEndPoint = (Point2D.Double)pathVector.elementAt(1);
				if (firstEndPoint.getX() != lastEndPoint.getX() || firstEndPoint.getY() != lastEndPoint.getY()) {
					double xPix = 0;
					if (Math.abs(firstEndPoint.getX()-lastEndPoint.getX()) <= 1) {
						xPix =  lastEndPoint.getX();
					} else {
						xPix = ((firstEndPoint.x+lastEndPoint.x)/2.0);
					}
					double yPix = 0;
					if (Math.abs(firstEndPoint.getY()-lastEndPoint.getY()) <= 1) {
						yPix =  lastEndPoint.getY();
					} else {
						yPix = ((firstEndPoint.y+lastEndPoint.y)/2.0);
					}
					Point2D.Double midPoint = new Point2D.Double( xPix, yPix );
					cleanPathVector.add(firstEndPoint);
					cleanPathVector.add(midPoint);
					cleanPathVector.add(lastEndPoint);
					pathVector = cleanPathVector;
				} else {
					Point2D.Double point = (Point2D.Double)pathVector.elementAt(0);
					cleanPathVector.add(point);
					cleanPathVector.add(point);
					cleanPathVector.add(point);
					pathVector = cleanPathVector;
				}
			} else {  //reduce path to (firstpoint) (midPoint) and (lastpoint)
				int numPoints = pathVector.size();
				int midPointNum = (numPoints)/2;
				Point2D.Double startpoint = (Point2D.Double)pathVector.elementAt(0);
				Point2D.Double midpoint = (Point2D.Double)pathVector.elementAt(midPointNum);
				Point2D.Double endpoint = (Point2D.Double)pathVector.elementAt(numPoints-1);
				cleanPathVector.add(startpoint);
				cleanPathVector.add(midpoint);
				cleanPathVector.add(endpoint);
				pathVector = cleanPathVector;
			}
		}
		
		public void removeDuplicatePointsFromPathVector() {
			Vector cleanPathVector = new Vector();
			Point2D.Double prevPoint = (Point2D.Double)pathVector.elementAt(0);
			cleanPathVector.add(prevPoint);

			for (int i=1; i<pathVector.size();i++) {
				Point2D.Double thisPoint = (Point2D.Double)pathVector.elementAt(i);
				if (prevPoint.getX() != thisPoint.getX() || prevPoint.getY() != thisPoint.getY()) {
					cleanPathVector.add(thisPoint);
				}
				prevPoint = thisPoint;
			}
			pathVector = cleanPathVector;
		}
		
		public void reduceResolutionFromPathVector(double minDistanceBetweenPoints_mm) {
			
			Vector reducedPathVector = new Vector();
			Point2D.Double prevPoint = (Point2D.Double)pathVector.elementAt(0);
			reducedPathVector.add(prevPoint);
			
			double distanceBetweenPoints_mm = 0.0;
					
			for (int i=1; i<pathVector.size()-1;i++) {
				Point2D.Double thisPoint = (Point2D.Double)pathVector.elementAt(i);
				//a2 = b2 + c2
				double xDistSquared =  Math.abs(((double)(prevPoint.getX()-thisPoint.getX())))*Math.abs(((double)(prevPoint.getX()-thisPoint.getX())));
				double yDistSquared =  Math.abs(((double)(prevPoint.getY()-thisPoint.getY())))*Math.abs(((double)(prevPoint.getY()-thisPoint.getY())));
			
				distanceBetweenPoints_mm += ( Math.sqrt( xDistSquared + yDistSquared ) / ((averagePixelWidthPerMm+averagePixelHeightPerMm)/2) ); 
				
				if ( distanceBetweenPoints_mm > minDistanceBetweenPoints_mm) {
					reducedPathVector.add(thisPoint);
					distanceBetweenPoints_mm = 0.0;
				}
				prevPoint = thisPoint;
			}
			
			reducedPathVector.add(pathVector.lastElement());
			
			pathVector = reducedPathVector;
		}
		
		public void invertYOfPathVector() {
			Vector invertedPathVector = new Vector();
			for (int i=0; i<pathVector.size();i++) {
				Point2D.Double thisPoint = (Point2D.Double)pathVector.elementAt(i);
				Point2D.Double invertedPoint = new Point2D.Double((thisPoint.x), ((int)canvasHeightInPixels) - thisPoint.y  );
				invertedPathVector.add(invertedPoint);
			}
			pathVector = invertedPathVector;
		}
		
		public double getRawRadiansUnderPi(double brushAngleRadians) {
			
			double rawRadians = brushAngleRadians;
			
			if (brushAngleRadians >= -Math.PI && brushAngleRadians <= Math.PI) {
				return rawRadians;
			} else if (rawRadians > Math.PI) {
				while (rawRadians > Math.PI) {
					rawRadians = rawRadians-(2*Math.PI);
				}
				return rawRadians;
			} else if (rawRadians < -Math.PI) {
				while (rawRadians < -Math.PI) {
					rawRadians = rawRadians+(2*Math.PI);
				}
				return rawRadians;
			}		
			return rawRadians;
		}
		
		public double getAngleofRotationalBrushInRadians(Point2D.Double firstPoint, Point2D.Double secondPoint, double brushAngleInRadians) {
		
			double nextBrushAngleInRadians = 0.0;
			
			double thisSlope = slope(firstPoint.getX(), firstPoint.getY(), secondPoint.getX(), secondPoint.getY());

			double lastRadianRawHeading = getRawRadiansUnderPi(brushAngleInRadians);
			double nextRadianRawHeading = Math.atan(thisSlope);

			if (firstPoint.getX() < secondPoint.getX()) {
				//regular slope do nothing
			} else if (firstPoint.getX() == secondPoint.getX()) {
				if (firstPoint.getY() < secondPoint.getY()) {
					nextRadianRawHeading = Math.PI/2;
				} else {
					nextRadianRawHeading = -Math.PI/2;
				}
			} else {
				if (thisSlope >= 0) {
					nextRadianRawHeading = nextRadianRawHeading+(Math.PI);
				} else {
					nextRadianRawHeading = nextRadianRawHeading-(Math.PI);
				}
			}
			nextBrushAngleInRadians = nextRadianRawHeading;
			
			//THIS HELPS TRACK SIDE OF RADIANS BRUSH IS ON - determines the direction of swing
/*			if (lastRadianRawHeading > nextRadianRawHeading ) {
				//+
				//check distance between 
				double counterClockwiseDistance = nextRadianRawHeading-lastRadianRawHeading;              // -10
				double clockwiseDistance =        nextRadianRawHeading+(2*Math.PI)-lastRadianRawHeading;  //   2
				if (Math.abs(counterClockwiseDistance) < Math.abs(clockwiseDistance)) {
					nextBrushAngleInRadians = brushAngleInRadians+counterClockwiseDistance;
				} else {
					nextBrushAngleInRadians = brushAngleInRadians+clockwiseDistance;
				}
			} else {
			    //-
				//check distance between 
				double counterClockwiseDistance = (nextRadianRawHeading-(2*Math.PI))-lastRadianRawHeading;  //-2
				double clockwiseDistance =        nextRadianRawHeading-lastRadianRawHeading;  //10
				if (Math.abs(counterClockwiseDistance) < Math.abs(clockwiseDistance)) {
					nextBrushAngleInRadians = brushAngleInRadians+counterClockwiseDistance;
				} else {
					nextBrushAngleInRadians = brushAngleInRadians+clockwiseDistance;
				}
			}
*/
			nextBrushAngleInRadians = ((double)((int)(nextBrushAngleInRadians*1000)))/1000;
			
			return nextBrushAngleInRadians;		
			
		}
		
		
		
		
	    static double slope(double x1, double y1, 
	    		double x2, double y2) 
	    { 
	    	if ((x2 - x1) != 0) {		
	    		return (y2 - y1) / (x2 - x1); 	}
	    	else {
	    		return  1000000.0;
	    	}
	    } 
		
		public boolean getPaletteKnifeRotation(Point2D.Double startPoint, Point2D.Double maxArcPoint, Point2D.Double endPoint) {

			boolean rotateInClockwiseDirection = true;
		
			if (startPoint.getX() == endPoint.getX()) {
				//VERTICAL LINES
				if (startPoint.getY() < endPoint.getY()) {
                    //LINE UP  
					//IF maxArcX < startX CLOCKWISE     
					//IF maxArcX > startX COUNTERCLOCKWISE     
					rotateInClockwiseDirection = maxArcPoint.getX() < startPoint.getX();
				} else {
				    //LINE DOWN
					//IF maxArcX > startX CLOCKWISE     
					//IF maxArcX < startX COUNTERCLOCKWISE     
					rotateInClockwiseDirection = maxArcPoint.getX() > startPoint.getX();
				}
			} else {
				double lineSlope = slope(startPoint.getX(), startPoint.getY(), endPoint.getX(), endPoint.getY());
				double lineHeadingInRadians = Math.atan(lineSlope);

				double maxArcSlope = slope(startPoint.getX(), startPoint.getY(), maxArcPoint.getX(), maxArcPoint.getY());
				double maxArcHeadingInRadians = Math.atan(maxArcSlope);				//REGULAR SLOPE

				//CHECK TO SEE IF RADIAN VALUE IS ON SAME SIDE OF Y AXIS
				if (Math.abs(lineHeadingInRadians - maxArcHeadingInRadians) <= Math.PI/2) {
					//IF ON SAME SIDE OF Y AXIS - SIGNS MATCH AND HANDLE NORMALLY
					rotateInClockwiseDirection = maxArcHeadingInRadians > lineHeadingInRadians;				
				} else { 
					//If they are on opposite sides of Y AXIS
					//If radians are negative clockwise / if positive, counterclockwise
					rotateInClockwiseDirection = maxArcHeadingInRadians < 0;
				}
				
				//IF ARC > LINE RADIANS  CLOCKWISE     
				//IF ARC < LINE RADIANS  COUNTERCLOCKWISE     
			}
			
			return rotateInClockwiseDirection;		
			
		}
	    
	    Point2D.Double getMidpoint(Point2D.Double p1, Point2D.Double p2) {
	    	Point2D.Double midpoint = new Point2D.Double( (p1.getX()+p2.getX())/2  , (p1.getY()+p2.getY())/2);
	    	return midpoint;
	    }
	    
	    boolean isLineLongerThanPaletteKnifeWidth(Point2D.Double a, Point2D.Double b) {
	    	double lineDistance = Math.sqrt( (((b.getX() - a.getX())*(b.getX() - a.getX())) + ((b.getY() - a.getY())*(b.getY() - a.getY()))) );
	    	double lineDistanceMM = lineDistance * ((averagePixelWidthPerMm+averagePixelHeightPerMm)/2);
	    	boolean lineLongerThanPaletteKnifeWidth = (lineDistanceMM > this.brushWidth_mm);
	    	return lineLongerThanPaletteKnifeWidth;
	    }
	    
	    Point2D.Double getPaletteKnifePerpendicularOffsetAtStart(Point2D.Double a, Point2D.Double b) {

	    	double widthOffset = (brushWidth_mm*((averagePixelWidthPerMm+averagePixelHeightPerMm)/2))/2;
	    	double lengthOffset = brushLength_mm*((averagePixelWidthPerMm+averagePixelHeightPerMm)/2);

	    	//f = 0.3;
	    	//xp = f * x1 + (1-f) * x2;
	    	//yp = f * y1 + (1-f) * y2;1
	    	double lineDistance = Math.sqrt( (((b.getX() - a.getX())*(b.getX() - a.getX())) + ((b.getY() - a.getY())*(b.getY() - a.getY()))) );
	    	double ratio =  widthOffset/lineDistance;		
	    	//get length of line
	    	//create ratio 
	    	//double len = l.p1.distance(l.p2);
	    	//double ratio = d/len;
	    	double mx = ratio*b.getX() + (1.0 - ratio)*a.getX();
	    	double my = ratio*b.getY() + (1.0 - ratio)*a.getY();
	    	
	    	Point2D.Double m = new Point2D.Double(mx, my);
	    	
	    	double dx = a.getX() - m.getX();
	    	double dy = a.getY() - m.getY();

	    	double dist = Math.sqrt(dx * dx + dy * dy);

	    	
	    	double normX = dx / dist;
	    	double normY = dy / dist;
	    	
	    	double xPerp = lengthOffset * normX;
	    	double yPerp = lengthOffset * normY;
	    	
	    	double px = 0.0;
	    	double py = 0.0;
	    	
	    	if (this.clockwiseRotation) {
		    	//If clockwise
	    		px = m.getX() - yPerp;  //PERP POINT
	    		py = m.getY() + xPerp;  //PERP POINT
	    	} else {
		    	//If counterclockwise
		    	px = m.getX() + yPerp;  //PERP POINT
		    	py = m.getY() - xPerp;  //PERP POINT
	    	}

	    	Point2D.Double brushOffset = new Point2D.Double(px, py);
	    	return brushOffset;
	    	
	    	/*
	    	52.5,               136.0
	    	73.90654659215544,  194.35734808071305   x+20  y+60
	    	622.0,              147.0
	    	600.5934534078445,  88.64265191928695    x-20  y-60


	    	628.0,191.0
	    	608.1384020862959,132.0989198712794
	    	59.0,165.0
	    	78.86159791370423,223.9010801287206
	    	*/
	    	
	    	
	    }
	    
	    Point2D.Double getPaletteKnifePerpendicularOffsetAtEnd(Point2D.Double b, Point2D.Double a) {

	    	double widthOffset = (brushWidth_mm*((averagePixelWidthPerMm+averagePixelHeightPerMm)/2))/2;
	    	double lengthOffset = brushLength_mm*((averagePixelWidthPerMm+averagePixelHeightPerMm)/2);

	    	//f = 0.3;
	    	//xp = f * x1 + (1-f) * x2;
	    	//yp = f * y1 + (1-f) * y2;1
	    	double lineDistance = Math.sqrt( (((b.getX() - a.getX())*(b.getX() - a.getX())) + ((b.getY() - a.getY())*(b.getY() - a.getY()))) );
	    	double ratio =  widthOffset/lineDistance;		
	    	//get length of line
	    	//create ratio 
	    	//double len = l.p1.distance(l.p2);
	    	//double ratio = d/len;
	    	double mx = ratio*b.getX() + (1.0 - ratio)*a.getX();
	    	double my = ratio*b.getY() + (1.0 - ratio)*a.getY();
	    	
	    	Point2D.Double m = new Point2D.Double(mx, my);
	    	
	    	double dx = a.getX() - m.getX();
	    	double dy = a.getY() - m.getY();

	    	double dist = Math.sqrt(dx * dx + dy * dy);

	    	
	    	double normX = dx / dist;
	    	double normY = dy / dist;
	    	
	    	double xPerp = lengthOffset * normX;
	    	double yPerp = lengthOffset * normY;
	    	
	    	double px = 0.0;
	    	double py = 0.0;
	    	
	    	if (this.clockwiseRotation) {
		    	//If clockwise
	    		px = m.getX() + yPerp;  //PERP POINT
	    		py = m.getY() - xPerp;  //PERP POINT
	    	} else {
		    	//If counterclockwise
		    	px = m.getX() - yPerp;  //PERP POINT
		    	py = m.getY() + xPerp;  //PERP POINT
	    	}

	    	Point2D.Double brushOffset = new Point2D.Double(px, py);
	    	return brushOffset;
	    	
	    }

	    Point2D.Double getPaletteKnifeOffsetOnLine(Point2D.Double a, Point2D.Double b, Point2D.Double c) {

	    	double lengthOffset = brushLength_mm*((averagePixelWidthPerMm+averagePixelHeightPerMm)/2);
	    	// 8
	    	double lineDistance = Math.sqrt( (((b.getX() - a.getX())*(b.getX() - a.getX())) + ((b.getY() - a.getY())*(b.getY() - a.getY()))) );
	    	// 42
	    	double ratio =  lengthOffset/lineDistance;		
	    	// 1/5
	    	double mx = ratio*b.getX() + (1.0 - ratio)*a.getX();
	    	double my = ratio*b.getY() + (1.0 - ratio)*a.getY();

	    	double deltaX = mx-a.getX();
	    	double deltaY = my-a.getY();
	    	
	    	Point2D.Double brushOffset = new Point2D.Double(c.getX()+deltaX, c.getY()+deltaY);
	    	return brushOffset;
	    	
	    }
	    
	    
	    
	}
