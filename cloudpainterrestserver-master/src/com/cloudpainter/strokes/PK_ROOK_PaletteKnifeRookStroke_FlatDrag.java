package com.cloudpainter.strokes;


import java.awt.geom.Point2D;
import java.util.Vector;

	import org.json.JSONArray;
import org.json.JSONObject;

	//12.1 cm

	//6 light
	//9 heavy



import com.cloudpainter.robots.RobotTemplate;

	public class PK_ROOK_PaletteKnifeRookStroke_FlatDrag  extends PaletteKnifeTemplate{

		
		public PK_ROOK_PaletteKnifeRookStroke_FlatDrag (int strokeNumber, JSONObject JSONStroke, RobotTemplate robotObject, double brushLength,  double brushWidth, String strokeApproachAxis) {
			
			super(JSONStroke, robotObject, (brushLength), brushWidth, strokeApproachAxis);

			//double kissingTheCanvas = 13.7; //17.3 - is keychain  //14.1  is for white knife;//13.8 ;
			
			//PaletteKnife Strokes have 3 points only!  Angle of Movement is from first to last
			//TODO: if more than 1 cm - calculate a shift in X/Y perpendicular to that arc
			
			Point2D.Double startPointAbsolute    = (Point2D.Double)pathVector.elementAt(0);
			Point2D.Double endPointAbsolute    = (Point2D.Double)pathVector.elementAt(2);
			
			Point2D.Double knifeOffsetAtStartAbsolute = getPaletteKnifeOffsetOnLine(startPointAbsolute, endPointAbsolute, startPointAbsolute);
			Point2D.Double knifeOffsetAtEndAbsolute   = getPaletteKnifeOffsetOnLine(startPointAbsolute, endPointAbsolute, endPointAbsolute);

			System.out.println( startPointAbsolute.getX() +","+startPointAbsolute.getY());
			System.out.println( knifeOffsetAtStartAbsolute.getX() +","+knifeOffsetAtStartAbsolute.getY());

			System.out.println( endPointAbsolute.getX() +","+endPointAbsolute.getY());
			System.out.println( knifeOffsetAtEndAbsolute.getX() +","+knifeOffsetAtEndAbsolute.getY());
			
			Point2D.Double knifeOffsetAt50percent           = getMidpoint(knifeOffsetAtStartAbsolute, knifeOffsetAtEndAbsolute);
			Point2D.Double knifeOffsetAt25percent           = getMidpoint(knifeOffsetAtStartAbsolute, knifeOffsetAt50percent);
			Point2D.Double knifeOffsetAt75percent           = getMidpoint(knifeOffsetAt50percent, knifeOffsetAtEndAbsolute);
			Point2D.Double knifeOffsetAt37percent           = getMidpoint(knifeOffsetAt25percent, knifeOffsetAt50percent);
			Point2D.Double knifeOffsetAt62percent           = getMidpoint(knifeOffsetAt50percent, knifeOffsetAt75percent);
			
			Point2D.Double canvasPointMm1 = this.getPointOnPaintingSurface(knifeOffsetAtStartAbsolute);
			Point2D.Double canvasPointMm2 = this.getPointOnPaintingSurface(knifeOffsetAt37percent);
			Point2D.Double canvasPointMm3 = this.getPointOnPaintingSurface(knifeOffsetAt62percent);
			Point2D.Double canvasPointMm4 = this.getPointOnPaintingSurface(knifeOffsetAtEndAbsolute);
			
			double startAngleOfRotationalBrushInRadians = 0.0;
			int direction = 1;
			
			double lastHeadingInRadians = 0.0;
			startAngleOfRotationalBrushInRadians = getAngleofRotationalBrushInRadians(startPointAbsolute, endPointAbsolute, lastHeadingInRadians);

			//if getAngleOfRotationalBrushInRadians has flipped the radian direction to save travel - flip direction of offset
			if (!this.clockwiseRotation) {
				direction = direction * -1;
			}

			startAngleOfRotationalBrushInRadians = startAngleOfRotationalBrushInRadians + (direction*(Math.PI));
			
			gcodeStringBuffer.append("\n");
			gcodeStringBuffer.append("; stroke "+strokeNumber+" \n");
			gcodeStringBuffer.append("; type:  "+this.toString()+" \n");
			gcodeStringBuffer.append("; recipe strokeA2AgBe_N \n");

			gcodeStringBuffer.append("G4 P1000                      ; PAUSE (REFILL PAINT)\n");
			//Set speeds
			gcodeStringBuffer.append("M204 P"+robo.maxAccelSpeedPainting+" T"+robo.maxAccelSpeedPainting+"               ; ACCEL SPEED\n");
			gcodeStringBuffer.append("M203 E5                        ; MAX ACCEL SPEED ROTATION\n");
			gcodeStringBuffer.append("G1 F"+robo.xyfullspeed+"                      ; XY SPEED\n");
			
			//start position
			gcodeStringBuffer.append("G1 X"+robo.mmX(canvasPointMm1.getX())+" Y"+robo.mmY(canvasPointMm1.getY())+" E0.0          ; START POINT WITH PALETTE KNIFE IN HOME POSITION\n");
			gcodeStringBuffer.append("G1 Z"+robo.mmZ(robo.zRetracted)+"                        ; LOWER PALETTE BELOW AXIS\n");

			double angleOfRotationalBrushInRadians = startAngleOfRotationalBrushInRadians;
			//19.2 is old max value
			gcodeStringBuffer.append("G1 Z"+robo.mmZ((robo.zLightTouch-5.2))+" E"+angleOfRotationalBrushInRadians+"                ; ROTATE INTO START OF SWEEP\n"); 
			gcodeStringBuffer.append("G1 Z"+robo.mmZ((robo.zLightTouch-0.2))+"                       ; LOWER JUST ABOVE CANVAS\n");
			gcodeStringBuffer.append("G1 Z"+robo.mmZ((robo.zLightTouch))+"                       ; TOUCH CANVAS (WHEN CALIBRATING THIS IS SLIGHTLY TOUCHING CANVAS\n)");
			gcodeStringBuffer.append("G4 P1000                       ; pause 1 sec\n");
			gcodeStringBuffer.append("G1 Z"+robo.mmZ((robo.zLightTouch-0.1))+"                       ; LIFT BACK UP A TOUCH\n");
			
			gcodeStringBuffer.append("M204 P10 T10                   ; LOWER ACCELERATION\n");
			gcodeStringBuffer.append("M203 E2                        ; LOWER ROTATIONAL ACCELERATION\n");
			gcodeStringBuffer.append("G1 X"+canvasPointMm2.getX()+" Y"+canvasPointMm2.getY()+" Z"+(robo.zLightTouch-0.1)+" E"+angleOfRotationalBrushInRadians+"          ; START POINT WITH PALETTE KNIFE IN HOME POSITION\n");
			gcodeStringBuffer.append("G1 X"+canvasPointMm3.getX()+" Y"+canvasPointMm3.getY()+" Z"+(robo.zLightTouch)+" E"+angleOfRotationalBrushInRadians+"          ; START POINT WITH PALETTE KNIFE IN HOME POSITION\n");
			gcodeStringBuffer.append("G1 X"+canvasPointMm4.getX()+" Y"+canvasPointMm4.getY()+" Z"+(robo.zLightTouch-1.1)+" E"+angleOfRotationalBrushInRadians+"          ; START POINT WITH PALETTE KNIFE IN HOME POSITION\n");

 
			gcodeStringBuffer.append("G1 Z"+(robo.zLightTouch-6.2)+" E0                      ; POSITION PALETTE KNIFE IN HOME POSITION\n");
			gcodeStringBuffer.append("G1 Z"+(robo.zLightTouch-8.2)+" E0                       ; POSITION PALETTE KNIFE IN HOME POSITION\n");
			//gcodeStringBuffer.append("G4 P10000                      ; PAUSE (REFILL PAINT)\n");
			
			gcodeStringBuffer.append("M204 P1000 T1000               ; RETURN TO FULL SPEED\n");
			gcodeStringBuffer.append("M203 E5                        ; RETURN ROTATION TO FULL SPEED\n");  
			
			gcodeStringBuffer.append("\n");
			gcodeStringBuffer.append("\n");

			
		}

		
		
		
	}
