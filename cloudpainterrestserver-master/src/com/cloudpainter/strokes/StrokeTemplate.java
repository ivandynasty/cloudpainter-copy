package com.cloudpainter.strokes;

import java.util.Vector;

public abstract class StrokeTemplate {

	public StrokeTemplate() {
		
	}

	abstract public String getGcodeString();

	abstract public Vector getIndividualGCodeLines();

	abstract public Vector getIndividualRoboForthLines();

	
}
