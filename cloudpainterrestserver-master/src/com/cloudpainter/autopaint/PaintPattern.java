package com.cloudpainter.autopaint;

import java.util.ArrayList;

public abstract class PaintPattern {
 
	
	public enum FindNextStrokeStartPointStrategy {
		CLOSEST, SAME_ROW, SAME_COLLUMN, RANDOM
	}

	public enum DirectionToSearchForNextStrokeStartRow {
		ABOVE, SAME, BELOW
	}

	public enum DirectionToSearchForNextStrokeStartCol {
		LEFT, SAME, RIGHT
	}
	
	//Not implemented
	//Not implemented
	//Not implemented
	FindNextStrokeStartPointStrategy scanForNextStartPointStrategy;
	DirectionToSearchForNextStrokeStartRow scanForNextRowStrategy;
	DirectionToSearchForNextStrokeStartCol scanForNextColStrategy;
	
	//An Array List that Specifies The Sequence if Stroke Lengths
	//to go 1 inch at a time , specify only one value 1.0
	//to go 1 inch then 2 inch and repeate - 1.0,2.0
	//to go in square pattern PaintMapDirection.SOUTH,...WEST,...NORTH...EAST
	public int strokeLengthIndex;
	ArrayList<Double> strokeLengthSequence;

	//An Array List that Specifies The Direction You Want A Stroke Sequence
	//to go south only, specify only one value PaintMapDirection.SOUTH
	//to go south then north repeatedly PaintMapDirection.SOUTH,PaintMapDirection.North
	//to go in square pattern PaintMapDirection.SOUTH,...WEST,...NORTH...EAST
	public int strokeDirectionIndex;
	ArrayList<PaintMapDirection> strokeDirectionSequence;  

	public PaintPattern() {
		strokeDirectionIndex = 0;
		strokeLengthIndex = 0;
	}

	public void setPaintStrokeDirectionSequence(ArrayList<PaintMapDirection> directionSequence){
		this.strokeDirectionSequence = directionSequence;
	}
	
	public void setPaintStrokeLengthPattern(ArrayList<Double> strokeLengthCycle){
		this.strokeLengthSequence = strokeLengthCycle;
	}
	
	public void setScanForNextStartPointStrategy(FindNextStrokeStartPointStrategy scanForNextStartPointStrategy){
		this.scanForNextStartPointStrategy = scanForNextStartPointStrategy;
	}
	
	public void setScanForNextRowStrategy(DirectionToSearchForNextStrokeStartRow scanForNextRowStrategy) {
		this.scanForNextRowStrategy = scanForNextRowStrategy;
	}
	
	public void setScanForNextColStrategy(DirectionToSearchForNextStrokeStartCol scanForNextColStrategy) {
		this.scanForNextColStrategy = scanForNextColStrategy;
	}
	
	public void resetStrokeLengthIndex() {
		strokeLengthIndex = 0;	
	}

	public void resetStrokeDirectionIndex() {
		strokeDirectionIndex = 0;	
	}
	
	public PaintMapDirection getNextStrokeDirection() {
		PaintMapDirection currentDir = strokeDirectionSequence.get(strokeDirectionIndex);
		strokeDirectionIndex++;
		if (strokeDirectionIndex == strokeDirectionSequence.size()) {
			strokeDirectionIndex = 0;
		}
		return currentDir;
	}

	public Double getNextStrokeLength() {
		Double currentLength = strokeLengthSequence.get(strokeLengthIndex);
		strokeLengthIndex++;
		if (strokeLengthIndex == strokeLengthSequence.size()) {
			strokeLengthIndex = 0;
		}
		return currentLength;
	}

}
