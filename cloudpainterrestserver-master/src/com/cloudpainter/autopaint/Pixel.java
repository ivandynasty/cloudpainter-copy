package com.cloudpainter.autopaint;

import java.util.Vector;

public class Pixel {

	int x;
	int y;
	
	int red;
	int green;
	int blue;
	
	float hue;
	float saturation;
	float brightness;

	float xPosition;
	float yPosition;

	static float rW  = 0.3f;
	static float gW  = 0.3f;
	static float bW  = 0.3f;
	static float hW  = 0.3f;
	static float sW  = 0.3f;
	static float brW = 0.3f;
	static float xW  = 0.3f;
	static float yW  = 0.3f;
	
	public Pixel(int x, int y, int red, int green, int blue, float h, float s, float b, float xp, float yp) {
		this.x = x;
		this.y = y;
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.hue = h;
		this.saturation = s;
		this.brightness = b;
		this.xPosition = xp;
		this.yPosition = yp;
	}
	
	public static void setWeights(float redW, float grnW, float bluW, float hueW, float satW, float briW, float xW, float yW ) {
		rW = redW;
		gW = grnW;
		bW = bluW;
		hW = hueW;
		sW = satW;
		brW = briW;
		xW = xW;
		yW = yW;
	}
	
	
}
