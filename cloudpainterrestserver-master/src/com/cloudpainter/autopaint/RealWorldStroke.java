package com.cloudpainter.autopaint;

import java.awt.Color;

import com.cloudpainter.painting.CanvasPoint;

public class RealWorldStroke
{
	private final Color strokeColor;
	private final CanvasPoint start;
	private final CanvasPoint end;

	public RealWorldStroke(
			final CanvasPoint start,
			final CanvasPoint end,
			final Color strokeColor ) {
		this.start = start;
		this.end = end;
		this.strokeColor = strokeColor;
	}

	public CanvasPoint getStart() {
		return start;
	}

	public CanvasPoint getEnd() {
		return end;
	}

	public Color getStrokeColor() {
		return strokeColor;
	}
}
