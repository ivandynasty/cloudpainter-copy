package com.cloudpainter.autopaint;

public class PaintMapStroke {
	
	//Every coordinate is in inches with 0,0 being bottom left.
	//standard cartesian coordinates with only positive values
	
	//xMin and yOfXMin should be point closest to 0,0 
	//Constructor enforces this
	
	private double xMin;
	private double yOfXMin;
	private double xMax;
	private double yOfXMax;
	
	public PaintMapStroke(double startX, double startY, double endX, double endY) {
		//always start stroke at coordinate closest to 0,0
		if (startX < endX) {
			this.xMin = startX;
			this.yOfXMin = startY;
			this.xMax = endX;
			this.yOfXMax = endY;
		} else if (startX > endX){
			this.xMin = endX;
			this.yOfXMin = endY;
			this.xMax = startX;
			this.yOfXMax = startY;
		} else if (startX == endX){ 
				if (startY < endY) {
					this.xMin = startX;
					this.yOfXMin = startY;
					this.xMax = endX;
					this.yOfXMax = endY;
				} else {
					this.xMin = endX;
					this.yOfXMin = endY;
					this.xMax = startX;
					this.yOfXMax = startY;
				}
		}
	}

	public double getxMin() {
		return xMin;
	}

	public double getyOfXMin() {
		return yOfXMin;
	}

	public double getxMax() {
		return xMax;
	}

	public double getyOfXMax() {
		return yOfXMax;
	}
	
	
	
}
