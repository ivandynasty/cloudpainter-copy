package com.cloudpainter.autopaint;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Vector;

import javax.imageio.ImageIO;



import com.cloudpainter.painting.BitmapPoint;
import com.cloudpainter.painting.CanvasPoint;
import com.cloudpainter.painting.Paint;

public class PaintMap
{

	private String paintMapImageUrl;

	private BufferedImage sourceImage;
	
	
	private final int width; // inches
	private final int height; // inches
	private final int strokesPerInch;

	// d2 = (30*(r1-r2))**2 + (59*(g1-g2))**2 + (11*(b1-b2))**2;
	private final double redWeight = 0.30;
	private final double greenWeight = 0.59;
	private final double blueWeight = 0.11;

	private ArrayList<ArrayList<PaintMapColor>> paintMap;
	private final PaletteMap paletteMap;
	private final HashSet<Color> gifPalette;
	private List<Paint> robotPalette;

	// creates paint map
	public PaintMap(
			final int width,
			final int height,
			final int strokesPerInch ) {
		this.width = width;
		this.height = height;
		this.strokesPerInch = strokesPerInch;
		gifPalette = new HashSet<Color>(
				256);
		paletteMap = new PaletteMap();
	}

	public ArrayList<ArrayList<PaintMapColor>> getPaintmap() {
		return paintMap;
	}
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return width;
	}

	//public void createPaintMapFromBufferedImage()
	
	public void createPaintMapFromBufferedImage(
			final BufferedImage image ) {
			paintMap = new ArrayList<ArrayList<PaintMapColor>>();
			final int paintMapRows = image.getHeight();
			final int paintMapCols = image.getWidth();

			sourceImage = image;
			for (int j = 0; j < paintMapCols; j++) {
				final ArrayList<PaintMapColor> paintCol = new ArrayList<PaintMapColor>();
				final int xPosOnBufferedImage = getSourceImageX(
						paintMapCols,
						j);
				//for (int i = paintMapRows - 1; i >= 0; i--) {
				for (int i = 0; i < paintMapRows; i++) {
					final int yPosOnBufferedImage = getSourceImageY(
							paintMapRows,
							i);
					final int rgbColor = sourceImage.getRGB(
							xPosOnBufferedImage,
							yPosOnBufferedImage);
					final PaintMapColor thisColor = new PaintMapColor(
							rgbColor);
					paintCol.add(thisColor);
					gifPalette.add(thisColor);
				}
				paintMap.add(paintCol);
				// paintCol.clear();
			}
		
	}
	
	public void updatePaintMapFromBufferedImage(
			final BufferedImage image ) {
			final int paintMapRows = image.getHeight();
			final int paintMapCols = image.getWidth();
			sourceImage = image;
			for (int j = 0; j < paintMapCols; j++) {
				final ArrayList<PaintMapColor> paintCol = new ArrayList<PaintMapColor>();
				final int xPosOnBufferedImage = getSourceImageX(
						paintMapCols,
						j);
				for (int i = 0; i < paintMapRows; i++) {
					final int yPosOnBufferedImage = getSourceImageY(
							paintMapRows,
							i);
					PaintMapColor rgbColor = new PaintMapColor(image.getRGB(
							xPosOnBufferedImage,
							yPosOnBufferedImage));
					if ( ( rgbColor.getRed() == 255 &&
							rgbColor.getGreen() == 0 &&
							rgbColor.getBlue() == 0 ) 
							) {
						this.setPaintMapPixelAsPainted(xPosOnBufferedImage,yPosOnBufferedImage,1);
					}
				}
			}
		
	}
	
	
	
	public BufferedImage getBufferedImageFromPaintMap() {

			final int paintMapRows = sourceImage.getHeight();
			final int paintMapCols = sourceImage.getWidth();
			BufferedImage paintmapImage = new BufferedImage(paintMapCols, paintMapRows, BufferedImage.TYPE_INT_RGB);
			for (int j = 0; j < paintMapCols; j++) {
				for (int i = 0; i < paintMapRows; i++) {
					final PaintMapColor pmColor = this.getPaintMapColorFromPaintMapXY(j, i);
					if (pmColor.isPainted) {
						paintmapImage.setRGB(j, i, new Color(255,0,0).getRGB());
					} else {
						paintmapImage.setRGB(j, i, pmColor.getRGB());
					}
					
				}
			}
			return paintmapImage;
	}
	
	// converts an image into a paint map
	public void createPaintMapFromFilename(
			final String imageFileName ) {
		paintMapImageUrl = imageFileName;
		paintMap = new ArrayList<ArrayList<PaintMapColor>>();
		final int paintMapRows = height * strokesPerInch;
		final int paintMapCols = width * strokesPerInch;
		try {
			sourceImage = ImageIO.read(new File(
					paintMapImageUrl)); // 228x302
			// get row worth of colors from image
			// get col worth of colors from image
			// by iteration through each paintMapRow and getting corresponding
			// pixel from sourceImage
			// go to start of first col
			// 0,0
			// get entire col
			// 0,0 ... 0,99
			// go to second col
			// 1,0
			// get entire col
			// 1,0 ... 1,99
			for (int j = 0; j < paintMapCols; j++) {
				final ArrayList<PaintMapColor> paintCol = new ArrayList<PaintMapColor>();
				final int xPosOnBufferedImage = getSourceImageX(
						paintMapCols,
						j);
				for (int i = paintMapRows - 1; i >= 0; i--) {
					final int yPosOnBufferedImage = getSourceImageY(
							paintMapRows,
							i);
					final int rgbColor = sourceImage.getRGB(
							xPosOnBufferedImage,
							yPosOnBufferedImage);
					final PaintMapColor thisColor = new PaintMapColor(
							rgbColor);
					paintCol.add(thisColor);
					gifPalette.add(thisColor);
				}
				paintMap.add(paintCol);
				// paintCol.clear();
			}
		}
		catch (final IOException e) {
			//p911 add logger
		}
	}

	// Gets the X position of the sourceImage that corresponds to the X position
	// of the PaintMap
	public int getSourceImageX(
			final int paintMapWidth,
			final int paintMapX ) {
		// eg
		// 50 cols in sourceImage
		// 200 cols in paintMap
		// 0 returns 0
		// 5 returns 1
		// 6 returns 2
		// 8 returns 2
		// 9 returns 3
		// 197 returns 49
		// 198 returns 49
		// 199 returns 49
		final int sourceImageWidth = sourceImage.getWidth();
		final double step = ((double) sourceImageWidth) / ((double) paintMapWidth); // .25
		final double sourceImageX = (paintMapX) * step;
		return (int) sourceImageX;
	}

	// Gets the X position of the sourceImage that corresponds to the X position
	// of the PaintMap
	public int getSourceImageY(
			final int paintMapHeight,
			final int paintMapY ) {
		// eg
		// 50 rows in sourceImage
		// 200 rows in paintMap
		// 0 returns 0
		// 5 returns 1
		// 6 returns 2
		// 8 returns 2
		// 9 returns 3
		// 197 returns 49
		// 198 returns 49
		// 199 returns 49
		final int sourceImageHeight = sourceImage.getHeight();
		final double step = ((double) sourceImageHeight) / ((double) paintMapHeight); // .25
		final double sourceImageY = (paintMapY) * step;
		return (int) sourceImageY;
	}

	// read paint map and get a random point that has not yet been painted
	public BitmapPoint getRandomStartPoint() {
		// If painting is complete -ie- no empty spots - it returns NULL
		//final int paintMapWidth = width * strokesPerInch;
		//final int paintMapHeight = height * strokesPerInch;
		final int randomX = getRandomInteger(width);
		final int randomY = getRandomInteger(height);
		// first try each point from random point onward ---
		for (int i = randomX; i < width; i++) {
			for (int j = randomY; j < height; j++) {
				if (!paintMap.get(
						i).get(
						j).isPainted()) {
					return new BitmapPoint(
							i,
							j);
				}
			}
		}
		// if nothing found after random point restart from 0,0 and try until
		// random point
		for (int i = 0; i < randomX; i++) {
			for (int j = 0; j < randomY; j++) {
				if (!paintMap.get(
						i).get(
						j).isPainted()) {
					return new BitmapPoint(
							i,
							j);
				}
			}
		}
		// if no points found, return NULL
		return null;
	}

	
	
	
	
	// read paint map and get a random point of a specific color that has not
	// yet been painted
	public BitmapPoint getRandomStartPointOfColor(
			final Color color ) {
		// If color is complete -ie- no empty spots of color- it returns NULL
		final int randomX = getRandomInteger(width);
		final int randomY = getRandomInteger(height);
		// first try each point from random point onward ---
		for (int i = randomX; i < width; i++) {
			for (int j = randomY; j < height; j++) {
				if (!paintMap.get(i).get(j).isPainted() && paintMap.get(i).get(j).equals(color)) {
					return new BitmapPoint(
							i,
							j);
				}
			}
		}
		// if nothing found after random point restart from 0,0 and try until
		// random point
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (!paintMap.get(i).get(j).isPainted() && paintMap.get(i).get(j).equals(color)) {
					return new BitmapPoint(
							i,
							j);
				}
			}
		}
		// if no points found, return NULL
		return null;
	}

	// read paint map and get the color from specified real world coordinate
	public Color getColorFromPaintMapCoordinate(
			final CanvasPoint coordinate ) {
		final int xCoord = (int) (coordinate.getX() * (strokesPerInch));
		final int yCoord = (int) (coordinate.getY() * (strokesPerInch));
		return paintMap.get(
				xCoord).get(
				yCoord);
	}

	public Color getColorFromPaintMapXY(int x, int y ) {
		return paintMap.get(
				x).get(
				y);
	}
	
	public PaintMapColor getPaintMapColorFromPaintMapXY(int x, int y ) {
		return paintMap.get(
				x).get(
				y);
	}
	
	// read paint map and get the next point that has not yet been painted
	public CanvasPoint getNextStartPoint(
			final double xCoord,
			final double yCoord ) {
		// If color is complete -ie- no empty spots of color- it returns NULL
		final int paintMapWidth = width * strokesPerInch;
		final int paintMapHeight = height * strokesPerInch;
		final int paintMapX = (int) (xCoord * (paintMapWidth));
		final int paintMapY = (int) (yCoord * (paintMapWidth));
		// first try each point from random point onward ---
		for (int i = paintMapX; i < paintMapWidth; i++) {
			for (int j = paintMapY; j < paintMapHeight; j++) {
				if (!paintMap.get(
						i).get(
						j).isPainted()) {
					final double xInches = ((double) i) / ((double) strokesPerInch);
					final double yInches = ((double) j) / ((double) strokesPerInch);
					return new CanvasPoint(
							xInches,
							yInches);
				}
			}
		}
		// if nothing found after random point restart from 0,0 and try until
		// random point
		for (int i = 0; i < paintMapX; i++) {
			for (int j = 0; j < paintMapY; j++) {
				if (!paintMap.get(
						i).get(
						j).isPainted()) {
					final double xInches = ((double) i) / ((double) strokesPerInch);
					final double yInches = ((double) j) / ((double) strokesPerInch);
					return new CanvasPoint(
							xInches,
							yInches);
				}
			}
		}
		// if no points found, return NULL
		return null;
	}

	// read paint map and get the next point of a specific color that has not
	// yet been painted
	public BitmapPoint getNextStartPointOfColor(
			final int xCoord,
			final int yCoord,
			final Color paletteColor ) {
		// If color is complete -ie- no empty spots of color- it returns NULL

		int paintMapX = (xCoord);
		int paintMapY = (yCoord);
		// first try each point from random point onward ---
		if (paintMapY == 0) {
			paintMapY = height - 1;
			paintMapX = paintMapX + 1;
		}
		else {
			paintMapY = paintMapY - 1;
		}
		for (int i = paintMapX; i < width; i++) {
			for (int j = paintMapY; j >= 0; j--) {
				// this is color in GIF
				final PaintMapColor colorFromMap = paintMap.get(
						i).get(
						j);
				if (!colorFromMap.isPainted() && colorFromMap.equals(paletteColor)) {
					return new BitmapPoint(
							i,
							j);
				}
			}
			paintMapY = height - 1;
		}
		// if nothing found after random point restart from top left and try
		// until random point

		paintMapX = 0;
		paintMapY = height - 1;
		for (int i = 0; i < width; i++) {
			for (int j = paintMapY; j >= 0; j--) {
				// this is color in GIF
				final PaintMapColor colorFromMap = paintMap.get(
						i).get(
						j);
				// robotColor is color in robot
				if (!colorFromMap.isPainted() && colorFromMap.equals(paletteColor)) {
					return new BitmapPoint(
							i,
							j);
				}
			}
			paintMapY = height - 1;
		}
		// if no points found, return NULL
		return null;
	}

	public BitmapPoint getNextStartPointOfColorRadially(
			final int xCoord,
			final int yCoord,
			final int maxCanvasDimension,
			final Color paletteColor ) {
		// If color is complete -ie- no empty spots of color- it returns NULL

		int paintMapX = (xCoord);
		int paintMapY = (yCoord);

		for(int radius=0; radius<maxCanvasDimension; radius++) {
			int thisX = paintMapX-radius;
			int thisY = paintMapY-radius;
			if (thisX<0) {
				thisX=0;
			}
			if (thisY<0) {
				thisY=0;
			}
				for (int xx=thisX; xx< thisX+((radius*2)+1); xx++) {
					for (int yy=thisY; yy< thisY+((radius*2)+1); yy++) {
						int xOnCanvas = xx;
						int yOnCanvas = yy;
						if (xOnCanvas>=maxCanvasDimension) {
							xOnCanvas=maxCanvasDimension-1;
						}
						if (yOnCanvas>=maxCanvasDimension) {
							yOnCanvas=maxCanvasDimension-1;
						}
						final PaintMapColor colorFromMap = paintMap.get(
								xOnCanvas).get(
										yOnCanvas);
						if (!colorFromMap.isPainted() && colorFromMap.equals(paletteColor)) {
							return new BitmapPoint(
									xOnCanvas,
									yOnCanvas);
						}
					}
				}
			}		
		return null;
	}
	
	
	
	public void recordStrokeOnPaintMap(
			final RealWorldStroke RealWorldStroke ) {
		final PaintMapStroke paintMapStroke = new PaintMapStroke(
				RealWorldStroke.getStart().getX(),
				RealWorldStroke.getStart().getY(),
				RealWorldStroke.getEnd().getX(),
				RealWorldStroke.getEnd().getY());
		recordStrokeOnPaintMap(paintMapStroke);
	}
	
	public void recordBitmapStrokeOnPaintMap(
			final BitmapStroke bitmapStroke, boolean invertY, int height ) {
		
			int startX = bitmapStroke.getStart().getX();
			int startY = bitmapStroke.getStart().getY();
			int endX =   bitmapStroke.getEnd().getX();
			int endY =   bitmapStroke.getEnd().getY();
					
			if(invertY) {
				startY = height-1-startY;
				endY = height-1-endY;
			}
			
			
		final PaintMapStroke paintMapStroke = new PaintMapStroke(
				startX,
				startY,
				endX,
				endY);
		recordStrokeOnPaintMap(paintMapStroke);
	}
	
	public void recordBitmapStrokeOnPaintMap(
			final PaintMapStroke stroke ) {
		int paintMapStartX = (int) (stroke.getxMin() );
		int paintMapStartY = (int) (stroke.getyOfXMin() );
		int paintMapEndX = (int) (stroke.getxMax() );
		int paintMapEndY = (int) (stroke.getyOfXMax() );
		// 5,5 --> 15,15 and gonna hit 5,5 6,6 7,7 8,8 etc
		try { 
			paintMap.get(0).get(paintMapStartY);
			paintMap.get(0).get(paintMapEndY);
			paintMap.get(paintMapStartX).get(0);
			paintMap.get(paintMapEndX).get(0);
			
		}	catch (final java.lang.ArrayIndexOutOfBoundsException exception) {
			if (paintMapStartX<0) {
				paintMapStartX = 0;
			}
			if (paintMapEndX>paintMap.size()-1 ) {
				paintMapEndX = paintMap.size()-1;
			}
		} catch (final java.lang.IndexOutOfBoundsException exception) {
			if (paintMapStartY<0) {
				paintMapStartY = 0;
			}
			if (paintMapEndY> paintMap.get(0).size()-1 ) {
				paintMapEndY = paintMap.get(0).size()-1;
			}
		}
		final double rise = (paintMapEndY - paintMapStartY);
		final double run = (paintMapEndX - paintMapStartX);
		boolean verticalLine = false;
		double slope = 0;
		if (run != 0) {
			slope = rise / run;
		}
		else {
			verticalLine = true;
		}
		try {
			if (!verticalLine) {
				for (int i = 0; i <= run; i++) {
					final int x = paintMapStartX + i;
					final int y = paintMapStartY + (int) ((i) * slope);
					paintMap.get(
							x).get(
							y).markAsPainted();
				}
			}
			else { // 67 94
				for (int i = 0; i <= rise; i++) {
					paintMap.get(
							paintMapStartX).get(
							(paintMapStartY + i)).markAsPainted();
				}
			}
		}
		catch (final java.lang.ArrayIndexOutOfBoundsException exception) {
		} catch (final java.lang.IndexOutOfBoundsException exception) {
		}
	}
	
	

	// given a stroke, record that the pixels have been painted on paint map
	public void recordStrokeOnPaintMap(
			final PaintMapStroke stroke ) {
		// given stroke - record as painted - that is change corresponding array
		// color value to NULL
		// eg 1,1 to 3,3 will draw line from 5,5 to 15,15 and turn all colors in
		// paint map to null.
		// eg 1,1 to 1,3 will null 5,5 to 5,15
		// eg 1,1 to 3,1 will null 5,5 to 15,5
		// get start point by converting 1,1 to PaintMap Space =
		// (1.0*strokesPerInch,1.0*strokesPerInch)
		int paintMapStartX = (int) (stroke.getxMin() * (strokesPerInch));
		int paintMapStartY = (int) (stroke.getyOfXMin() * (strokesPerInch));
		int paintMapEndX = (int) (stroke.getxMax() * (strokesPerInch));
		int paintMapEndY = (int) (stroke.getyOfXMax() * (strokesPerInch));
		// 5,5 --> 15,15 and gonna hit 5,5 6,6 7,7 8,8 etc
		try { 
			paintMap.get(0).get(paintMapStartY);
			paintMap.get(0).get(paintMapEndY);
			paintMap.get(paintMapStartX).get(0);
			paintMap.get(paintMapEndX).get(0);
			
		}	catch (final java.lang.ArrayIndexOutOfBoundsException exception) {
			//p911 add logger.error("x went out of bounds - adjusting it");
			//p911 add logger.debug("start x:" + paintMapStartX + " y:" + paintMapStartY);
			//p911 add logger.debug("end   x:" + paintMapEndX + " y:" + paintMapEndY);
			if (paintMapStartX<0) {
				paintMapStartX = 0;
			}
			if (paintMapEndX>paintMap.size()-1 ) {
				paintMapEndX = paintMap.size()-1;
			}
		} catch (final java.lang.IndexOutOfBoundsException exception) {
			//p911 add logger.error("y went out of bounds - adjusting it");
			//p911 add logger.debug("start x:" + paintMapStartX + " y:" + paintMapStartY);
			//p911 add logger.debug("end   x:" + paintMapEndX + " y:" + paintMapEndY);
			if (paintMapStartY<0) {
				paintMapStartY = 0;
			}
			if (paintMapEndY> paintMap.get(0).size()-1 ) {
				paintMapEndY = paintMap.get(0).size()-1;
			}
		}
		// then calculate slope - since PaintMapStroke is always minX -> maxX we
		// only have
		// to worry about positive,0,& infinite slopes -
		// this restriction is handled in PaintMapStroke Constructor
		final double rise = (paintMapEndY - paintMapStartY);
		final double run = (paintMapEndX - paintMapStartX);
		boolean verticalLine = false;
		double slope = 0;
		if (run != 0) {
			slope = rise / run;
		}
		else {
			verticalLine = true;
		}
		try {
			if (!verticalLine) {
				for (int i = 0; i <= run; i++) {
					final int x = paintMapStartX + i;
					final int y = paintMapStartY + (int) ((i) * slope);
					paintMap.get(
							x).get(
							y).markAsPainted();
				}
			}
			else { // 67 94
				for (int i = 0; i <= rise; i++) {
					paintMap.get(
							paintMapStartX).get(
							(paintMapStartY + i)).markAsPainted();
				}
			}
		}
		catch (final java.lang.ArrayIndexOutOfBoundsException exception) {
			//p911 add logger.error("failed to as painted - x went out of bounds");
			//p911 add logger.debug("start x:" + paintMapStartX + " y:" + paintMapStartY);
			//p911 add logger.debug("end   x:" + paintMapEndX + " y:" + paintMapEndY);
		} catch (final java.lang.IndexOutOfBoundsException exception) {
			//p911 add logger.error("failed to as painted - y went out of bounds");
			//p911 add logger.debug("start x:" + paintMapStartX + " y:" + paintMapStartY);
			//p911 add logger.debug("end   x:" + paintMapEndX + " y:" + paintMapEndY);
		}
	}

	public void setPaintMapPixelAsPainted(int x, int y, int radius) {

		PaintMapColor currentColor = paintMap.get(
				x).get(
				y);
		
		int adjX = x;
		int adjY = y;

		if (radius==1) {
			paintMap.get(
					adjX).get(
					adjY).markAsPainted();
		} else {
			for (int i=x-radius+1;i<=x+radius-1;i++) {
				for (int j=y-radius+1;j<=y+radius-1;j++) {
					adjX = i;
					adjY = j;
					try {
						if (paintMap.get(adjX).get(adjY)!=null) { 
							PaintMapColor foundColor = paintMap.get(
									adjX).get(
											adjY);
							//if (currentColor.getRGB()==foundColor.getRGB()) {
								paintMap.get(
									adjX).get(
											adjY).markAsPainted();			
							//}
						}
					} catch (Exception e) {
						
					}
					
				}
			}
		}		
		
	}
	
	// given first stroke and direction, return a full stroke
	public BitmapPoint getEndOfStroke(
			final BitmapPoint firstCoordinate,
			final PaintMapDirection direction,
			final int maxDistancePixels, 
			boolean reverseDirection,
			boolean overlapPreviouslyPaintedCoordinate) {
		// Calculate the relative change in x and y to determine movement
		// thorugh map
		
		double xChange = 0;
		double yChange = 0;
		if ((direction == PaintMapDirection.WEST) || (direction == PaintMapDirection.NORTH_WEST) || (direction == PaintMapDirection.SOUTH_WEST)) {
			xChange = -1;
		}
		if ((direction == PaintMapDirection.NORTH) || (direction == PaintMapDirection.SOUTH)) {
			xChange = 0;
		}
		if ((direction == PaintMapDirection.EAST) || (direction == PaintMapDirection.NORTH_EAST) || (direction == PaintMapDirection.SOUTH_EAST)) {
			xChange = 1;
		}
		if ((direction == PaintMapDirection.SOUTH) || (direction == PaintMapDirection.SOUTH_WEST) || (direction == PaintMapDirection.SOUTH_EAST)) {
			yChange = -1;
		}
		if ((direction == PaintMapDirection.WEST) || (direction == PaintMapDirection.EAST)) {
			yChange = 0;
		}
		if ((direction == PaintMapDirection.NORTH) || (direction == PaintMapDirection.NORTH_WEST) || (direction == PaintMapDirection.NORTH_EAST)) {
			yChange = 1;
		}

		if ((direction == PaintMapDirection.SOUTH_WEST_WEST)  ) {
			xChange = -1.0;
			yChange = -0.5;
		}
		if ((direction == PaintMapDirection.SOUTH_WEST_WEST_WEST)  ) {
			xChange = -1.0;
			yChange = -0.333334;
		}
		if ((direction == PaintMapDirection.SOUTH_SOUTH_WEST) ) {
			xChange = -0.5;
			yChange = -1.0;
		}
		if ((direction == PaintMapDirection.SOUTH_SOUTH_SOUTH_WEST) ) {
			xChange = -0.333334;
			yChange = -1.0;
		}

		if ((direction == PaintMapDirection.NORTH_WEST_WEST)  ) {
			xChange = -1;
			yChange = 0.5;
		}
		if ((direction == PaintMapDirection.NORTH_WEST_WEST_WEST)  ) {
			xChange = -1;
			yChange = 0.333334;
		}
		if ((direction == PaintMapDirection.NORTH_NORTH_WEST) ) {
			xChange = -0.5;
			yChange = 1;
		}
		if ((direction == PaintMapDirection.NORTH_NORTH_NORTH_WEST) ) {
			xChange = -0.333334;
			yChange = 1;
		}
		
		
		if ((direction == PaintMapDirection.SOUTH_EAST_EAST)  ) {
			xChange = 1.0;
			yChange = -0.5;
		}
		if ((direction == PaintMapDirection.SOUTH_EAST_EAST_EAST)  ) {
			xChange = 1.0;
			yChange = -0.333334;
		}
		if ((direction == PaintMapDirection.SOUTH_SOUTH_EAST) ) {
			xChange = 0.5;
			yChange = -1.0;
		}
		if ((direction == PaintMapDirection.SOUTH_SOUTH_SOUTH_EAST) ) {
			xChange = 0.333334;
			yChange = -1.0;
		}

		if ((direction == PaintMapDirection.NORTH_EAST_EAST)  ) {
			xChange = 1.0;
			yChange = 0.5;
		}
		if ((direction == PaintMapDirection.NORTH_EAST_EAST_EAST)  ) {
			xChange = 1.0;
			yChange = 0.333334;
		}
		if ((direction == PaintMapDirection.NORTH_NORTH_EAST) ) {
			xChange = 0.5;
			yChange = 1;
		}
		if ((direction == PaintMapDirection.NORTH_NORTH_NORTH_EAST) ) {
			xChange = 0.333334;
			yChange = 1;
		}
		
		if (reverseDirection) {
			xChange = xChange*-1.0;
			yChange = yChange*-1.0;
		}
		
		
		final int firstCol_X = ((int) (firstCoordinate.getX() * strokesPerInch));
		final int firstRow_Y = ((int) (firstCoordinate.getY() * strokesPerInch));
		final Color currentColor = ((PaintMapColor)(paintMap.get(
				firstCol_X).get(
				firstRow_Y)));


		double nextCol_X = firstCol_X;
		double nextRow_Y = firstRow_Y;
		BitmapPoint finalCoordinate = firstCoordinate;
		Vector pointsTraversed = new Vector();
		// While current X >= 0 and X < WIDTH
		// While current Y >= 0 and Y < WIDTH
		// While color is the same as the starting point
		// While less than max distance travelled
		boolean endOfStrokeFound = false;
		double pixelsTravelled = 0;
		while (!endOfStrokeFound) {
			nextCol_X = nextCol_X + xChange;
			nextRow_Y = nextRow_Y + yChange;
			double thisDistance = Math.sqrt(((double)xChange*(double)xChange)+((double)yChange*(double)yChange));
			
			// first check if in bounds
			if (pixelsTravelled < maxDistancePixels) {
				if ((((int)nextCol_X) >= 0) && (((int)nextCol_X) < paintMap.size())) {
					if ((((int)nextRow_Y) >= 0) && (((int)nextRow_Y) < paintMap.get(
							((int)nextCol_X)).size())) {
						final boolean isAlreadyPainted = paintMap.get(
								((int)nextCol_X)).get(
										((int)nextRow_Y)).isPainted;
						boolean proceedToNextPixel = !isAlreadyPainted;
						if (overlapPreviouslyPaintedCoordinate) {
							proceedToNextPixel = true;
						}
						if (proceedToNextPixel) {
								final Color nextColor = paintMap.get(
										((int)nextCol_X)).get(
												((int)nextRow_Y));
								if (nextColor.equals(currentColor)) {
											finalCoordinate = new BitmapPoint(
												((int)nextCol_X),
												((int)nextRow_Y));
											pointsTraversed.add(finalCoordinate);	
											pixelsTravelled = pixelsTravelled+thisDistance;
								}
							else {
								endOfStrokeFound = true;
							}
							
						}
							else {
							endOfStrokeFound = true;
						}
					}
					else {
						endOfStrokeFound = true;
					}
				}
				else {
					endOfStrokeFound = true;
				}
			}
			else {
				endOfStrokeFound = true;
			}
		}
		for (int q=0;q<pointsTraversed.size();q++) {
			finalCoordinate.addMidPoints((BitmapPoint)pointsTraversed.get(q));
		}
		return finalCoordinate;
	}

	public int getRandomInteger(
			final int size ) {
		// creates random number between 0 and (size-1)
		final Random randomGenerator = new Random();
		final int randomInt = randomGenerator.nextInt(size);
		return randomInt;
	}

	public HashSet<Color> getUnsortedColorPallete() {
		return gifPalette;
	}

	public ArrayList<Color> getSortedColorPallete() { // lightToDark
		final ArrayList<Color> sortedColors = new ArrayList<Color>();
		for (final Color color : gifPalette) {
			sortedColors.add(color);
		}
		Collections.sort(
				sortedColors,
				new ColorSorting());
		return sortedColors;
	}

	public ArrayList<Paint> getSortedRobotColorPallete() { // lightToDark
		final ArrayList<Paint> sortedPaintColors = new ArrayList<Paint>();
		for (final Paint paint : robotPalette) {
			sortedPaintColors.add(paint);
		}
		Collections.sort(
				sortedPaintColors,
				new PaintSorting());
		return sortedPaintColors;
	}

	public class ColorSorting implements
			Comparator<Color>
	{
		@Override
		public int compare(
				final Color c1,
				final Color c2 ) {
			final double averageC1Color = (((redWeight * c1.getRed()) + (greenWeight * c1.getGreen()) + (blueWeight * c1.getBlue()))) / 3;
			final double averageC2Color = (((redWeight * c2.getRed()) + (greenWeight * c2.getGreen()) + (blueWeight * c2.getBlue()))) / 3;
			return Double.valueOf(
					averageC2Color).compareTo(
					Double.valueOf(averageC1Color));
		}
	}

	public class PaintSorting implements
			Comparator<Paint>
	{
		@Override
		public int compare(
				final Paint p1,
				final Paint p2 ) {
			final Color c1 = new Color(Integer.parseInt( p1.getHexColor().substring(1),	16));
			final Color c2 = new Color(	Integer.parseInt(p2.getHexColor().substring(1),16));

			final double averageC1Color = (((redWeight * c1.getRed()) + (greenWeight * c1.getGreen()) + (blueWeight * c1.getBlue()))) / 3;
			final double averageC2Color = (((redWeight * c2.getRed()) + (greenWeight * c2.getGreen()) + (blueWeight * c2.getBlue()))) / 3;
			return Double.valueOf(
					averageC2Color).compareTo(
					Double.valueOf(averageC1Color));
		}
	}

	public String getPaintMapImageUrl() {
		return paintMapImageUrl;
	}

	public void setPaintMapImageUrl(
			final String paintMapImageUrl ) {
		this.paintMapImageUrl = paintMapImageUrl;
	}

	public void mapPaintMapColorsToRobotPallete(
			final List<Paint> robotPaintPalette ) {
		robotPalette = robotPaintPalette;
		// for each gif color, find closest robotPaintPallete color
		for (final Color gifColor : gifPalette) {
			double smallestColorDistance = 0;
			boolean firstComparison = true;
			Paint closestMatch = null; //
			for (final Paint robotPaintColor : robotPaintPalette) {
				final Color robotColor = new Color(
						Integer.parseInt(
								robotPaintColor.getHexColor().substring(
										1),
								16));
				final double thisColorDistance = getColourDistance(
						gifColor,
						robotColor);
				if (firstComparison || (thisColorDistance < smallestColorDistance)) {
					closestMatch = robotPaintColor;
					smallestColorDistance = thisColorDistance;
					firstComparison = false;
				}
			}
			paletteMap.addMapping(
					gifColor,
					closestMatch); // (gifPaletteColor, robotPaletteColor)
		}
	}

	public double getColourDistance(
			final Color c1,
			final Color c2 ) {
		final int r = c1.getRed() - c2.getRed();
		final int g = c1.getGreen() - c2.getGreen();
		final int b = c1.getBlue() - c2.getBlue();
		// double rmean = ( c1.getRed() + c2.getRed() )/2;
		final double weightR = redWeight; // 2 + rmean/256;
		final double weightG = greenWeight;// 4.0;
		final double weightB = blueWeight; // 2 + (255-rmean)/256;
		return Math.sqrt(weightR * r * r + weightG * g * g + weightB * b * b);
	}

	public PaletteMap getPaletteMap() {
		return paletteMap;
	}

	public Paint getRobotPaintColor(
			final Color color ) {
		for (final Paint paint : robotPalette) {
			final Color paintColor = new Color(
					Integer.parseInt(
							paint.getHexColor().substring(
									1),
							16));
			if (color.equals(paintColor)) {
				return paint;
			}
		}
		return null;
	}

}
