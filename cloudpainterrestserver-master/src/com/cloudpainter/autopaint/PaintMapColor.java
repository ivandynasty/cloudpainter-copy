package com.cloudpainter.autopaint;

import java.awt.Color;

public class PaintMapColor extends Color {

	private static final long serialVersionUID = 1L;
	boolean isPainted = false;
	
	public PaintMapColor(int rgb) {
		super(rgb);
		//Pure White will always be marked as painted
		if ( ( this.getRed() == 255 &&
				this.getGreen() == 255 &&
				this.getBlue() == 255 ) 
				) {
			markAsPainted();
		}
	}
	
	public PaintMapColor(int r, int g, int b) {
		super(r, g, b);
	}

	public void markAsPainted() {
		isPainted = true;
	}
	
	public boolean isPainted() {
		return isPainted;
	}
	
	public boolean equals(Object obj) {
        return ((Color)obj).getRGB() == this.getRGB();
    }
	
}
