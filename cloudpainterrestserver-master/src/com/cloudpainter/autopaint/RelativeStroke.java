package com.cloudpainter.autopaint;

import java.awt.Color;

import com.cloudpainter.painting.RelativePoint;

public class RelativeStroke
{
	private final Color strokeColor;
	private final RelativePoint start;
	private final RelativePoint end;

	public RelativeStroke(
			final RelativePoint start,
			final RelativePoint end,
			final Color strokeColor ) {
		this.start = start;
		this.end = end;
		this.strokeColor = strokeColor;
	}

	public RelativePoint getStart() {
		return start;
	}

	public RelativePoint getEnd() {
		return end;
	}

	public Color getStrokeColor() {
		return strokeColor;
	}
}
