package com.cloudpainter.autopaint;

import java.util.Vector;

public class PixelCluster {

	Vector pixels = new Vector();
	
	Pixel avgPixel = new Pixel(0,0,0,0,0,0,0,0,0,0);

	int avgR = 0;
	int avgG = 0;
	int avgB = 0;
	float avgH = 0;
	float avgS = 0;
	float avgBr = 0;
	float avgXP = 0;
	float avgYP = 0;
	
	public PixelCluster() {
		pixels = new Vector();
	}
	
	public void addPixel(Pixel pic) {
		pixels.add(pic);
	}

	public void removePixel(Pixel pic) {
		for (int i=0; i<pixels.size();i++) {
			Pixel thisPic = (Pixel)pixels.elementAt(i);
			if (thisPic.x==pic.x && thisPic.y==pic.y) {
				pixels.remove(i);
				i = pixels.size()+1;
			}
		}
	}
	
	public void calculateAverage() {
		
		float totalR = 0;
		float totalG = 0;
		float totalB = 0;
		
		float totalH = 0;
		float totalS = 0;
		float totalBr = 0;
		
		float totalXP = 0;
		float totalYP = 0;
		
		for (int i=0; i<pixels.size();i++) {

			totalR += ((Pixel)pixels.elementAt(i)).red;
			totalG += ((Pixel)pixels.elementAt(i)).green;
			totalB += ((Pixel)pixels.elementAt(i)).blue;
			
			totalH += ((Pixel)pixels.elementAt(i)).hue;
			totalS += ((Pixel)pixels.elementAt(i)).saturation;
			totalBr += ((Pixel)pixels.elementAt(i)).brightness;
			
			totalXP += ((Pixel)pixels.elementAt(i)).xPosition;
			totalYP += ((Pixel)pixels.elementAt(i)).yPosition;
			
		}
		
		avgR = (int)(totalR/pixels.size());
		avgG = (int)(totalG/pixels.size());
		avgB = (int)(totalB/pixels.size());
		
		avgH = totalH/(float)pixels.size();
		avgS = totalS/(float)pixels.size();
		avgBr = totalBr/(float)pixels.size();

		avgXP = totalXP/(float)pixels.size();
		avgYP = totalYP/(float)pixels.size();

		avgPixel = new Pixel(0, 0, avgR, avgG, avgB, avgH, avgS, avgBr, avgXP, avgYP);
		
	}

	public Pixel getAvgPixel() {
		return avgPixel;
	}
	
	public double getMeanDifference(Pixel otherPixel) {
		float oR = otherPixel.red;
		float oG = otherPixel.green;
		float oB = otherPixel.blue;
		float oH = otherPixel.hue;
		float oS = otherPixel.saturation;
		float oBr = otherPixel.brightness;
		float oXP = otherPixel.xPosition;
		float oYP = otherPixel.yPosition;
		double difference = 
				(Math.sqrt(((avgR-oR)*(avgR-oR)))*otherPixel.rW) +
				(Math.sqrt(((avgG-oG)*(avgG-oG)))*otherPixel.gW) +
				(Math.sqrt(((avgB-oB)*(avgB-oB)))*otherPixel.bW) +
				(Math.sqrt(((avgH-oH)*(avgH-oH)))*otherPixel.hW) +
				(Math.sqrt(((avgS-oS)*(avgS-oS)))*otherPixel.sW) +
				(Math.sqrt(((avgBr-oBr)*(avgBr-oBr)))*otherPixel.brW) +
				(Math.sqrt(((avgXP-oXP)*(avgXP-oXP)))*otherPixel.xW) +
				(Math.sqrt(((avgYP-oYP)*(avgYP-oYP)))*otherPixel.yW);
		//double means = Math.sqrt(difference);
		return difference;
	}
	
}
