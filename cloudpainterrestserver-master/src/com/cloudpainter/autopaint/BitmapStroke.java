package com.cloudpainter.autopaint;

import java.awt.Color;
import java.util.Vector;

import com.cloudpainter.painting.BitmapPoint;
import com.cloudpainter.painting.CanvasPoint;

public class BitmapStroke
{
	private Color strokeColor;
	private final BitmapPoint start;
	private final BitmapPoint end;
	private Vector allPoints;

	public BitmapStroke(
			final BitmapPoint start,
			final BitmapPoint end,
			final Color strokeColor) {
		this.start = start;
		this.end = end;
		this.strokeColor = strokeColor;
		this.allPoints = new Vector();
		this.allPoints.add(this.start);
		this.allPoints.add(this.end);
	}
	
	public BitmapStroke(
			final BitmapPoint start,
			final BitmapPoint end,
			final Color strokeColor,
			Vector allPoints) {
		this.start = start;
		this.end = end;
		this.strokeColor = strokeColor;
		this.allPoints = allPoints;
	}

	public Vector getAllPoints() {
		return allPoints;
	}
	
	public BitmapPoint getStart() {
		return start;
	}

	public BitmapPoint getEnd() {
		return end;
	}

	public void setStrokeColor(Color newColor) {
		strokeColor = newColor;
	}
	
	public Color getStrokeColor() {
		return strokeColor;
	}
	
	public String getStrokeColorAsHexString() {
		String hex = "#"+Integer.toHexString(strokeColor.getRGB()).substring(2);
		return hex;
	}
	
	
	public double getLength(
			BitmapPoint point, BitmapPoint endpoint ) {
			final double xDistance = endpoint.getX() - point.getX();
			final double yDistance = endpoint.getY() - point.getY();
			final double distance = Math.sqrt((xDistance * xDistance) + (yDistance * yDistance));
			return distance;
	}
	
	public boolean hasSameStrokeGeometry(BitmapStroke one, BitmapStroke two) {
		if ( 
				one.getStart().getX() == two.getStart().getX() &&
				one.getStart().getY() == two.getStart().getY() &&
				one.getEnd().getX() == two.getEnd().getX() &&
				one.getEnd().getY() == two.getEnd().getY() ) {
			return true;
		} else {
			return false;
		}
	}

	public boolean hasReversedStrokeGeometry(BitmapStroke one, BitmapStroke two) {
		if ( 
				one.getStart().getX() == two.getEnd().getX() &&
				one.getStart().getY() == two.getEnd().getY() &&
				one.getEnd().getX() == two.getStart().getX() &&
				one.getEnd().getY() == two.getStart().getY() ) {
			return true;
		} else {
			return false;
		}
	}
	
}
