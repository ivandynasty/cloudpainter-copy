package com.cloudpainter.autopaint;

import java.util.Vector;

public class StrokeVectors {

	private Vector strokeVectors = new Vector();
	
	public StrokeVectors() {
	}

	public void addStrokes(BitmapStroke bs) {
		strokeVectors.add(bs);
	}
	
	public Vector getStrokeVector() {
		return strokeVectors;
	}
	
}
