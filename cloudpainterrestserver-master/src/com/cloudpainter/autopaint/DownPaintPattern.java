package com.cloudpainter.autopaint;

import java.util.ArrayList;

public class DownPaintPattern extends
		PaintPattern
{

	public DownPaintPattern(
			final ArrayList<Double> strokeLengthSequence,
			final ArrayList<PaintMapDirection> strokeDirectionSequence,
			final FindNextStrokeStartPointStrategy scanForNextStartPointStrategy,
			final DirectionToSearchForNextStrokeStartRow scanForNextRowStrategy,
			final DirectionToSearchForNextStrokeStartCol scanForNextColStrategy ) {
		setPaintStrokeLengthPattern(strokeLengthSequence);
		setPaintStrokeDirectionSequence(strokeDirectionSequence);
		setScanForNextStartPointStrategy(scanForNextStartPointStrategy);
		setScanForNextRowStrategy(scanForNextRowStrategy);
		setScanForNextColStrategy(scanForNextColStrategy);
	}

	public void Main(
			final String[] args ) {

		final ArrayList<Double> strokeLengthSequence = new ArrayList<Double>();
		strokeLengthSequence.add(4.0);

		final ArrayList<PaintMapDirection> strokeDirectionSequence = new ArrayList<PaintMapDirection>();
		strokeDirectionSequence.add(PaintMapDirection.SOUTH);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_WEST);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_EAST);

		new DownPaintPattern(
				strokeLengthSequence,
				strokeDirectionSequence,
				null, // FindNextStrokeStartPointStrategy.SAME_COLLUMN, NOT
						// IMPLEMENTED
				null, // DirectionToSearchForNextStrokeStartRow.BELOW, NOT
						// IMPLEMENTED
				null // DirectionToSearchForNextStrokeStartCol.RIGHT NOT
						// IMPLEMENTED
		);

	}

}
