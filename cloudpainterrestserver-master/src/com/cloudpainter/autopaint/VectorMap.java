package com.cloudpainter.autopaint;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Vector;

import com.cloudpainter.painting.BitmapPoint;

public class VectorMap {

	//PIXELColumns is a VECTOR of PIXELVECTORS
	//EACH Column has each ROW
	//ECH PIXELROW HAS COLLECTION OF VECTORS THAT RUN THROUGH IT
	public Vector pixelColumns = new Vector();
	//public Vector pixelRows = new Vector();
	
	public Vector vectorColumns = new Vector();
	//public Vector vectorRows = new Vector();
	


	
	//public Vector avgPixelVectorColumns = new Vector();
	
	
	public VectorMap(int width, int height) {
		for (int yy=0; yy<height;yy++) {
			Vector yys = new Vector();
			for (int xx=0; xx<width;xx++) {
				 yys.add(new Vector());
			}
			pixelColumns.add( yys);	
		}
		
		for (int yy=0; yy<height;yy++) {
			Vector vvs = new Vector();
			for (int xx=0; xx<width;xx++) {
				 vvs.add((float)0.0);
			}
			vectorColumns.add( vvs);	
		}

	}

	public Vector getPixelVectors(int x, int y) {
		return ((Vector) ((Vector)pixelColumns.elementAt(x)).elementAt(y));
	}

	public void setPixelVectors(int x, int y, Vector pv) {
		((Vector)pixelColumns.elementAt(x)).set(y, pv);
	}
	
	public void addVectorToPixelVector(BitmapStroke bs) {
		Vector allVectors = bs.getAllPoints();
		for (int i=0;i<allVectors.size();i++) {
			int x = ((BitmapPoint)allVectors.elementAt(i)).getX();
			int y = ((BitmapPoint)allVectors.elementAt(i)).getY();
			onlyAddIfDoesNotAlreadyExist(x, y, bs);
		
		}
		
	}
	
	public void onlyAddIfDoesNotAlreadyExist(int x, int y, BitmapStroke bs) {
		Vector bitmapStrokes = ((Vector)((Vector)pixelColumns.elementAt(x)).elementAt(y));
		boolean sameStrokeFound = false;
		for (int q=0;q<bitmapStrokes.size();q++) {
			BitmapStroke thisBs = (BitmapStroke)bitmapStrokes.elementAt(q);
			if (thisBs.hasSameStrokeGeometry(thisBs,bs)) {
				sameStrokeFound = true;
			} else if (thisBs.hasReversedStrokeGeometry(thisBs,bs)) {
				sameStrokeFound = true;
			} else {
				//do ntohing
			}
		}
		//MAKE SURE IT IS NORMALIZED
		if (!sameStrokeFound) {
			((Vector)(((Vector)pixelColumns.elementAt(x)).elementAt(y))).add(normalizeStroke(bs));
		}
	}

	public BitmapStroke normalizeStroke(BitmapStroke bs) {
		//always paint going down
		//so make start point northernmost point.
		//if y1 and y2 equal make start point smallest x
		BitmapPoint p1 = bs.getStart();
		BitmapPoint p2 = bs.getEnd();
		BitmapStroke normalizedBitmapStroke = bs;
		if (p1.getY()>p2.getY()) {
			normalizedBitmapStroke =new BitmapStroke(p2, p1, bs.getStrokeColor());
		} else if (p1.getY()==p2.getY()) {
			if (p1.getX()>p2.getX()) {
				normalizedBitmapStroke =new BitmapStroke(p2, p1, bs.getStrokeColor());
			} 
		} 
		return normalizedBitmapStroke;
	}
	
	public void setVectorAngle(int xx, int yy, float vectorAngle) {
		//System.out.println("1");
		((Vector)vectorColumns.elementAt(xx)).set(yy, vectorAngle);
	}

	public float getVectorAngle(int xx, int yy) {
		return (float) ((Vector)vectorColumns.elementAt(xx)).elementAt(yy);
	}
	
/*	public void averageVectors(int x, int y) {
		Vector pixVec = getPixelVectors(x,y);
		int vectorNo = pixVec.size();
		int totalX = 0;
		int totalY = 0;
		for (int i=0;i<vectorNo;i++) {
			totalX += (pixVec.elementAt(i)
		}
		
		//go to x y
		//get all the vectors

		//v3.x = (v1.x + v2.x) / 2;
		//v3.y = (v1.y + v2.y) / 2;
			
		
		
		
		
	}
*/
	
	public static double GetAngleOfLineBetweenTwoPoints(double x1, double y1, double x2, double y2) {
        double xDiff = x2 - x1;
        double yDiff = y2 - y1;
        return Math.toDegrees(Math.atan2(yDiff, xDiff));
    }
    
	
	float getAverageAngle(float a1, float a2) {
		return Math.min((a1-a2)<0?a1-a2+360:a1-a2, (a2-a1)<0?a2-a1+360:a2-a1);
	}
	
}
