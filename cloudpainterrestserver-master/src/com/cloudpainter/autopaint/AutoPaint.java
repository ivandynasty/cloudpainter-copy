package com.cloudpainter.autopaint;

import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.util.ArrayList;

import com.cloudpainter.painting.Paint;

public abstract class AutoPaint {

	//SOURCE IMAGE is the SOURCE GIF that is also used in tracing.
	//it is the painting people are trying to make
	Image sourceImage;// = Toolkit.getDefaultToolkit().getImage(fileName); 

	//PAINTING IMAGE is the result of the painting actually executed on
	//the canvas
	Image paintingImage;// = Toolkit.getDefaultToolkit().getImage(fileName); 
	int paintingWidth;
	int paintingHeight;
	
	//PaintWells are the wells available to paint from
	ArrayList<Paint> paintWells;
	
	//paintedPixels is a grid of which pixels have been painted
	//it is good to keep track of painted pixels to ascertain 
	//parts of the canvas that have not yet been painted
	boolean paintedPixels[][];
	
	
	//Plug-ins using AutoPaint are attempting to draw the SOURCE IMAGE on the
	//PAINTING IMAGE.
	
	//A bare minimum plug in can simply paint random strokes generated by analysis
	//of the SOURCE IMAGE.  To do this it needs the available PaintWells
	//it would then analyze source image, find areas to paint.  Find closest matching
	//Paint Well.  Calculate several strokes and return the strokes and chosen paint wells
	
	public AutoPaint(
			String sourceUrl, 
			String paintingUrl, 
			int paintingWidth, 
			int paintingHeight,
			ArrayList<Paint> paintWells
			) {
		sourceImage = Toolkit.getDefaultToolkit().getImage(sourceUrl); 
		paintingImage = Toolkit.getDefaultToolkit().getImage(paintingUrl); 
		this.paintingWidth = paintingWidth;
		this.paintingHeight = paintingHeight;
		this.paintWells = paintWells;
		
		paintedPixels = new boolean[paintingWidth][paintingHeight];
		for (int i=0;i<paintingWidth;i++) {
			for (int j=0;j<paintingHeight;j++) {
				paintedPixels[i][j] = false;
			}
		}
	}
	
	public void recordPaintedPixels(ArrayList<Point> pointPairs) {
		int index =0;
		Point startPoint = null;
		Point endPoint = null;
		for (Point point: pointPairs) {
			if (index%2==0) {
				startPoint = point;
			} else {
				endPoint = point;
				recordPaintedPixels(startPoint,endPoint);
			}
			index++;
		}
	}

	public void recordPaintedPixels(Point startPoint, Point endPoint) {
		//TODO calculate all points between startPoint and endPoint
		//and set those points to FALSE in paintedPixels
	}
	
	
	
	
	
	
	
	
	
	
	
	public void setSourceImage(Image sourceImage) {
		this.sourceImage = sourceImage;
	}

	public void setPaintingImage(Image paintingImage) {
		this.paintingImage = paintingImage;
	}

	public void setPaintingWidth(int paintingWidth) {
		this.paintingWidth = paintingWidth;
	}

	public void setPaintingHeight(int paintingHeight) {
		this.paintingHeight = paintingHeight;
	}

	public void setPaintWells(ArrayList<Paint> paintWells) {
		this.paintWells = paintWells;
	}
	
	
	
}
