package com.cloudpainter.autopaint;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.Vector;

import javax.imageio.ImageIO;

import com.cloudpainter.aistrokes.AIStrokes;
import com.cloudpainter.painting.BitmapPoint;
import com.cloudpainter.utils.ColorUtils;
import com.cloudpainter.utils.ElasticsearchUtility;
import com.cloudpainter.utils.TrapezoidImageTransformer;

public class FeedbackStrokes {


	AIStrokes aiStrokes = new AIStrokes();
	
	BufferedImage goalBufferedImage;//      = new BufferedImage(0, 0, 0);
	BufferedImage progressBufferedImage;//  = new BufferedImage(0, 0, 0);
	
	static int numberOfBuckets = 8;
	static int numberOfIterations = 10;
	
	Vector pixelVector = new Vector();
	Vector pixelClusterVector = new Vector();

	static int width = 0;
	static int height = 0;

	Vector rows = new Vector();
	Vector columns = new Vector();

	
	
	public FeedbackStrokes(int width, int height) {
	}
	
	public static void main(String[] args) {

		float rd  = (float) (0.5);				
		float gn  = (float) (0.5);	
		float be  = (float) (0.5);

		float he  = (float) (0.0);	
		float sn  = (float) (0.5);
		float bs  = (float) (0.5);

		float xs  = (float) (0.1);	
		float ys  = (float) (0.1);	

		Pixel.setWeights(rd, gn, be, he, sn, bs, xs, ys);

		String signature = "55505555";

		for (int i=0; i<16; i++) {
			for (int j=0; j<1; j++) {

			String imageName = "C:/tempworkspace/nv_0"+i+".png";                                //cloudpainterrestserver/currentpainting/kmeans_02.png";
			String outputName = "C:/tempworkspace/nv_clusters_"+signature+"_0"+i+"_"+j+".png";          //cloudpainterrestserver/currentpainting/kmeans_out_02.png";
			String strokeName = "C:/tempworkspace/nv_strokes_"+signature+"_0"+i+"_"+j+".png";  //cloudpainterrestserver/currentpainting/kmeans_out_02.png";
			
			//read in image


			BufferedImage input = getBufferedImage(imageName);				
			width = input.getWidth();
			height = input.getWidth();
			FeedbackStrokes kmcc = new FeedbackStrokes(width, height);
			
			//GAUSSIAN BLUR FILTER
			System.out.println("applying blur");
			int[] filter = {1, 2, 1, 2, 4, 2, 1, 2, 1};
			int filterWidth = 3;
			//int[] filter = {1, 4, 6, 4, 1, 4, 16, 24, 16, 4, 6, 24, 36, 24, 6, 4, 16, 24, 16, 4, 1, 4, 6, 4, 1};
			//int filterWidth = 5;
			BufferedImage blurred = blur(input, filter, filterWidth);

			//CLUSTER THE IMAGE
			System.out.println("clustering image");
			kmcc.clusterInputColors(blurred, numberOfBuckets, numberOfIterations);

			//SAVE AND SHOW THE CLUSTERED IMAGE
			BufferedImage output = kmcc.rasterizeClusters(kmcc.width, kmcc.height);
			kmcc.saveBufferedImage(output, outputName);

			//CREATE AI STROKE OBJECT
			System.out.println("generating AI Strokes");
			AIStrokes ais = new AIStrokes();
			ais.generatePaintMap(output);
			ais.generateVectorMap(output);

			
			
			//Prep image for showing things
			BufferedImage strokeImage = new BufferedImage(width*4, height*4, BufferedImage.TYPE_INT_RGB);
			for (int yy=0; yy<height*4;yy++) {
				for (int xx=0; xx<width*4;xx++) {
					strokeImage.setRGB(xx, yy, Color.GRAY.getRGB());
				}
			}

			//DRAW ALL THE STROKES ON AN IMAGE
			//System.out.println("drawing strokes on image");
			//for (int as = 0; as<allStrokes.getStrokeVector().size(); as++) {
			//	BitmapStroke ls = (BitmapStroke)allStrokes.getStrokeVector().elementAt(as);
			//	drawStrokeVectorsOnImage(strokeImage , ls.getStart(), ls.getEnd(), ls.getStrokeColor(), false);
			//}

			//paint the Vector
		/*	for (int yy=0; yy<height;yy=yy+2) {
				for (int xx=0; xx<width;xx=xx+2) {
					float thisAngle = (float) ais.getVectorMap().getVectorAngle(xx, yy);
					BitmapPoint startXY = new BitmapPoint(xx,yy);
					BitmapPoint endXY = ais.getEndBitmapPoint(startXY , thisAngle);
					drawStrokeVectorsOnImage(strokeImage , startXY, endXY, Color.RED, false, 4);
					//strokeImage.setRGB(xx*4, yy, Color.WHITE.getRGB());

				}
			}
			*/
			
			int strokes = 0;
			for (int yy=399; yy>=0; yy=yy-2) {
				for (int xx=0; xx<400;xx=xx+2) {
					
					if ( !ais.getPaintmap().getPaintMapColorFromPaintMapXY(xx, yy).isPainted) {
						BitmapStroke bs2 = ais.generateSingleStrokeFromVectorMap(xx, yy, 80, false);
						for (int bss = 0; bss<bs2.getAllPoints().size()-1; bss++) {
							BitmapPoint thisPoint = (BitmapPoint) bs2.getAllPoints().elementAt(bss);
							BitmapPoint nextPoint = (BitmapPoint) bs2.getAllPoints().elementAt(bss+1);
							drawStrokeVectorsOnImage(strokeImage , thisPoint, nextPoint, bs2.getStrokeColor(), false, 4, 32);
						}
						strokes++;
					}
				}
			}
			kmcc.saveBufferedImage(strokeImage , strokeName);
			System.out.println("strokes: "+strokes );
		}	
		}
		//Compare Pictures...
		//Save Difference
		//Find Biggest Difference
		
		
		
			}
		
	
	public int getNextSetOfFeedbackStrokes(boolean executeAIStrokes, String rootDir, String indexName, int numberOfStrokes) {
		int strokesCreated = 0;
		if (executeAIStrokes) {
			//MAKING GOAL IMAGE
			String timelapseFolder = rootDir+indexName+"\\";
			File makeDir = new File(rootDir+indexName);
			makeDir.mkdir();
			String sourceImageName = rootDir+indexName+".png";
			String goalImageName = rootDir+indexName+"_"+"0goal.png";
			String liveProgressImageName = rootDir+indexName+"_"+"1progress.jpg";
			String heatmapImageName = rootDir+indexName+"_"+"2heatmap.png";
			String areasCompletedImageName = rootDir+indexName+"_"+"3completed.png";
			String vectorMapImageName = rootDir+indexName+"_"+"4vectorMap.png";
			
			String jsonFileName = rootDir+indexName+"_"+"strokes.json";
		
//read in source image from neural network
			BufferedImage input = getBufferedImage(sourceImageName);				
			width = input.getWidth();
			height = input.getWidth();
			System.out.println("applying blur");
			//GAUSSIAN BLUR FILTER
			System.out.println("applying blur");
			int[] filter = {1, 2, 1, 2, 4, 2, 1, 2, 1};
			int filterWidth = 3;
			//int[] filter = {1, 4, 6, 4, 1, 4, 16, 24, 16, 4, 6, 24, 36, 24, 6, 4, 16, 24, 16, 4, 1, 4, 6, 4, 1};
			//int filterWidth = 5;
			BufferedImage blurred = blur(input, filter, filterWidth);
			System.out.println("clustering image");
			clusterInputColors(blurred, numberOfBuckets, numberOfIterations);
			System.out.println("saving goal image");
			BufferedImage output = rasterizeClusters(width, height);
			saveBufferedImage(output, goalImageName);
			System.out.println("generating AI Strokes");
			AIStrokes ais = new AIStrokes();
			ais.generatePaintMap(output);
			ais.generateVectorMap(output);

//read in completed image
//if it exists - adjust paint map according to it
			BufferedImage completedBufferedImage = getBufferedImage(areasCompletedImageName);
			if (completedBufferedImage!= null) {
				ais.updatePaintMapFromBufferedImage(completedBufferedImage);
			}			
			
//compare progress image to goal image and make them the same size
			progressBufferedImage = getBufferedImage(liveProgressImageName);
			goalBufferedImage = getBufferedImage(goalImageName);
			progressBufferedImage = ais.scale(progressBufferedImage, goalBufferedImage.getWidth(), goalBufferedImage.getHeight());
			//296
			System.out.println("creating heatmap");
			BufferedImage heatmapBufferedImage = TrapezoidImageTransformer.getDifferenceBufferedImage(goalBufferedImage, progressBufferedImage, 1);
			saveBufferedImage(heatmapBufferedImage, heatmapImageName );
			
			//now that heat map exists - find hottest spot
			Point greatestDif = new Point(0,0); 
			int startIndex = ElasticsearchUtility.getMaxStrokeNumber(indexName)+1;
			Color hottestColor = null;
			BufferedImage heatmapWithStrokes = ColorUtils.makeCopyOfBufferedImage(heatmapBufferedImage);
			BufferedImage progressImageWithStrokes = ColorUtils.makeCopyOfBufferedImage(progressBufferedImage);
			int nextX = getRandomInteger(heatmapBufferedImage.getWidth());
			int nextY = getRandomInteger(heatmapBufferedImage.getWidth());
			
			for (int i=0; i<numberOfStrokes; i++) {
				//find area with biggest difference between it and hottest color / then find the X hottest place with that color
				if (hottestColor == null) {
					greatestDif = ais.getGreatestDif(ais.getPaintmap(), heatmapBufferedImage);
					hottestColor = new Color(goalBufferedImage.getRGB(greatestDif.x, greatestDif.y));
					System.out.println("HottestColor: "+hottestColor);
				} else {
					//findPointClose to end of last strokes
					BitmapPoint bmp = ais.getPaintmap().getNextStartPointOfColorRadially(nextX, nextY, width, hottestColor); 
					if (bmp!=null) {
						greatestDif = new Point(bmp.getX(), bmp.getY()) ; 
					} else {
						System.out.println("all strokes of color found");
						greatestDif = null;
						i=numberOfStrokes;
					}
				}
				if (greatestDif!=null) {
					strokesCreated++;
					heatmapBufferedImage = ais.drawOnGreatestDif(heatmapBufferedImage, greatestDif);
					
					//flip y axis to make strokes
					//BitmapStroke ls = getAutoGeneratedSingleStroke(indexName, goalBufferedImage, greatestDif);
					BitmapStroke ls = ais.generateSingleStrokeFromVectorMap(greatestDif.x, greatestDif.y, 100, true);
					boolean invertBitmapY = false;
					//save to paintmap
					//ais.recordBitmapStrokeOnPaintMap(ls, invertBitmapY, goalBufferedImage.getHeight());
					invertBitmapY = false;
					boolean invertY = true;  //CHECK ALL OTHER PLACES TOO
					System.out.println("points: "+ls.getAllPoints().size());
					heatmapWithStrokes = ais.drawStrokesOnHeatmap(heatmapWithStrokes, ls.getStart(), ls.getEnd(), hottestColor, invertBitmapY);

					
					progressImageWithStrokes = ais.drawStrokesOnHeatmap(progressImageWithStrokes, ls.getStart(), ls.getEnd(), hottestColor, invertBitmapY);
					if (i%100==0) {
						String TEMPpaddedNumber = ElasticsearchUtility.paintingIdInMillion(startIndex);
						String TEMPpaddedFilename = timelapseFolder + "temp_"+TEMPpaddedNumber+"_"+i+"_.png";
						saveBufferedImage(progressImageWithStrokes, TEMPpaddedFilename );
					}
					
					
					
					nextX = ls.getStart().getX();
					nextY = ls.getStart().getY();
					ais.addBrushStroke(ls);
					ais.saveSingleStrokeToElasticSearch(indexName, goalBufferedImage, ls, startIndex+i, invertY);
				
				}
			}
			
			//saveBufferedImage(heatmap, differenceImageFileLocation);
			String paddedNumber = ElasticsearchUtility.paintingIdInMillion(startIndex);
			String paddedFilename = timelapseFolder + "heatmap_"+paddedNumber+".png";
			saveBufferedImage(heatmapBufferedImage, paddedFilename );

			paddedFilename = timelapseFolder + "h_strokes_"+paddedNumber+".png";
			saveBufferedImage(heatmapWithStrokes, paddedFilename );
			
			String paddedProgressFilename = timelapseFolder + "prog_"+paddedNumber+".png";
			saveBufferedImage(progressBufferedImage, paddedProgressFilename );

			String paddedWithStrokesProgressFilename = timelapseFolder + "p_strokes_"+paddedNumber+".png";
			saveBufferedImage(progressImageWithStrokes, paddedWithStrokesProgressFilename );

			String paintmapFilename = timelapseFolder + "paintmap_"+paddedNumber+".png";
			saveBufferedImage(ais.getPaintmap().getBufferedImageFromPaintMap(), paintmapFilename );


			String paddedGoalImageFilename = timelapseFolder + "goal_"+paddedNumber+".png";
			saveBufferedImage(goalBufferedImage, paddedGoalImageFilename );

			
			BufferedImage strokeImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			for (int yy=0; yy<height;yy++) {
				for (int xx=0; xx<width;xx++) {
					strokeImage.setRGB(xx, yy, Color.GRAY.getRGB());
				}
			}
			for (int yy=height-1; yy>=0; yy=yy-2) {
				for (int xx=0; xx<width;xx=xx+2) {
					//if ( !ais.getPaintmap().getPaintMapColorFromPaintMapXY(xx, yy).isPainted) {
						BitmapStroke bs2 = ais.generateSingleStrokeFromVectorMap(xx, yy, 80, false);
						for (int bss = 0; bss<bs2.getAllPoints().size()-1; bss++) {
							BitmapPoint thisPoint = (BitmapPoint) bs2.getAllPoints().elementAt(bss);
							BitmapPoint nextPoint = (BitmapPoint) bs2.getAllPoints().elementAt(bss+1);
							drawStrokeVectorsOnImage(strokeImage , thisPoint, nextPoint, bs2.getStrokeColor(), false, 1, 16);
						}
					//}
				}
			}
			saveBufferedImage(strokeImage , vectorMapImageName);
			String paddedVectorImageName = timelapseFolder + "vectors_"+paddedNumber+".png";
			
			saveBufferedImage(strokeImage , paddedVectorImageName);
			saveBufferedImage(ais.getPaintmap().getBufferedImageFromPaintMap(), areasCompletedImageName );
			
//save areascompleted according to paintmap			
		}
		return strokesCreated;
	}

	

	
	
	
	
	

	public static BufferedImage drawStrokeVectorsOnImage(BufferedImage bimage, BitmapPoint startPoint, BitmapPoint endPoint, Color strokeColor, boolean invertY, int spacing, int alpha) {
		Color alphaStroke = new Color(strokeColor.getRed(), strokeColor.getGreen(),strokeColor.getBlue(), alpha); //strokeColor;

		int startX = startPoint.getX();
		int startY = startPoint.getY();
		int endX = endPoint.getX();
		int endY = endPoint.getY();

		if (invertY) {
			startY = bimage.getHeight()-1-startY;
			endY = bimage.getHeight()-1-endY;
		}
		Graphics2D g = bimage.createGraphics();
		g.setColor(alphaStroke);

		BasicStroke basicStroke = new BasicStroke(3);
		g.setStroke(basicStroke);
		g.drawLine(startX*spacing, startY*spacing, endX*spacing, endY*spacing);
		g.dispose();
		return bimage;

	}


	//gaussian blur = 1/16 [ 1 2 1 ]
	//                       2 4 2
	//                       1 2 1
	//	int[] filter = {1, 2, 1, 2, 4, 2, 1, 2, 1};
	//	int filterWidth = 3;
	//	BufferedImage blurred = blur(img, filter, filterWidth);

	public BufferedImage rasterizeClusters(int width, int height) {
		BufferedImage output =  new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

		for (int i=0;i<pixelClusterVector.size();i++) {
			PixelCluster pc = (PixelCluster)pixelClusterVector.elementAt(i);
			for (int j=0; j<pc.pixels.size(); j++) {
				Pixel pixel = (Pixel)(pc.pixels.elementAt(j));
				int rgbColor = Color.HSBtoRGB(pc.avgH, pc.avgS, pc.avgBr);
				output.setRGB(pixel.x, pixel.y, rgbColor);
			}
		}
		return output;
	}

	public void clusterInputColors(BufferedImage image, int numberOfColors, int numberOfIterations) {
		//read from bottom left to top right to put all values in vector
		pixelVector = new Vector();
		pixelClusterVector = new Vector();
		for (int y=height-1; y>=0; y--) {
			for (int x=0;x<width;x++) {
				Color pixelColor = new Color(image.getRGB(x, y));
				float[] hsb = Color.RGBtoHSB(pixelColor.getRed(),pixelColor.getGreen(), pixelColor.getBlue(), null);
				float hue = hsb[0];
				float saturation = hsb[1];
				float brightness = hsb[2];
				pixelVector.add( new Pixel( 
						x,
						y, 
						pixelColor.getRed(),
						pixelColor.getGreen(),
						pixelColor.getBlue(),
						hue, 
						saturation, 
						brightness, 
						(float) ((float)x/(float)width),
						(float) ((float)y/(float)width))
						);
			}
		}
		//now select random 8 and create buckets from them
		for (int b=0;b<numberOfColors;b++) {
			int position = getRandomInteger(width*height);
			Pixel ptc = (Pixel)pixelVector.elementAt(position);
			PixelCluster thisPC = new PixelCluster();
			thisPC.addPixel(ptc);
			thisPC.calculateAverage();
			thisPC.removePixel(ptc);
			pixelClusterVector.add(thisPC);
		}
		//now there are 8 clusters put all the pixels in the closest matching avg
		int iterations = numberOfIterations;
		//DO THIS A NUMBER OF TIMES - BUT UNTIL THEN
		for (int r=0; r<iterations; r++) {
			putPixelsInClosestClusters();
			recalculateClusterAverages();
			if (r != iterations-1) {
				emptyClusters();
			}
		}
		
		
		for (int j=0; j<pixelClusterVector.size();j++) {
			PixelCluster pc = (PixelCluster)pixelClusterVector.elementAt(j);
			System.out.println("COLOR: "+pc.avgR+" "+pc.avgG+ " "+pc.avgB + " "+pc.avgBr );
		}
		
		/*
		 * 

COLOR: 2 5 16 0.06
COLOR: 26 49 52 0.25
COLOR: 59 75 74 0.33
COLOR: 98 108 93 0.45
COLOR: 67 125 50 0.49
COLOR: 140 142 122 0.59
COLOR: 186 182 162 0.74
COLOR: 234 228 210 0.92

COLOR: 12 26 35 0.16
COLOR: 43 63 65 0.29
COLOR: 79 91 85 0.38
COLOR: 60 122 50 0.47
COLOR: 115 125 98 0.51
COLOR: 150 150 132 0.62
COLOR: 193 188 168 0.77
COLOR: 237 231 213 0.93

COLOR: 12 25 34 0.15
COLOR: 46 61 69 0.29
COLOR: 88 96 89 0.40
COLOR: 42 107 48 0.42
COLOR: 99 137 63 0.54
COLOR: 136 138 122 0.58
COLOR: 184 180 159 0.74
COLOR: 233 226 208 0.91


COLOR: 234 228 210 0.92
COLOR: 186 182 162 0.74
COLOR: 98 108 93 0.45
COLOR: 59 75 74 0.33
COLOR: 26 49 52 0.25
COLOR: 2 5 16 0.069
COLOR: 140 142 122 0.59
COLOR: 68 125 50 0.49

		 * 
		 */
		
		System.out.println("DONE");

	}

	public void putPixelsInClosestClusters() {
		int allPixelsSize = pixelVector.size();
		for (int i=0; i<allPixelsSize; i++) {
			Pixel ptm = (Pixel)pixelVector.elementAt(i); 
			double minDifference = Double.MAX_VALUE;
			int smallestDifferenceIndex = 0;
			for (int j=0; j<pixelClusterVector.size();j++) {
				PixelCluster pc = (PixelCluster)pixelClusterVector.elementAt(j);
				double thisDifference = pc.getMeanDifference(ptm);
				if (thisDifference < minDifference) {
					minDifference = thisDifference;
					smallestDifferenceIndex = j;
				}
			}
			//add pixel to cluster with smallest difference
			((PixelCluster)pixelClusterVector.elementAt(smallestDifferenceIndex)).addPixel(ptm);
		}
	}


	;
	public void recalculateClusterAverages() {
		for (int j=0; j<pixelClusterVector.size();j++) {
			PixelCluster pc = (PixelCluster)pixelClusterVector.elementAt(j);
			pc.calculateAverage();
		}
	}

	public void emptyClusters() {
		for (int j=0; j<pixelClusterVector.size();j++) {
			((PixelCluster)pixelClusterVector.elementAt(j)).pixels.clear();
		}
	}


	public static BufferedImage getBufferedImage(String pathToFile) {
		System.out.println("OpeningFile");
		File cf = new File(pathToFile);
		BufferedImage contentImage = null;
		try {
			contentImage = ImageIO.read(cf);
		} catch (IOException e) {
		}
		return contentImage;
	}

	public void saveBufferedImage(BufferedImage difImage, String fileName) {
		System.out.println("Saving Image: "+fileName);
		File outputfile = new File(fileName);
		try {
			ImageIO.write(difImage, "png", outputfile);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getRandomInteger(
			final int size ) {

		// creates random number between 0 and (size-1)
		final Random randomGenerator = new Random();
		final int randomInt = randomGenerator.nextInt(size);
		return randomInt;

	}


	public static BufferedImage blur(BufferedImage image, int[] filter, int filterWidth) {
		if (filter.length % filterWidth != 0) {
			throw new IllegalArgumentException("filter contains a incomplete row");
		}

		final int width = image.getWidth();
		final int height = image.getHeight();

		int intstreamsum = 0;
		for (int i=0;i<filter.length;i++ ) {
			intstreamsum += filter[i];
		}
		final int sum = intstreamsum; //IntStream.of(filter).sum();

		int[] input = image.getRGB(0, 0, width, height, null, 0, width);

		int[] output = new int[input.length];

		final int pixelIndexOffset = width - filterWidth;
		final int centerOffsetX = filterWidth / 2;
		final int centerOffsetY = filter.length / filterWidth / 2;

		// apply filter
		for (int h = height - filter.length / filterWidth + 1, w = width - filterWidth + 1, y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				int r = 0;
				int g = 0;
				int b = 0;
				for (int filterIndex = 0, pixelIndex = y * width + x;
						filterIndex < filter.length;
						pixelIndex += pixelIndexOffset) {
					for (int fx = 0; fx < filterWidth; fx++, pixelIndex++, filterIndex++) {
						int col = input[pixelIndex];
						int factor = filter[filterIndex];

						// sum up color channels seperately
						r += ((col >>> 16) & 0xFF) * factor;
						g += ((col >>> 8) & 0xFF) * factor;
						b += (col & 0xFF) * factor;
					}
				}
				r /= sum;
				g /= sum;
				b /= sum;
				// combine channels with full opacity
				output[x + centerOffsetX + (y + centerOffsetY) * width] = (r << 16) | (g << 8) | b | 0xFF000000;
			}
		}

		BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		result.setRGB(0, 0, width, height, output, 0, width);
		return result;
	}


}
