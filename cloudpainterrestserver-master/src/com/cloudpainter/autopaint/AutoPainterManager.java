package com.cloudpainter.autopaint;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
/*
import org.apache.log4j.Logger;

import com.cloudpainter.server.jpa.PaintingService;
import com.cloudpainter.server.jpa.SettingsService;
import com.cloudpainter.server.paint.PaintStrokeObject;
import com.cloudpainter.server.util.ElasticsearchUtility;
import com.cloudpainter.shared.paint.BrushStroke;
import com.cloudpainter.shared.paint.CanvasPoint;
import com.crowdpainter.shared.Paint;
*/

//when instantiated give it paint service dao
//in it have method for load new image
//setter with new image

public class AutoPainterManager
{
/*
	static private Logger logger = Logger.getLogger(AutoPainterManager.class);
	
	private PaintPattern paintPattern;
	private PaintMap paintMap;

	int paintingWidth;
	int paintingHeight;
	int strokesSinceRandomXY = 0;
	int maxStrokesBeforeJumpToRandomXY = 10;

	private PaintingService paintingService;
	
	public AutoPainterManager(
			final PaintingService paintingService,
			final SettingsService settingsService,
			final String paintMapImageLocation ) {
		final ArrayList<Double> strokeLengthSequence = new ArrayList<Double>();
		strokeLengthSequence.add(4.0);
		final ArrayList<PaintMapDirection> strokeDirectionSequence = new ArrayList<PaintMapDirection>();
		strokeDirectionSequence.add(PaintMapDirection.SOUTH);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_WEST);
		strokeDirectionSequence.add(PaintMapDirection.SOUTH_EAST);
		if (paintPattern == null) {
			paintPattern = new DownPaintPattern(
					strokeLengthSequence,
					strokeDirectionSequence,
					null, // FindNextStrokeStartPointStrategy.SAME_COLLUMN, NOT
							// IMPLEMENTED
					null, // DirectionToSearchForNextStrokeStartRow.BELOW, NOT
							// IMPLEMENTED
					null // DirectionToSearchForNextStrokeStartCol.RIGHT NOT
							// IMPLEMENTED
			);
		}
		
		this.paintingService = paintingService;
		paintingWidth = paintingService.getLastPainting().getWidth();
		paintingHeight = paintingService.getLastPainting().getHeight();
		
		System.out.println("Painting width: "+paintingWidth);
		System.out.println("Painting width: "+paintingHeight);
				
		updatePaintMap(paintingService.getLastPainting().getWidth(),
				paintingService.getLastPainting().getHeight(),
				paintMapImageLocation);
		
		//		paintMap = new PaintMap(
		//				paintingService.getLastPainting().getWidth(),
		//				paintingService.getLastPainting().getHeight(),
		//				4);
		//		//,(int) Math.round((1.0 / settingsService.getSettings().getBrushSize()))
		//		paintMapImageLocation
		//		
		//		paintMap.createPaintMap(paintMapImageLocation);
		//		paintMap.mapPaintMapColorsToRobotPallete(this.paintPaletteService.getPaintPalette());
		
		boolean done = false;
		int index = 0;
		final int paintingId = paintingService.getLastPainting().getId();
		final int recordBatch = 5000;
		List<PaintStrokeObject> paintStrokeList;

		while (!done) {
			paintStrokeList = paintingService.getBasePaintStrokes(
					index,
					recordBatch,
					paintingId);
			index = index + recordBatch;
			if (paintStrokeList.size() < recordBatch) {
				done = true;
			}
			for (final PaintStrokeObject pso : paintStrokeList) {
				recordCompletionOfStrokeInPaintMap(new RealWorldStroke(
						new CanvasPoint(
								pso.getStartX(),
								pso.getStartY()),
						new CanvasPoint(
								pso.getEndX(),
								pso.getEndY()),
						pso.getNoAlphaColor()));
			}
		}
	}
	public void updatePaintMap(
			final int width,
			final int height,
			final String paintMapImageUrl ) {
		
		BufferedImage sourceImage;
		int dpi = 4;
		try {
			sourceImage = ImageIO.read(new File(paintMapImageUrl));
			dpi = (int) (((double)sourceImage.getWidth())/((double)width));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			dpi = 4;
			logger.error("DPI could not be calculated from paintmap.gif. using default 4.");
			e.printStackTrace();
		}
		paintMap = new PaintMap(
				width,
				height,
				dpi);
		paintMap.createPaintMap(paintMapImageUrl);
		paintMap.mapPaintMapColorsToRobotPallete(paintingService.getPaintPalette());
	}

	// int strokesSinceRandomStart = 0;
	// int strokesToTakeBeforeRandomXY = 10;

	// use this to get next N strokes
	public ArrayList<RealWorldStroke> getRealWorldStrokes(
			final Double lastXPos,
			final Double lastYPos,
			final Paint robotColor,
			final int numberOfStrokes ) {
		Double lastX = lastXPos;
		Double lastY = lastYPos;
		if (strokesSinceRandomXY >= maxStrokesBeforeJumpToRandomXY) {
			final Random randomGenerator = new Random();
			final int randomX = randomGenerator.nextInt(paintingWidth);
			final int randomY = randomGenerator.nextInt(paintingHeight);
			lastX = 1.0 * randomX;
			lastY = 1.0 * randomY;
			strokesSinceRandomXY = 0;
		}
		else {
			strokesSinceRandomXY = strokesSinceRandomXY + numberOfStrokes;
		}

		ArrayList<RealWorldStroke> nextSetOfStrokes = getRealWorldStrokesOfSpecificColor(
				lastX,
				lastY,
				new Color(
						Integer.parseInt(
								robotColor.getHexColor().substring(
										1),
								16)),
				numberOfStrokes);
		// If no strokes come back for specified color, autoPainterManger
		// returns to lightest color in the pallete and starts painting areas
		// until darkest
		if (nextSetOfStrokes == null) {
			final ArrayList<Paint> sortedRobotColors = getSortedRobotColorPallete();
			for (final Paint nextRobotColor : sortedRobotColors) {
				final ArrayList<RealWorldStroke> RealWorldStrokes = new ArrayList<RealWorldStroke>();
				CanvasPoint lastCoord = new CanvasPoint(
						lastX,
						lastY);
				for (int i = 0; i < numberOfStrokes; i++) {
					final CanvasPoint nextStart = paintMap.getNextStartPointOfColor(
							lastCoord.getX(),
							lastCoord.getY(),
							new Color(
									Integer.parseInt(
											nextRobotColor.getHexColor().substring(
													1),
											16)));
					if (nextStart != null) {
						final CanvasPoint nextEnd = paintMap.getEndOfStroke(
								nextStart,
								paintPattern.getNextStrokeDirection(),
								paintPattern.getNextStrokeLength());
						lastCoord = nextEnd;
						final RealWorldStroke completeStroke = new RealWorldStroke(
								nextStart,
								nextEnd,
								new Color(
										Integer.parseInt(
												nextRobotColor.getHexColor().substring(
														1),
												16)));
						RealWorldStrokes.add(completeStroke);

					}
					else {
						// if null is returned it means no strokes could be
						// found
						i = numberOfStrokes;
					}
				}
				// return null if no strokes of the color are found so calling
				// algorithm can change colors
				if (RealWorldStrokes.size() == 0) {
					nextSetOfStrokes = null;
				}
				else {
					nextSetOfStrokes = RealWorldStrokes;
				}
				// once set of strokes for specified color is found, it returns
				// it
				if (nextSetOfStrokes != null) {
					break;
				}
			}
		}
		// record stroke both here and when it gets drawn
		//		if ((nextSetOfStrokes != null) && (nextSetOfStrokes.size() > 0)) {
		//			for (final RealWorldStroke strokeToRecord : nextSetOfStrokes) {
		//				recordCompletionOfStrokeInPaintMap(strokeToRecord);
		//			}
		//		}
		return nextSetOfStrokes;
	}

	// Only Access this from getRealWorldStrokes
	private ArrayList<RealWorldStroke> getRealWorldStrokesOfSpecificColor(
			final Double lastX,
			final Double lastY,
			final Color lastColor,
			final int numberOfStrokes ) {
		final ArrayList<RealWorldStroke> RealWorldStrokes = new ArrayList<RealWorldStroke>();
		CanvasPoint lastCoord = new CanvasPoint(
				lastX,
				lastY);
		for (int i = 0; i < numberOfStrokes; i++) {
			final CanvasPoint nextStart = paintMap.getNextStartPointOfColor(
					lastCoord.getX(),
					lastCoord.getY(),
					lastColor);
			if (nextStart != null) {
				final CanvasPoint nextEnd = paintMap.getEndOfStroke(
						nextStart,
						paintPattern.getNextStrokeDirection(),
						paintPattern.getNextStrokeLength());
				lastCoord = nextEnd;
				RealWorldStroke nextStroke = new RealWorldStroke(
						nextStart,
						nextEnd,
						lastColor);
				recordCompletionOfStrokeInPaintMap(nextStroke);					
				RealWorldStrokes.add(nextStroke);
			}
			else {
				// if null is returned it means no strokes could be found
				i = numberOfStrokes;
			}
		}

		// return null if no strokes of the color are found so calling algorithm
		// can change colors
		if (RealWorldStrokes.size() == 0) {
			return null;
		}
		else {
			return RealWorldStrokes;
		}
	}
	private void recordCompletionOfStrokeInPaintMap(
			final RealWorldStroke stroke ) {
		paintMap.recordStrokeOnPaintMap(stroke);
	}

	public void recordCompletionOfStrokeInPaintMap(
			final BrushStroke brushStroke ) {
		final List<CanvasPoint> allPoints = brushStroke.getPoints();
		for (int i = 0; i < allPoints.size() - 1; i++) {
			final Color color = new Color(
					Integer.parseInt(
							brushStroke.getColor().getHexColor().substring(
									1),
							16));
			final RealWorldStroke stroke = new RealWorldStroke(
					allPoints.get(i),
					allPoints.get(i + 1),
					color);
			paintMap.recordStrokeOnPaintMap(stroke);
		}
	}

	// Instead of setting color palletes / this can be used to get it from GIF
	public ArrayList<Paint> getSortedRobotColorPallete() {
		return paintMap.getSortedRobotColorPallete();
	}

	public PaintPattern getPaintPattern() {
		return paintPattern;
	}

	public void setPaintPattern(
			final PaintPattern paintPattern ) {
		this.paintPattern = paintPattern;
	}

	public PaintMap getPaintMap() {
		return paintMap;
	}

	public void setRobotPaintPallete(
			final List<Paint> robotPaintPallete ) {
		paintMap.mapPaintMapColorsToRobotPallete(robotPaintPallete);
	}
	
	public List<BrushStroke> getAutoPaintStrokes(double lastX, double lastY, Paint lastColor, int strokeCount, double brushSize){
		List<BrushStroke> strokes = new ArrayList<BrushStroke>();
		final ArrayList<RealWorldStroke> realWorldStrokesFromAutoPainter = getRealWorldStrokes(
				lastX,
				lastY,
				lastColor,
				strokeCount);
		if ((realWorldStrokesFromAutoPainter != null) && (realWorldStrokesFromAutoPainter.size() > 0)) {
			for (final RealWorldStroke autoStroke : realWorldStrokesFromAutoPainter) {
				final Paint strokePaint = getPaintMap().getRobotPaintColor(
						autoStroke.getStrokeColor()); 
				final String strokeSource = "autopaint";
				final String strokeId = ""+System.currentTimeMillis();
				final BrushStroke newStroke = new BrushStroke(
						strokePaint,
						strokeSource+"|"+strokeId,
						brushSize);
				newStroke.addPoint(autoStroke.getStart());
				newStroke.addPoint(autoStroke.getEnd());
				strokes.add(newStroke);
				ElasticsearchUtility.SendStrokesToElasticSearchCluster(paintingService.getStrokeNumber(), newStroke, this.paintingService.getLastPainting());
				paintingService.incrementStrokeNumber();
			}
		}
		return strokes;
	}
	*/
}
