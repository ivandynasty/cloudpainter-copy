package com.cloudpainter.autopaint;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map.Entry;

import com.cloudpainter.painting.Paint;

public class PaletteMap
{

	private final HashMap<Color, Paint> colorMap;

	PaletteMap() {
		colorMap = new HashMap<Color, Paint>();
	}

	public Color getGifColor(
			final Paint robotColor ) {
		for (final Entry<Color, Paint> entry : (colorMap).entrySet()) {
			if (entry.getValue() == robotColor) {
				return entry.getKey();
			}
		}
		return null;
	}

	public Paint getRobotColor(
			final Color gifColor ) {
		return colorMap.get(gifColor);
	}

	public void addMapping(
			final Color gifPaletteColor,
			final Paint robotPaletteColor ) {
		colorMap.put(
				gifPaletteColor,
				robotPaletteColor);
	}

}
