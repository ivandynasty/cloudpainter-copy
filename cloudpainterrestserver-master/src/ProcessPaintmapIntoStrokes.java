
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.cloudpainter.processpaintmap.PaintmapProcessor;
 
//http://localhost:8082/CloudPainterRestServer/restservices/processpaintmapintostrokes/paint.gif

@Path("/processpaintmapintostrokes")
public class ProcessPaintmapIntoStrokes {

	@GET
	@Produces("application/xml")
	public String processPaintmap() {
		String result = "@Produces(\"application/xml\") Service to Process paintmap.gif";
		result += " // put file in currentpainting and call this service with filename as argument";
		return "<ProcessPaintmapIntoStrokes>" + "<processPaintmap>" + result + "</processPaintmap>" + "</ProcessPaintmapIntoStrokes>";
	}
 
	@Path("{paintmapname}")
	@GET
	@Produces("application/xml")
	public String convertCtoFfromInput(@PathParam("paintmapname") String paintmapname) {

		//pTODO: change it to painting ini file instead of paintmap file
		String currentPaintingDir = "C:\\cloudpainterrestserver\\currentpainting\\";
		String paintmapFilename = paintmapname;
		
		PaintmapProcessor pp = new PaintmapProcessor();

		//load paintmap.gif
		pp.loadSourceImage(currentPaintingDir, paintmapFilename);

		//load paintmap_coverage.gif (white is unpainted / everything else is painted
		pp.loadProgressImage(currentPaintingDir, paintmapFilename);
		
		
		//get colors from paintmap.gif
		//create palette

		//create array of strokes
		//update/save paintmap_coverage.gif with every stroke
		//save json file of strokes
		//send stroke to ES Cluster

		
		
		String result = "@Produces(\"application/xml\") Processing [" + paintmapname + "] into strokes";

		return "<ProcessPaintmapIntoStrokes>" + "<processPaintmap>" + result + "</processPaintmap>" + "</ProcessPaintmapIntoStrokes>";
	}
}