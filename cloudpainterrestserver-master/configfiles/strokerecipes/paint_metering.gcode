; generated by CloudPainter GCODE Generator - Version 2018.08.11
; code for dd_embellishment_004
; on robot com.cloudpainter.robots.PandoraVOne@15ca7889
; "paintwells":[ "#fefffe" ]


M107                ; ??? 
G21                 ; set units to millimeters
G90                 ; use absolute coordinates
M82                 ; use absolute distances for extrusion
G92 E0.0              ; ??? 
T0                  ;Rotate paintbrush T0, dispensing paint T1  


;strokeA2a
M204 P1000 T1000               ; ACCEL SPEED
G1 E-12.67                ; ROTATE INTO START OF SWEEP (-2.67 RADIANS LESS THAN DIRECTION OF STROKE)
G1 E12.67                ; ROTATE INTO START OF SWEEP (-2.67 RADIANS LESS THAN DIRECTION OF STROKE)
G1 E-12.67                ; ROTATE INTO START OF SWEEP (-2.67 RADIANS LESS THAN DIRECTION OF STROKE)
G1 E0                ; ROTATE INTO START OF SWEEP (-2.67 RADIANS LESS THAN DIRECTION OF STROKE)

T1                             ;Rotate paintbrush T0, dispensing paint T1  
G1 E4.0
G1 E-4.0
G1 E4.0
G1 E-4.0
