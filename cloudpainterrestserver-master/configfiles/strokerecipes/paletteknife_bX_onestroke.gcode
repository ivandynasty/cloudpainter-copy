; generated by CloudPainter GCODE Generator - Version 2018.08.11
; code for dd_embellishment_004
; on robot com.cloudpainter.robots.PandoraVOne@15ca7889
; "paintwells":[ "#fefffe" ]


M107                ; ??? 
G21                 ; set units to millimeters
G90                 ; use absolute coordinates
M82                 ; use absolute distances for extrusion
G92 E0.0              ; ??? 
T0                  ;Rotate paintbrush T0, dispensing paint T1  

;Simple sweep of the brush with no movement

;strokeA2AgBa_W
M204 P1000 T1000               ; MAX ACCEL SPEED XYZ
M203 E1000                     ; MAX ACCEL SPEED ROTATION 
G1 F10000                      ; XY SPEED
G1 X350.0 Y200.0 E0.0          ; START POINT WITH PALETTE KNIFE IN HOME POSITION
G1 Z8.0                        ; LOWER PALETTE BELOW AXIS 
G1 Z11.0 E-3.14                ; ROTATE INTO START OF SWEEP 
G1 Z16.0                       ; LOWER JUST ABOVE CANVAS
G1 Z16.2                       ; TOUCH CANVAS (WHEN CALIBRATING THIS IS SLIGHTLY TOUCHING CANVAS)
G4 P1000                       ; pause 1 sec
G1 Z16.1                       ; LIFT BACK UP A TOUCH
M204 P10 T10                   ; LOWER ACCELERATION
M203 E2                        ; LOWER ROTATIONAL ACCELERATION
G1 X350.0 Y200.0 Z16.1 E-2.51  ; MOVE 0   / Z 0     / ROTATE 0.628 ( 36 degrees) CLOCKWISE  
M204 P10 T10                   ; LOWER ACCELERATION
G1 X350.0 Y200.0 Z15.1 E-1.89  ; MOVE 0   / Z -1.0  / ROTATE 0.628 ( 36 degrees) CLOCKWISE  
G1 X350.0 Y200.0 Z10.0 E0.0    ; MOVE 0   / Z -5.0  / ROTATE 1.884 (108 degrees) CLOCKWISE 
G1 Z8 E0                       ; POSITION PALETTE KNIFE IN HOME POSITION
M204 P1000 T1000               ; RETURN TO FULL SPEED
M203 E1000                     ; RETURN ROTATION TO FULL SPEED  
G1 Z0                          ; RETURN Z TO 0
G1 X0 Y0                       ; RETURN X Y TO 0
;G4 P10000                      ; PAUSE (REFILL PAINT)
