; generated by CloudPainter GCODE Generator - Version 2018.08.11
; code for dd_embellishment_004
; on robot com.cloudpainter.robots.PandoraVOne@15ca7889
; "paintwells":[ "#fefffe" ]


M107                ; ??? 
G21                 ; set units to millimeters
G90                 ; use absolute coordinates
M82                 ; use absolute distances for extrusion
G92 E0.0              ; ??? 
T0                  ;Rotate paintbrush T0, dispensing paint T1  


;strokeAf2a
M204 P1000 T1000               ; ACCEL SPEED
G1 F10000                      ; XY SPEED
G1 X350.0 Y200.0 E0.0          ; START POINT WITH PALETTE KNIFE IN HOME POSITION
G1 Z8.0                        ; LOWER PALETTE BELOW AXIS 
G1 Z11.0 E-2.67                ; ROTATE INTO START OF SWEEP (-2.67 RADIANS LESS THAN DIRECTION OF STROKE)
G1 Z16.0                       ; LOWER JUST ABOVE CANVAS
G1 Z16.2                       ; TOUCH CANVAS (WHEN CALIBRATING THIS IS SLIGHTLY TOUCHING CANVAS)
G1 Z16.1                       ; LIFT BACK UP
M204 P10 T10                   ; LOWER ACCELERATION
G1 X356.66 Y200.0 Z16.1 E-2.0 ; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / FLAT Z 
G1 X363.33 Y200.0 Z15.1 E-1.33; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / LIFT Z 1.0 
G1 X370.0 Y200.0 Z10.0 E0.67  ; MOVE 6.66 / ROTATE 2 RADIAN CLOCKWISE / LIFT Z 5.0 
G1 Z8 E0                       ; POSITION PALETTE KNIFE IN HOME POSITION
M204 P1000 T1000               ; RETURN TO FULL SPEED
G1 Z0                          ; RETURN Z TO 0
G1 X0 Y0                       ; RETURN X Y TO 0
G4 P10000                      ; PAUSE (REFILL PAINT)

;strokeAf2b
M204 P1000 T1000               ; ACCEL SPEED
G1 F10000                      ; XY SPEED
G1 X350.0 Y300.0 E0.0          ; START POINT WITH PALETTE KNIFE IN HOME POSITION
G1 Z8.0                        ; LOWER PALETTE BELOW AXIS 
G1 Z11.0 E-2.67                ; ROTATE INTO START OF SWEEP (-2.67 RADIANS LESS THAN DIRECTION OF STROKE)
G1 Z16.0                       ; LOWER JUST ABOVE CANVAS
G1 Z16.2                       ; TOUCH CANVAS (WHEN CALIBRATING THIS IS SLIGHTLY TOUCHING CANVAS)
G1 Z16.1                       ; LIFT BACK UP
M204 P10 T10                   ; LOWER ACCELERATION
G1 X356.66 Y300.0 Z16.1 E-2.0  ; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / FLAT Z
G1 X363.33 Y300.0 Z15.1 E-1.33; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / LIFT Z 1.0
G1 X370.0 Y300.0 Z10.0 E0.67   ; MOVE 6.66 / ROTATE 2 RADIAN CLOCKWISE / LIFT Z 5.0 
G1 Z8 E0                       ; POSITION PALETTE KNIFE IN HOME POSITION
M204 P1000 T1000               ; RETURN TO FULL SPEED
G1 Z0                          ; RETURN Z TO 0
G1 X0 Y0                       ; RETURN X Y TO 0
G4 P10000                      ; PAUSE (REFILL PAINT)

;strokeAf2c
M204 P1000 T1000               ; ACCEL SPEED
G1 F10000                      ; XY SPEED
G1 X350.0 Y400.0 E0.0          ; START POINT WITH PALETTE KNIFE IN HOME POSITION
G1 Z8.0                        ; LOWER PALETTE BELOW AXIS 
G1 Z11.0 E-2.67                ; ROTATE INTO START OF SWEEP (-2.67 RADIANS LESS THAN DIRECTION OF STROKE)
G1 Z16.0                       ; LOWER JUST ABOVE CANVAS
G1 Z16.2                       ; TOUCH CANVAS (WHEN CALIBRATING THIS IS SLIGHTLY TOUCHING CANVAS)
G1 Z16.1                       ; LIFT BACK UP
M204 P10 T10                   ; LOWER ACCELERATION
G1 X356.66 Y400.0 Z16.1 E-2.0  ; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / FLAT Z
G1 X363.33 Y400.0 Z15.1 E-1.33; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / LIFT Z 1.0
G1 X370.0 Y400.0 Z10.0 E0.67   ; MOVE 6.66 / ROTATE 2 RADIAN CLOCKWISE / LIFT Z 5.0 
G1 Z8 E0                       ; POSITION PALETTE KNIFE IN HOME POSITION
M204 P1000 T1000               ; RETURN TO FULL SPEED
G1 Z0                          ; RETURN Z TO 0
G1 X0 Y0                       ; RETURN X Y TO 0
G4 P10000                      ; PAUSE (REFILL PAINT)

;strokeAf2d
M204 P1000 T1000               ; ACCEL SPEED
G1 F10000                      ; XY SPEED
G1 X350.0 Y500.0 E0.0          ; START POINT WITH PALETTE KNIFE IN HOME POSITION
G1 Z8.0                        ; LOWER PALETTE BELOW AXIS 
G1 Z11.0 E-2.67                ; ROTATE INTO START OF SWEEP (-2.67 RADIANS LESS THAN DIRECTION OF STROKE)
G1 Z16.0                       ; LOWER JUST ABOVE CANVAS
G1 Z16.2                       ; TOUCH CANVAS (WHEN CALIBRATING THIS IS SLIGHTLY TOUCHING CANVAS)
G1 Z16.1                       ; LIFT BACK UP
M204 P10 T10                   ; LOWER ACCELERATION
G1 X356.66 Y500.0 Z16.1 E-2.0  ; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / FLAT Z 
G1 X363.33 Y500.0 Z15.1 E-1.33; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / LIFT Z 1.0
G1 X370.0 Y500.0 Z10.0 E0.67   ; MOVE 6.66 / ROTATE 2 RADIAN CLOCKWISE / LIFT Z 5.0 
G1 Z8 E0                       ; POSITION PALETTE KNIFE IN HOME POSITION
M204 P1000 T1000               ; RETURN TO FULL SPEED
G1 Z0                          ; RETURN Z TO 0
G1 X0 Y0                       ; RETURN X Y TO 0
G4 P10000                      ; PAUSE (REFILL PAINT)

;strokeAf2e
M204 P1000 T1000               ; ACCEL SPEED
G1 F10000                      ; XY SPEED
G1 X350.0 Y600.0 E0.0          ; START POINT WITH PALETTE KNIFE IN HOME POSITION
G1 Z8.0                        ; LOWER PALETTE BELOW AXIS 
G1 Z11.0 E-2.67                ; ROTATE INTO START OF SWEEP (-2.67 RADIANS LESS THAN DIRECTION OF STROKE)
G1 Z16.0                       ; LOWER JUST ABOVE CANVAS
G1 Z16.2                       ; TOUCH CANVAS (WHEN CALIBRATING THIS IS SLIGHTLY TOUCHING CANVAS)
G1 Z16.1                       ; LIFT BACK UP
M204 P10 T10                   ; LOWER ACCELERATION
G1 X356.66 Y600.0 Z16.1 E-2.0 ; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / FLAT Z 
G1 X363.33 Y600.0 Z15.1 E-1.33; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / LIFT Z 1.0
G1 X370.0 Y600.0 Z10.0 E0.67  ; MOVE 6.66 / ROTATE 2 RADIAN CLOCKWISE / LIFT Z 5.0 
G1 Z8 E0                       ; POSITION PALETTE KNIFE IN HOME POSITION
M204 P1000 T1000               ; RETURN TO FULL SPEED
G1 Z0                          ; RETURN Z TO 0
G1 X0 Y0                       ; RETURN X Y TO 0
G4 P10000                      ; PAUSE (REFILL PAINT)

;strokeAf2f
M204 P1000 T1000               ; ACCEL SPEED
G1 F10000                      ; XY SPEED
G1 X350.0 Y700.0 E0.0          ; START POINT WITH PALETTE KNIFE IN HOME POSITION
G1 Z8.0                        ; LOWER PALETTE BELOW AXIS 
G1 Z11.0 E-2.67                ; ROTATE INTO START OF SWEEP (-2.67 RADIANS LESS THAN DIRECTION OF STROKE)
G1 Z16.0                       ; LOWER JUST ABOVE CANVAS
G1 Z16.2                       ; TOUCH CANVAS (WHEN CALIBRATING THIS IS SLIGHTLY TOUCHING CANVAS)
G1 Z16.1                       ; LIFT BACK UP
M204 P10 T10                   ; LOWER ACCELERATION
G1 X356.66 Y700.0 Z16.1 E-2.0  ; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / FLAT Z 
G1 X363.33 Y700.0 Z15.1 E-1.33; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / LIFT Z 1.0
G1 X370.0 Y700.0 Z10.0 E0.67   ; MOVE 6.66 / ROTATE 2 RADIAN CLOCKWISE / LIFT Z 5.0 
G1 Z8 E0                       ; POSITION PALETTE KNIFE IN HOME POSITION
M204 P1000 T1000               ; RETURN TO FULL SPEED
G1 Z0                          ; RETURN Z TO 0
G1 X0 Y0                       ; RETURN X Y TO 0
G4 P10000                      ; PAUSE (REFILL PAINT)

;strokeAf2g
M204 P1000 T1000               ; ACCEL SPEED
G1 F10000                      ; XY SPEED
G1 X350.0 Y800.0 E0.0          ; START POINT WITH PALETTE KNIFE IN HOME POSITION
G1 Z8.0                        ; LOWER PALETTE BELOW AXIS 
G1 Z11.0 E-2.67                ; ROTATE INTO START OF SWEEP (-2.67 RADIANS LESS THAN DIRECTION OF STROKE)
G1 Z16.0                       ; LOWER JUST ABOVE CANVAS
G1 Z16.2                       ; TOUCH CANVAS (WHEN CALIBRATING THIS IS SLIGHTLY TOUCHING CANVAS)
G1 Z16.1                       ; LIFT BACK UP
M204 P10 T10                   ; LOWER ACCELERATION
G1 X356.66 Y800.0 Z16.1 E-2.0  ; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / FLAT Z
G1 X363.33 Y800.0 Z15.1 E-1.33; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / LIFT Z 1.0 
G1 X370.0 Y800.0 Z10.0 E0.67   ; MOVE 6.66 / ROTATE 2 RADIAN CLOCKWISE / LIFT Z 5.0 
G1 Z8 E0                       ; POSITION PALETTE KNIFE IN HOME POSITION
M204 P1000 T1000               ; RETURN TO FULL SPEED
G1 Z0                          ; RETURN Z TO 0
G1 X0 Y0                       ; RETURN X Y TO 0
G4 P10000                      ; PAUSE (REFILL PAINT)

;strokeAf2h
M204 P1000 T1000               ; ACCEL SPEED
G1 F10000                      ; XY SPEED
G1 X350.0 Y900.0 E0.0          ; START POINT WITH PALETTE KNIFE IN HOME POSITION
G1 Z8.0                        ; LOWER PALETTE BELOW AXIS 
G1 Z11.0 E-2.67                ; ROTATE INTO START OF SWEEP (-2.67 RADIANS LESS THAN DIRECTION OF STROKE)
G1 Z16.0                       ; LOWER JUST ABOVE CANVAS
G1 Z16.2                       ; TOUCH CANVAS (WHEN CALIBRATING THIS IS SLIGHTLY TOUCHING CANVAS)
G1 Z16.1                       ; LIFT BACK UP
M204 P10 T10                   ; LOWER ACCELERATION
G1 X356.66 Y900.0 Z16.1 E-2.0  ; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / FLAT Z
G1 X363.33 Y900.0 Z15.1 E-1.33; MOVE 6.66 / ROTATE 0.66 RADIAN CLOCKWISE / LIFT Z 1.0 
G1 X370.0 Y900.0 Z10.0 E0.67   ; MOVE 6.66 / ROTATE 2 RADIAN CLOCKWISE / LIFT Z 5.0 
G1 Z8 E0                       ; POSITION PALETTE KNIFE IN HOME POSITION
M204 P1000 T1000               ; RETURN TO FULL SPEED
G1 Z0                          ; RETURN Z TO 0
G1 X0 Y0                       ; RETURN X Y TO 0
G4 P10000                      ; PAUSE (REFILL PAINT)
